<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBH_DTL6Pwau1jtQx5_NqP4VRpsiTs7oY4"></script>
<section id="header">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                    <div class="text-center">
                        <h1 class="text-white animated fadeInDown">¿TIENES ALGUNA DUDA?</h1>
                    </div>
            </div>
        </div>
    </div>
</section>


<div style="clear: both; height: 20px;" class="clear"></div>
<div class="container-fluid titulosobrerosa"> 
<h2><span style="z-index: 9999999999999; position: relative;">CONTÁCTANOS</span></h2>
</div>
<div style="clear: both; height: 50px;" class="clear"></div>



<section id="formulario" style="padding: 7em 0 !important;">
    <div class="overlay" style="display: none"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 delay-5s" id="contact-logo">
                <div class="text-center">
                <img src="images/square-logo.png" alt="" class="img-fluid">
                </div>
            </div>

            <div class="col-md-6 delay-3" id="contact-formulario">
              <div class="text-center ">
                  <h1 class="modal-title text-center">Mensaje enviado</h1>
                  <br>
                  <p>Gracias por tu preferencia.</p>
                  <p>Uno de nuestros ejecutivos te contactará pronto.</p>
              </div>
              
           </div>
        </div>
    </div>
</section>

<section id="ubicacion" class="animated zoomIn">
<!-- MAPA -->
<div class="container-fluid shortMap">
        <div id="map-canvas"></div>
    </div>
    <!-- / MAPA -->
    
    <div class="ubicacion descripcionoculta">
        <center>
          <!-- <img src="images/logo-small.png" alt="STRATECH" class="mx-auto d-block img-fluid imagen-desc"> -->
          <p class="sr-only">Epicentro</p>
          <p>
          Calle: La noche No. 2440<br>
          Colonia: Jardines del bosque <br>
          C.P. 44520<br>
          Guadalajara, Jalisco.
          <br>
          Tel: +52 (33) 1395 4831
          </p>
        </center>
    </div>
</section>


<link href="css/contacto.css" rel="stylesheet">
