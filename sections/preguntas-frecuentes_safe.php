<div class="navbarHeight"></div>

<section class="container-fluid p-0" id="preguntas-frecuentes-section">

	<!-- Para SEO -->
	<h1 class="sr-only">Prendasys</h1>
	<h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>

	<!-- div.container-fluid#header-buscador>.container>.col-md-10.offset-md-1>(h3.titulo{¿Tienes preguntas sobre cómo funciona Prendasys?})+(h4.tag{Estamos aquí para ayudarte})+(.buscador-container>form#buscador>(.input-group#buscador-field>input.form-control[type="text"][placeholder="Buscar Preguntas Frecuentes"]+.input-group-btn>button.btn.buscador-btn[type="submit"]{Buscar})>) -->

	<div class="parallaxParent" id="header-buscador">
		<div class="container">
			<div class="col-md-10 offset-md-1">
				<h3 class="titulo text-center text-white">¿Tienes preguntas sobre<br>cómo funciona <strong>Prendasys</strong>?</h3>
				<h4 class="tag text-center text-white mb-4">Estamos aquí para <strong>ayudarte</strong></h4>
				<div class="buscador-container">
					<form action="" id="buscador">
						<div class="input-group" id="buscador-field">
							<input type="text" class="form-control" placeholder="Preguntas Frecuentes" id="busqueda">
							<div class="input-group-btn buscarw">
								<!-- <button class="btn buscador-btn" type="submit"><span class="btn-hover"></span>Buscar</button> -->
								<a href="#categorias" class="btn buscador-btn buscarw">
                                    <span class="d-none d-lg-block d-xl-block">Buscar</span>
                                    <span class="d-block d-lg-none d-xl-none"><i class="fas fa-search"></i></span>
                                </a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="image-parallax" style="background:url(images/preguntas-frecuentes/header-bg_old2.jpg)top right no-repeat;"></div>
	</div>

<div class="container-fluid" id="categorias">
	<div class="continer-fluid gris-bg py-4">
		<div class="container">
		  <h3 class="titulo text-center mb-3">Explora por categoría</h3>
		  <br>
		  <!-- Nav pills -->
		  <ul class="nav nav-pills" role="tablist" id="categorias-btns">
            <!-- BTN para desplazar a preguntas -->
            <a href="#preguntas" id="anchor" class="sr-only"></a>
            <!-- BTN para desplazar a preguntas -->
		    <li class="nav-item">
		    	<a class="nav-link active" data-toggle="pill" data-target="#empenos" title="empenos">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-empenos.png" alt="Empeños"></i>
		    		<span>Empeños</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#promociones" title="promociones">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-promociones.png" alt="Promociones"></i>
		    		<span>Promociones</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#servicio-al-cliente" title="servicio-al-cliente">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-servicio-cliente.png" alt="Servicio al Cliente"></i>
		    		<span>Servicio al Cliente</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#control" title="control">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-control.png" alt="Control"></i>
		    		<span>Control</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#inventarios" title="inventarios">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-inventarios.png" alt="Inventarios"></i>
		    		<span>Inventarios</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#ventas-y-apartados" title="ventas-y-apartados">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-ventas-apartados.png" alt="Ventas y apartados"></i>
		    		<span>Ventas y apartados</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#operaciones-de-riesgo" title="operaciones-de-riesgo">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-operaciones-riesgo.png" alt="Operaciones de riesgo"></i>
		    		<span>Operaciones de riesgo</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#seguridad" title="seguridad">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-seguridad.png" alt="Seguridad"></i>
		    		<span>Seguridad</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#catalogos" title="catalogos">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-catalogos.png" alt="Catálogos"></i>
		    		<span>Catálogos</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#backoffice" title="backoffice">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-backoffice.png" alt="Backoffice"></i>
		    		<span>Backoffice</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#operacion" title="operacion">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-operacion.png" alt="Operación"></i>
		    		<span>Operación</span>
		    	</a>
		    </li>
		    <li class="nav-item">
		    	<a class="nav-link" data-toggle="pill" data-target="#licenciamiento" title="licenciamiento">
		    		<i class="icon"><img src="images/preguntas-frecuentes/icons-old/icon-licenciamiento.png" alt="Licenciamiento"></i>
		    		<span>Licenciamiento</span>
		    	</a>
		    </li>
		  </ul>
	  </div>
	</div>  

  <!-- Tab panes -->
  <div class="tab-content pt-1" id="preguntas">
    <div id="empenos" class="container tab-pane active" title="empenos">
    	<div class="title-orange">
    		<h2>Empeños</h2>
    	</div>

        <!-- TE AMO emmet -->
    	<!-- .barra.faqs>(.clearfix[style="height:15px;"])+.col-12>.panel-group#accordion-empeno>(.panel.panel-default>(.panel-heading>h4.panel-title>a[data-toggle="collapse"][data-target="#empeno-$"]{pregunta..}>i.fa.fa-caret-right.float-right)+(.collapse#empeno-$[data-parent="#accordion-empeno"]>.panel-body>.padding>p.response-par{respuesta.. lorem ipsum..}))*15 -->
    	
		<div class="barra faqs">
            <div class="clearfix" style="height:15px;"></div>
            <div class="col-12">        
                <div class="panel-group" id="accordion-empeno">
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a data-toggle="collapse" data-target="#empeno-1">¿Cómo puedo prestar mejor? <i class="fa fa-caret-down float-right"></i></a>
                        </h4>
                      </div>
                      <div id="empeno-1" class="collapse show" data-parent="#accordion-empeno">
                        <div class="panel-body">
                            <div class="padding">
                                <p class="response-par">Manteniendo un valor de referencia actualizado para todos tus artículos y administrando las políticas de préstamos de manera centralizada.</p>
                            </div>
                        </div>
                      </div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-2">¿Es posible poder controlar los montos a prestar por sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-2" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Totalmente posible con las configuraciones de tabuladores por sucursal lo puedes lograr.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-3">¿Puedo empeñar cualquier producto?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-3" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si, con Prendasys tú configuras los tipos y artículos que desees recibir en tus sucursales.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-4">¿Puedo empeñar diferentes prendas en el mismo contrato?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-4" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si, con Prendasys es muy rápido hacerlo y además siempre tendrás el detalle de lo prestado por cada una de las prendas.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-5">¿Cómo saber cuándo se realizan empeños de artículos especiales?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-5" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">En el módulo de operaciones de riesgo podrás configurar alertas que te informen sobre empeños de artículos especiales.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-6">Cuando recibo un nuevo modelo de producto, ¿lo tengo que registrar en cada una de mis sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-6" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">No, con Prendasys lo registras una sola vez y tú decides a qué sucursal permitirle su recepción.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-7">¿Es posible que una sucursal reciba artículos que no sabría cómo evaluar?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-7" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si, con Prendasys es posible hacerlo gracias a sus herramientas comerciales.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-8">¿Es posible solo cotizar un avalúo?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-8" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si es posible, además tendrías información de los motivos por los cuales no se concreten los empeños.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-9">¿Cómo puedo disminuir el porcentaje de mis adjudicaciones?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-9" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Utilizando la configuración de incentivos para que tus clientes siempre tengan oportunidad de recuperar sus prendas.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-10">¿Es posible realizar préstamos con transferencias bancarias?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-10" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Totalmente posible, Prendasys te permitirá configurar múltiples formas de pagos para tus préstamos.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-11">¿Puedo generar tentativas de pago?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-11" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si, puedes generar tentativas de pago que permitirán a tu cliente establecer un compromiso de pago.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-12">¿Cómo puedo alertar a mis sucursales de clientes sospechosos?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-12" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Solo tienes que inactivar al cliente y registrar las alertas necesarias para que ninguna sucursal pueda otorgar empeños a ese cliente.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-13">¿Es posible almacenar datos biométricos de mis clientes?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-13" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Prendasys cuenta con un almacén digital de datos por cliente, en donde además de la huella digital, puedes almacenar otros datos como: imagen de su identificación oficial, firma del cliente, fotografía y otros documentos que requieras.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-14">¿Es posible configurar diferentes tasas de intereses por plazo y tipo de producto?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-14" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Con Prendasys es posible.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                    <div class="panel panel-default">
                    	<div class="panel-heading">
                    		<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#empeno-15">¿Es posible configurar tasas de intereses promocionales?<i class="fa fa-caret-right float-right"></i></a></h4>
                    	</div>
                    	<div class="collapse" id="empeno-15" data-parent="#accordion-empeno">
                    		<div class="panel-body">
                    			<div class="padding">
                    				<p class="response-par">Si es posible, con la opción de rango de fechas en las tasas de intereses.</p>
                    			</div>
                    		</div>
                    	</div>
                    </div>
                </div>
            </div>
    	</div>
    </div>
    <div id="promociones" class="container tab-pane fade" title="promociones">
    	<div class="title-orange">
    		<h2>Promociones</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-promociones">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#promociones-1">¿Cómo puedo otorgar beneficios en los empeños a mis mejores clientes?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="promociones-1" data-parent="#accordion-promociones">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con Prendasys puedes otorgar de manera automática mejores tasas de interés y porcentajes de préstamos sin la intervención del usuario.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#promociones-2">¿Puedo otorgar descuentos en refrendos?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="promociones-2" data-parent="#accordion-promociones">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Lo puedes hacer por frecuencia del refrendo o por día de la semana ya sea de manera permanente o por un determinado tiempo. </p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#promociones-3">¿Puedo otorgar a mis clientes promociones en empeños por aniversario de las sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="promociones-3" data-parent="#accordion-promociones">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con Prendasys puedes configurar listas de promociones en empeños por artículo y rango de fechas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="servicio-al-cliente" class="container tab-pane fade" title="servicio-al-cliente">
    	<div class="title-orange">
    		<h2>Servicio al Cliente</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-servicio-cliente">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#servicio-cliente-1">¿Es posible evitar duplicidad de registro de clientes?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="servicio-cliente-1" data-parent="#accordion-servicio-cliente">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible, Prendasys maneja un identificador único por cliente que te permitirá tener registrados a tus clientes una sola vez, pero no solo eso, sino que te permitirá tener disponible la información de tus clientes en todas tus sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#servicio-cliente-2">¿Es posible que mis clientes refrenden en cualquier sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="servicio-cliente-2" data-parent="#accordion-servicio-cliente">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, con Prendasys es posible.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#servicio-cliente-3">¿Pueden mis clientes pagar sus refrendos y empeños con tarjetas bancarias?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="servicio-cliente-3" data-parent="#accordion-servicio-cliente">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si pueden, Prendasys te permite configurar diferentes formas de pago para que tus clientes se sientan más seguros.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#servicio-cliente-4">¿Cómo puedo premiar la lealtad de mis clientes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="servicio-cliente-4" data-parent="#accordion-servicio-cliente">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Utilizando las herramientas de lealtad que te ofrece Prendasys.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#servicio-cliente-5">¿Cómo puedo saber qué otros servicios puedo ofrecer a mis clientes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="servicio-cliente-5" data-parent="#accordion-servicio-cliente">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Configurando encuestas que serían aplicadas a tus clientes al momento de empeñar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="control" class="container tab-pane fade" title="control">
    	<div class="title-orange">
    		<h2>Control</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-control">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-1">¿Puedo generar facturas electrónicas a mis clientes?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="control-1" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Puedes generar tanto la factura de intereses como la venta de artículos desde la misma sucursal.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-2">¿Puedo generar factura de todas mis operaciones no facturadas en el día?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-2" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, Prendasys te brinda las opciones de generar la factura global por día o por rango de fechas, solo tienes que configurar la opción que más te convenga.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-3">¿Cómo puedo evitar el robo hormiga en mis cajas?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-3" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con nuestro control de cortes ciegos de caja en donde los usuarios primero deben registrar lo que físicamente tienen de efectivo para que posteriormente Prendasys les diga cuánto debieron de haber tenido, lo puedes evitar, ya que con esta operación las diferencias tanto sobrantes como faltantes en los cortes de caja se quedan registrados en los flujos de efectivo de la sucursal.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-4">¿Puedo configurar máximos de efectivo por caja por sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-4" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, es posible configurar los máximos de efectivo que cada caja debe manejar para evitar situaciones que pongan en desventaja a la sucursal en caso de un siniestro.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-5">¿Puedo controlar las cancelaciones de las operaciones en empeños y ventas?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-5" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">A través de las autorizaciones configurables que tiene Prendasys es posible controlar dichas cancelaciones.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-6">¿Puedo conocer los motivos por los cuales me cancelan las operaciones?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-6" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible, ya que Prendasys cuenta con un módulo de configuración de cancelaciones que te permitirán administrar diversos motivos para que posteriormente sean analizados.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-7">¿Puedo refrendar empeños de contratos que ya se adjudicaron?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-7" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Se puede refrendar cualquier contrato sin importar en qué etapa se encuentre.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-8">¿Es posible que mis pases a piso las realice ciertos días del mes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-8" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">En Prendasys puedes configurar la frecuencia con que necesites realizar. Esta operación es muy fácil y sencilla, además la puedes modificar las veces que tú lo requieras.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-9">¿Es posible actualizar mis precios de venta de acuerdo con los nuevos valores de referencia de mis artículos?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-9" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con Prendasys es totalmente posible, solo tienes que activar esa funcionalidad.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-10">¿Es posible configurar un precio para la reimpresión de contratos?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-10" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">En el apartado de configuraciones de préstamos lo podrás realizar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-11">¿Es posible configurar un monto de presto mínimo por sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-11" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">En el apartado de configuraciones de préstamos lo podrás realizar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-12">¿Puedo adecuar la impresión de contratos al formato que utilizo actualmente?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-12" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Prendasys cuenta con una configuración de contratos que te permitirá configurar la totalidad de tus datos según lo requiera tu formato.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-13">¿Cómo puedo realizar la migración de información de mi sistema a Prendasys?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-13" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Mediante nuestro proceso automático de migración de cartera, podrás realizar este proceso en cuestión de minutos.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-14">¿Puedo operar un esquema de franquicias?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-14" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, totalmente. con Prendasys puedes configurar tu operación para que puedan trabajar con una franquicia en donde solo puedan ver tus catálogos y políticas de operación mas no modificarlas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#control-15">¿Es posible migrar el historial de comportamiento de mis clientes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="control-15" data-parent="#accordion-control">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con Prendasys es posible.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="inventarios" class="container tab-pane fade" title="inventarios">
    	<div class="title-orange">
    		<h2>Inventarios</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-inventarios">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-1">Si inicié un inventario, ¿puedo guardarlo y continuar después?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="inventarios-1" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, puedes guardarlo y continuar con el inventario pendiente en otro momento.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-2">¿Puedo consultar la existencia de un producto de otra sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-2" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, existe la opción de consultar existencias de artículos de otras sucursales, solo requieres que todas tus sucursales estén sincronizando su información.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-3">¿Puedo generar inventarios parciales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-3" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Sí, en el módulo de inventarios podrás encontrar opciones para generar inventarios por muestras de artículos. Estas muestras se pueden configurar, por ejemplo: inventarios por rangos de precios, artículos más vendidos o por tipo de producto.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-4">¿Cómo puedo distinguir los artículos iguales pero de diferentes clientes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-4" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Prendasys además de manejar un ID del producto en particular, maneja un número de serie único por cada producto que ingresa al almacén de resguardo derivado de una operación de empeño.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-5">¿Puedo enviar artículos de una sucursal a otra de mi misma cadena?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-5" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Sí, mediante la opción de transferencia puedes enviar y recibir artículos entre sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-6">¿Cómo puedo conocer el valor que tiene cada uno de mis almacenes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-6" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Mediante las diversas consultas y reportes que te proporciona Prendasys para cada uno de tus almacenes.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-7">¿Los traspasos entre almacenes los tengo que realizar manualmente?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-7" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">No, los traspasos entre almacenes los realiza Prendasys de manera automática dependiendo de la operación a realizar, no necesitas realizar nada manualmente.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-8">¿Puedo realizar auditorías físicas en cualquier lugar de mi almacén sin importar la conexión de datos?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-8" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si puedes, con Prendasys es posible realizar auditorías o tomas de inventarios físicos en cualquier área de tu almacén sin necesidad de estar conectados a la fuente de datos.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#inventarios-9">¿Cómo puedo migrar mis existencias de piso y apartados a Prendasys?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="inventarios-9" data-parent="#accordion-inventarios">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Mediante los procesos de carga masiva de datos podrás realizar de manera rápida y efectiva la migración de tus existencias de venta y apartados.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="ventas-y-apartados" class="container tab-pane fade" title="ventas-y-apartados">
    	<div class="title-orange">
    		<h2>Ventas y apartados</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-ventas-apartados">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-1">¿Es posible manejar diferentes descuentos en artículos para venta de acuerdo con la antigüedad de este?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="ventas-apartados-1" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, es posible configurando la lista de descuento de acuerdo con la antigüedad de los artículos en el área de piso.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-2">¿Puedo consultar las existencias de otras sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-2" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, con Prendasys es posible consultar las existencias en línea de otras sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-3">¿Puedo redondear los importes de mis ventas?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-3" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, puedes hacerlo configurando el tipo y monto de redondeo que necesitas aplicar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-4">¿Puedo controlar las devoluciones sobre venta?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-4" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, es posible controlar las devoluciones configurando los diferentes mecanismos que Prendasys te ofrece, desde autorización para realizarla hasta los días para realizar la devolución.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-5">¿Puedo pagar con cualquier forma de pago?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-5" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible. Prendasys te permitirá configurar múltiples formas de pago para tus ventas y apartados.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-6">¿Puedo manejar apartados?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-6" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, puedes manejar apartados y configurar tanto los días como el porcentaje de anticipo.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-7">¿Qué pasa cuando un apartado vence y el cliente no pasó a liquidarlo?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-7" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Puedes configurar el regreso automático al almacén de ventas o bien generar un ticket por el importe pagado del apartado para que posteriormente sea aplicado en otro apartado o venta.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-8">¿Cómo puedo revisar las utilidades de mis ventas?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-8" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Mediante los reportes de utilidades por cliente y por producto podrás revisarlas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#ventas-apartados-9">¿Puedo facturar mis apartados?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="ventas-apartados-9" data-parent="#accordion-ventas-apartados">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, con Prendasys es posible facturar apartados, ya que maneja el concepto de complemento de pago.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="operaciones-de-riesgo" class="container tab-pane fade" title="operaciones-de-riesgo">
    	<div class="title-orange">
    		<h2>Operaciones de riesgo</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-operaciones-riesgo">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operaciones-riesgo-1">¿Qué es una operación de riesgo?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="operaciones-riesgo-1" data-parent="#accordion-operaciones-riesgo">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Es el registro de un movimiento que puede llegar a beneficiar a las personas incorrectas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operaciones-riesgo-2">¿Qué tipos de operaciones de riesgo existen?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operaciones-riesgo-2" data-parent="#accordion-operaciones-riesgo">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Prendasys cuenta con todo un módulo de configuración de operaciones de riesgo que valida varios aspectos que se pueden llegar a dar dentro de: avalúos, empeños, refrendos, liquidaciones, descuentos, promociones, devoluciones, cancelaciones, cada usuario las configura de acuerdo con sus políticas de operación.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operaciones-riesgo-3">¿Cómo controla Prendasys esas operaciones de riesgo?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operaciones-riesgo-3" data-parent="#accordion-operaciones-riesgo">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Cuando se llega a ejecutar el movimiento en algunos de los diferentes procesos configurados, este movimiento es enviado a un monitor de autorizaciones para ser validado y autorizado para su procesamiento.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operaciones-riesgo-4">¿Puedo proteger a mi cliente quien ha estado abonando su empeño a que sea él el único que pueda liquidar su empeño?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operaciones-riesgo-4" data-parent="#accordion-operaciones-riesgo">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible proteger a tu cliente de ese tipo de operación, mediante la configuración de una operación de riesgo en liquidaciones sobre saldos menores al monto empeñado.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="seguridad" class="container tab-pane fade" title="seguridad">
    	<div class="title-orange">
    		<h2>Seguridad</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-seguridad">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-1">¿Se pueden manejar derechos por usuario?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="seguridad-1" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Sí. Prendasys permite asignar derechos por usuarios o por roles de usuario.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-2">¿Qué es un rol?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="seguridad-2" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Un rol es un perfil de usuario con derechos en específico. Este rol podrá asignarse a un usuario y este último heredará los derechos del ROL.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-3">Si un usuario tiene derechos a unas opciones en el sistema heredadas por un rol, ¿puedo otorgarle permisos adicionales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="seguridad-3" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, totalmente posible.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-4">¿Quién otorga las contraseñas de usuarios?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="seguridad-4" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Inicialmente el departamento de sistemas crea contraseñas genéricas, para que posteriormente el dueño del usuario pueda cambiar su contraseña frecuentemente desde una opción especial dentro de Prendasys.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-5">¿Cuántos usuarios cajeros puedo dar de alta?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="seguridad-5" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Prendasys te permite dar de alta tantos usuarios como los requieras, sin límite establecido.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#seguridad-6">¿Es posible limitar los derechos de los usuarios por sucursal?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="seguridad-6" data-parent="#accordion-seguridad">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, con Prendasys es posible asignar en qué sucursales podrá operar cada usuario.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="catalogos" class="container tab-pane fade" title="catalogos">
    	<div class="title-orange">
    		<h2>Catálogos</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-catalogos">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-1">¿Quién administra los catálogos?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="catalogos-1" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con Prendasys podrás administrar tus catálogos desde un punto central llamado BackOffice para todas tus sucursales evitando recapturas de datos.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-2">¿Cómo les llega la información registrada a las sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-2" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">PrendaSys cuenta con su motor propio de replicación que eficientiza tanto el tiempo como el medio de conexión haciéndolo de una manera muy rápida y efectiva, y ahorrándote tiempo en la administración de réplicas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-3">¿Cómo se administran los precios de referencia de los metales finos?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-3" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Existen 2 maneras de hacerlo. Una forma general por tipo de metal y otra por tipo de metal – kilataje, en cualquiera de las formas podrás inclusive establecer diferencias entre sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-4">¿Puedo dar de alta nuevas sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-4" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, con Prendasys es muy fácil hacerlo, solo requieres tener la licencia del producto para configurárselo a la nueva sucursal.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-5">¿Puedo dar de alta diversas empresas?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-5" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, es posible dar de alta diferentes empresas y solo se requiere de una licencia de BackOffice para hacerlo.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-6">¿Puedo medir la efectividad de las campañas de comunicación que contrate?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-6" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, lo puedes medir, solo es cuestión de configurar el medio de comunicación y la sucursal estará preguntando a tus clientes por ellos. Es muy fácil y efectivo.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-7">¿Puedo inactivar productos para que ya no reciban nuevos empeños?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-7" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible. Puedes inactivarlo para su valuación, pero seguirán activos para su venta en caso de que tengas existencia.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-8">¿Cuánto tarda el proceso en dar de alta un nuevo producto para su empeño?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-8" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Es realmente rápido, bastará con dar de alta el producto, asignarle su precio de referencia y listo.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-9">¿Puedo configurar mis tickets de venta?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-9" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Puedes configurar tus formatos de tickets de venta para que imprima solo la información que tú requieras.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#catalogos-10">¿Los productos tienen la clasificación del SAT?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="catalogos-10" data-parent="#accordion-catalogos">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Contamos con los catálogos de clave producto servicio y clave unidad que se relacionan al producto, mismos que aparecerán en la factura para el cumplimiento de la facturación 3.3</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="backoffice" class="container tab-pane fade" title="backoffice">
    	<div class="title-orange">
    		<h2>Backoffice</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-backoffice">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-1">¿Cuántas sucursales debo tener activas para poder operar el BackOffice?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="backoffice-1" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Desde una sucursal activa hasta toda una franquicia de cientos de sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-2">¿Qué información puedo consultar en el concentrado?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-2" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Encontrarás información clave de tu negocio. Conoce tu nivel de empeños, desempeños, adjudicaciones, ventas y demás por día, mes o en un periodo específico y consulta registros por venta de artículos o con mayor utilidad.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-3">¿Qué sucede con la sincronización de información si estoy un día sin internet?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-3" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Cuando la conexión al concentrador se reestablezca el sistema enviará toda la información pendiente de sincronizar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-4">¿Cada cuánto se sincroniza la información de mis sucursales al BackOffice?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-4" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Dependiendo la necesidad y el medio de comunicación que tengas en tus sucursales, es configurable el tiempo pudiendo ser por minutos y horas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-5">¿Es posible consultar las existencias de todas mis sucursales en los diferentes almacenes desde el BackOffice?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-5" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Si, es posible que consultes tus existencias por sucursal almacén y sobre todo que conozcan el valor de inventarios que tienes en cada una de ellas.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-6">¿Es posible conocer los flujos de efectivo de mis sucursales en tiempo real?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-6" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible conocer estos saldos en tiempo real, solo requieres que tus sucursales estén conectadas a internet.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-7">¿Es posible conocer el estatus de las auditorías que se estén realizando en las sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-7" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible. Con Prendasys auditas a tus auditores, ya que brinda información de las auditorías abiertas, finalizadas, canceladas con sus respectivos valores y diferencias de inventarios, tanto en piezas como en pesos.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#backoffice-8">¿Es posible generar nuevos reportes?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="backoffice-8" data-parent="#accordion-backoffice">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Totalmente posible. Con Prendasys tú eres dueño de tu propia información y podrás explorarla de la manera más conveniente para la administración de tu negocio.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="operacion" class="container tab-pane fade" title="operacion">
    	<div class="title-orange">
    		<h2>Operación</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-operacion">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-1">¿Por qué Prendasys me manda una alerta al mandar imprimir en una impresora de ticket y no me genera la impresión?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="operacion-1" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Aunque la impresora esté correctamente configurada en Prendasys, si no está en línea mandará esa alerta de impresión, por lo que se tendrá que realizar una prueba de impresión desde las herramientas de Windows para comprobar conexión. Revisa cables y que la impresora esté encendida.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-2">¿Por qué no puedo seleccionar una sucursal para generar un refrendo foráneo?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-2" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Para generar un refrendo foráneo es necesario que exista comunicación entre la sucursal dueña del contrato y la sucursal que desea cobrar el refrendo. Para ello es necesario comprobar que esté correctamente configurada la URL de la sucursal dueña del contrato en el catálogo de sucursales, así como también comprobar que el usuario de la base de datos tiene derechos sobre la información de la sucursal a donde se desea conectar. Finalmente deberemos asegurarnos de que exista conexión entre ambas sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-3">¿Por qué no me despliega las descripciones de los productos nuevos en una entrada por traspaso de inventarios entre sucursales?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-3" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Para realizar una entrada por transferencia que contiene artículos nuevos es necesario que exista comunicación entre la sucursal que realizó la salida y la sucursal que recibirá el traspaso. Para ello es necesario comprobar que esté correctamente configurada la URL de la sucursal origen en el catálogo de sucursales, así como también, comprobar que el usuario de la base de datos tiene derechos sobre la información de la sucursal a donde se desea conectar. Finalmente deberemos asegurarnos de que existe conexión entre ambas sucursales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-4">¿Qué debo hacer cuando Prendasys no permita realizar un cambio de conexión?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-4" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Verificar que esté bien escrito el nombre del servidor y base de datos a donde deseas conectarte, así mismo, se deberá revisar los derechos del usuario SQL. Finalmente revisar el enlace con la sucursal hacia donde se requiere conectar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-5">¿Qué debo hacer cuando no pueda acceder a ninguna funcionalidad de sucursal, aún y cuando tenga los permisos asignados?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-5" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Lo que se tiene que hacer es otorgarle permiso al usuario para que opere en la sucursal donde desea ingresar.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-6">¿Por qué cuando cambio mi contraseña en sucursal, al poco tiempo la tengo que volver a cambiar?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-6" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Esto sucede porque la sucursal en donde se está realizando el cambio de contraseña no está teniendo comunicación con BackOffice, por lo que es necesario comprobar las configuraciones y la comunicación de sucursal hacia el Backoffice.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#operacion-7">¿Qué significa el mensaje “UUID No existe, intente en 10 minutos” al cancelar una factura?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="operacion-7" data-parent="#accordion-operacion">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Este mensaje sucede cuando la factura fue validada por el timbrador oficial pero la información aún no se encuentra en la base de datos principal del SAT; es por ello que debemos esperar 10 minutos, que es el tiempo que sugiere el SAT para sincronizar la información con todos los timbradores autorizados.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>
    <div id="licenciamiento" class="container tab-pane fade" title="licenciamiento">
    	<div class="title-orange">
    		<h2>Licenciamiento</h2>
    	</div>
    	<div class="barra faqs">
    		<div class="clearfix" style="height:15px;"></div>
    		<div class="col-12">
    			<div class="panel-group" id="accordion-licenciamiento">
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#licenciamiento-1">¿Mi licencia principal tiene límite de usuarios?<i class="fa fa-caret-down float-right"></i></a></h4>
    					</div>
    					<div class="collapse show" id="licenciamiento-1" data-parent="#accordion-licenciamiento">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Con tu licencia de servicio de suscripción anual de Prendasys tienes usuarios ilimitados para el equipo de cómputo donde se instale. Si tienes otros equipos de cómputo en tu casa de empeño, puedes adquirir licencias adicionales.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#licenciamiento-2">¿Qué debo hacer si el equipo donde tengo la licencia de Prendasys falló y requiero formatearlo?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="licenciamiento-2" data-parent="#accordion-licenciamiento">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Primero deberás realizar el respaldo de la información y revocar tu licencia de suscripción para poder reinstalarla en el equipo.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#licenciamiento-3">¿Qué debo hacer para revocar una licencia de Prendasys de un equipo?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="licenciamiento-3" data-parent="#accordion-licenciamiento">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Para revocar una licencia realice los siguientes pasos:</p>
    								<ul>
										<li>- Ir a la opción Configuración.</li>
										<li>- Opciones Generales.</li>
										<li>- Hacer clic en el botón de Revocar. Se mostrará un mensaje de advertencia indicando que al revocar se cerrará el sistema.</li>
										<li>- Haz clic en “Si” para continuar.</li>
										<li>- Al seleccionar la opción de “Si”, se iniciará el proceso de revocación y solicitará los siguientes datos:
											<ol>
												<li>Datos generales</li>
												<li>Serial</li>
												<li>Motivo de revocación.</li>
												<li>Correo electrónico y contraseña. (Estos datos los recibiste en el correo electrónico registrado al activar tu licencia).</li>
											</ol>
										</li>
										<li>- Para finalizar da clic en el botón. Si los datos proporcionados son correctos, se mostrará un mensaje indicando que el proceso fue satisfactorio. El sistema se cerrará junto con las instancias que tengas abiertas en otros equipos y recibirás un correo electrónico confirmando el nuevo serial, mismo que podrás activar nuevamente. Nota: El proceso de revocación solo elimina el serial, el sistema sigue instalado en el equipo.</li>
									</ul>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#licenciamiento-4">¿Cuándo se debe revocar la licencia?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="licenciamiento-4" data-parent="#accordion-licenciamiento">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Cuando desees cambiar o formatear tu equipo, antes de estas actividades deberás renovar tu licencia para que puedas utilizarla en el equipo nuevo o formateado.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    				<div class="panel panel-default">
    					<div class="panel-heading">
    						<h4 class="panel-title"><a href="" data-toggle="collapse" data-target="#licenciamiento-5">¿Qué versión de SQL requiero para implementar Prendasys?<i class="fa fa-caret-right float-right"></i></a></h4>
    					</div>
    					<div class="collapse" id="licenciamiento-5" data-parent="#accordion-licenciamiento">
    						<div class="panel-body">
    							<div class="padding">
    								<p class="response-par">Puedes utilizar tanto la versión Express como la Estándar, el tipo de versión de SQL dependerá de la carga de trabajo que se genere en cada sucursal.</p>
    							</div>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

  </div>
</div>

<div class="clearfix">
	<div class="container text-center">
		<div class="row">
			<div class="col-sm-12 py-5">
				<a href="#categorias" class="btt-round"><i class="fa fa-chevron-up"></i></a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid py-5 aviso-bottom">
      <div class="container">
        <div class="col-md-10 offset-md-1 text-center">		        
          <h4 class="text-gray my-3 light-txt anima">
            <strong>¿Necesitas más ayuda?</strong><br>
            <small>Lo comprendemos perfectamente</small>
          </h4>
          <a href="caracteristicas" class="btn btn-lg ghostBtn btn-ayuda-gray mx-auto anima">Aquí te ayudamos</a>
        </div>
      </div>
  </div>

</section>