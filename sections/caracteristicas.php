<?php
$enviado1 = 0;
$error1 = null;

//echo '<pre>'.print_r($_REQUEST, 1).'</pre>';
if(!empty($_REQUEST['name']) && $_REQUEST['name'] && !empty($_REQUEST['email']) && $_REQUEST['email'])
  require_once "sections/functions/send_contact.php";

?>

<div class="navbarHeight"></div>

<!-- PRENDASYS - HOME / Landing (más adelante será "características") -->

<section class="container-fluid" id="home-section">
  <!-- Para SEO -->
  <h1 class="sr-only">Prendasys</h1>
  <h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>

<!-- Cuando ya sea "Carácterísticas" quitar éste ("top-container") porque ya no habrá formulario flotante -->
<div class="top-container">

  <div id="slideshow">
    <!-- Cuando ya sea "Carácterísticas" usar "img" porque ya no habrá formulario flotante -->
    <div class="item">
      <!--<img src="images/caracteristicas/slideshow/slide1.jpg" class="d-none d-lg-block" alt="Prendasys">
      <img src="images/caracteristicas/slideshow/slide1-xs.jpg" class="d-lg-none" alt="Prendasys">
      -->
      <div class="slide d-none d-lg-block" id="slide1"></div><!--href="#info-bloques"-->
      <div class="slide-xs d-lg-none" id="slide1-xs">
        <img src="images/caracteristicas/slideshow/slide1-xs.jpg" alt="Prendasys">
      </div>
    </div>
    <!--<div class="item">
      <img src="images/caracteristicas/slideshow/slide2.jpg" class="d-none d-lg-block" alt="Prendasys">
      <img src="images/caracteristicas/slideshow/slide2-xs.jpg" class="d-lg-none" alt="Prendasys">
      
      <a href="#info-bloques" class="slide d-none d-lg-block" id="slide2"></a>
      <a href="#info-bloques" class="slide-xs d-lg-none" id="slide2-xs">
        <img src="images/caracteristicas/slideshow/slide2-xs.jpg" alt="Prendasys">
      </a>
    </div>-->
    <div class="item">
      <!--<img src="images/caracteristicas/slideshow/slide3.jpg" class="d-none d-lg-block" alt="Prendasys">
      <img src="images/caracteristicas/slideshow/slide3-xs.jpg" class="d-lg-none" alt="Prendasys">
      -->
      <a href="#info-bloques" class="slide d-none d-lg-block" id="slide3"></a>
      <a href="#info-bloques" class="slide-xs d-lg-none" id="slide3-xs">
        <img src="images/caracteristicas/slideshow/slide3-xs.jpg" alt="Prendasys">
      </a>
    </div>
  </div>


<div class="container">

  <div id="formulario-container"><a name="contacto" id="contacto"></a>

    <div id="formulario" class="clearfix row">
      <div class="heading-form form-group col-md-12">
        <h2>Contáctanos</h2>
        <span>Nos encantará atenderte <!--tus llamadas o resolver tus dudas--></span>
      </div>
      <div class="line" style="margin: 10px 0 15px;"></div>
    </div>


    <!--[if lte IE 8]>
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
    <![endif]-->
    <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
    <script>
        hbspt.forms.create({
        portalId: "7853198",
        formId: "5201cc71-310e-4f0a-81ac-7af32fc74844"
    });
    </script>

    <?php /*
    <form action="./" method="post" id="formulario" class="clearfix">
      <div class="heading-form form-group">
        <h2>Contáctanos</h2>
        <span>Nos encantará atenderte <!--tus llamadas o resolver tus dudas--></span>
      </div>
      <div class="line" style="margin: 10px 0 15px;"></div>
      <div class="col-12 form-group">
        <label for="name" class="form-label sr-only">Tu nombre completo*</label>
        <input type="text" name="name" id="name" class="form-control form-control-lg restrict1" value="" placeholder="Tu nombre completo*" required>
        <div class="invalid-feedback">Este campo es obligatorio</div>
      </div>
      <div class="col-12 form-group">
        <label for="email" class="form-label sr-only">E-mail*</label>
        <input type="email" name="email" id="email" class="form-control form-control-lg email" value="" placeholder="E-mail*" required>
        <div class="invalid-feedback">Este campo es obligatorio</div>
      </div>
      <div class="col-12 form-group">
        <label for="phone" class="form-label sr-only">Tu teléfono*</label>
        <input type="text" name="phone" id="phone" class="form-control form-control-lg telefono" value="" placeholder="Tu teléfono*" required>
        <div class="invalid-feedback">Este campo es obligatorio</div>
      </div>
      <div class="col-12 form-group">
        <select class="minimal form-control custom-select-lg" name="state" id="state" required>
            <option value="">*Selecciona tu Estado</option>
            <option value="Aguascalientes">Aguascalientes</option>
            <option value="Baja California">Baja California</option>
            <option value="Baja California Sur">Baja California Sur</option>
            <option value="Campeche">Campeche</option>
            <option value="Chiapas">Chiapas</option>
            <option value="Chihuahua">Chihuahua</option>
            <option value="Coahuila">Coahuila</option>
            <option value="Colima">Colima</option>
            <option value="Ciudad de México">Ciudad de México</option>
            <option value="Durango">Durango</option>
            <option value="Guanajuato">Guanajuato</option>
            <option value="Guerrero">Guerrero</option>
            <option value="Hidalgo">Hidalgo</option>
            <option value="Jalisco">Jalisco</option>
            <option value="Estado de México">Estado de México</option>
            <option value="Michoacán">Michoacán</option>
            <option value="Morelos">Morelos</option>
            <option value="Nayarit">Nayarit</option>
            <option value="Nuevo León">Nuevo León</option>
            <option value="Oaxaca">Oaxaca</option>
            <option value="Puebla">Puebla</option>
            <option value="Querétaro">Querétaro</option>
            <option value="Quintana Roo">Quintana Roo</option>
            <option value="San Luis Potosí">San Luis Potosí</option>
            <option value="Sinaloa">Sinaloa</option>
            <option value="Sonora">Sonora</option>
            <option value="Tabasco">Tabasco</option>
            <option value="Tamaulipas">Tamaulipas</option>
            <option value="Tlaxcala">Tlaxcala</option>
            <option value="Veracruz">Veracruz</option>
            <option value="Yucatán">Yucatán</option>
            <option value="Zacatecas">Zacatecas</option>
            <option value="Otro">Otro</option>
         </select>
         <div class="invalid-feedback">Debes seleccionar tu estado</div>
      </div>
      <div class="col-md-6 form-group">
         <label for="city" class="form-label sr-only">Tu Ciudad*</label>
         <input type="text" name="city" id="city" class="form-control form-control-sm" value="" placeholder="Tu Ciudad*" required>
         <div class="invalid-feedback">Este campo es obligatorio</div>
      </div>

      <div class="col-md-12 form-group">
        <label for="message" class="form-label sr-only">Déjanos un mensaje*</label>
        <textarea name="message" id="message" class="form-control form-control-sm required" value="" placeholder="Déjanos un mensaje*" required></textarea>
        <div class="invalid-feedback">Este campo es obligatorio</div>
      </div>

      <div class="recaptcha-holder"></div>

      <div class="form-group col-md-12">
          <div class="checkbox">
              <label class="text-aviso text-form"><input type="checkbox" class="aviso" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Por favor acepta nuestros términos de privacidad')" required> Leí y acepto el tratamiento de mis datos personales de acuerdo al <a href="pdf/aviso-privacidad.pdf" class="link-aviso" target="_blank">aviso de privacidad</a></label>
          </div>
          <div class="checkbox">
              <label class="text-aviso text-form"><input type="checkbox" name="boletin" value="1"> Me gustaría que me añadan a su lista de correo</label>
          </div>
      </div>

      <?php if (!empty($error1)) echo '<div style="background-color: #bc1010;"><p>'.$error1.'</p></div>';?>
      <div class="col-md-12 form-group">
        <label for="" class="form-label sr-only"></label>
        <button class="btn btn-lg send-btn" value="submit" type="submit" style="">Enviar</button>
      </div>
    </form>*/ ?>
  </div>
</div>
<!-- Cuando ya sea "Carácterísticas" quitar éste (</div>) porque ya no habrá formulario flotante -->
</div>

  <!-- DESCRIPCIÓN INICIAL -->

  <div id="initial-desc">
    <div class="container">
      <div class="col-md-10 offset-md-1">
        <h2 class="text-center anima"><span class="orangeTxt">Prenda</span>sys</h2>
        <h3 class="text-center anima light">El sistema experto en cadenas de <strong>casas de empeño</strong></h3>
        <h4 class="text-center anima light">El aliado para operar eficazmente tu negocio prendario<!--, desde dos sucursales, hasta una cadena con <strong>alcance nacional</strong>--></h4>        
      </div>
    </div>
  </div>

  <!-- BLOQUES -->

  <div class="container-fluid" style="background:#f2f2f2;">    
    <div class="row" id="info-bloques">
      <div id="bloques">
          <div class="item x2">
            <!-- Empeños - BTN -->
            <a href="#content" id="" class="bloque2x2 empenos-btn">
              <div class="caption">          
                <h3 class="label">Empeños</h3>
                <p class="description">Mejora tus valuaciones</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/empenos.jpg" alt="Empeños">
            </a>
          </div>
          <div class="item x1">
            <!-- Control Eficaz - BTN -->
            <a href="#content" id="" class="bloque1x1 ctrl-eficaz-btn">
              <div class="caption">          
                <h3 class="label">Control Eficaz</h3>
                <p class="description">Administra tus objetivos de forma rentable</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/control-eficaz.jpg" alt="Control Eficaz">
            </a>
            <!-- Servicio al cliente - BTN -->
            <a href="#content" id="" class="bloque1x1 serv-cliente-btn">
              <div class="caption">          
                <h3 class="label">Servicio al Cliente</h3>
                <p class="description">Pon a tus clientes en el centro</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/servicio-al-cliente.jpg" alt="Servicio al Cliente">
            </a>
          </div>
          <div class="item x1">
            <!-- Prevención - BTN -->
            <a href="#content" id="" class="bloque1x1 prevencion-btn">
              <div class="caption">          
                <h3 class="label">Prevención</h3>
                <p class="description">Anticípate y evita operaciones riesgosas</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/prevencion.jpg" alt="Prevencion">
            </a>
            <!-- Inventarios - BTN -->
            <a href="#content" id="" class="bloque1x1 inventarios-btn">
              <div class="caption">
                <h3 class="label">Inventarios</h3>
                <p class="description">Olvídate de mercancía sin vender</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/inventarios.jpg" alt="Inventarios">
            </a>
          </div> 
          <div class="item x2">
            <!-- Ventas y Apartados - BTN -->
            <a href="#content" id="" class="bloque2x2 vtas-apartados-btn">
              <div class="caption">          
                <h3 class="label">Ventas y Apartados</h3>
                <p class="description">Aumenta tus ingresos con nuestras herramientas comerciales y una estrategia omnicanal</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/ventas-apartados.jpg" alt="Ventas y Apartados">
            </a>
          </div>
          <div class="item x1">
            <!-- Operaciones de Riesgo - BTN -->
            <a href="#content" id="" class="bloque1x1 op-riesgo-btn">
              <div class="caption">          
                <h3 class="label">Operaciones de Riesgo</h3>
                <p class="description">Blinda a tus clientes y sucursales</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/operaciones-riesgo.jpg" alt="Operaciones de Riesgo">
            </a>
            <!-- Auditorías Offline - BTN -->
            <a href="#content" id="" class="bloque1x1 aud-offline-btn">
              <div class="caption">          
                <h3 class="label">Auditorías Offline</h3>
                <p class="description">Permite a tus auditores llegar lejos</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/auditorias-offline.jpg" alt="Auditorías Offline">
            </a>
          </div>
          <div class="item x2">
            <!-- Seguridad - BTN -->
            <a href="#content" id="" class="bloque2x2 seguridad-btn">
              <div class="caption">          
                <h3 class="label">Seguridad</h3>
                <p class="description">Tu información siempre segura</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/seguridad.jpg" alt="Seguridad">
            </a>
          </div>
          <div class="item x1">
            <!-- Catálogos y Configuraciones - BTN -->
            <a href="#content" id="" class="bloque1x1 cat-config-btn">
              <div class="caption">          
                <h3 class="label">Catálogos y Configuraciones</h3>
                <p class="description">Estandariza tus procesos de avalúo y crece tu negocio</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/catalogos-configuraciones.jpg" alt="Catálogos y Configuraciones">
            </a>
            <!-- Backoffice - BTN -->
            <a href="#content" id="" class="bloque1x1 backoffice-btn">
              <div class="caption">          
                <h3 class="label">Backoffice</h3>
                <p class="description">Concentra la información en un solo lugar</p>
                <div class="btn ghostBtn">Ver información</div>
              </div>
              <img src="images/caracteristicas/bloques/backoffice.jpg" alt="Backoffice">
            </a>
          </div>
          <!--<div class="item foo-item x2"></div>-->
      </div>
    </div>
  </div>  

  <div class="clearfix off-sm" style="height:90px;clear:both;"></div>
    
  <div id="content">
    <!-- Empeños -->
    <div id="empeno-bloque" class="bloque">
      <div class="title">
        <h2>
          Empeños
          <div class="arrows">
            <!--<a href="javascript:void(0);" class="foo"><i class="fa fa-chevron-left"></i></a>-->
            <a href="javascript:void(0);" class="ctrl-eficaz-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img1.jpg" alt="Presta mejor" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Presta mejor</h3>
              <p>Implementa estrategias comerciales centralizadas que te permitan operar de manera óptima en todas tus sucursales sin tener expertos valuadores en cada una de ellas.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img2.jpg" alt="Crece gracias a mejores avalúos" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Crece gracias a mejores avalúos</h3>
              <p>Logra de manera automática los mejores avalúos y por consecuencia clientes frecuentes a tu negocio.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img3.jpg" alt="Reduce tus adjudicaciones" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Reduce tus adjudicaciones</h3>
              <p>Configura diferentes incentivos para que tus clientes recuperen sus prendas e incrementa la rentabilidad de tus préstamos</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img4.jpg" alt="Utiliza diferentes formas de pago" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Utiliza diferentes formas de pago</h3>
              <p>Para su mayor comodidad permite a tus clientes refrendar, abonar y liquidar con cualquier forma de pago.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img5.jpg" alt="Aprovecha los beneficios de operar en red" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Aprovecha los beneficios de operar en red</h3>
              <p>Controla de manera segura transferencias de efectivo entre tus sucursales.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/empeno/img6.jpg" alt="Incentiva la visita a tus sucursales" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Incentiva la visita a tus sucursales</h3>
              <p>Define políticas de apartados y configúralas para facilitar a tus clientes la adquisición de mercancía.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Control Eficaz -->
    <div id="ctrl-eficaz-bloque" class="bloque">
      <div class="title">
        <h2>
          Control Eficaz
          <div class="arrows">
            <a href="javascript:void(0);" class="empenos-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="serv-cliente-btn"><i class="fa fa-chevron-right"></i></a>
          </div>        
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/control-eficaz/img1.jpg" alt="Facturación electrónica" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Facturación electrónica</h3>
              <p>Simplifica la operación al realizar tus facturas electrónicas de manera fácil y rápida.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/control-eficaz/img2.jpg" alt="Controla las diferencias de los cortes de caja" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Controla las diferencias de los cortes de caja</h3>
              <p>Realiza cortes de caja ciegos y mantén tus ingresos en regla. Utiliza los retiros de dinero, cortes auditoría, parcial o final cuando los requieras.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Servicio al cliente -->
    <div id="serv-cliente-bloque" class="bloque">
      <div class="title">
        <h2>
          Servicio al Cliente
          <div class="arrows">
            <a href="javascript:void(0);" class="ctrl-eficaz-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="prevencion-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/servicio-cliente/img1.jpg" alt="Identifica a tu cliente y hazlo sentir importante" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Identifica a tu cliente y hazlo sentir importante</h3>
              <p>Gracias a su identificador único reduces al máximo la duplicidad de datos y se concentra de manera óptima la información de un mismo cliente sin importar en cual sucursal realice sus operaciones.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/servicio-cliente/img2.jpg" alt="Premia la lealtad de tus clientes" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Premia la lealtad de tus clientes</h3>
              <p>Configura perfiles de clientes que te permitirán otorgar beneficios en tasas de interés y montos de préstamos preferenciales. </p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/servicio-cliente/img3.jpg" alt="Facilita la vida de tus clientes" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Facilita la vida de tus clientes</h3>
              <p>Brindándoles la oportunidad de refrendar en cualquiera de tus sucursales.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/servicio-cliente/img4.jpg" alt="Conoce mejor a tus clientes" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Conoce mejor a tus clientes</h3>
              <p>Configura encuestas que te permitan conocer sus necesidades y así capitalizar el análisis de esa información para generar nuevas estrategias comerciales que te ayudarán a crecer.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Prevención -->
    <div id="prevencion-bloque" class="bloque">
      <div class="title">
        <h2>
          Prevención
          <div class="arrows">
            <a href="javascript:void(0);" class="serv-cliente-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="inventarios-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/prevencion/img1.jpg" alt="Opera sin preocupaciones" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Opera sin preocupaciones</h3>
              <p>Prendasys cuenta con herramientas para establecer controles en materia de prevención de lavado de dinero.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/prevencion/img2.jpg" alt="Alerta a tus sucursales de indicadores fuera de rango" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Alerta a tus sucursales de indicadores fuera de rango</h3>
              <p>Con el análisis de información que te brinda Prendasys podrás identificarlos de manera oportuna.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/prevencion/img3.jpg" alt="Prevé las operaciones riesgosas" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Prevé las operaciones riesgosas</h3>
              <p>Anticípate con las operaciones que pueden poner en riesgo tu capital o el de tus clientes, con las herramientas que Prendasys te brinda.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/prevencion/img4.jpg" alt="Titulo.." class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Establece parámetros personalizados de control</h3>
              <p>Establece máximos y mínimos para operar tus empeños.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Inventarios -->
    <div id="inventarios-bloque" class="bloque">
      <div class="title">
        <h2>Inventarios
          <div class="arrows">
            <a href="javascript:void(0);" class="prevencion-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="vtas-apartados-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/inventarios/img1.jpg" alt="Optimiza tu inventario" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Optimiza tu inventario</h3>
              <p>Implementa las mejoras prácticas para el control de tu inventario en vitrina, configurando promociones basadas en la antigüedad de estos.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/inventarios/img2.jpg" alt="Mueve tu inventario donde el consumidor lo demande" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Mueve tu inventario donde el consumidor lo demande</h3>
              <p>Transfiere productos de una sucursal a otra de forma segura y práctica, conservando su antigüedad para una correcta aplicación de políticas de descuento.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/inventarios/img3.jpg" alt="Minimiza el robo hormiga" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Minimiza el robo hormiga</h3>
              <p>Realiza auditorías totales o selectivas en tus diferentes almacenes y detecta oportunamente desviaciones.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/inventarios/img4.jpg" alt="Detecta oportunidades" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Detecta oportunidades</h3>
              <p>Consulta de manera centralizada el valor de tus inventarios y crea políticas comerciales que te permitan el desplazamiento por volumen.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Ventas y Apartados -->
    <div id="vtas-apartados-bloque" class="bloque">
      <div class="title">
        <h2>Ventas y Apartados
          <div class="arrows">
            <a href="javascript:void(0);" class="inventarios-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="op-riesgo-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img1.jpg" alt="Incrementa tus ventas" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Incrementa tus ventas</h3>
              <p>Implementa estrategias comerciales como promociones y ventas de impulso con precios especiales.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img2.jpg" alt="Sigue las ventas por cadena y sucursal" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Sigue las ventas por cadena y sucursal</h3>
              <p>Configura metas de venta en global y por sucursal y mide los resultados día a día.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img3.jpg" alt="Consulta existencias" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Consulta existencias</h3>
              <p>Es fácil agilizar la venta al identificar existencias de productos en diferentes sucursales y así crear estrategias para su venta.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img4.jpg" alt="Monitorea tus utilidades" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Monitorea tus utilidades</h3>
              <p>Configura precios de venta óptimos con base a las condiciones de las prendas.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img5.jpg" alt="E-commerce" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>E-commerce</h3>
              <p>Conecta Prendays a las mejores plataformas, vende en línea en cualquier lugar y hora.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/ventas-apartados/img6.jpg" alt="Herramientas comerciales" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Herramientas comerciales</h3>
              <p>Activa tus sucursales con promociones y descuentos exclusivos y premia la preferencia de tus clientes.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Operaciones de Riesgo -->
    <div id="op-riesgo-bloque" class="bloque">
      <div class="title">
        <h2>Operaciones de Riesgo
          <div class="arrows">
            <a href="javascript:void(0);" class="vtas-apartados-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="aud-offline-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/operaciones-riesgo/img1.jpg" alt="Cuida a tus clientes y sus prendas" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Cuida a tus clientes y sus prendas</h3>
              <p>Configura alertas sobre operaciones riesgosas que pueden generarse por tu propio personal al tener disponible información valiosa.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/operaciones-riesgo/img2.jpg" alt="Protege a tus sucursales de operaciones ficticias" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Protege a tus sucursales de operaciones ficticias</h3>
              <p>Da de alta alertas sobre artículos de especial cuidado para un mayor control en el empeño y evitar pérdidas.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/operaciones-riesgo/img3.jpg" alt="Administra las alertas" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Administra las alertas</h3>
              <p>Es posible desde un monitor, desde cualquier lugar y tú decides cuál autorizas de acuerdo con tus políticas de operación.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/operaciones-riesgo/img4.jpg" alt="Titulo4" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Análisis de riesgos</h3>
              <p>Analiza las situaciones inapropiadas que más frecuentemente se realizan y configura nuevos parámetros de operación.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Auditorías Offline -->    
    <div id="aud-offline-bloque" class="bloque">
      <div class="title">
        <h2>
          Auditorías Offline
          <div class="arrows">
            <a href="javascript:void(0);" class="op-riesgo-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="seguridad-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/auditorias-offline/img1.jpg" alt="Realiza auditorias físicas hasta lo más profundo de tu almacén" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Realiza auditorías físicas hasta lo más profundo de tu almacén</h3>
              <p>Permite que tus auditores se desplacen por toda la bodega para realizar tomas físicas de inventarios sin necesidad de estar todo el tiempo conectado.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/auditorias-offline/img2.jpg" alt="Monitorea a tus auditores" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Monitorea a tus auditores</h3>
              <p>Conoce de manera inmediata el avance e información generada en cada una de las auditorías de todas las sucursales.</p>
            </div>
          </div>          
        </div>
      </div>
    </div>
    <!-- Seguridad -->
    <div id="seguridad-bloque" class="bloque">
      <div class="title">
        <h2>
          Seguridad
          <div class="arrows">
            <a href="javascript:void(0);" class="aud-offline-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="cat-config-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/seguridad/img1.jpg" alt="Protege tu patrimonio" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Protege tu patrimonio</h3>
              <p>Resguarda tu información de manera segura, tus datos siempre estarán seguros con Prendasys.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/seguridad/img2.jpg" alt="Crea diferentes roles" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Crea diferentes roles</h3>
              <p>Prendasys te permite generar diferentes roles de acuerdo con las necesidades de tu operación y así controlar los accesos por usuario.</p>
            </div>
          </div>          
        </div>
      </div>
    </div>
    <!-- Catálogos y Configuraciones -->
    <div id="cat-config-bloque" class="bloque">
      <div class="title">
        <h2>Catálogos y Configuraciones
          <div class="arrows">
            <a href="javascript:void(0);" class="seguridad-btn"><i class="fa fa-chevron-left"></i></a>
            <a href="javascript:void(0);" class="backoffice-btn"><i class="fa fa-chevron-right"></i></a>
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/catalogos-configuraciones/img1.jpg" alt="Optimiza tu tiempo" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Optimiza tu tiempo</h3>
              <p>Eficienta tus operaciones al eliminar actividades repetitivas y administra tus catálogos desde un solo lugar.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/catalogos-configuraciones/img2.jpg" alt="Manejo de empresas y sucursales" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Manejo de empresas y sucursales</h3>
              <p>Puedes manejar la información a nivel empresa y sucursal y así distinguir entre diferentes razones sociales.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/catalogos-configuraciones/img3.jpg" alt="Replicamos tu información" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Replicamos tu información</h3>
              <p>Con nuestro motor de replicación olvídate de la pérdida de información y de tener problemas para el control de éstas.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/catalogos-configuraciones/img4.jpg" alt="Altamente configurable" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Altamente configurable</h3>
              <p>Parametriza Prendasys según las necesidades de tus casas de empeño.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Backoffice -->
    <div id="backoffice-bloque" class="bloque">
      <div class="title">
        <h2>
          Backoffice
          <div class="arrows">
            <a href="javascript:void(0);" class="cat-config-btn"><i class="fa fa-chevron-left"></i></a>
            <!--<a href="javascript:void(0);" class="-btn"><i class="fa fa-chevron-right"></i></a>-->
          </div>
        </h2>
      </div>
      <div class="container contenido">
        <div class="row">
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/backoffice/img1.jpg" alt="Generación de reportes" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Generación de reportes</h3>
              <p>Conoce tu nivel de empeños, desempeños, adjudicaciones, ventas y demás por día, mes o en un periodo específico y consulta registros por venta de productos o con mayor utilidad.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/backoffice/img2.jpg" alt="Cuenta con información confiable" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Cuenta con información confiable</h3>
              <p>Ten siempre a la mano información actualizada y toma de manera oportuna mejores decisiones.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/backoffice/img3.jpg" alt="Dueño de tu información" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Dueño de tu información</h3>
              <p>Administra y protege la información de tus sucursales y concentrador. Tú eres el dueño de las bases de datos y nadie sin tu permiso tendrá acceso a ellas.</p>
            </div>
          </div>
          <div class="col-md-6 p-0 content-element">
            <img src="images/caracteristicas/backoffice/img4.jpg" alt="Mantén tus finanzas sanas" class="d-block mx-auto img-fluid">
            <div class="texto">
              <h3>Mantén tus finanzas sanas</h3>
              <p>Consulta de manera centralizada las utilidades de tus operaciones y crea políticas operativas que te permitan ir alineando e incrementando tus utilidades.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="clearfix off-sm" style="height:90px;clear:both;"></div>

  <div class="container-fluid orange-bg py-5 aviso-bottom">
    <div class="container">
      <div class="col-md-10 offset-md-1 text-center">
        <!--<h4 class="text-white my-3 light-txt anima"><strong>Prendasys</strong> es el sistema más completo para administrar <strong>Casas de Empeño</strong>, contáctanos sin compromiso para darte <strong>más detalles</strong>.</h4>-->
        <h4 class="text-white my-3 light-txt anima">Compártenos tus dudas y conoce más de <strong>Prendasys</strong></h4>
        <!--<a href="#top" class="btn ghostBtn mx-auto slide d-none d-lg-block anima">Contáctanos</a>
        <a href="#contacto" class="btn ghostBtn mx-auto slide-xs d-lg-none anima">Contáctanos</a>-->
        <a href="#top" class="btn ghostBtn mx-auto slide d-none d-xl-block anima">Regístrate aquí</a>
        <a href="#contacto" class="btn ghostBtn mx-auto slide-xs d-xl-none anima">Regístrate aquí</a>
      </div>
    </div>
  </div>

</section>


<script>
   var renderGoogleInvisibleRecaptcha = function() {
       for (var i = 0; i < document.forms.length; ++i) {
           var form = document.forms[i];
           var holder = form.querySelector('.recaptcha-holder');
           if (null === holder)
               continue;
           (function(frm){
               var holderId = grecaptcha.render(holder,{
                       'sitekey': '6LfpG6YZAAAAAPef90xYhUtTki-A0jsK3ZU--WQ7',
                       'size': 'invisible',
                       'badge' : 'bottomright', // possible values: bottomright, bottomleft, inline
                       'callback' : function (recaptchaToken) {
                           HTMLFormElement.prototype.submit.call(frm);
                       }
                   });
               frm.onsubmit = function (evt){
                   evt.preventDefault();
                   grecaptcha.execute(holderId);
               };
           })(form);
       }
   };

//console.log('p1');
</script>
<script src="//www.google.com/recaptcha/api.js?onload=renderGoogleInvisibleRecaptcha&render=explicit" async defer></script>