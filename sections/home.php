<div class="navbarHeight"></div>

<!-- PRENDASYS - HOME -->

<section class="container-fluid" id="home-section">
    <!-- Para SEO -->
    <h1 class="sr-only">Prendasys</h1>
    <h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>

  	<div id="slideshow">
    <?php
      $list_banners = $db->select("
        SELECT *   
        FROM banners 
        WHERE published = 1  
        ORDER BY ABS(priority) ASC");
      //echo '<pre>'. print_r($list_banners, 1) .'</pre>'; //die(1);

      foreach ($list_banners as $key => $itemb) {
          $image_sm = '';
          if($itemb->image_sm)
              $image_sm = '<img src="images/home/slideshow/'. $itemb->image_sm.'" alt="'. $itemb->title .'" class="d-none d-md-block d-lg-none d-xl-none">';

          $image_xs = '';
          if($itemb->image_xs)
              $image_xs = '<img src="images/home/slideshow/'. $itemb->image_xs.'" alt="'. $itemb->title .'" class="d-block d-md-none d-lg-none d-xl-none">';

          echo '
            <div class="item">
                '. ($itemb->url?'<a href="'. $itemb->url .'" target="'. $itemb->url_target .'">':'') .'
                    <img src="images/home/slideshow/'. $itemb->image .'" alt="'. $itemb->title .'" class="d-none d-lg-block d-xl-block">
                  '. $image_sm . $image_xs .'
                '. ($itemb->url?'</a>':'') .'
            </div>';

      }
    ?>
  	</div>

    <div class="clearfix"></div>

    <div class="container-fluid" id="info-bloques">

      <div class="bloque">
        <div class="row p-0">
          <div class="col-md-6 back-gray d-flex">
            <div class="content ml-auto  align-self-center half-container left-site_container half-container_sm box-shadom-right">
              <h6 class="smalletter">Transforma tus casas de empeño</h6>
              <div class="info">
                <p class="anima" >Incrementa sustancialmente el rendimiento de tu negocio prendario con el sistema más completo y amigable.</p>
                <a href="contacto#divided-formulario" class="btn-info-orange btn ghostBtn mx-auto slide d-xl-block anima">Más información</a>
              </div>              
            </div>
          </div>
          <div class="col-md-6 p-0">
            <div class="parallaxParent" id="parallax1">
              <div class="image-parallax" style="background:url(images/home/image1.jpg)top center no-repeat;"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="bloque">
        <div class="row p-0 flex-row-reverse">
          <div class="col-md-6 back-orange d-flex">
            <div class="content mr-auto  align-self-center half-container right-site_container half-container_sm box-shadom-left">
              <h6 class="smalletter anima">Te ayudamos a que tu negocio prendario no se detenga</h6>
              <div class="info">
                <p class="anima" >Prendasys es un software escalable que se adapta a las necesidades de tus casas de empeño y cuenta con dos versiones distintas que se alinea a tu visión.</p>
                <a href="versiones" class="btn-info-gray btn ghostBtn mx-auto slide d-xl-block anima">Más información</a>
              </div>              
            </div>
          </div>
          <div class="col-md-6 p-0">
            <div class="parallaxParent" id="parallax2">
              <div class="image-parallax" style="background:url(images/home/image2.jpg)top center no-repeat;"></div>
            </div>
          </div>
        </div>
      </div>

      <div class="bloque">
        <div class="row p-0">
          <div class="col-md-6 back-gray d-flex">
            <div class="content ml-auto  align-self-center half-container left-site_container half-container_sm box-shadom-right">
              <h6 class="smalletter anima">Nos empeñamos en tu crecimiento</h6>
              <div class="info">
                <p class="anima" >Altamente especializado en casas de empeño para cubrir 100% las necesidades de administración y operación de tus casas de empeño.</p>
                <a href="porque-prendasys" class="btn-info-orange btn ghostBtn mx-auto slide d-xl-block anima">Más información</a>
              </div>              
            </div>
          </div>
          <div class="col-md-6 p-0">
            <div class="parallaxParent" id="parallax3">
              <div class="image-parallax" style="background:url(images/home/image3.jpg)top center no-repeat;"></div>
            </div>
          </div>
        </div>
      </div>

    </div>

    <div class="w-100 d-block position-relative mb-5" >	    
	    <div class="col-md-10 ventas-seccion offset-md-1">
	        <h3 class="text-center anima light title-ventas"><strong>Impulsa las ventas en tus sucursales y en línea</strong></h3>  
	    </div>

	    <div class="container p-0 ">
  			<div class="row">

	  			<div class="col-xl-3 col-lg-6">
	  				<div class="col-12 col-md-12 col-lg-9 d-flex justify-content-center" >
	  					<div class="center-ventas" >
			  				<div class="position-relative">
			  					<img src="images/home/icono1.png" class="img-responsive anima text-center" />
			  				</div>
			  				<div class="position-relative d-block center">
			  					<h3 class="title-tipov anima my-3" >Implementación ágil</h3>
			  					<p class="text-tipov anima" >La estrategia de implementación de Prendasys garantiza una instalación de manera eficaz para que tus casas de empeño operen de inmediato.</p>
			  				</div>
			  			</div>
	  				</div>
	  			</div>

	  			<div class="col-xl-3 col-lg-6">
	  				<div class="col-12 col-md-12 col-lg-9 d-flex justify-content-center" >
	  					<div class="center-ventas" >
			  				<div class="position-relative">
			  					<img src="images/home/icono2.png" class="img-responsive anima" />
			  				</div>
			  				<div class="position-relative d-block center">
			  					<h3 class="title-tipov anima my-3" >E-commerce</h3>
			  					<p class="text-tipov anima" >Implementa una estrategia omnicanal abriendo nuevos canales de venta. Conecta Prendasys a las mejores plataformas de ecommerce y vende en línea las 24 horas del día.</p>
			  				</div>
			  			</div>
	  				</div>
	  			</div>

	  			<div class="col-xl-3 col-lg-6">
	  				<div class="col-12 col-md-12 col-lg-9 d-flex justify-content-center" >
	  					<div class="center-ventas" >
			  				<div class="position-relative">
			  					<img src="images/home/icono3.png" class="img-responsive anima" />
			  				</div>
			  				<div class="position-relative d-block center">
			  					<h3 class="title-tipov anima my-3" >Altamente configurable</h3>
			  					<p class="text-tipov anima" >Configura Prendasys de acuerdo a las necesidades de tus sucursales y optimiza tu tiempo al eliminar actividades repetitivas.</p>
			  				</div>
			  			</div>
	  				</div>
	  			</div>

	  			<div class="col-xl-3 col-lg-6">
	  				<div class="col-12 col-md-12 col-lg-9 d-flex justify-content-center" >
	  					<div class="center-ventas" >
			  				<div class="position-relative">
			  					<img src="images/home/icono4.png" class="img-responsive anima" />
			  				</div>
			  				<div class="position-relative d-block center">
			  					<h3 class="title-tipov anima my-3" >Versiones escalables</h3>
			  					<p class="text-tipov anima" >Comienza tu proyecto con la versión que mejor se adecúe a las necesidades de tu empresa y crece la funcionalidad de tu sistema en el momento que lo requieras.</p>
			  				</div>
			  			</div>
	  				</div>
	  			</div>

        </div>
  		</div>
    </div>

	<div class="clearfix"></div>

	
    <div id="caracteristicas-block">
    	<div class="container d-md-flex justify-content-md-center p-0 ">  			
  			<div class="conmed p-0"><!--col-12 float-md-right col-md-6 col-lg-6  -->
  				<div class="caract-div-txt "><!--col-12 col-md-12 col-lg-10  float-md-right  -->
  					<div class="info text-center">
  						<!-- <h2 class="title-caracteristicas anima" >Características</h2>
  						<span class="line anima" ></span> -->
              <div class="row flex-row-reverse">                
                <div class="col-sm-6 align-self-center">
                  <div class="info animated" id="info-equipo" style="animation-delay:0.6180s;">
                    <h2 class="title-tipov anima" >Migrarte a Prendasys es tan fácil</h2>
                    <p class="text-tipov mt-2 anima" >Con sus procesos de carga automatizado, migra la información de tu sistema anterior a Prendasys en un par de minutos.</p>
                    <a href="implementaciones-agiles" class="btn-info-orange btn ghostBtn d-block mx-auto slide animated">Más información</a>                
                  </div>
                </div>
                <div class="col-sm-6 align-self-center">
                  <img src="images/home/equipo.png" alt="" class="img-fluid mx-auto my-3 d-block animated" id="img-equipo">
                </div>
              </div>  						
              
  					</div>
  				</div>
  			</div>
        <!--
  			<div class="col-12 col-md-6 col-lg-6 conmed float-md-left p-0">
  				<div class="center">
  					<a href="caracteristicas" class="btn btn-pestana anima">Ver + <i class="fa fa-chevron-right"></i></a>
  					<img src="images/home/caracteristicas.jpg" class="img-responsive w-100 anima" />
  				</div>
  			</div>
        -->
  		</div>
    </div>
    
    <div class="container-fluid py-5 aviso-bottom">
        <div class="container">
          <div class="col-md-10 offset-md-1 text-center">
            
            <h4 class="text-white my-3 light-txt anima"><strong>Prendasys</strong> es el sistema más completo para administrar tus Casas de Empeño. Contáctanos sin compromiso para darte más detalles.</h4>
            <a href="caracteristicas" class="btn btn-lg ghostBtn mx-auto anima">Regístrate aquí</a>
          </div>
        </div>
    </div>	

</section>
