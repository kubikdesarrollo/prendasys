<div class="navbarHeight"></div>

<!-- PRENDASYS - HOME -->

<section class="container-fluid" id="home-section">
  <!-- Para SEO -->

    <div id="header">
        <h1 class="sr-only">¿Por qué Prendasys?</h1>
        <h2 class="sr-only">Mejora tus destrezas para dirigir tu empresa. Recibe las novedades y presentaciones de <strong>nuestros webinars</strong></h2>
        <a href="#info-bloques" class="d-none d-xl-block" id="header-bg">
            <img src="images/implementaciones-agiles/header.jpg" alt="¿Porqué Prendasys?" class="img-fluid mx-auto d-block">
        </a>
        <a href="caracteristicas" class="d-none d-md-block d-lg-block d-xl-none">
            <img src="images/implementaciones-agiles/header-sm.jpg" class="img-fluid mx-auto" alt="¿Porqué Prendasys?">              
        </a>
        <a href="caracteristicas" class="d-block d-md-none d-lg-none d-xl-none">
            <img src="images/implementaciones-agiles/header-xs.jpg" class="img-fluid mx-auto" alt="¿Porqué Prendasys?">
        </a>
    </div>
    
    <div class="clearfix"></div>

    <div class="container-fluid" id="info-bloques">

        <div class="bloque">
            <div class="row p-0 flex-row-reverse">
                <div class="col-12 col-md-6 d-flex bloque b-der">
                    <div class="content mr-auto  align-self-center  right-site_container ">
                        <h6 class="smalletter anima">Implementación ágil</h6>
                        <div class="info1">
                            <p class="anima" >Sea cual sea el tamaño de tu negocio te aseguramos una <strong>instalación rápida</strong> para que tus casas de empeño sigan operando sin complicaciones.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax1">
                      <div class="image-parallax" style="background:url(images/implementaciones-agiles/image1.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0">
                <div class="col-12 col-md-6 d-flex bloque b-izq">
                    <div class="content ml-auto  align-self-center left-site_container ">
                        <h6 class="smalletter text-center text-md-right">¡Hazles más fácil su trabajo!</h6>
                        <div class="info2">
                            <p class="anima" >Bríndale a tu equipo una herramienta rápida de aprender a utilizar.<br><br> Al tener acceso a una herramienta <strong>intuitiva y amigable</strong>, tus colaboradores son más proactivos y se comprometen más con tu empresa.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax2">
                      <div class="image-parallax" style="background:url(images/implementaciones-agiles/image2.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0 flex-row-reverse">
                <div class="col-12 col-md-6 d-flex bloque b-der">
                    <div class="content mr-auto  align-self-center right-site_container ">
                        <h6 class="smalletter anima">Simplicidad a gran escala</h6>
                        <div class="info3">
                            <p class="anima" >Prendasys es un sistema creado para tomar mejores decisiones <br><br> Es tecnología diseñada para <strong>mejorar la productividad </strong>de todas las actividades de tu negocio prendario.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax3">
                      <div class="image-parallax" style="background:url(images/implementaciones-agiles/image3.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>
    

    </div>

	<div class="clearfix"></div>

    <div class="container-fluid orange-bg py-5 aviso-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1 text-center">            
                <h4 class="text-white my-3 light-txt anima">Siempre listos para <strong>ayudarte</strong></h4>
                <a href="contacto#divided-formulario" class="btn btn-lg ghostBtn mx-auto anima">Habla con un experto</a>
            </div>
        </div>
    </div>	

</section>
