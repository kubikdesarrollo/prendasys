<footer>	
	<div class="footercopy">
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <p class="legal">Prendasys &copy; <?php echo date("Y") ?> Sistema Especializado en Cadenas de Casas de Empeño</p>
                </div>
                <div class="col-lg-4">
                    <p class="legal-links">
                        <a href="contacto">Contacto</a> | <a href="pdf/aviso-privacidad.pdf" target="_blank">Aviso de Privacidad</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <h4 class="nav-footer-label">Software y Servicios</h4>
                    <a href="caracteristicas" class="f-link <?php if($section=='caracteristicas'){ echo("current"); }?>">Características</a>
                    <a href="versiones" class="f-link <?php if($section=='versiones'){ echo("current"); }?>">Versiones</a>
                    <a href="implementaciones-agiles" class="f-link <?php if($section=='implementaciones-agiles'){ echo("current"); }?>">Implementaciones Ágiles</a>
                    <a href="requerimientos-tecnicos" class="f-link <?php if($section=='requerimientos-tecnicos'){ echo("current"); }?>">Requerimientos Técnicos</a>
                    <a href="preguntas-frecuentes" class="f-link <?php if($section=='preguntas-frecuentes'){ echo("current"); }?>">Preguntas Frecuentes</a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <h4 class="nav-footer-label">Recursos</h4>
                    <a href="webinars" class="f-link <?php if($section=='webinars'){ echo("current"); }?>">Webinars</a>
                    <a href="soporte" class="f-link <?php if($section=='soporte'){ echo("current"); }?>">Soporte</a>
                    <a href="soporte-conexion-remota" class="f-link <?php if($section=='soporte-conexion-remota'){ echo("current"); }?>">Soporte Conexión Remota</a>
                    <h4 class="nav-footer-label">
                        <a href="porque-prendasys" class="f-link <?php if($section=='porque-prendasys'){ echo("current"); }?>" style="color:#fea301;">¿Por qué Prendasys?</a>
                    </h4>
                    <h4 class="nav-footer-label">
                        <a href="contacto" class="f-link <?php if($section=='contacto'){ echo("current"); }?>" style="color:#fea301;">Contacto</a>
                    </h4>
                </div>
                <div class="col-lg-3 off-sm"><!-- foo content --></div>
                <div class="col-lg-3 col-md-4 col-sm-12" id="final-col">
                    <a href="https://www.bisoft.mx/" target="_blank" id="bisoft-link">
                        <span style="margin:5px 5px 0 0">Un producto de</span>
                        <img src="images/bisoft.png" alt="Bisoft">
                    </a>
                    <a href="mailto:comercial@bisoft.com.mx" class="mail">comercial@bisoft.com.mx</a>
                    <a href="tel:6677156511" class="phone" target="_blank"><i class="icon"><img src="images/phone-icon.png"></i> 667 715 6511</a>
                    <ul id="redesFooter">
                        <li class="redesBtn">
                            <a href="https://www.facebook.com/Prendasys" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                            </a>
                        </li>
                        <!--               
                        <li class="redesBtn">
                            <a href="https://twitter.com/PrendaSys" target="_blank" class="redesIcon" id="twitterF" title="Síguenos en Twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>
                        -->
                        <li class="redesBtn">
                            <a href="https://www.linkedin.com/showcase/prendasys/?viewAsMember=true" target="_blank" class="redesIcon" id="linkedinF" title="Síguenos en Linked-in">
                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                            </a>
                        </li>
                                    
                    </ul>
                </div>
            </div>
        </div>
        <!--<a href="https://kubik.mx/" target="_blank" class="firmaKBI center-block my-4" title="Kubik Digital Agency"></a>-->
    </div>

    <div class="alert alert-dismissible fade" id="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Las <strong>cookies</strong> de este sitio web se usan para personalizar el contenido y los anuncios, ofrecer funciones de redes sociales y analizar el tráfico. <a href="#" data-dismiss="alert" class="alert-link orangeTxt" aria-label="close">Acepto</a>
    </div>

</footer>
