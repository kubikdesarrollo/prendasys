<header>
	<a id="top"></a>
	<nav id="topNav" class="navbar navbar-expand-lg navbar-light bg-light fixed-top">    
	    <div class="container">
	    	<a class="navbar-brand big mx-auto animated fadeIn delay450" href="./">
	    		<img src="images/logo.png" alt="Prendasys">
	    	</a>	    
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
		    	<div class="hamburger-menu"></div>
		    </button>			
		    <div class="navbar-collapse collapse">
		        <ul class="navbar-nav ml-auto">
		            <li class="dropdown">
						<a href="javascript:void(0);" class="subMenuBtn dropdown-toggle mainMenuBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span>Software y Servicios</span>
		                	<small class="tag orangeTxt off-sm">Adaptado a tu negocio</small>
		                </a>
						<ul class="dropdown-menu">
							<li><a href="caracteristicas" class="dropdown-item">Características</a></li>
							<li><a href="versiones" class="dropdown-item">Versiones</a></li>
							<li><a href="implementaciones-agiles" class="dropdown-item">Implementaciones Ágiles</a></li>
							<li><a href="requerimientos-tecnicos" class="dropdown-item">Requerimientos Técnicos</a></li>
							<li><a href="preguntas-frecuentes" class="dropdown-item">Preguntas Frecuentes</a></li>
						</ul>
					</li>
		        	<li class="dropdown">
						<a href="javascript:void(0);" class="subMenuBtn dropdown-toggle mainMenuBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span>Recursos</span>
		                	<small class="tag orangeTxt off-sm">Asesoría especializada</small>
		                </a>
						<ul class="dropdown-menu">
							<li><a href="webinars" class="dropdown-item">Webinars</a></li>
							<li><a href="soporte" class="dropdown-item">Soporte</a></li>
							<li><a href="soporte-conexion-remota" class="dropdown-item">Soporte Conexión Remota</a></li>
						</ul>
					</li>
		            <li class="nav-item">
		                <a class="mainMenuBtn <?php if($section=='porque-prendasys'){ echo("current"); }?>" href="porque-prendasys">
		                	<span>¿Por qué Prendasys?</span>
		                	<small class="tag orangeTxt off-sm">Un software especializado</small>
		            	</a>
		            </li>
		            <li class="nav-item">
		                <a class="mainMenuBtn <?php if($section=='contacto'){ echo("current"); }?>" href="contacto">
		                	<span>Contacto</span>
		                	<small class="tag orangeTxt off-sm">Con gusto te atendemos</small>
		            	</a>
		            </li>		            
		        </ul>	        
	    	</div>
	    </div>	    
	</nav>
	<!--<a href="#top" id="btn-to-top" class="off"><i class="fas fa-chevron-up"></i></a>-->
	<div class="base-whatsapp animated fadeInRightBig"><a href="https://bit.ly/2ExUuiK" id="whatsapp-btn" class="pulsar" target="_blank" title="Contáctanos vía Whatsapp"></a></div>
	<div class="specialbase-whatsapp animated fadeInUpBig"><a href="https://bit.ly/2ExUuiK" id="whatsapp-btn2" class="pulsar" target="_blank" title="Contáctanos vía Whatsapp"></a></div>
</header>