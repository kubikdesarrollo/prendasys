<style>
    section#gracias{min-height:480px;height:calc(100vh - 180px);padding:0;position:relative;}
    section#gracias .container{height:100%;}
    section#gracias .container .row{height:100%;}
    /** {line-height: 1.2;margin:0;}*/
    h1 {color:#555;font-size:2em;font-weight:400;}
    p {margin:0 auto;}
    @media only screen and (max-width: 280px) {
        h1 {font-size: 1.5em;margin: 0 0 0.3em;}
    }
    .navbar-height{height:100px;}
</style>

<div class="navbar-height"></div>
<section class="container-fluid" id="gracias">
    <div class="container" style="text-align:center;">
        <div class="row">
            <div class="col-md-10 offset-md-1 align-self-center">
                <h1>Gracias por tus comentarios</h1>
                <p>Nos comunicaremos contigo a la brevedad.</p>
                <br>
                <p><a href="./" class="btn">Volver</a></p>
            </div>
        </div>
    </div>
</section>
