<div class="navbarHeight"></div>

<!-- PRENDASYS - HOME -->

<section class="container-fluid" id="home-section">
  <!-- Para SEO -->
  	<h1 class="sr-only">Prendasys</h1>
  	<h2 class="sr-only">Merces una asesoría especializada</h2>
    <h3 class="sr-only">Estamos preparados para brindártela</h3>

	<div id="slideshow">
        <div class="item">
            <a href="mailto:prendasys@bisoft.com.mx" target="_blank">
                <img src="images/soporte-conexion-remota/banner-conexion-remota.jpg" class="img-fluid mx-auto d-none d-xl-block" alt="Prendasys">
                <img src="images/soporte-conexion-remota/banner-conexion-remota-sm.jpg" class="img-fluid mx-auto d-none d-md-block d-lg-block d-xl-none" alt="Prendasys">
                <img src="images/soporte-conexion-remota/banner-conexion-remota-xs.jpg" class="img-fluid mx-auto d-block d-md-none d-lg-none d-xl-none" alt="Prendasys">
            </a>
        </div>
  	</div>

    <div class="clearfix"></div>

    <div class="w-100 d-block position-relative back-blue2 p-0 mt-0 mb-3">
        <div class="container">
            <div class="secion-bienvenido back-blue">
                <h3 class="text2">Uno de nuestros especialistas tomará el control de tu<br> equipo <strong>con tu previa autorización</strong> y te brindará el soporte necesario.</h3>
                <!-- <span class="triangulo" ></span> -->
            </div>
        </div>
    </div>


    <div class="w-100 d-block position-relative p-0 mt-0 mb-0">
        <div class="container">
            <div class=" ">
            	<img src="images/soporte-conexion-remota/team-viewer.jpg" class="w-100" alt="Prendasys">
            	<div class="info-team mb-5" >
	                <h1 class="text2 mb-5">Sigue estos pasos para<strong> comenzar</strong></h1>
	                <h3 class="text2 d-block d-sm-flex d-lg-flex d-md-flex">
	                	<div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-md-left text-lg-left align-self-center" >
                            <img src="images/soporte-conexion-remota/uno.png" class="" alt="Prendasys">
                        </div>
	                	<div class="col-12 col-sm-10 col-md-10 col-lg-11 align-self-center">En caso de no tener el programa <strong>TeamViewer QS</strong> descargado e instalado, hazlo desde la siguiente liga: <a href="https://get.teamviewer.com/soportepl" target="_blank" id="teamviewer-link">Descargar TeamViewer</a></div>
	                </h3>
	                <h3 class="text2 d-block d-sm-flex d-lg-flex d-md-flex">
	                	<div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-md-left text-lg-left align-self-center" >
                            <img src="images/soporte-conexion-remota/dos.png" class="" alt="Prendasys">
                        </div>
	                	<div class="col-12 col-sm-10 col-md-10 col-lg-11 align-self-center" >Contacta a tu asesor y solicita el <strong>soporte vía remota</strong>.</div>
	                </h3>
	                <h3 class="text2 d-block d-sm-flex d-lg-flex d-md-flex">
	                	<div class="col-12 col-sm-2 col-md-2 col-lg-1 text-center text-md-left text-lg-left align-self-center" >
                            <img src="images/soporte-conexion-remota/tres.png" class="" alt="Prendasys">
                        </div>
	                	<div class="col-12 col-sm-10 col-md-10 col-lg-11 align-self-center" >Proporciona a tu asesor tu <strong>ID y contraseña</strong> para que su equipo pueda conectarse a tu computadora.</div>
	                </h3>
            	</div>

            </div>
        </div>
    </div>
   

	<div class="clearfix"></div>	
    
    <div class="container-fluid orange-bg py-5 aviso-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1 text-center">
                <div class="btn btn-lg ghostBtn mx-auto anima">¡Comencemos!</div>
            </div>
        </div>
    </div>	

    

</section>
