<style>
    ul#redesFooter {text-align: center !important;}
</style>

<footer>	
	<div class="footercopy">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <p class="legal text-center legal-links">Prendasys &copy; <?php echo date("Y") ?> Sistema especializado en cadenas de Casas de Empeño<br><a href="pdf/aviso-privacidad.pdf" target="_blank" class="d-block mt-2">Aviso de Privacidad</a></p>
                </div>
                <!--
                <div class="col-lg-4">
                    <p class="legal-links">
                        <a href="javascript:void(0);">Contacto</a> | <a href="javascript:void(0);">Aviso de Privacidad</a>
                    </p>
                </div>
                -->
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="container">
            <div class="row">
                <!--
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <h4 class="nav-footer-label">Software y Servicios</h4>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Características</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Versiones</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Implementaciones Ágiles</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Requerimientos Técnicos</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Preguntas Frecuentes</a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6">
                    <h4 class="nav-footer-label">Recursos</h4>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Webinars</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Soporte</a>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Soporte Conexión Remota</a>
                    <h4 class="nav-footer-label">¿Porqué Prendasys?</h4>
                    <a href="javascript:void(0);" class="f-link <?php if($section==''){ echo("current"); }?>">Beneficios</a>
                </div>
                <div class="col-lg-3 off-sm"></div>-->
                <div class="col-12 text-center" id="final-col">
                    <a href="https://www.bisoft.mx/" target="_blank" id="bisoft-link">
                        <span style="margin:5px 5px 0 0">un producto de:</span>
                        <img src="images/bisoft.png" alt="Bisoft">
                    </a>
                    <a href="mailto:comercial@bisoft.com.mx" class="mail">comercial@bisoft.com.mx</a>
                    <a href="tel:6677156511" class="phone" target="_blank"><i class="icon"><img src="images/phone-icon.png"></i> 667 715 6511</a>
                    <ul id="redesFooter">
                        <li class="redesBtn">
                            <a href="https://www.facebook.com/Prendasys" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                            </a>
                        </li>                
                        <li class="redesBtn">
                            <a href="https://twitter.com/PrendaSys" target="_blank" class="redesIcon" id="twitterF" title="Síguenos en Twitter">
                                <i class="fab fa-twitter" aria-hidden="true"></i>
                            </a>
                        </li>              
                    </ul>
                </div>
            </div>
        </div>
        <!--<a href="https://kubik.mx/" target="_blank" class="firmaKBI center-block my-4" title="Kubik Digital Agency"></a>-->
    </div>
    <!--
    <div class="alert alert-dismissible fade" id="alert">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        Las <strong>cookies</strong> de este sitio web se usan para personalizar el contenido y los anuncios, ofrecer funciones de redes sociales y analizar el tráfico. <a href="#" data-dismiss="alert" class="alert-link orangeTxt" aria-label="close">Acepto</a>
    </div>
    -->

</footer>
