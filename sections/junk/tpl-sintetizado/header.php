<style>
	@media screen and (max-width: 480px){
		.navbar-brand {right:initial !important;left:calc(50% - 115px);}
	}
</style>

<header>
	<a id="top"></a>
	<nav id="topNav" class="navbar navbar-expand-lg navbar-light bg-light fixed-top">    
	    <div class="container">
	    	<a class="navbar-brand big mx-auto d-block animated fadeIn delay450" href="#top"><img src="images/logo.png" alt="Prendasys"></a>
	    	<!--<a class="navbar-brand big mx-auto animated fadeIn delay450" href="#top"><img src="images/logo.png" alt="Prendasys"></a>
	    		    
		    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
		    	<div class="hamburger-menu"></div>
		    </button>
			-->	
			<!--
		    <div class="navbar-collapse collapse">
		        <ul class="navbar-nav ml-auto">
		            <li class="dropdown">
						<a href="javascript:void(0);" class="subMenuBtn dropdown-toggle mainMenuBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span>Software y Servicios</span>
		                	<small class="tag orangeTxt off-sm">Adaptado a tu negocio</small>
		                </a>
						<ul class="dropdown-menu">
							<li><a href="javascript:void(0);" class="dropdown-item">Características</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Versiones</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Implementaciones Ágiles</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Requerimientos Técnicos</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Preguntas Frecuentes</a></li>
						</ul>
					</li>
		        	<li class="dropdown">
						<a href="javascript:void(0);" class="subMenuBtn dropdown-toggle mainMenuBtn" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
							<span>Recursos</span>
		                	<small class="tag orangeTxt off-sm">Asesoría especializada</small>
		                </a>
						<ul class="dropdown-menu">
							<li><a href="javascript:void(0);" class="dropdown-item">Webinars</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Soporte</a></li>
							<li><a href="javascript:void(0);" class="dropdown-item">Soporte Conexión Remota</a></li>
						</ul>
					</li>
		            <li class="nav-item">
		                <a class="mainMenuBtn <?php if($section=='software-y-servicios'){ echo("current"); }?>" href="javascript:void(0);">
		                	<span>¿Porqué Prendasys?</span>
		                	<small class="tag orangeTxt off-sm">Un software especializado</small>
		            	</a>
		            </li>
		            <li class="nav-item">
		                <a class="mainMenuBtn <?php if($section=='software-y-servicios'){ echo("current"); }?>" href="#top">
		                	<span>Contacto</span>
		                	<small class="tag orangeTxt off-sm">Con gusto te atendemos</small>
		            	</a>
		            </li>		            
		        </ul>
		    	-->
		        <!--
		        <ul id="redesFooter" class="off-sm">
	                <li class="redesBtn">
	                    <a href="https://www.facebook.com/BSaludfarmacia" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
	                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
	                    </a>
	                </li>	                                                
	                <li class="redesBtn">
	                    <a href="" target="_blank" class="redesIcon" id="twitterF" title="Síguenos en Twitter">
	                        <i class="fab fa-twitter" aria-hidden="true"></i>
	                    </a>
	                </li>               
	                <li class="redesBtn">
	                    <a href="" target="_blank" class="redesIcon" id="instagramF" title="Síguenos en Instagram">
	                        <i class="fab fa-instagram" aria-hidden="true"></i>
	                    </a>
	                </li>
	            </ul>
	        	-->		        
	    	</div>
	    </div>	    
	</nav>
	<a href="#top" id="btn-to-top" class="off"><i class="fas fa-chevron-up"></i></a>
	<div class="base-whatsapp animated fadeInRightBig"><a href="https://bit.ly/2ExUuiK" id="whatsapp-btn" class="pulse" target="_blank" title="Contáctanos vía Whatsapp"></a></div>
</header>