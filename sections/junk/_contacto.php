<div class="navbarHeight"></div>

<section class="container-fluid p-0" id="contacto-section">

    <!-- Para SEO -->
	<h1 class="sr-only">Prendasys</h1>
	<h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>
	    

    <div id="header-contacto" class="parallaxParent">
        <div class="info-slide">
            <img src="images/contact/conexiones.png" class="animated fadeInRight" id="conexiones">
            <div class="container">
                <div class="back-blanco mb-2 text-center">                            
                    <h4 class="text-gray my-3 light-txt anima regis-text"> Registrarte podría ser el comienzo <br> de una productiva relación<br> </h4>
                    <a href="#formulario" class="btn btn-lg ghostBtn btn-info-orange mx-auto anima">Ir al formulario</a>
                </div>
                <div class="col-lg-8 offset-lg-2 mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <h4 class="title-contacto-lugar my-sm-3">México</h4>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="" class="data-text">
                                    <img src="images/contact/email.png" class="" alt="Prendasys" /> comercial<i class="fa fa-at"></i>bisoft.com.mx
                                </a>
                            </div>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="" class="data-text">
                                    <img src="images/contact/phone.png" class="" alt="Prendasys" /> +52 667 715 6511
                                </a>
                            </div>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="" class="data-text">
                                    <img src="images/contact/whattsap.png" class="" alt="Prendasys" /> +52 667 216 9428
                                </a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <h4 class="title-contacto-lugar my-sm-3">Latinoamérica</h4>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="mailto:comercial@bisoft.com.mx" target="_blank" class="data-text">
                                    <img src="images/contact/email.png" class="" alt="Prendasys" /> comercial<i class="fa fa-at"></i>bisoft.com.mx
                                </a>
                            </div>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="" class="data-text">
                                    <img src="images/contact/phone.png" class="" alt="Prendasys" /> +507 387 7036
                                </a>
                            </div>
                            <div class="w-100 d-block text-left pb-2">
                                <a href="" class="data-text">
                                    <img src="images/contact/whattsap.png" class="" alt="Prendasys" /> +507 6726 7509
                                </a>
                            </div>
                        </div> 
                    </div>    
                </div>            
                <ul id="redesContacto" class="top-prendasys">
                    <li class="redesBtn">
                        <a href="https://www.facebook.com/Prendasys" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                    </li>               
                    <li class="redesBtn">
                        <a href="https://www.linkedin.com/showcase/prendasys/?viewAsMember=true" target="_blank" class="redesIcon" id="linkedinF" title="Síguenos en Linked-in">
                            <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                        </a>
                    </li>                            
                </ul>
            </div>

        </div>
        <div class="image-parallax" style="background:url(images/contact/fondo-contacto.jpg)bottom left no-repeat;"></div>
	</div>


  <div class="w-100 d-block fondo-formulario">
      <div class="container">
          <div class="col-12">
            <div class="row flex-row-reverse" id="divided-formulario">
                <div class="col-md-12 col-lg-6">
                    <form action="contacto" id="formulario" class="pt-5 pb-5 w-100 d-block" method="post">
                        <div class="row">
                            <div class="form-group col-12 col-md-12">
                                <label class="text-form">Nombre y Apellido:</label>
                                <input type="text" class="form-control restrict1" placeholder="" name="nombre" minlength="3" maxlength="80" required>
                            </div>
                            <!-- <div class="form-group col-md-6">
                              <input type="text" class="form-control restrict1" placeholder="*Apellido" name="apellido" minlength="3" maxlength="80" required>
                            </div> -->
                            <div class="form-group col-md-6">
                                <label class="text-form">Correo:</label>
                                <input type="email" class="form-control" placeholder="" name="email" maxlength="80" required>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="text-form">Teléfono:</label>
                                <input type="text" class="form-control telefono" placeholder="" name="telefono" pattern=".{14,20}" maxlength="20" required>
                            </div>

                            <div class="form-group col-md-12">
                                <label class="text-form">Nombre de Casa de Empeño:</label>
                                <input type="text" class="form-control" placeholder="" name="farmacia" minlength="3" maxlength="80" required>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="text-form">País:</label>
                                <select class="minimal form-control sel-pais" name="pais" data-target="" required>
                                    <option value="">Selecciona un país</option>
                                    <option value="México">México</option>
                                    <option value="Panamá">Panamá</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Otro">Otro</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6 d-sel-estado">
                                <label class="text-form">Estado:</label>                                
                                <select class="minimal form-control sel-estado" name="estado" required>
                                    <option value="">Estado / Provincia</option>
                                </select>
                            </div>
                            <div class="form-group col-md-6">
                                <label class="text-form">Ciudad:</label>                                
                                <input type="text" class="form-control" placeholder="" name="ciudad" minlength="3" maxlength="80" required>
                            </div>
                            <div class="form-group col-md-12 mb-8px">
                                <label class="text-form">Comentario:</label>
                                <textarea class="form-control restrict3" placeholder="" name="comentarios" maxlength="720"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="checkbox">
                                    <label class="text-aviso text-form"><input type="checkbox" class="aviso" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Por favor acepta nuestros términos de privacidad')" required> Leí y acepto el tratamiento de mis datos personales de acuerdo al <a href="aviso-de-privacidad" class="link-aviso" target="_blank">AVISO DE PRIVACIDAD</a></label>
                                </div>
                                <div class="checkbox">
                                    <label class="text-aviso text-form"><input type="checkbox" name="boletin" value="1"> Me gustaría que me añadan a su lista de correo</label>
                                </div>
                            </div>
                            <div class="form-group col-md-6">
                                <div class="d-form-error"></div>
                                <div class="g-recaptcha" data-sitekey="6Lffk9EUAAAAAIYF7cP13o49L_OrV0TW8U15dhgL"></div>
                            </div>
                            <div class="form-group col-md-6 mb-0">
                                <button class="btn btn-lg send-btn" value="submit" type="submit" style="">Enviar</button>
                            </div>
                        </div>
                    </form>

                </div>
                <div class="col-md-12 col-lg-6 align-self-start">
                    <a id="trigger-img"></a>
                    <img src="images/contact/operadora.png" alt="Háblanos de tu empresa" class="d-block d-lg-none d-xl-none mx-auto img-fluid position-relative mt-3" id="">
                </div>
                <div id="operadora">
                    <img src="images/contact/operadora.png" alt="Háblanos de tu empresa" class="d-block" id="operadora-img">
                </div>

            </div>
          </div>
      </div>
  </div>

  <div class="clearfix" style="height:1px;"></div>

    <div id="caracteristicas-block">
        <div class="container d-md-flex justify-content-md-center p-0 ">            
            <div class="conmed p-0"><!--col-12 float-md-right col-md-6 col-lg-6  -->
                <div class="caract-div-txt "><!--col-12 col-md-12 col-lg-10  float-md-right  -->
                    <div class="info text-center">
                        <!-- <h2 class="title-caracteristicas anima" >Características</h2>
                        <span class="line anima" ></span> -->
                        <div class="row flex-row-reverse">                
                            <div class="col-md-6 align-self-center">
                                <img src="images/contact/crecimiento-vision-cut.png" alt="" class="img-fluid mx-auto my-3 d-block anima">
                            </div>
                            <div class="col-md-6 align-self-center">
                                <h4 class="text-gray my-3 light-txt anima">
                                    <strong class="mb-3 d-block">Prendasys es un producto de:</strong>
                                    <img src="images/contact/bisoft.png" class="mt-2 mb-3" alt="Prendasys" />
                                </h4>
                                <div class="w-100 d-block position-relative">
                                    <h4 class="text-gray info-bisoft my-lg-4 my-md-2 light-txt anima">
                                        <strong>Bisoft</strong> es una empresa desarrolladora de software especialmente en soluciones verticales. Nos impulsa el crecimiento de tu empresa. Como compañía estamos siempre preparados para mejorar la productividad de tu negocio.
                                        <br><br>
                                        Nuestros softwares son innovadores y se pueden adaptar a las necesidades de cualquier empresa; al tuyo también. Somos una familia con 3 décadas en el mercado mexicano y latinoamericano.
                                    </h4>                
                                </div>
                            </div>
                        </div>                        
              
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid pb-5 ">
      <div class="container">
        <div class="col-md-10 offset-md-1 text-center">		  

            <div class="clearfix"></div>

            <ul id="redesContacto">
                <li class="redesBtn">
                    <a href="https://www.facebook.com/BisoftLatam" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                        <i class="fab fa-facebook-f" aria-hidden="true"></i>
                    </a>
                </li>                 
                <li class="redesBtn">
                    <a href="https://twitter.com/BisoftMx" target="_blank" class="redesIcon" id="twitterF" title="Síguenos en Twitter">
                        <i class="fab fa-twitter" aria-hidden="true"></i>
                    </a>
                </li>
                <li class="redesBtn">
                    <a href="https://www.instagram.com/bisoft_latam/" target="_blank" class="redesIcon" id="instagramF" title="Síguenos en Instagram">
                        <i class="fab fa-instagram" aria-hidden="true"></i>
                    </a>
                </li>                
                <li class="redesBtn">
                    <a href="https://mx.linkedin.com/showcase/bisoft-latam/?viewAsMember=true" target="_blank" class="redesIcon" id="linkedinF" title="Síguenos en Linked-in">
                        <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                    </a>
                </li>                            
            </ul>
            
            <hr class="dashed clearfix">
            
            <a href="https://www.bisoft.mx/" target="_blank" class="btn btn-lg ghostBtn btn-info-orange mx-auto anima">Conócenos mejor</a>
        </div>
      </div>
  </div>


</section>
