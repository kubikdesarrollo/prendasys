
<div class="navbarHeight"></div>

<!-- PRENDASYS - HOME -->

<section class="container-fluid" id="home-section">
  <!-- Para SEO -->
  	<h1 class="sr-only">Prendasys</h1>
  	<h2 class="sr-only">Cuenta con nosotros</h2>

	<div id="slideshow">
    <!-- Empiezo en el 2 porque falta diseñar el No.1 -->
        <div class="item">
            <a href="mailto:prendasys@bisoft.com.mx" target="_blank">
                <img src="images/soporte/banner-soporte.jpg" class="img-fluid mx-auto d-none d-xl-block" alt="Prendasys">
                <img src="images/soporte/banner-soporte-sm.jpg" class="img-fluid mx-auto d-none d-md-block d-lg-block d-xl-none" alt="Prendasys">
                <img src="images/soporte/banner-soporte-xs.jpg" class="img-fluid mx-auto d-block d-md-none d-lg-none d-xl-none" alt="Prendasys">
            </a>
        </div>
  	</div>

    <div class="clearfix"></div>

    <div class="container-fluid" id="info-bloques">

        <div class="bloque">
            <div class="row p-0">
                <div class="col-md-6 back-gray d-flex justify-content-end b-izq">
                    <div class="content align-self-center half-container half-container_sm left-site_container">
                        <h6 class="smalletter">Estamos contigo hoy y mañana</h6>
                        <div class="info">
                            <p class="anima" >Somos un equipo que te ofrece una combinación perfecta de atención a cliente, especialización y conocimiento profundo en el producto.</p>
                        </div>                  
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax1">
                        <div class="image-parallax" style="background:url(images/soporte/soporte1.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0 flex-row-reverse">
                <div class="col-md-6 back-gray d-flex b-der">
                    <div class="content  align-self-center half-container half-container_sm right-site_container">
                        <h6 class="smalletter anima">Somos tu socio de negocio</h6>
                        <div class="info">
                            <p class="anima" >Te acompañamos en cada etapa del proyecto desde el análisis, implementación y transformación de tus casas de empeño para garantizar que obtengas el máximo provecho de Prendasys.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax2">
                        <div class="image-parallax" style="background:url(images/soporte/soporte2.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0">
                <div class="col-md-6 back-gray d-flex justify-content-end b-izq">
                    <div class="content align-self-center half-container half-container_sm left-site_container">
                        <h6 class="smalletter anima">Un solo equipo</h6>
                        <div class="info">
                            <p class="anima" >Aprender a usar Prendasys es muy fácil, es un sistema intuitivo que logra que tu equipo de trabajo lo domine rápidamente y así no dejar de enfocarse en tareas importantes como atender a tus clientes.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax3">
                        <div class="image-parallax" style="background:url(images/soporte/soporte3.jpg)top left no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0 flex-row-reverse">
                <div class="col-md-6 back-gray d-flex b-der">
                    <div class="content  align-self-center half-container half-container_sm right-site_container">
                        <h6 class="smalletter anima">Así es como te ayudamos</h6>
                        <div class="info">
                            <p class="anima" >Nos especializamos en trabajar como una extensión de tu equipo para garantizar que nuestro sistema tenga el efecto estratégico en tu negocio prendario.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax4">
                        <div class="image-parallax" style="background:url(images/soporte/soporte4.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="bloque">
            <div class="row p-0">
                <div class="col-md-6 back-gray d-flex justify-content-end b-izq">
                    <div class="content align-self-center half-container half-container_sm left-site_container">
                        <h6 class="smalletter anima">Especialistas trabajando a tu lado</h6>
                        <div class="info">
                            <p class="anima" >Conocemos los procesos de tu negocio prendario, sabemos cómo ayudarte a crecer.</p>
                        </div>              
                    </div>
                </div>
                <div class="col-md-6 p-0">
                    <div class="parallaxParent" id="parallax5">
                        <div class="image-parallax" style="background:url(images/soporte/soporte5.jpg)top center no-repeat;"></div>
                    </div>
                </div>
            </div>
        </div>

    </div>

   

	<div class="clearfix"></div>	
    
    <div class="container-fluid orange-bg py-5 aviso-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1 text-center">
            
                <h4 class="text-white my-3 light-txt anima">Tenemos las respuestas <br> que <strong>necesitas</strong></h4>
                <a href="caracteristicas" class="btn btn-lg ghostBtn mx-auto anima">Clic para conocerlas</a>
            </div>
        </div>
    </div>	

</section>
