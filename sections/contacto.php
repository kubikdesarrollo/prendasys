<?php
    $enviado1 = 0;
    $error1 = null;

    //echo '<pre>'.print_r($_REQUEST, 1).'</pre>';
    /*if(!empty($_REQUEST['name']) && $_REQUEST['name'] && !empty($_REQUEST['email']) && $_REQUEST['email'])
      require_once "sections/functions/enviar_contacto.php";*/

?>
<div class="navbarHeight"></div>

<section class="container-fluid p-0" id="contacto-section">

    <!-- Para SEO -->
	<h1 class="sr-only">Prendasys</h1>
	<h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>

    <div id="slideshow">
    <!-- Empiezo en el 2 porque falta diseñar el No.1 -->
        <div class="item">
            <a href="#divided-formulario" target="_blank">
                <img src="images/contact/header.jpg" class="img-fluid mx-auto d-none d-xl-block" alt="Prendasys Contacto">
                <img src="images/contact/header-sm.jpg" class="img-fluid mx-auto d-none d-md-block d-lg-block d-xl-none" alt="Prendasys Contacto">
                <img src="images/contact/header-xs.jpg" class="img-fluid mx-auto d-block d-md-none d-lg-none d-xl-none" alt="Prendasys Contacto">
            </a>
        </div>
    </div>
	    

    <div id="header-contacto" class="parallaxParent">
        <div class="info-slide">
            <div class="container">
                <div class="back-blanco mb-2 text-center">                            
                    <h4 class="text-gray my-3 light-txt anima regis-text"> Contáctanos por el medio de tu preferencia </h4>
                </div>
                <div class="col-lg-10 offset-lg-1 mb-4">
                    <div class="row">
                        <div class="col-md-6 d-block d-md-flex d-lg-flex ">
                            <div class="col-md-5 d-block text-center ">
                                <img src="images/contact/mexico.png" class="mt-2 mb-0 position-relative w-md-100" alt="Prendasys" />
                            </div>
                            <div class="col-md-7 p-0">
                                <h4 class="title-contacto-lugar my-sm-3">México</h4>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="mailto:comercial@bisoft.com.mx" target="_blank" class="data-text">
                                        <img src="images/contact/email.png" class="" alt="Prendasys" /> comercial<i class="fa fa-at"></i>bisoft.com.mx
                                    </a>
                                </div>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="tel:+526677156511" target="_blank" class="data-text">
                                        <img src="images/contact/phone.png" class="" alt="Prendasys" /> +52 667 715 6511
                                    </a>
                                </div>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="https://api.whatsapp.com/send?phone=526672169428&text=Hola!%20Me%20podrían%20dar%20más%20información%20de%20su%20producto?" target="_blank" class="data-text">
                                        <img src="images/contact/whattsap.png" class="" alt="Prendasys" /> +52 667 216 9428
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 d-block d-md-flex d-lg-flex ">
                            <div class="col-md-5 d-block text-center ">
                                <img src="images/contact/latinoamerica.png" class="mt-2 mb-0 position-relative w-md-100" alt="Prendasys" />
                            </div>
                            <div class="col-md-7 p-0">
                                <h4 class="title-contacto-lugar my-sm-3">Latinoamérica</h4>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="mailto:comercial@bisoft.com.pa" target="_blank" class="data-text">
                                        <img src="images/contact/email.png" class="" alt="Prendasys" /> comercial<i class="fa fa-at"></i>bisoft.com.pa
                                    </a>
                                </div>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="tel:+525073877036" target="_blank" class="data-text">
                                        <img src="images/contact/phone.png" class="" alt="Prendasys" /> +507 387 7036
                                    </a>
                                </div>
                                <div class="w-100 d-block text-center text-md-left pb-2">
                                    <a href="https://bit.ly/3ikcLSQ" target="_blank" class="data-text">
                                        <img src="images/contact/whattsap.png" class="" alt="Prendasys" /> +507 6726 7509
                                    </a>
                                </div>
                            </div>
                        </div> 
                    </div>    
                </div>            
                <ul id="redesContacto" class="top-prendasys">
                    <li class="redesBtn">
                        <a href="https://www.facebook.com/Prendasys" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                            <i class="fab fa-facebook-f" aria-hidden="true"></i>
                        </a>
                    </li>               
                    <li class="redesBtn">
                        <a href="https://www.linkedin.com/showcase/prendasys/?viewAsMember=true" target="_blank" class="redesIcon" id="linkedinF" title="Síguenos en Linked-in">
                            <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                        </a>
                    </li>                            
                </ul>
            </div>

        </div>
        <!-- <div class="image-parallax" style="background:url(images/contact/fondo-contacto.jpg)bottom left no-repeat;"></div> -->
	</div>


  <div class="w-100 d-block fondo-formulario">
      <div class="container">
          <div class="col-12">
            <div class="" id="divided-formulario"><?php /*row flex-row-reverse*/ ?>
                
                <h1 class="w-100 d-block position-relative text-center mt-5 whiteText TitleForm1" >Háblanos un poco de tu empresa</h1>
                <h2 class="w-100 d-block position-relative text-center mb-3 whiteText TitleForm2">y te compartiremos más información de nosotros</h2>



                <!--[if lte IE 8]>
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
                <![endif]-->
                <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
                <script>
                    hbspt.forms.create({
                    portalId: "7853198",
                    formId: "b7c8f795-a43a-4e14-a849-e0d209796993"
                });
                </script>

                <?php /*<form action="./contacto" id="formulario" class="pt-5 pb-5 w-100 " method="post">
                    <div class="d-md-flex d-lg-flex " >
                        <div class="col-md-6 col-lg-6">
                            <div class="row">
                                <div class="form-group col-12 col-md-12">
                                    <label class="text-form">Nombre y Apellido:</label>
                                    <input type="text" class="form-control restrict1" placeholder="" name="name" id="name" minlength="3" maxlength="80" required>
                                </div>
                                <!-- <div class="form-group col-md-6">
                                  <input type="text" class="form-control restrict1" placeholder="*Apellido" name="apellido" minlength="3" maxlength="80" required>
                                </div> -->
                                <div class="form-group col-md-6">
                                    <label class="text-form">Correo:</label>
                                    <input type="email" class="form-control" placeholder="" name="email" id="email" maxlength="80" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-form">Teléfono:</label>
                                    <input type="text" class="form-control telefono" placeholder="" name="phone" id="phone" pattern=".{14,20}" maxlength="20" required>
                                </div>

                                <div class="form-group col-md-12">
                                    <label class="text-form">Nombre de Casa de Empeño:</label>
                                    <input type="text" class="form-control" placeholder="" name="home" id="home" minlength="3" maxlength="80" required>
                                </div>
                                <div class="form-group col-md-12">
                                    <label class="text-form">País:</label>
                                    <select class="minimal form-control sel-pais" name="pais" data-target="" required>
                                        <option value="">Selecciona tu país</option>
                                        <option value="México">México</option>
                                        <option value="Panamá">Panamá</option>
                                        <option value="Argentina">Argentina</option>
                                        <option value="Otro">Otro</option>
                                    </select>
                                </div>
                                
                            </div>
                        </div>
                        <div class="col-md-6 col-lg-6 ">
                            <div class="row">
                                <div class="form-group col-md-6 d-sel-estado">
                                    <label class="text-form">Estado:</label>  
                                    <select class="minimal form-control sel-estado" name="state" id="state" required>
                                        <option value="">Estado / Provincia</option>
                                        <option value="Aguascalientes">Aguascalientes</option>
                                        <option value="Baja California">Baja California</option>
                                        <option value="Baja California Sur">Baja California Sur</option>
                                        <option value="Campeche">Campeche</option>
                                        <option value="Chiapas">Chiapas</option>
                                        <option value="Chihuahua">Chihuahua</option>
                                        <option value="Coahuila">Coahuila</option>
                                        <option value="Colima">Colima</option>
                                        <option value="Ciudad de México">Ciudad de México</option>
                                        <option value="Durango">Durango</option>
                                        <option value="Guanajuato">Guanajuato</option>
                                        <option value="Guerrero">Guerrero</option>
                                        <option value="Hidalgo">Hidalgo</option>
                                        <option value="Jalisco">Jalisco</option>
                                        <option value="Estado de México">Estado de México</option>
                                        <option value="Michoacán">Michoacán</option>
                                        <option value="Morelos">Morelos</option>
                                        <option value="Nayarit">Nayarit</option>
                                        <option value="Nuevo León">Nuevo León</option>
                                        <option value="Oaxaca">Oaxaca</option>
                                        <option value="Puebla">Puebla</option>
                                        <option value="Querétaro">Querétaro</option>
                                        <option value="Quintana Roo">Quintana Roo</option>
                                        <option value="San Luis Potosí">San Luis Potosí</option>
                                        <option value="Sinaloa">Sinaloa</option>
                                        <option value="Sonora">Sonora</option>
                                        <option value="Tabasco">Tabasco</option>
                                        <option value="Tamaulipas">Tamaulipas</option>
                                        <option value="Tlaxcala">Tlaxcala</option>
                                        <option value="Veracruz">Veracruz</option>
                                        <option value="Yucatán">Yucatán</option>
                                        <option value="Zacatecas">Zacatecas</option>
                                        <option value="Otro">Otro</option>
                                     </select>
                                     <div class="invalid-feedback">Debes seleccionar tu estado</div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="text-form">Ciudad:</label>                                
                                    <input type="text" class="form-control" placeholder="" name="city" id="city" minlength="3" maxlength="80" required>
                                </div>
                                <div class="form-group col-md-12 mb-8px">
                                    <label class="text-form">Comentario:</label>
                                    <textarea class="form-control restrict3" placeholder="" name="message" id="message" maxlength="720"></textarea>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="checkbox">
                                        <label class="text-aviso text-form"><input type="checkbox" class="aviso" onchange="this.setCustomValidity('')" oninvalid="this.setCustomValidity('Por favor acepta nuestros términos de privacidad')" required> Leí y acepto el tratamiento de mis datos personales de acuerdo al <a href="pdf/aviso-privacidad.pdf" class="link-aviso" target="_blank">aviso de privacidad</a></label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="text-aviso text-form"><input type="checkbox" name="boletin" value="1"> Me gustaría que me añadan a su lista de correo</label>
                                    </div>
                                </div>
                                
                            </div>
                           <!--  <a id="trigger-img"></a>
                            <img src="images/contact/operadora.png" alt="Háblanos de tu empresa" class="d-block d-lg-none d-xl-none mx-auto img-fluid position-relative mt-3" id=""> -->
                        </div>
                    </div>
                    <div class="col-12 d-md-flex d-lg-flex " >
                        <div class="form-group col-md-6">
                            <div class="recaptcha-holder"></div>
                        </div>
                        <div class="form-group col-md-6 mb-0">
                            <button class="btn btn-lg send-btn" value="submit" type="submit" style="">Enviar</button>
                        </div>  
                    </div>
                </form>
                <!-- <div id="operadora">
                    <img src="images/contact/operadora.png" alt="Háblanos de tu empresa" class="d-block" id="operadora-img">
                </div> --> */ ?>
                <div class="clearfix" style="height:3em;"></div>
            </div>
          </div>
      </div>
  </div>

  <div class="clearfix" style="height:1px;"></div>

    <div id="caracteristicas-block" class="parallaxParent" >
        <div class="container d-md-flex justify-content-md-center p-0 ">            
            <div class="conmed p-0"><!--col-12 float-md-right col-md-6 col-lg-6  -->
                <div class="caract-div-txt d-flex"><!--col-12 col-md-12 col-lg-10  float-md-right  -->
                    <div class="info text-center d-flex" style="z-index: 1;">
                        <!-- <h2 class="title-caracteristicas anima" >Características</h2>
                        <span class="line anima" ></span> -->
                        <div class="row flex-row-reverse">                
                            <div class="col-md-6 align-self-center">
                                <!-- <img src="images/contact/crecimiento-vision-cut.png" alt="" class="img-fluid mx-auto my-3 d-block anima"> -->
                            </div>
                            <div class="col-md-6 align-self-center">
                                <h4 class="text-gray my-0 light-txt anima">
                                    <strong class="mb-3 d-block">Prendasys es un producto de</strong>
                                    <img src="images/contact/bisoft.png" class="mt-2 mb-0" alt="Prendasys" />
                                </h4>
                                <div class="w-100 d-block position-relative">
                                    <h4 class="text-gray info-bisoft my-lg-2 my-md-2 light-txt anima">
                                        <strong>Bisoft</strong> es una empresa desarrolladora de software especialmente en soluciones verticales. Como compañía estamos siempre preparados para mejorar la productividad de tu negocio.<br><br>Nuestros sistemas son innovadores y se pueden adaptar a las necesidades de cualquier empresa; nos impulsa tu crecimiento.<br><br>Somos una familia con 3 décadas en el mercado mexicano y latinoamericano.
                                    </h4> 

                                    <ul id="redesContacto">
                                        <li class="redesBtn">
                                            <a href="https://www.facebook.com/BisoftLatam" target="_blank" class="redesIcon" id="facebookF" title="Síguenos en Facebook">
                                                <i class="fab fa-facebook-f" aria-hidden="true"></i>
                                            </a>
                                        </li>                 
                                        <li class="redesBtn">
                                            <a href="https://twitter.com/BisoftLatam" target="_blank" class="redesIcon" id="twitterF" title="Síguenos en Twitter">
                                                <i class="fab fa-twitter" aria-hidden="true"></i>
                                            </a>
                                        </li>
                                        <li class="redesBtn">
                                            <a href="https://www.instagram.com/bisoft_latam/" target="_blank" class="redesIcon" id="instagramF" title="Síguenos en Instagram">
                                                <i class="fab fa-instagram" aria-hidden="true"></i>
                                            </a>
                                        </li>                
                                        <li class="redesBtn">
                                            <a href="https://mx.linkedin.com/showcase/bisoft-latam/?viewAsMember=true" target="_blank" class="redesIcon" id="linkedinF" title="Síguenos en Linked-in">
                                                <i class="fab fa-linkedin-in" aria-hidden="true"></i>
                                            </a>
                                        </li>                            
                                    </ul> 
            
                                    <a href="https://www.bisoft.mx/" target="_blank" class="btn btn-lg ghostBtn btn-info-orange mx-auto anima mb-5 ">Conócenos mejor</a>
                                </div>
                            </div>
                        </div>                        
              
                    </div>
                </div>
            </div>
        </div>
        <div class="image-parallax" style="background:url(images/contact/contacto-25ago2020.png)bottom no-repeat;"></div>
    </div>


</section>
<script>
   var renderGoogleInvisibleRecaptcha = function() {
       for (var i = 0; i < document.forms.length; ++i) {
           var form = document.forms[i];
           var holder = form.querySelector('.recaptcha-holder');
           if (null === holder)
               continue;
           (function(frm){
               var holderId = grecaptcha.render(holder,{
                       'sitekey': '6LfpG6YZAAAAAPef90xYhUtTki-A0jsK3ZU--WQ7',
                       'size': 'invisible',
                       'badge' : 'bottomright', // possible values: bottomright, bottomleft, inline
                       'callback' : function (recaptchaToken) {
                           HTMLFormElement.prototype.submit.call(frm);
                       }
                   });
               frm.onsubmit = function (evt){
                   evt.preventDefault();
                   grecaptcha.execute(holderId);
               };
           })(form);
       }
   };

//console.log('p1');
</script>
<script src="//www.google.com/recaptcha/api.js?onload=renderGoogleInvisibleRecaptcha&render=explicit" async defer></script>