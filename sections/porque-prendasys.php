<div class="navbarHeight"></div>

<section id="porque-prendasys-section" class="container-fluid p 0">
	
	<div id="header">
      <!-- <a href="#beneficios" class="d-none d-lg-block d-xl-block" id="header-bg">
      	<div class="header-info animated fadeInDown">
      		<h1 class="sr-only">¿Por qué <strong><span class="orange-txt">Prenda</span>sys</strong>?</h1>
      		<h2 class="anima text-center">Conecta todas las áreas y funciones de tus casas de empeño en un solo sistema y logra la <strong>excelencia operativa</strong>.</h2>
  			
  			<div class="btn ghostBtn">Ver Beneficios<br><i class="fa fa-chevron-down animated bounce infinite"></i></div>
      	</div>
      	<img src="images/porque-prendasys/pantalla.png" alt="¿Porqué Prendasys?" class="header-element animated fadeInLeftBig" style="animation-delay: 1s, 4ms">
      </a>
      <a href="#beneficios" class="d-block d-lg-none d-xl-none" id="header-bg-sm">
      	<img src="images/porque-prendasys/header-sm.jpg" alt="¿Porqué Prendasys?" class="d-block img-fluid mx-auto">
      </a> -->
      <h1 class="sr-only">¿Por qué Prendasys?</h1>
      <h2 class="sr-only">Conecta todas las áreas y funciones de tus casas de empeño en un solo sistema y logra la excelencia operativa</h2>
        <a href="#beneficios" class="d-none d-xl-block" id="header-bg">
            <!-- <img src="images/porque-prendasys/pantalla.png" alt="¿Porqué Prendasys?" class="img-fluid mx-auto d-block header-element animated fadeInLeftBig"> -->
            <img src="images/porque-prendasys/header.jpg" alt="¿Porqué Prendasys?" class="img-fluid mx-auto d-block">
        </a>
        <a href="caracteristicas" class="d-none d-md-block d-lg-block d-xl-none">
            <img src="images/porque-prendasys/header-sm.jpg" class="img-fluid mx-auto" alt="¿Porqué Prendasys?">              
        </a>
        <a href="caracteristicas" class="d-block d-md-none d-lg-none d-xl-none">
            <img src="images/porque-prendasys/header-xs.jpg" class="img-fluid mx-auto" alt="¿Porqué Prendasys?">
        </a>
    </div>

    <div class="container-fluid mt-3" id="beneficios">
    	<div class="container">
    		<div class="col-md-8 offset-md-2">
    			<h3 class="text-center main-title">¿Por qué <strong><span class="orange-txt">Prenda</span>sys</strong>?</h3>
    			<h3 class="text-center second-title"><strong>Prendasys</strong> es un software especializado en negocios prendarios que te brinda la flexibilidad de ajustarlo a tus necesidades</h3>
    		</div>
    	</div>
    	<div id="beneficios-container" class="container-fluid p-0 mt-5">
    		<div class="container">
    			<div class="concepto" id="trigger-roi">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-roi-6meses.png" alt="ROI 6 Meses" class="img-fluid mx-auto d-block animate-left animated" id="roi-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">ROI 6 Meses</h4>
    						<p class="texto texto-right" id="roi-text">Recupera tu inversión en <strong>6 meses</strong> tras la implementación de Prendasys.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-prem">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-presta-mejor.png" alt="Presta Mejor" class="img-fluid mx-auto d-block animate-right animated" id="prem-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Presta Mejor</h4>
    						<p class="texto texto-left" id="prem-text">Mejora hasta en un <strong>30%</strong> tus valuaciones y presta mejor.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-ant">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-robo-hormiga.png" alt="Menos robo hormiga" class="img-fluid mx-auto d-block animate-left animated" id="ant-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Menos robo hormiga</h4>
    						<p class="texto texto-right" id="ant-text">Olvídate de los inventarios viejos. Tus clientes recuperarán más prendas y reducirás el riesgo de <strong>extravíos</strong>.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-canv">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-canales-venta.png" alt="Más canales de venta" class="img-fluid mx-auto d-block animate-right animated" id="canv-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Más canales de venta</h4>
    						<p class="texto texto-left" id="canv-text">Prendasys es compatible con diversas <strong>plataformas de e-commerce</strong>. Dale a tu negocio prendario la oportunidad de ser omnicanal.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-cien">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-100-especializado.png" alt="100% especializado" class="img-fluid mx-auto d-block animate-left animated" id="cien-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">100% especializado</h4>
    						<p class="texto texto-right" id="cien-text">El más completo y único sistema para la administración y operación de <strong>casas de empeño</strong>. Olvídate de desarrollos adicionales.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-act">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-actualizaciones.png" alt="Actualizaciones" class="img-fluid mx-auto d-block animate-right animated" id="act-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Actualizaciones</h4>
    						<p class="texto texto-left" id="act-text"><strong>Prendasys</strong> es un software en constante innovación que brinda a sus clientes varias actualizaciones al año.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-adu">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icono-administracion-unica.png" alt="Administración única" class="img-fluid mx-auto d-block animate-left animated" id="adu-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Administración única</h4>
    						<p class="texto texto-right" id="adu-text"><strong>Administra tus catálogos</strong> y reglas de operación desde un punto y compártelos en todas tus sucursales.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-infc">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-informacion-centralizada.png" alt="Información centralizada" class="img-fluid mx-auto d-block animate-right animated" id="infc-img">
	    					</div>
	    				</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Información centralizada</h4>
    						<p class="texto texto-left" id="infc-text"><strong>Centraliza tu información</strong> para obtener una vista precisa de un extremo a otro. Al actualizar tu información podrás detectar riesgos y atenderlos a tiempo.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-optp">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-optimizacion-procesos.png" alt="Optimización de procesos" class="img-fluid mx-auto d-block animate-left animated" id="optp-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Optimización de procesos</h4>
    						<p class="texto texto-right" id="optp-text">Conecta los departamentos claves de tu operación y <strong>reduce errores</strong> generados por los procesos manuales.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-seg">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-seguridad.png" alt="Más seguridad" class="img-fluid mx-auto d-block animate-right animated" id="seg-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Más seguridad</h4>
    						<p class="texto texto-left" id="seg-text">Crea roles personalizados para controlar los accesos, resguardar tu información y mantener tus <strong>datos seguros</strong>.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-ver2">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-2-versiones.png" alt="2 versiones" class="img-fluid mx-auto d-block animate-left animated" id="ver2-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">2 versiones</h4>
    						<p class="texto texto-right" id="ver2-text">Cuenta con dos versiones adaptables a tu negocio y configurable para mejorar la productividad: <strong>Full o Standard</strong></p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-anda">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-analitica-datos.png" alt="Analítica de datos " class="img-fluid mx-auto d-block animate-right animated" id="anda-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Analítica de datos</h4>
    						<p class="texto texto-left" id="anda-text">Dale poder a tus datos con la <strong>integración de plataformas</strong> de analíticas de datos.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-impa">
    				<div class="row">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-implementacion-agil.png" alt="Implementación ágil" class="img-fluid mx-auto d-block animate-left animated" id="impa-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Implementación ágil</h4>
    						<p class="texto texto-right" id="impa-text">Consigue la misma experiencia de usuario implementando Prendasys en la nube, en tu infraestructura o de <strong>forma híbrida</strong>.</p>
    					</div>
    				</div>
    			</div>
    			<div class="concepto" id="trigger-msinc">
    				<div class="row flex-row-reverse">
    					<div class="col-md-5 align-self-center">
    						<div class="icon-vessel">
    							<img src="images/porque-prendasys/icon-motor-sincronizacion.png" alt="Motor de sincronización" class="img-fluid mx-auto d-block animate-right animated" id="msinc-img">
    						</div>
    					</div>
    					<div class="col-md-7 align-self-center">
    						<h4 class="sr-only">Motor de sincronización</h4>
    						<p class="texto texto-left" id="msinc-text">Prendasys cuenta con su propio motor de sincronización que minimiza <strong>hasta en un 80%</strong> el esfuerzo del área de sistemas.</p>
    					</div>
    				</div>
    			</div>
    		</div>
    	</div>
    	
    	
    </div>
    <div class="container-fluid pale-peach-bg py-5 aviso-bottom">
	    <div class="container">
	      <div class="col-md-10 offset-md-1 text-center">		        
	        <h4 class="text-white my-3 light-txt anima">
		        Crece tu negocio prendario<br>
		        con un <strong>software especializado</strong>
	        </h4>
	        <a href="contacto#divided-formulario" class="btn btn-lg ghostBtn mx-auto anima">Habla con un especialista</a>
	      </div>
	    </div>
	</div>

</section>
