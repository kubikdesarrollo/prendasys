<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
error_reporting(0);

function get_ip_address() {
    // check for shared internet/ISP IP
    if (!empty($_SERVER['HTTP_CLIENT_IP']) && filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)) {
        return $_SERVER['HTTP_CLIENT_IP'];
    }

    // check for IPs passing through proxies
    if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
        // check if multiple ips exist in var
        if (strpos($_SERVER['HTTP_X_FORWARDED_FOR'], ',') !== false) {
            $iplist = explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
            foreach ($iplist as $ip) {
                if (filter_var($ip, FILTER_VALIDATE_IP))
                    return $ip;
            }
        } else {
            if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP))
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
        }
    }
    if (!empty($_SERVER['HTTP_X_FORWARDED']) && filter_var($_SERVER['HTTP_X_FORWARDED'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_X_FORWARDED'];
    if (!empty($_SERVER['HTTP_X_CLUSTER_CLIENT_IP']) && filter_var($_SERVER['HTTP_X_CLUSTER_CLIENT_IP'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_X_CLUSTER_CLIENT_IP'];
    if (!empty($_SERVER['HTTP_FORWARDED_FOR']) && filter_var($_SERVER['HTTP_FORWARDED_FOR'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_FORWARDED_FOR'];
    if (!empty($_SERVER['HTTP_FORWARDED']) && filter_var($_SERVER['HTTP_FORWARDED'], FILTER_VALIDATE_IP))
        return $_SERVER['HTTP_FORWARDED'];

    // return unreliable ip since all else failed
    return $_SERVER['REMOTE_ADDR'];
}

//echo '<pre>'.print_r($_POST, 1).'</pre>'; die(1);

//$remitente = 'contacto@gcc.com.mx' ;
$name = empty($_POST['name']) ? '' : strip_tags( trim($_POST['name']) );
$phone = empty($_POST['phone']) ? '' : strip_tags( trim($_POST['phone']) );
$email = empty($_POST['email']) ? '' : strip_tags( trim($_POST['email']) );
$home = empty($_POST['home']) ? '' : strip_tags( trim($_POST['home']) );
$pais = empty($_POST['pais']) ? '' : strip_tags( trim($_POST['pais']) );
$state = empty($_POST['state']) ? '' : strip_tags( trim($_POST['state']) );
$city = empty($_POST['city']) ? '' : strip_tags( trim($_POST['city']) );
$message = empty($_POST['message']) ? '' : strip_tags( trim($_POST['message']) );
$bulletin = empty($_POST['boletin']) ? 'No': 'Si';

if($name && $email ){
    require_once('recaptchalib.php');
    $reCaptcha = new ReCaptcha("6LfpG6YZAAAAAM7CTRH809u6Mq36CH9Hh3H6_d_Q");
    if ($_POST["g-recaptcha-response"]) {
        $resp = $reCaptcha->verifyResponse(
            $_SERVER["REMOTE_ADDR"],
            $_POST["g-recaptcha-response"]
        );
    }

    if ($resp != null ) { //&& $resp->success

        $ip = get_ip_address();
        //guardamos en db
        $conn = new mysqli('internal-db.s229004.gridserver.com', 'db229004_prenda', 'a#_c7FQ1g!a', 'db229004_prendasys');

        $origin = 12;
        if(!empty($_SESSION['origen']) && $_SESSION['origen'])
            $origin = $_SESSION['origen']*1;

        $stmt = $conn->prepare("INSERT INTO `contact`(`id`, `name`, `phone`, `email`, `home`, `country`, `state`, `city`, `comments`, `bulletin`, `ip`, `origin`) VALUES (NULL,?,?,?,?,?,?,?,?,?,?,?)"); //, `date`, `origin` //,NULL,?,?
        $stmt->bind_param("ssssssssssi", utf8_decode($name), $phone, utf8_decode($email), utf8_decode($home), utf8_decode($pais), utf8_decode($state), utf8_decode($city), utf8_decode($message), utf8_decode($bulletin), $ip, $origin); // //si
        $stmt->execute();
        //$stmt->execute();
        //echo '<pre>'. print_r($stmt->error,1) .'</pre>';

        $stmt->close();
        $conn->close();

        /*$mensaje = '<div style="color: #242424; font-size:18px;">
            Contacto desde p&aacute;gina web '. $domain .'</div><br />
            <strong>Nombre:</strong> '.$name.'<br />
            <strong>Teléfono:</strong> '.$phone.'<br />
            <strong>E-mail:</strong> '.$email.'<br />
            <strong>Nombre de Casa de Empeño:</strong> '.$home.'<br />
            <strong>País:</strong> '.$pais.'<br />
            <strong>Estado:</strong> '.$state.'<br />
            <strong>Ciudad:</strong> '.$city.'<br />
            <strong>Boletin:</strong> '.$bulletin.'<br />
            <strong>Comentarios:</strong><br/>'.$message;

        //$destino = 'miguel@kubik.mx';
        $destino = 'comercial@bisoft.com.mx, zaidae@bisoft.com.mx, helenp@bisoft.com.mx, jesusivanh@bisoft.com.mx, georginad@comercial.com.mx, miguel@kubik.mx'; */

        /*$encabezados  = 'MIME-Version: 1.0' . "\r\n";
        $encabezados .= 'Content-type: text/html; charset=utf8' . "\r\n";
        //$encabezados .= 'Cc: coach.pardave@gmail.com ' . "\r\n";
        //$encabezados .= 'Bcc: miguel@kubik.mx' . "\r\n";
        $encabezados .= 'From: smtp@distritoaguacate.com'. "\r\n";
        //$encabezados = "From: $remitente\nReply-To: $remitente\nContent-Type: text/html;" ;
        if(mail($destino , $asunto, $mensaje, $encabezados) ){*/

        //Enviar correo de bienvenida
        /*require("vendor/phpmailer/phpmailer/src/PHPMailer.php");
        require("vendor/phpmailer/phpmailer/src/SMTP.php");
        require("vendor/phpmailer/phpmailer/src/Exception.php");
        include 'inc/class.email.php'; 

        $comunica = new EnvioEmails; 

        if( $comunica->enviar($destino,'', 'Contacto - Prendasys - '. $email, utf8_decode($mensaje),'smtp@k-i.co','Prendasys') ){ //*/

            $enviado1 = 1;
            echo '<script >window.location="gracias-contacto";</script>';
            die(1);
        /*}else{
            $error1="Error al enviar el correo. Vuelva a intentarlo.";
        }*/
    } else {
        $error1="Validaci&oacute;n incompleta. Vuelva a intentarlo.";
    }
}