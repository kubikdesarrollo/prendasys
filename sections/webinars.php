<div class="navbarHeight"></div>

<section id="webinars-section" class="container-fluid p 0">
	
	<div id="header">
      	<h1 class="sr-only">Webinars</h1>
      	<h2 class="sr-only">Impulsa tus ventas con nuestros <strong>webinars gratuitos</strong></h2>
        <a href="contacto#divided-formulario" class="d-none d-xl-block">
           	<img src="images/webinars/header.jpg" class="img-fluid mx-auto" alt="Webinars">
        </a>
        <a href="contacto#divided-formulario" class="d-none d-md-block d-lg-block d-xl-none">
            <img src="images/webinars/header-sm.jpg" class="img-fluid mx-auto" alt="Webinars">              
        </a>
        <a href="contacto#divided-formulario" class="d-block d-md-none d-lg-none d-xl-none">
            <img src="images/webinars/header-xs.jpg" class="img-fluid mx-auto" alt="Webinars">
        </a>
    </div>
    <div id="comodidad">
        <a href="contacto#divided-formulario" class="d-block parallaxParent" id="parallax1">
            <div class="textbanner1 animated" id="textbanner1">
                <div class="container">
                    <div class="row flex-row-reverse">
                        <div class="col-lg-6 align-self-center">
                            <div class="half-container right-site_container texto"><!--  half-container_sm -->
                                <p>Desde la comodidad del escritorio de tu casa u oficina conoce las novedades y consejos de la <strong>industria prendaria</strong> impartidos por <strong>especialistas</strong> en el tema.</p>
                                <div class="btn ghostBtn">Únete aquí a nuestro equipo</div>
                            </div>
                        </div>
                        <div class="col-lg-6"></div>                        
                    </div>
                </div>
            </div>
            <div class="image-parallax d-none d-lg-block d-xl-block ">
                <img src="images/webinars/image1.jpg" alt="comodidad" class="">
            </div>
            <img src="images/webinars/image1-sm.jpg" alt="comodidad" class="img-fluid mx-auto d-block d-lg-none d-xl-none">
        </a>
    </div>
    <div id="mejora">
        <a href="contacto#divided-formulario" class="d-block parallaxParent" id="parallax2">
            <div class="textbanner2 animated" id="textbanner2">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 align-self-center texto">
                            <div class="half-container left-site_container"><!--  half-container_sm -->
                                <p>Mejora tus destrezas para dirigir tu empresa. Recibe las novedades y presentaciones de <strong>nuestros webinars</strong>.</p>
                                <div class="btn ghostBtn">Haz brillar tu negocio prendario</div>
                            </div>
                        </div>
                        <div class="col-lg-6"></div>                        
                    </div>
                </div>
            </div>
            <div class="image-parallax d-none d-lg-block d-xl-block ">
                <img src="images/webinars/image2.jpg" alt="mejora" class="">
            </div>
            <img src="images/webinars/image2-sm.jpg" alt="mejora" class="img-fluid mx-auto d-block d-lg-none d-xl-none">
        </a>
    </div>

    <div class="clearfix"></div>

    <div class="container-fluid orange-bg py-5 aviso-bottom">
        <div class="container">
            <div class="col-md-10 offset-md-1 text-center">            
                <h4 class="text-white my-3 light-txt anima">Crece tu negocio prendario con un <strong>software especializado</strong></h4>
                <a href="https://bit.ly/2ExUuiK" target="_blank" class="btn btn-lg ghostBtn mx-auto anima">Habla con un especialista</a>
            </div>
        </div>
    </div>

</section>
