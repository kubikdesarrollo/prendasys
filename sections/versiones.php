<div class="navbarHeight"></div>

<section class="container-fluid p-0" id="versiones-section">

    <!-- PRENDASYS - VERSIONES -->
    <div class="container-fluid" id="home-section">
    <!-- Para SEO -->
        <h1 class="sr-only">Prendasys</h1>
        <h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>
         
        <div id="slideshow">
            <div class="item">
                <a href="contacto#divided-formulario" class="slide">
                    <img src="images/versiones/header.jpg" class="img-fluid mx-auto d-none d-xl-block" alt="Prendasys">
                    <img src="images/versiones/header-sm.jpg" class="img-fluid mx-auto d-none d-md-block d-lg-block d-xl-none" alt="Prendasys">
                    <img src="images/versiones/header-xs.jpg" class="img-fluid mx-auto d-block d-md-none d-lg-none d-xl-none" alt="Prendasys">
                </a>
            </div>
        </div>
      <div class="clearfix"></div>
    </div>

    <div class="container mt-2" id="versiones-review" >
        <div class="clearfix" style="height:5px;"></div>
        <div class="tabs">
            <div class="tab-button-outer mb-3">
                <ul id="tab-button">
                  <li class="is-active">
                    <a href="#tab01" class="d-block mx-auto slide anima">Backoffice</a>
                  </li>
                  <li>
                    <a href="#tab02" class="d-block mx-auto slide anima">Sucursal</a>
                  </li>
                </ul>
            </div>
                  
            <div id="tab01" class="tab-contents">
                <div class="title-orange">
                    <h2>Backoffice</h2>
                </div>
                <div class="tabla table-backoffice" >
                    <div class="content-info" >                    
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Concentrador</h2>
                                        <h4 style="font-weight:400;">Revisa la información de tus casas de empeño a nivel sucursal o corporativo </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Reportes</h2>
                                        <p style="font-size:14px;" >Analiza tu nivel de empeños, desempeños, adjudicaciones, ventas y demás por día, mes o en un periodo específico desde una sucursal hasta toda la cadena.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Dueño de tu información</h2>
                                        <p style="font-size:14px;" >Administra y protege la información de tus sucursales y concentrador, tú eres dueño de las bases de datos y nadie sin tu permiso tendrá acceso a ellas.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info">
                                        <h2 style="font-size:21px;font-weight:400;" >Catálogos y configuraciones</h2>
                                        <h4 style="font-weight:400;">Enfoca tu tiempo en atender a tus clientes </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Optimiza tu tiempo</h2>
                                        <p style="font-size:14px;" >Elimina actividades repetitivas y administra tus catálogos desde un solo lugar.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Replicamos tu información</h2>
                                        <p style="font-size:14px;" >Olvídate de la pérdida de información y problemas para el control de ésta.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Altamente configurable</h2>
                                        <p style="font-size:14px;" >Parametriza Prendasys según las necesidades de tus casas de empeño.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Seguridad</h2>
                                        <h4 style="font-weight:400;">Otorga permisos personalizados </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Tu información está segura</h2>
                                        <p style="font-size:14px;" >Crea roles para controlar los accesos y resguarda tu información. Tus datos siempre estarán seguros.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Operaciones de riesgo</h2>
                                        <h4 style="font-weight:400;">Blinda a tus clientes y sucursales </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Protege a tus clientes y sus prendas</h2>
                                        <p style="font-size:14px;" >Configura alertas sobre operaciones riesgosas al tener disponible información valiosa.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Monitor de operaciones de riesgo</h2>
                                <p style="font-size:14px;" >Decide qué operación autorizar de acuerdo con tus políticas operativas.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Auditorías offline</h2>
                                        <h4 style="font-weight:400;">Permite a tus auditores llegar lejos </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Que no te detenga la conexión</h2>
                                        <p style="font-size:14px;" >Permite que tus auditores se desplacen por toda la bodega para realizar tomas físicas de inventarios sin necesidad de estar conectados todo el tiempo.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Monitorea a tus auditores</h2>
                                        <p style="font-size:14px;" >Conoce el avance e información generada en cada una de sus auditorías de todas las sucursales de manera inmediata.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>  

            <div id="tab02" class="tab-contents table-responsive">            
                <div class="title-orange">
                    <h2>Sucursal</h2>
                </div>             
                <div class="tabla table-backoffice" >
                    <div class="content-info" >                    
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Empeños</h2>
                                        <h4 style="font-weight:400;">Hazlo como las grandes cadenas </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Presta mejor </h2>
                                        <p style="font-size:14px;" >Implementa estrategias comerciales centralizadas que te permitan operar de manera óptima en cada una de tus sucursales sin tener expertos valuadores en cada una de ellas.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Utiliza diferentes formas de pago</h2>
                                        <p style="font-size:14px;" >Permite a tus clientes refrendar, abonar y liquidar en cualquier forma de pago para su mayor comodidad.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Prevención de robo hormiga</h2>
                                        <p style="font-size:14px;" >Realiza cortes de caja ciego y mantén tus ingresos en regla. Utiliza los retiros de dinero, cortes auditoría, parcial o final cuando los requieras.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Mantén tus finanzas sanas</h2>
                                        <p style="font-size:14px;" >Gran variedad de reportes que te permitirán analizar tus operaciones y tomar mejores decisiones.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Identifica rápidamente a tu cliente y hazlo sentir importante</h2>
                                        <p style="font-size:14px;" >Gracias a su identificador único reduce al máximo la duplicidad de datos y concentra de manera óptima la información de un mismo cliente sin importar en cuál sucursal realice sus operaciones.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Opera sin preocupaciones</h2>
                                        <p style="font-size:14px;" >Prendasys cuenta con herramientas que te permitirán establecer los controles en materia de prevención de lavado de dinero.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Prevé las operaciones riesgosas</h2>
                                        <p style="font-size:14px;" >Anticípate con las operaciones que pueden poner en riesgo tu capital o el de tus clientes</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Facilita la vida de tus clientes</h2>
                                        <p style="font-size:14px;" >Brindales la oportunidad de refrendar en cualquiera de tus sucursales.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                                

                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Facturación electrónica</h2>
                                        <p style="font-size:14px;" >Realiza tus facturas electrónicas fácil y rápido.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="no text-center" ></span></div></td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>                               

                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Inventarios</h2>
                                        <h4 style="font-weight:400;">Pon tu inventario donde tu cliente lo requiera </h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Optimiza tu inventario</h2>
                                        <p style="font-size:14px;" >Implementa las mejoras prácticas para el control de tu inventario en vitrina, configurando promociones basadas en la antigüedad de estos.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Ten tu inventario donde el consumidor lo demande</h2>
                                        <p style="font-size:14px;" >Transfiere productos de una sucursal a otra de una manera segura y práctica, conservando la antigüedad para una correcta aplicación de políticas dedescuento.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Minimiza el robo hormiga</h2>
                                        <p style="font-size:14px;" >Realiza auditorías totales o selectivas en tus diferentes almacenes y detecta oportunamente desviaciones.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>

                            </tbody>
                        </table>
                        <table class="table table-responsive">
                            <thead>
                                <tr>
                                    <th class="data-info" >
                                        <h2 style="font-size:21px;font-weight:400;" >Ventas y apartados</h2>
                                        <h4 style="font-weight:400;">Ganas tú y ganan tus clientes</h4>
                                        <span class="line-orange" ></span>
                                    </th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Standard</h3></th>
                                    <th class="title-stand"><h3 class="text-center w-100 d-block pb-5" >Full</h3></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Analiza el ritmo de ventas de tu cadena</h2>
                                        <p style="font-size:14px;" >Configura metas de venta por sucursal y mide los resultados día a día</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Genera tráfico en tus sucursales</h2>
                                        <p style="font-size:14px;" >Consulta existencias en otras sucursales y configura políticas de apartados facilitando a tus clientes la adquisición de mercancía.</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="data-info" >
                                        <h2 style="font-size:16px;" >Monitorea tus utilidades</h2>
                                        <p style="font-size:14px;" >Configura precios de venta óptimos en base a condiciones de prendas</p>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Standard</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                    <td class="pl-0 pr-0" >
                                        <h3 class="d-block d-md-none d-lg-none text-center w-100 d-block pb-2" >Full</h3>
                                        <div class="cuadro" ><span class="yes text-center" ><img src="images/versiones/palomita.png" class="palomita " alt="Prendasys"></span></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>            
            </div>
        </div>
        <div class="clearfix" style="height:44px;"></div>          
    </div>

<!-- div.container-fluid#tabla-final>.container>.col-md-10.offset-md-1.table-responsive-lg>table.table.table-striped.table-bordered.table-dark>tbody>(tr>(td{concepto$})*2)*7 -->
  <div class="container-fluid" id="tabla-final">
    <div class="container">
      <h3 class="generic-title text-center my-2 mx-auto d-block anima">Arquitectura<br><small class="mt-2 mb-5">Impleméntalo en cualquier entorno</small></h3>
      <div class="col-md-10 offset-md-1">

        <div class="tabla" id="tabla-arquitectura">
          <div class="row non">
            <div class="col-sm-3 align-self-center concepto">Implementación</div>
            <div class="col-sm-3 align-self-center">
              <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/server.png" alt="On-premises"></i>
              <span class="label d-block mx-auto mb-2">On-premises</span>
            </div>
            <div class="col-sm-3 align-self-center">
              <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/cloud.png" alt="Cloud"></i>
              <span class="label d-block mx-auto mb-2">Cloud</span>
            </div>
            <div class="col-sm-3 align-self-center">
              <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/hibrid.png" alt="Híbrido"></i>
              <span class="label d-block mx-auto mb-2">Híbrido</span>
            </div>
          </div>
          <div class="row par">
            <div class="col-sm-3 align-self-center concepto">Licenciamiento</div>
            <div class="col-sm-9 align-self-center">
                <div class="col-sm-6 align-self-center">
                    <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/users.png" alt="Multiusuario"></i>
                    <span class="label d-block mx-auto mb-2">Multiusuario</span>
                </div>
            </div>
          </div>
          <div class="row non txt-only">
            <div class="col-sm-3 align-self-center concepto">Sistema operativo</div>
            <div class="col-sm-9 align-self-center">
                <div class="col-sm-6 align-self-center">
                    <span class="label d-block mx-auto mb-2">Windows</span>
                </div>
            </div>
          </div>
          <div class="row par txt-only">
            <div class="col-sm-3 align-self-center concepto">Bases de datos</div>
            <div class="col-sm-9">
              <div class="row">
                <div class="col-sm-6 align-self-center">SQL Express</div>
                <div class="col-sm-6 align-self-center">SQL Estandar</div>
              </div>
            </div>
          </div>
          <div class="row non">
            <div class="col-sm-3 align-self-center concepto">Sincronización de datos</div>
            <div class="col-sm-9 align-self-center">
                <div class="col-sm-6 align-self-center">
                    <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/syncphony-logo.png" alt="Motor propio Syncphony"></i>
                    <span class="label d-block mx-auto mb-2">Motor propio de replicación</span>
                </div>
            </div>
          </div>
          <div class="row par">
            <div class="col-sm-3 align-self-center concepto">Respaldos</div>
            <div class="col-sm-9 align-self-center">
                <div class="col-sm-6 align-self-center">
                    <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/backup.png" alt="Automáticos"></i>
                    <span class="label d-block mx-auto mb-2">Automáticos</span>
                </div>
            </div>
          </div>
          <div class="row non">
            <div class="col-sm-3 align-self-center concepto">Valor agregado</div>
            <div class="col-sm-9">
              <div class="row">
                <div class="col-sm-6 align-self-center">
                  <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/ecommerce.png" alt="Integración con plataforma de e-commerce"></i>
                  <span class="label d-block mx-auto mb-2">Integración con plataforma de e-commerce</span>
                </div>
                <div class="col-sm-6 align-self-center">
                  <i class="icono-tb d-block mx-auto mt-1"><img src="images/versiones/white-icons/statics.png" alt="Plataforma de analítica de datos"></i>
                  <span class="label d-block mx-auto mb-2">Plataforma de analítica de datos</span>
                </div>
              </div>
            </div>
            
          </div>
        </div>

      </div>
    </div>
  </div>

  <div class="clearfix">
    <div class="container text-center">
      <div class="row">
        <div class="col-sm-12 py-5">
          <a href="#top" class="btt-round"><i class="fa fa-chevron-up"></i></a>
        </div>
      </div>
    </div>
  </div>

  <div class="container-fluid py-5 aviso-bottom">
      <div class="container">
        <div class="col-md-10 offset-md-1 text-center">             
          <h4 class="text-gray my-3 light-txt anima">
            ¿Necesitas saber qué versión se ajusta mejor a<br>
            <strong>tu negocio prendario</strong>?<br>
          </h4>
          <a href="caracteristicas" class="btn btn-lg ghostBtn btn-ayuda-gray mx-auto anima">Te damos más información</a>
        </div>
      </div>
  </div>


</section>
