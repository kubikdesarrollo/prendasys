<div class="navbarHeight"></div>

<section class="container-fluid p-0" id="preguntas-frecuentes-section">

	<!-- Para SEO -->
	<h1 class="sr-only">Prendasys</h1>
	<h2 class="sr-only">Sistema Especializado en cadenas de Casas de Empeño</h2>

	<div class="parallaxParent" id="header-buscador">
        <img src="images/preguntas-frecuentes/header-bg-sm.jpg" alt="Preguntas Frecuentes" class="d-sm-block d-sm-block d-lg-none d-xl-none mx-auto img-fluid">
		<div class="container">
			<div class="col-lg-7"><!-- col-md-10 offset-md-1 -->
				<h3 class="titulo text-center">¿Tienes preguntas sobre<br>cómo funciona <strong>Prendasys</strong>?</h3>
				<h4 class="tag text-center mb-4">Estamos aquí para <strong>ayudarte</strong></h4>
				<div class="buscador-container">
					<form action="" id="buscador">
						<div class="input-group" id="buscador-field">
							<input type="text" class="form-control" placeholder="Escríbenos tu duda" id="busqueda">
							<div class="input-group-btn buscarw">
								<!-- <button class="btn buscador-btn" type="submit"><span class="btn-hover"></span>Buscar</button> -->
								<a href="#preguntas" class="btn buscador-btn buscarw">
                                    <span class="d-none d-lg-block d-xl-block">Buscar</span>
                                    <span class="d-block d-lg-none d-xl-none"><i class="fas fa-search"></i></span>
                                </a>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
		<div class="image-parallax"></div>

	</div>

<div class="container-fluid" id="categorias">
	<div class="continer-fluid gris-bg py-4">
		<div class="container">
		  <h3 class="titulo text-center mb-3">Explora por categoría</h3>
		  <br>
		  <!-- Nav pills -->
		  <ul class="nav nav-pills" role="tablist" id="categorias-btns">
            <!-- BTN para desplazar a preguntas -->
            <a href="#preguntas" id="anchor" class="sr-only"></a>
            <!-- BTN para desplazar a preguntas -->

<?php
$list_faqs_type = $db->select("
    SELECT *   
    FROM faqs_type 
    WHERE published = 1  
    ORDER BY ABS(priority) ASC");
    //echo '<pre>'. print_r($list_faqs_type, 1) .'</pre>'; //die(1);

foreach ($list_faqs_type as $key => $item) {
    echo '
            <li class="nav-item">
                <a class="nav-link '. ($key == 0?'active':'') .'" data-toggle="pill" data-target="#tip_'. $item->id .'" title="tip_'. $item->id .'">
                    <i class="icon"><img src="images/preguntas-frecuentes/'. $item->image .'" alt="'. $item->title .'"></i>
                    <div class="back-line"></div>
                    <span>'. $item->title .'</span>
                </a>
            </li>';
}
            
?>
		  </ul>
	  </div>
	</div>  

  <!-- Tab panes -->
  <div class="tab-content pt-1" id="preguntas">


<?php
$list_faqs0 = $db->select("
    SELECT *   
    FROM faqs 
    WHERE published = 1  
    ORDER BY ABS(priority) ASC");
    //echo '<pre>'. print_r($list_faqs, 1) .'</pre>'; //die(1);

$list_faqs = array();
foreach ($list_faqs0 as $key => $item) {
    $list_faqs[ $item->type_id ][] = $item;
}

$lista_preguntas_js = array();

foreach ($list_faqs_type as $key2 => $item2) {

    echo '
    <div id="tip_'. $item2->id .'" class="container tab-pane '. ($key2 == 0?'active':'fade') .'" title="tip_'. $item2->id .'">
        <div class="title-orange">
            <h2>'. $item2->title .'</h2>
        </div>      
        <div class="barra faqs">
            <div class="clearfix" style="height:15px;"></div>
            <div class="col-12">        
                <div class="panel-group" id="accordion-tip_'. $item2->id .'">';


    if(!empty($list_faqs[$item2->id])){
        foreach ($list_faqs[$item2->id] as $key3 => $item3) {

            $lista_preguntas_js[] = array( 
                'label' => $item3->question .' - '. $item2->title, 
                "value" => 'tip_'. $item2->id .' - tip_'. $item2->id .'-'. $item3->id);

            echo '
                    <div class="panel panel-default">
                      <div class="panel-heading">
                        <h4 class="panel-title">
                          <a class="bq" href="" data-toggle="collapse" data-target="#tip_'. $item2->id .'-'. $item3->id .'">'. $item3->question .' <i class="fa fa-caret-'. ($key3 == 0?'down':'right') .' float-right"></i></a>
                        </h4>
                      </div>
                      <div id="tip_'. $item2->id .'-'. $item3->id .'" class="collapse '. ($key3 == 0?'show':'') .'" data-parent="#accordion-tip_'. $item2->id .'">
                        <div class="panel-body">
                            <div class="padding">
                                '. str_replace('<p>', '<p class="response-par">', $item3->answer) .'
                            </div>
                        </div>
                      </div>
                    </div>'; 

        }
    }

    echo '
                </div>
            </div>
        </div>
    </div>';
}
?>
  </div>
</div>


<div class="clearfix">
	<div class="container text-center">
		<div class="row">
			<div class="col-sm-12 py-5">
				<a href="#categorias" class="btt-round"><i class="fa fa-chevron-up"></i></a>
			</div>
		</div>
	</div>
</div>

<div class="container-fluid py-5 aviso-bottom">
      <div class="container">
        <div class="col-md-10 offset-md-1 text-center">		        
          <h4 class="text-gray my-3 light-txt anima">
            <strong>¿Necesitas más ayuda?</strong><br>
            <small>Lo comprendemos perfectamente</small>
          </h4>
          <a href="caracteristicas" class="btn btn-lg ghostBtn btn-ayuda-gray mx-auto anima">Aquí te ayudamos</a>
        </div>
      </div>
  </div>

</section>

<script type="text/javascript">
var lista_preguntas = <?= json_encode($lista_preguntas_js) ?>;
var busqueda_pregunta = '';
//console.log(lista_preguntas);
</script>