<div class="navbarHeight"></div>

<!-- PRENDASYS - REQUERIMIENTOS TÉCNICOS -->

<section class="container-fluid" id="requerimientos-tecnicos-section">

	<div class="container-fluid p-0">
		<div id="header">
			<a href="contacto" class="d-none d-xl-block" id="header-bg">
				<img src="images/requerimientos-tecnicos/laptop.png" alt="Requerimientos Técnicos" class="img-fluid mx-auto d-block header-element animated fadeInRightBig">
				<img src="images/requerimientos-tecnicos/header.jpg" alt="Requerimientos Técnicos" class="img-fluid mx-auto d-block">
			</a>
			<a href="contacto" class="d-none d-md-block d-lg-block d-xl-none">
				<img src="images/requerimientos-tecnicos/header-sm.jpg" class="img-fluid mx-auto" alt="Prendasys">				
			</a>
			<a href="contacto" class="d-block d-md-none d-lg-none d-xl-none">
				<img src="images/requerimientos-tecnicos/header-xs.jpg" class="img-fluid mx-auto" alt="Prendasys">
			</a>
        </div>
        <div class="container-fluid" id="equipos">
        	<div class="container">
        		<div class="col-md-8 offset-md-2" id="panel-rounded">

        			<h3 class="generic-title text-center my-2 mx-auto d-block anima">Te acompañamos en todo el proceso de Instalación</h3>
        			
					<div class="panel-group" id="accordion">
	                    <div class="panel panel-default">
	                      <div class="panel-heading">
	                        <h4 class="panel-title">
	                          <a data-toggle="collapse" data-target="#un-equipo">
	                          	<i class="icon">
	                          		<img src="images/requerimientos-tecnicos/icon-1equipo.png" alt="Un solo equipo">
	                          	</i>
	                          	<span>Un solo equipo</span>
	                          	<i class="fa fa-caret-down float-right"></i>
	                          </a>
	                        </h4>
	                      </div>
	                      <div id="un-equipo" class="collapse show" data-parent="#accordion">
	                        <div class="panel-body">
	                        	<div class="row"><div class="col-md-3	"></div>
	                        		<div class="col-md-6">
	                        			<h3 class="titulo">Servidor / Punto de Venta</h3>
										<ul class="data">
											<li>Procesador Core I3 @ 2 Ghz o similar</li> 
											<li>Memoria RAM 4 GB</li>
											<li>Disco duro 120 GB</li>
											<li>Windows 10 Pro.</li>
										</ul>
	                        		</div>
	                        		<div class="col-md-6 d-none">
	                        			<h3 class="titulo">Punto de Venta</h3>
										<ul class="data">
											<li>Procesador Core I3 @ 2 Ghz o similar</li> 
											<li>Memoria RAM 4 GB</li>
											<li>Disco duro 120 GB</li>
											<li>Windows 10 Pro.</li>
										</ul>
	                        		</div>
	                        	</div>
	                        </div>
	                      </div>
	                    </div>

	                    <div class="panel panel-default">
	                      <div class="panel-heading">
	                        <h4 class="panel-title">
	                          <a data-toggle="collapse" data-target="#dos-equipos">
	                          	<i class="icon">
	                          		<img src="images/requerimientos-tecnicos/icon-2equipos.png" alt="Dos equipos">
	                          	</i>
	                          	<span>Dos equipos</span>
	                          	<i class="fa fa-caret-right float-right"></i>
	                          </a>
	                        </h4>
	                      </div>
	                      <div id="dos-equipos" class="collapse" data-parent="#accordion">
	                        <div class="panel-body">
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<h3 class="titulo">Servidor</h3>
										<ul class="data">
											<li>Procesador Core I5 @ 2 Ghz o similar</li> 
											<li>Memoria RAM 6 GB</li>
											<li>Disco duro 240 GB</li>
											<li>Windows 10 Pro.</li>
										</ul>
	                        		</div>
	                        		<div class="col-md-6">
	                        			<h3 class="titulo">Punto de Venta</h3>
										<ul class="data">
											<li>Procesador Core I3 @ 2 Ghz o similar </li>
											<li>Memoria RAM 4 GB</li>
											<li>Disco duro 120 GB</li>
											<li>Windows 10 Pro.</li>
										</ul>
	                        		</div>
	                        	</div>
	                        </div>
	                      </div>
	                    </div>

	                    <div class="panel panel-default">
	                      <div class="panel-heading">
	                        <h4 class="panel-title">
	                          <a data-toggle="collapse" data-target="#mas-dos-equipos">
	                          	<i class="icon">
	                          		<img src="images/requerimientos-tecnicos/icon-mas2equipos.png" alt="Más de dos equipos">
	                          	</i>
	                          	<span>Más de dos equipos</span>
	                          	<i class="fa fa-caret-right float-right"></i>
	                          </a>
	                        </h4>
	                      </div>
	                      <div id="mas-dos-equipos" class="collapse" data-parent="#accordion">
	                        <div class="panel-body">
	                        	<div class="row">
	                        		<div class="col-md-6">
	                        			<h3 class="titulo">Servidor</h3>
										<ul class="data">
											<li>Procesador Core I7 @ 2 Ghz o similar</li> 
											<li>Memoria RAM 8 GB</li>
											<li>Disco duro 240 GB</li>
											<li>Windows 10</li>
										</ul>
	                        		</div>
	                        		<div class="col-md-6">
	                        			<h3 class="titulo">Punto de Venta</h3>
										<ul class="data">
											<li>Procesador Core I3 @ 2 Ghz o similar </li>
											<li>Memoria RAM 4 GB</li>
											<li>Disco duro 120 GB</li>
											<li>Windows 10</li>
										</ul>
	                        		</div>
	                        	</div>
	                        </div>
	                      </div>
	                    </div>

	                </div>

        		</div>
        	</div>
        </div>

		<div class="container-fluid py-5" id="complementos">
	       	<div class="container">
	       		<h3 class="generic-title text-center mt-3 mb-5 mx-auto d-block anima">Complementos ideales</h3>
	       		<div class="row">
	       			<div class="col-md-3 anima">
	       				<i class="icono"><img src="images/requerimientos-tecnicos/icono-cajon.png" alt="Cajón de dinero" class="img-fluid mx-auto d-block"></i>
	       				<h4 class="tile text-center">Cajón de dinero</h4>
	       				<p class="desc text-center">Estándar. Compatible con miniprinter.</p>
	       			</div>
	       			<div class="col-md-3 anima">
	       				<i class="icono"><img src="images/requerimientos-tecnicos/icono-impresora.png" alt="Impresora de ticket" class="img-fluid mx-auto d-block"></i>
	       				<h4 class="tile text-center">Impresora de ticket</h4>
	       				<p class="desc text-center">Térmica de 80 milímetros. Puerto USB.</p>
	       			</div>
	       			<div class="col-md-3 anima">
	       				<i class="icono"><img src="images/requerimientos-tecnicos/icono-lector.png" alt="Lector de código de barras" class="img-fluid mx-auto d-block"></i>
	       				<h4 class="tile text-center">Lector de código de barras</h4>
	       				<p class="desc text-center">Estándar CCD o Láser, base o manual.</p>
	       			</div>
	       			<div class="col-md-3 anima">
	       				<i class="icono"><img src="images/requerimientos-tecnicos/icono-internet.png" alt="Internet - 10 Mbps" class="img-fluid mx-auto d-block"></i>
	       				<h4 class="tile text-center">Internet - 10 Mbps</h4>
	       				<p class="desc text-center d-none">Los navegadores recomendados son Chrome y Mozilla Firefox.</p>
	       			</div>
	       		</div>
	       	</div>
		</div>

		<div class="container-fluid grey-bg py-5 aviso-bottom">
		    <div class="container">
		      <div class="col-md-10 offset-md-1 text-center">		        
		        <h4 class="text-white my-3 light-txt anima">
		        	<strong>Es más sencillo de lo que te imaginas</strong><br>
		        	<small>Regístrate para brindarte asesoría personalizada</small>
		        </h4>
		        <a href="contacto#divided-formulario" class="btn btn-lg ghostBtn mx-auto anima">Regístrate aquí</a>
		      </div>
		    </div>
		</div>

	</div>

</section>