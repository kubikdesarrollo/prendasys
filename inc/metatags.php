<?php
	$title1 = 'PRENDASYS';
	$author = (isset($default_config['DEFAULT_AUTHOR']) && $default_config['DEFAULT_AUTHOR']) ? $default_config['DEFAULT_AUTHOR'] : 'kubik.mx';
	$titulo = (isset($default_config['DEFAULT_TITLE']) && $default_config['DEFAULT_TITLE']) ? $default_config['DEFAULT_TITLE'] : $title1;
	$descripcion = (isset($default_config['DEFAULT_DESCRIPTION']) && $default_config['DEFAULT_DESCRIPTION']) ? $default_config['DEFAULT_DESCRIPTION'] : $title1;
	$keywords = (isset($default_config['DEFAULT_KEYWORDS']) && $default_config['DEFAULT_KEYWORDS']) ? $default_config['DEFAULT_KEYWORDS'] : $title1;
	$fb_img = (isset($default_config['DEFAULT_SHARE_IMAGE']) && $default_config['DEFAULT_SHARE_IMAGE']) ? $default_config['DEFAULT_SHARE_IMAGE'] : $domain . 'images/shareimg.jpg';

	$section_t = $section;
	switch($section){
		case 'home': 
			$section_t = 'home';
			$titulo = 'Prendasys Sistema Experto para Casas de Empeño';
    		$descripcion = 'Presta mejor y mejora hasta en un 30% tus valuaciones. Acércate al sistema especializado en casas de empeño.';
    		$keywords = 'sistema para casas de empeño, presta mejor, sistema prendario, sistema para empeños, sistema para empeñar, control de empeños, sistema administrativo para casas de empeño';
		break;
		case 'caracteristicas': 
			$section_t = 'caracteristicas';
			$titulo = 'Prendasys sistema para prestar mejor y disminuir tus adjudicaciones - Características';
    		$descripcion = 'Sistema especializado en casas de empeño que cubre 100% las necesidades de administración y operación de tu negocio.';
    		$keywords = 'sistema para casas de empeño, empeños de autos, ventas y apartados, control eficaz, inventarios, robo hormiga, auditoría de préstamos';
    	break;
    	case 'versiones': 
			$section_t = 'versiones';
			$titulo = 'Prendasys | Sistema para casas de empeño escalable y configurable - Versiones';
    		$descripcion = 'Sistema escalable y altamente configurable que se adapta a las necesidades de tus casas de empeño y cuenta con dos versiones distintas. Cambia de licenciamiento en el momento que lo requieras.';
    		$keywords = 'sistema para casas de empeño, sistema configurable, backoffice, sistema sencillo para casas de empeño, sql express, sistema multiusuario';
    	break;
    	case 'implementaciones-agiles': 
			$section_t = 'implementaciones-agiles';
			$titulo = 'Prendasys es el sistema más fácil y rápido de instalar - Implementaciones ágiles';
    		$descripcion = 'La estrategia de implementación de Prendasys garantiza su instalación de manera eficaz para que tus casas de empeño operen de forma efectiva sea cual sea su tamaño.';
    		$keywords = 'sistema para casas de empeño, sistema de fácil instalación, sql express, ecommerce, sistema multiusuario, replicas, implantaciones';
		break;
		case 'requerimientos-tecnicos': 
			$section_t = 'requerimientos-tecnicos';
			$titulo = 'Prendasys el sistema para casas de empeño con requerimientos mínimos - Requerimientos técnicos';
    		$descripcion = 'Los requerimientos son mínimos y te acompañamos en todo el proceso de instalación. Contrata un sistema para casas de empeño sencillo de instalar.';
    		$keywords = 'sql express, sql standar, sistema especializado para casas de empeño, sistema web para casas de empeño, sistema sencillo para casas de empeño';
		break;
		case 'preguntas-frecuentes': 
			$section_t = 'preguntas-frecuentes';
			$titulo = 'Preguntas frecuentes de Prendasys';
    		$descripcion = 'La manera más rápida de atender tus dudas y sobre cómo funciona Prendasys en empeños, promociones, servicio al cliente, control, inventarios, ventas y apartados, backoffice, seguridad, catálogos, operaciones de riesgo y muchos temas más.';
    		$keywords = 'preguntas frecuentes prendasys, promociones casas de empeño, programas de lealtad para casas de empeño, traspasos entre casas de empeño, control para casas de empeño, expertos en casas de empeño';
		break;
		case 'webinars': 
			$section_t = 'webinars';
			$titulo = 'Webinars en Prendasys';
    		$descripcion = 'Conoce de especialistas en procesos de casas de empeño estrategias para mejorar la productividad de tu negocio prendario.';
    		$keywords = 'webinars, productividad de negocios prendarios, tips para casas de empeño, expertos en casas de empeño, casos de éxito prendasys, asesorías en operaciones prendarias soporte';
		break;
		case 'soporte': 
			$section_t = 'soporte';
			$titulo = 'Soporte de Prendasys';
    		$descripcion = 'Somos tu socio de negocios. Cuenta con nosotros en todo el proceso y obtén el máximo valor de Prendasys.';
    		$keywords = 'soporte, sistema para casas de empeño, expertos en casas de empeño, sistema especializado en casas de empeño, socios negocio prendario, asesorías en operaciones prendarias';
		break;
		case 'soporte-conexion-remota': 
			$section_t = 'soporte-conexion-remota';
			$titulo = 'Soporte de conexión remota Prendasys';
    		$descripcion = 'Nuestro equipo de especialistas se conecta contigo para brindarte el soporte necesario y ofrecerte una experiencia de atención integral.';
    		$keywords = 'Soporte Prendasys, TeamViewer, Rapidez, buen servicio, calidad de atención al cliente, asesorías';
		break;
		case 'porque-prendasys': 
			$section_t = 'porque-prendasys';
			$titulo = '¿Por qué contratar Prendasys?';
    		$descripcion = 'Recupera tu inversión en menos de 6 meses y mejora la productividad de tu negocio prendario, optimiza procesos, controla inventarios, entre muchos beneficios más.';
    		$keywords = 'aumenta tu productividad, vende más, presta mejor, expertos en optimizar procesos prendarios, innovación para tus casas de empeño, ecommerce, analítica de datos, trazabilidad de operaciones';
		break;
		case 'contacto': 
			$section_t = 'contacto';
			$titulo = 'Prendasys Contacto';
    		$descripcion = 'Contáctanos. Habla con un especialista de nuestro equipo para brindarte la asesoría que necesitas.';
    		$keywords = 'contacto prendays, sistema para casas de empeño, siempre contigo, atención rápida, expertos en operaciones prendarias';
		break;
		

	};