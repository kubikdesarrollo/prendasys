/* custom js */
$(document).ready(function() {
 
  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax1"})
      .setTween("#parallax1 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);

  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax2"})
      .setTween("#parallax2 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax3"})
      .setTween("#parallax3 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);
  

});
