/* custom js */
$(document).ready(function() {


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#header-buscador"})
      .setTween("#header-buscador > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);
  

  $(".collapse").on('show.bs.collapse', function(){
      $(this).parent().find("i.fa").removeClass("fa-caret-right").addClass("fa-caret-down");
  });
  $(".collapse").on('hide.bs.collapse', function(){
      $(this).parent().find("i.fa").removeClass("fa-caret-down").addClass("fa-caret-right");
  });

  (function () {
      $("#categorias-btns .nav-item .nav-link").on('click', function() {
        $("#anchor").click();
      })
    })();
    

  //console.log(lista_preguntas);



  var input = document.getElementById("busqueda");
  new Awesomplete(input, {
      minChars: 2,
      maxItems: 5,
        list: lista_preguntas/*,
        // insert label instead of value into the input.
        replace: function(suggestion) {
          this.input.value = suggestion.label;
        }*/
    });

    $('.buscador-btn').on('click',function(event){
        event.preventDefault();
        //console.log($('input[type="text"]').val());
        var search = busqueda_pregunta; //$('input[type="text"]').val();
        search = search.split(" - ");
        /*search[0] = search[0].replace(' ','-').replace('ñ','n').replace('ó','o').replace('á','a');
        var search2 = $('input[type="text"]').val();
        search2 = search2.split(" - ");*/
        //console.log(search);
        //search2[1] = search2[1];
        //BOTON categoria
        $('#categorias-btns .nav-link').removeClass('active');
        $('.nav-link[title='+search[0]+']').addClass('active').click();
        //TABS accordiones
        $('#preguntas .tab-pane').removeClass('active').addClass('fade');
        $('.tab-pane[title='+search[0]+']').addClass('active').removeClass('fade');
        //PREGUNTA individual
        $('.tab-pane[title='+search[0]+']').find('.collapse').removeClass('show');
        $('.tab-pane[title='+search[0]+']').find('i.fa').removeClass('fa-caret-down').addClass('fa-caret-right');
        $('.tab-pane[title='+search[0]+']').find('.collapse[id='+search[1]+']').addClass('show');
        //$('.collapse[id='+search2[1]+']').hasClass('show').parent().find("i.fa").removeClass("fa-caret-right").addClass("fa-caret-down");
        //$('.tab-pane[title='+search[0]+']').find('.bq[data-target='+search2[1]+'] .fa').removeClass('fa-caret-right').addClass('fa-caret-down');
        //$("#anchor").click();
        $("#busqueda").delay(1200).val("");
    });

 

});
