$(document).ready(function() {

  // hide/show "back-to-top-btn"

  var topBtn = 0;
  $(window).scroll(function () { 
      var scroll_pos = $(window).scrollTop();
      if(scroll_pos>160 && topBtn == 0){
        $("#btn-to-top").removeClass('off');
        topBtn = 1;
      }
      if(scroll_pos<161 && topBtn == 1){
        $("#btn-to-top").addClass('off');
        topBtn = 0;
      }
  });

  // MENU HAMBURGUESA

  (function () {
    $('.navbar-toggler').on('click', function() {
      $('.hamburger-menu').toggleClass('animate');
    })
  })();

  // scroll-to nav

  $(function() {
      $('a[href*="#"]:not([href="#"])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html, body').animate({
              scrollTop: (target.offset().top *1) - ($("#topNav").height()*1)
            }, 1000, 'easeInOutExpo');
            return false;
          }
        }
      });
    });

  // Check if user saw the "Cookies Alert"

  var key = 'hadAlert',
      hadAlert = localStorage.getItem(key);
  if (!hadAlert) {
      $('#alert').addClass('show');
  }
  $('#alert').on('close.bs.alert', function () {
      localStorage.setItem(key, true);
  });


 
  // validaciones varias
  $('.restrict1').on('input', function() {
      this.value = this.value.replace(/[^a-zA-ZñÑáéíóúÁÉÍÓÚ ]/g, ''); // acepta solo letras
  });
  $('.restrict2').on('input', function() {
      this.value = this.value.replace(/[^0-9]/g, ''); // acepta solo numeros
  });
  $('.restrict3').on('input', function() {
      this.value = this.value.replace(/[^a-zA-Z0-9ñÑáéíóúÁÉÍÓÚ .,:@]/g, ''); // solo acepta letras y numeros
  });

  //validacion de telefonos
  $('.telefono').mask('(000) 000-0000');

  $(".email").change(function(){
    if( $(this).val() == '' || isValidEmailAddress($(this).val()) ){
      $(this).css({'background-color': '#ffffff', 'border-color': '#ced4da'});
    }else{
      $(this).css({'background-color': '#ffeeee', 'border-color': '#e00'});
    }
  });

});

function isValidEmailAddress(emailAddress) {
  var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
  return pattern.test(emailAddress);
}