$(document).ready(function() {


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "150%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#header-contacto"})
      .setTween("#header-contacto > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);


/* OPERADORA */
var controller = new ScrollMagic.Controller();
var tween = TweenMax.staggerFromTo("#operadora", 1, {left: -1200}, {left: -180, ease: Power4.easeOut});
var scene = new ScrollMagic.Scene({triggerElement: "#trigger-img"})
      .setTween(tween)
      .addTo(controller);



});



/* custom js */
$(document).ready(function() {
  // validacion de formulario
	$("#formulario").submit(function(e){
	    var isemail = isValidEmailAddress( $("#email").val() );
	    if(!isemail){
	        alert('Email invalido');
	        e.preventDefault();
	        return false;
	    }else{
	        $("#formulario .send-btn").before('Enviando...').remove();
	    }
	});
});
