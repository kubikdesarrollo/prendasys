/* custom js */
$(document).ready(function() {


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#header-buscador"})
      .setTween("#header-buscador > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);
  

$(".collapse").on('show.bs.collapse', function(){
    $(this).parent().find("i.fa").removeClass("fa-caret-right").addClass("fa-caret-down");
});
$(".collapse").on('hide.bs.collapse', function(){
    $(this).parent().find("i.fa").removeClass("fa-caret-down").addClass("fa-caret-right");
});

(function () {
    $("#categorias-btns .nav-item .nav-link").on('click', function() {
      $("#anchor").click();
    })
  })();
  


var input = document.getElementById("busqueda");
new Awesomplete(input, {
      minChars: 2,
      maxItems: 5,
        list: [
//
//  EMPEÑOS/
//
{label:"Cómo puedo prestar mejor? - Empeños", value:"empenos - empeno-1"},
{label:"Es posible poder controlar los montos a prestar por sucursal? - Empeños", value:"empenos - empeno-2"},
{label:"Puedo empeñar cualquier producto? - Empeños", value:"empenos - empeno-3"},
{label:"Puedo empeñar diferentes prendas en el mismo contrato? - Empeños", value:"empenos - empeno-4"},
{label:"Cómo saber cuándo se realizan empeños de artículos especiales? ", value:"empenos - empeno-5"},
{label:"Cuándo recibo un nuevo modelo de producto, lo tengo que registrar en cada una de mis sucursales? - Empeños", value:"empenos - empeno-6"},
{label:"Es posible que una sucursal reciba artículos que no sabría cómo evaluar? - Empeños", value:"empenos - empeno-7"},
{label:"Es posible solo cotizar un avalúo? - Empeños", value:"empenos - empeno-8"},
{label:"Cómo puedo disminuir el porcentaje de mis adjudicaciones? - Empeños", value:"empenos - empeno-9"},
{label:"Es posible realizar préstamos con transferencias bancarias? - Empeños", value:"empenos - empeno-10"},
{label:"Puedo generar tentativas de pago? - Empeños", value:"empenos - empeno-11"},
{label:"Cómo puedo alertar a mis sucursales de clientes sospechosos? - Empeños", value:"empenos - empeno-12"},
{label:"Es posible almacenar datos biométricos de mis clientes? - Empeños", value:"empenos - empeno-13"},
{label:"Es posible configurar diferentes tasas de intereses por plazo y tipo de producto? - Empeños", value:"empenos - empeno-14"},
{label:"Es posible configurar tasas de intereses promocionales? - Empeños", value:"empenos - empeno-15"},
//
//PROMOCIONES/
//
{label:"¿Cómo puedo otorgar beneficios en los empeños a mis mejores clientes? - Promociones", value:"promociones - promociones-1"},
{label:"¿Puedo otorgar descuentos en refrendos? - Promociones", value:"promociones - promociones-2"},
{label:"¿Puedo otorgar a mis clientes promociones en empeños por aniversario de las sucursales? - Promociones", value:"promociones - promociones-3"},
//
//SERVICIO AL CLIENTE/
//
{label:"Es posible evitar duplicidad de registro de clientes? - Servicio al cliente", value:"servicio-al-cliente - servicio-cliente-1"},
{label:"Es posible que mis clientes refrenden en cualquier sucursal? - Servicio al cliente", value:"servicio-al-cliente - servicio-cliente-2"},
{label:"Pueden mis clientes pagar sus refrendos y empeños con tarjetas bancarias? - Servicio al cliente", value:"servicio-al-cliente - servicio-cliente-3"},
{label:"Cómo puedo premiar la lealtad de mis clientes? - Servicio al cliente", value:"servicio-al-cliente - servicio-cliente-4"},
{label:"Cómo puedo saber qué otros servicios puedo ofrecer a mis clientes? - Servicio al cliente", value:"servicio-al-cliente - servicio-cliente-5"},
//
//CONTROL/
//
{label:"Puedo generar facturas electrónicas a mis clientes? - Control", value:"control - control-1"},
{label:"Puedo generar factura de todas mis operaciones no facturadas en el día? - Control", value:"control - control-2"},
{label:"Cómo puedo evitar el robo hormiga en mis cajas? - Control", value:"control - control-3"},
{label:"Puedo configurar máximos de efectivo por caja por sucursal? - Control", value:"control - control-4"},
{label:"Puedo controlar las cancelaciones de las operaciones en empeños y ventas? - Control", value:"control - control-5"},
{label:"Puedo conocer los motivos por los cuales me cancelan las operaciones? - Control", value:"control - control-6"},
{label:"Puedo refrendar empeños de contratos que ya se adjudicaron? - Control", value:"control - control-7"},
{label:"Es posible que mis pases a piso las realice ciertos días del mes? - Control", value:"control - control-8"},
{label:"Es posible actualizar mis precios de venta de acuerdo con los nuevos valores de referencia de mis artículos? - Control", value:"control - control-9"},
{label:"Es posible configurar un precio para la reimpresión de contratos? - Control", value:"control - control-10"},
{label:"Es posible configurar un monto de presto mínimo por sucursal? - Control", value:"control - control-11"},
{label:"Puedo adecuar la impresión de contratos al formato que utilizo actualmente? - Control", value:"control - control-12"},
{label:"Cómo puedo realizar la migración de información de mi sistema a Prendasys? - Control", value:"control - control-13"},
{label:"Puedo operar un esquema de franquicias? - Control", value:"control - control-14"},
{label:"Es posible migrar el historial de comportamiento de mis clientes? - Control", value:"control - control-15"},
//
//INVENTARIOS/
//
{label:"Si inicié un inventario, puedo guardarlo y continuar después? - Inventarios", value:"inventarios - inventarios-1"},
{label:"Puedo consultar la existencia de un producto de otra sucursal? - Inventarios", value:"inventarios - inventarios-2"},
{label:"Puedo generar inventarios parciales? - Inventarios", value:"inventarios - inventarios-3"},
{label:"Cómo puedo distinguir los artículos iguales pero de diferentes clientes? - Inventarios", value:"inventarios - inventarios-4"},
{label:"Puedo enviar artículos de una sucursal a otra de mi misma cadena? - Inventarios", value:"inventarios - inventarios-5"},
{label:"Cómo puedo conocer el valor que tiene cada uno de mis almacenes? - Inventarios", value:"inventarios - inventarios-6"},
{label:"Los traspasos entre almacenes los tengo que realizar manualmente? - Inventarios", value:"inventarios - inventarios-7"},
{label:"Puedo realizar auditorías físicas en cualquier lugar de mi almacén sin importar la conexión de datos? - Inventarios", value:"inventarios - inventarios-8"},
{label:"Cómo puedo migrar mis existencias de piso y apartados a Prendasys? - Inventarios", value:"inventarios - inventarios-9"},
//
//VENTAS Y APARTADOS/
//
{label:"Es posible manejar diferentes descuentos en artículos para venta de acuerdo con la antigüedad de este? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-1"},
{label:"Puedo consultar las existencias de otras sucursales? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-2"},
{label:"Puedo redondear los importes de mis ventas? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-3"},
{label:"Puedo controlar las devoluciones sobre venta? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-4"},
{label:"Puedo pagar con cualquier forma de pago? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-5"},
{label:"Puedo manejar apartados? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-6"},
{label:"Qué pasa cuando un apartado vence y el cliente no pasó a liquidarlo? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-7"},
{label:"Cómo puedo revisar las utilidades de mis ventas? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-8"},
{label:"Puedo facturar mis apartados? - Ventas y apartados", value:"ventas-y-apartados - ventas-apartados-9"},
//
//OPERACIONES DE RIESGO/
//
{label:"Qué es una operación de riesgo? - Operaciones de riesgo", value:"operaciones-de-riesgo - operaciones-riesgo-1"},
{label:"Qué tipos de operaciones de riesgo existen? - Operaciones de riesgo", value:"operaciones-de-riesgo - operaciones-riesgo-2"},
{label:"Cómo controla Prendasys esas operaciones de riesgo? - Operaciones de riesgo", value:"operaciones-de-riesgo - operaciones-riesgo-3"},
{label:"Puedo proteger a mi cliente quien ha estado abonando su empeño a que sea él el único que pueda liquidar su empeño? - Operaciones de riesgo", value:"operaciones-de-riesgo - operaciones-riesgo-4"},
//
//SEGURIDAD/
//
{label:"Se pueden manejar derechos por usuario? - Seguridad", value:"seguridad - seguridad-1"},
{label:"Qué es un rol? - Seguridad", value:"seguridad - seguridad-2"},
{label:"Si un usuario tiene derechos a unas opciones en el sistema heredadas por un rol, puedo otorgarle permisos adicionales? - Seguridad", value:"seguridad - seguridad-3"},
{label:"Quién otorga las contraseñas de usuarios? - Seguridad", value:"seguridad - seguridad-4"},
{label:"Cuántos usuarios cajeros puedo dar de alta? - Seguridad", value:"seguridad - seguridad-5"},
{label:"Es posible limitar los derechos de los usuarios por sucursal? - Seguridad", value:"seguridad - seguridad-6"},
//
//CATALOGOS/
//
{label:"Quién administra los catálogos? - Catálogos", value:"catalogos - catalogos-1"},
{label:"Cómo les llega la información registrada a las sucursales? - Catálogos", value:"catalogos - catalogos-2"},
{label:"Cómo se administran los precios de referencia de los metales finos? - Catálogos", value:"catalogos - catalogos-3"},
{label:"Puedo dar de alta nuevas sucursales? - Catálogos", value:"catalogos - catalogos-4"},
{label:"Puedo dar de alta diversas empresas? - Catálogos", value:"catalogos - catalogos-5"},
{label:"Puedo medir la efectividad de las campañas de comunicación que contrate? - Catálogos", value:"catalogos - catalogos-6"},
{label:"Puedo inactivar productos para que ya no reciban nuevos empeños? - Catálogos", value:"catalogos - catalogos-7"},
{label:"Cuánto tarda el proceso en dar de alta un nuevo producto para su empeño? - Catálogos", value:"catalogos - catalogos-8"},
{label:"Puedo configurar mis tickets de venta? - Catálogos", value:"catalogos - catalogos-9"},
{label:"Los productos tienen la clasificación del SAT? - Catálogos", value:"catalogos - catalogos-10"},
//
//BACKOFFICE/
//
{label:"Cuántas sucursales debo tener activas para poder operar el Backoffice? - Backoffice", value:"backoffice - backoffice-1"},
{label:"Qué información puedo consultar en el concentrado? - Backoffice", value:"backoffice - backoffice-2"},
{label:"Qué sucede con la sincronización de información si estoy un día sin internet? - Backoffice", value:"backoffice - backoffice-3"},
{label:"Cada cuánto se sincroniza la información de mis sucursales al Backoffice? - Backoffice", value:"backoffice - backoffice-4"},
{label:"Es posible consultar las existencias de todas mis sucursales en los diferentes almacenes desde el Backoffice? - Backoffice", value:"backoffice - backoffice-5"},
{label:"Es posible conocer los flujos de efectivo de mis sucursales en tiempo real? - Backoffice", value:"backoffice - backoffice-6"},
{label:"Es posible conocer el estatus de las auditorías que se estén realizando en las sucursales? - Backoffice", value:"backoffice - backoffice-7"},
{label:"Es posible generar nuevos reportes? - Backoffice", value:"backoffice - backoffice-8"},
//
//OPERACIÓN/
//
{label:"Por qué Prendasys me manda una alerta al mandar imprimir en una impresora de ticket y no me genera la impresión? - Operación", value:"operacion - operacion-1"},
{label:"Por qué no puedo seleccionar una sucursal para generar un refrendo foráneo? - Operación", value:"operacion - operacion-2"},
{label:"Por qué no me despliega las descripciones de los productos nuevos en una entrada por traspaso de inventarios entre sucursales? - Operación", value:"operacion - operacion-3"},
{label:"Qué debo hacer cuando Prendasys no permita realizar un cambio de conexión? - Operación", value:"operacion - operacion-4"},
{label:"Qué debo hacer cuando no pueda acceder a ninguna funcionalidad de sucursal, aún y cuando tenga los permisos asignados? - Operación", value:"operacion - operacion-5"},
{label:"Por qué cuando cambio mi contraseña en sucursal, al poco tiempo la tengo que volver a cambiar? - Operación", value:"operacion - operacion-6"},
{label:"Qué significa el mensaje “UUID No existe, intente en 10 minutos” al cancelar una factura? - Operación", value:"operacion - operacion-7"},
//
//LICENCIAMIENTO/
//
{label:"Mi licencia principal tiene límite de usuarios? - Licenciamiento", value:"licenciamiento - licenciamiento-1"},
{label:"Qué debo hacer si el equipo donde tengo la licencia de Prendasys falló y requiero formatearlo? - Licenciamiento", value:"licenciamiento - licenciamiento-2"},
{label:"Qué debo hacer para revocar una licencia de Prendasys de un equipo? - Licenciamiento", value:"licenciamiento - licenciamiento-3"},
{label:"Cuándo se debe revocar la licencia? - Licenciamiento", value:"licenciamiento - licenciamiento-4"},
{label:"Qué versión de SQL requiero para implementar Prendasys? - Licenciamiento", value:"licenciamiento - licenciamiento-5"}

//* trucha, el último elemento va sin coma

        ]/*,
        // insert label instead of value into the input.
        replace: function(suggestion) {
          this.input.value = suggestion.label;
        }*/
    });

    $('.buscador-btn').on('click',function(event){
        event.preventDefault();
        console.log($('input[type="text"]').val());
        var search = $('input[type="text"]').val();
        search = search.split(" - ");
        search[0] = search[0].replace(' ','-').replace('ñ','n').replace('ó','o').replace('á','a');
        var search2 = $('input[type="text"]').val();
        search2 = search2.split(" - ");
        search2[1] = search2[1];
        //BOTON categoria
        $('#categorias-btns .nav-link').removeClass('active');
        $('.nav-link[title='+search[0]+']').addClass('active').click();
        //TABS accordiones
        $('#preguntas .tab-pane').removeClass('active').addClass('fade');
        $('.tab-pane[title='+search[0]+']').addClass('active').removeClass('fade');
        //PREGUNTA individual
        $('.tab-pane[title='+search[0]+']').find('.collapse').removeClass('show');
        $('.tab-pane[title='+search[0]+']').find('i.fa').removeClass('fa-caret-down').addClass('fa-caret-right');
        $('.tab-pane[title='+search[0]+']').find('.collapse[id='+search2[1]+']').addClass('show');
        //$('.tab-pane[title='+search[0]+']').find('.bq[data-target='+search2[1]+']').children('.fa').removeClass('fa-caret-right').addClass('fa-caret-down');
        $("#anchor").click();
        $("input[type=text], textarea").delay(1200).val("");
    });
/*

    document.getElementById('categorias-btns').onclick = function (event) {
        event = event || window.event;
        var target = event.target || event.srcElement,
            link = target.src ? target.parentNode : target,
            options = {index: link, event: event},
            links = this.getElementsByTagName('a');
    };
*/
   /*
   var product = getUrlVars()["product"];
    if(product!==undefined){
        console.log('.nav-link[title='+product+']');
        $('.nav-link[title='+product+']').click();
    }

    function getUrlVars() {
        var vars = {};
        var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi,    
        function(m,key,value) {
          vars[key] = value;
        });
        return vars;
      }
    $('.buscador-btn').click(function(){
      window.location.href + "#preguntas"
    });

    */
 

});
