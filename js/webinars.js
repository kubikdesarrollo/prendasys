/* custom js */
$(document).ready(function() {
 
  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax1"})
      .setTween("#parallax1 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);

  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax2"})
      .setTween("#parallax2 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#parallax1"})
        .setClassToggle("#textbanner1", "bounce") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#parallax2"})
        .setClassToggle("#textbanner2", "bounce") // add class toggle
        .addTo(controller);

  

});
