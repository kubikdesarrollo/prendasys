/* custom js */

// $('.base-whatsapp').removeClass('fadeInRightBig').css({'top':'100%'});


$(document).ready(function() {
  // validacion de formulario
  $("#formulario").submit(function(e){
    var isemail = isValidEmailAddress( $("#email").val() );
    if(!isemail){
        alert('Email invalido');
        e.preventDefault();
        return false;
    }else{
        $("#formulario .send-btn").before('Enviando...').remove();
    }
  });

  // slideshow inicial

  $("#slideshow").owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      navigation : false, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      nav: false,
      controls: false,
      items : 1,
      itemsMobile : true,
      loop:true,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true
      //navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"]

  });

  // slideshow-bloques

  $("#bloques").owlCarousel({
      autoWidth:true,
      margin:0,
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      nav: true,
      controls: true,
      itemsMobile : true,
      loop:true,
      autoplay: false,
      dots:false,
      navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
      responsive:{
          0:{
              items:1
          },
          768:{
              items:2
          },
          1000:{
              items:3
          },
          1440:{
              items:4
          }
      }
  });

  //$(window).resize(function() {
    var width = $(window).width();
    if (width < 768){
      $('#bloques').owlCarousel('remove', '<div class="item foo-item"></div>').owlCarousel('update')
    }
  //});

  // Validacion Bootstrap4 formulario
  // Disable form submissions if there are invalid fields

  /*(function() {
    'use strict';
    window.addEventListener('load', function() {
      // Get the forms we want to add validation styles to
      var forms = document.getElementsByClassName('needs-validation');
      // Loop over them and prevent submission
      var validation = Array.prototype.filter.call(forms, function(form) {
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          var isemail = isValidEmailAddress( $("#email").val() );
          if(!isemail){
            alert('Email invalido');
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
        }, false);
      });
    }, false);
  })();*/

  // SCROLL REVEAL
 
  var sequenceInterval = 150;
    window.sr = ScrollReveal({reset: false});
    // Custom reveal sequencing by container
    $('#home-section').each(function() {
      var sequenceDelay = 0;
      $(this).find('.anima').each(function() {
        sr.reveal(this, {
          delay: sequenceDelay
        });
        sequenceDelay += sequenceInterval;
      });
    });

  // bloques
  /*
  var contentVisible = 0;

  $('.empenos-btn').click(function () {
      if($('#content .bloque').hasClass('active') == true && contentVisible == 1){
        $('#content .bloque.active').slideUp('fast').removeClass('active');
        contentVisible = 0;
      }
      if($('#content .bloque').hasClass('active') == false && contentVisible == 0){
        $('#content #empeno-bloque').slideDown().addClass('active');
        contentVisible = 1;
      }
  });
  $('.ctrl-eficaz-btn').click(function () {
      if($('#content .bloque').hasClass('active') == true && contentVisible == 1){
        $('#content .bloque.active').slideUp('fast').removeClass('active');
        contentVisible = 0;
      }
      if($('#content .bloque').hasClass('active') == false && contentVisible == 0){
        $('#content #ctrl-eficaz-bloque').slideDown().addClass('active');
        contentVisible = 1;
      }
  });
  */

  $('.empenos-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #empeno-bloque').slideDown().addClass('active');
      return false;
  });

  $('.ctrl-eficaz-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #ctrl-eficaz-bloque').slideDown().addClass('active');
      return false;
  });
  $('.serv-cliente-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #serv-cliente-bloque').slideDown().addClass('active');
      return false;
  });
  $('.prevencion-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #prevencion-bloque').slideDown().addClass('active');
      return false;
  });
  $('.inventarios-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #inventarios-bloque').slideDown().addClass('active');
      return false;
  });
  $('.vtas-apartados-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #vtas-apartados-bloque').slideDown().addClass('active');
      return false;
  });
  $('.op-riesgo-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #op-riesgo-bloque').slideDown().addClass('active');
      return false;
  });
  $('.aud-offline-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #aud-offline-bloque').slideDown().addClass('active');
      return false;
  });
  $('.seguridad-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #seguridad-bloque').slideDown().addClass('active');
      return false;
  });
  $('.cat-config-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #cat-config-bloque').slideDown().addClass('active');
      return false;
  });
  $('.backoffice-btn').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      $('#content #backoffice-bloque').slideDown().addClass('active');
      return false;
  });

  $(".empenos-btn").click();

  // Arrow Up
  /*
  $('.up-bloques').click(function() {
      $('#content .bloque.active').slideUp('fast').removeClass('active');
      return false;
  });
  */

  // $('.base-whatsapp').css({"right":"calc(50% - 36px)", "top":"calc(100% - 111px)"}).addClass("fadeInUpBig");

});
