$(document).ready(function() {


  var sequenceInterval = 150;
    window.sr = ScrollReveal({reset: false});
    // Custom reveal sequencing by container
    $('#requerimientos-tecnicos-section').each(function() {
      var sequenceDelay = 0;
      $(this).find('.anima').each(function() {
        sr.reveal(this, {
          delay: sequenceDelay
        });
        sequenceDelay += sequenceInterval;
      });
    });


  $(".collapse").on('show.bs.collapse', function(){
      $(this).parent().find("i.fa").removeClass("fa-caret-right").addClass("fa-caret-down");
  });
  $(".collapse").on('hide.bs.collapse', function(){
      $(this).parent().find("i.fa").removeClass("fa-caret-down").addClass("fa-caret-right");
  });




});
