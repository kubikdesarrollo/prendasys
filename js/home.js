/* custom js */
$(document).ready(function() {
 

  // slideshow inicial

  $("#slideshow").owlCarousel({
      animateOut: 'fadeOut',
      animateIn: 'fadeIn',
      navigation : true, // Show next and prev buttons
      slideSpeed : 300,
      paginationSpeed : 400,
      nav: true,
      controls: true,
      items : 1,
      itemsMobile : true,
      loop:true,
      autoplay: true,
      autoplayTimeout: 5000,
      autoplayHoverPause: true,
      navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"]

  });


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax1"})
      .setTween("#parallax1 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);

  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax2"})
      .setTween("#parallax2 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);


  var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}});
  // build scenes
  new ScrollMagic.Scene({triggerElement: "#parallax3"})
      .setTween("#parallax3 > .image-parallax", {y: "80%", ease: Linear.easeNone})
      //.addIndicators()
      .addTo(controller);  
 

 /*
  var sequenceInterval = 150;
    window.sr = ScrollReveal({reset: false});
    // Custom reveal sequencing by container
    $('#home-section').each(function() {
      var sequenceDelay = 0;
      $(this).find('.anima').each(function() {
        sr.reveal(this, {
          delay: sequenceDelay
        });
        sequenceDelay += sequenceInterval;
      });
    });
*/

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#caracteristicas-block"})
        .setClassToggle("#img-equipo", "pulse") // add class toggle
        .addTo(controller);    

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#caracteristicas-block"})
        .setClassToggle("#info-equipo", "headShake") // add class toggle
        .addTo(controller);   

  

});
