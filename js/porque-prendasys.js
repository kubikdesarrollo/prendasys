$(document).ready(function() {

// ROI

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-roi"})
        .setClassToggle("#roi-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-roi"})
        .setClassToggle("#roi-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// PREM

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-prem"})
        .setClassToggle("#prem-img", "pulse") // add class toggle
        .addTo(controller);  
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-prem"})
        .setClassToggle("#prem-text", "visible") // add class toggle
        .addTo(controller);        

////////////////////////////////////////////////////////////////////////////////////////////////////////

// ANT

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-ant"})
        .setClassToggle("#ant-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-ant"})
        .setClassToggle("#ant-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// CANV

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-canv"})
        .setClassToggle("#canv-img", "pulse") // add class toggle
        .addTo(controller);   
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-canv"})
        .setClassToggle("#canv-text", "visible") // add class toggle
        .addTo(controller);      

////////////////////////////////////////////////////////////////////////////////////////////////////////

// CIEN

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-cien"})
        .setClassToggle("#cien-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-cien"})
        .setClassToggle("#cien-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// ACT

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-act"})
        .setClassToggle("#act-img", "pulse") // add class toggle
        .addTo(controller);  
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-act"})
        .setClassToggle("#act-text", "visible") // add class toggle
        .addTo(controller);      

////////////////////////////////////////////////////////////////////////////////////////////////////////

// ADU

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-adu"})
        .setClassToggle("#adu-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-adu"})
        .setClassToggle("#adu-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// INFC

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-infc"})
        .setClassToggle("#infc-img", "pulse") // add class toggle
        .addTo(controller);   
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-infc"})
        .setClassToggle("#infc-text", "visible") // add class toggle
        .addTo(controller);      

////////////////////////////////////////////////////////////////////////////////////////////////////////

// OPTP

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-optp"})
        .setClassToggle("#optp-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-optp"})
        .setClassToggle("#optp-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// SEG

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-seg"})
        .setClassToggle("#seg-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-seg"})
        .setClassToggle("#seg-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// VER2

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-ver2"})
        .setClassToggle("#ver2-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-ver2"})
        .setClassToggle("#ver2-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// ANDA

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-anda"})
        .setClassToggle("#anda-img", "pulse") // add class toggle
        .addTo(controller);    
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-anda"})
        .setClassToggle("#anda-text", "visible") // add class toggle
        .addTo(controller);      

////////////////////////////////////////////////////////////////////////////////////////////////////////

// IMPA

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-impa"})
        .setClassToggle("#impa-img", "pulse") // add class toggle
        .addTo(controller);
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-impa"})
        .setClassToggle("#impa-text", "visible") // add class toggle
        .addTo(controller);

////////////////////////////////////////////////////////////////////////////////////////////////////////

// MSINC

var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Expo.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-msinc"})
        .setClassToggle("#msinc-img", "pulse") // add class toggle
        .addTo(controller);   
var controller = new ScrollMagic.Controller({globalSceneOptions: {ease: Back.easeOut}});
new ScrollMagic.Scene({triggerElement: "#trigger-msinc"})
        .setClassToggle("#msinc-text", "visible") // add class toggle
        .addTo(controller);

});

