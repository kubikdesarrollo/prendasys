<?php
	session_start();
	include 'inc/config.php';
	include 'inc/globals.php';
	include 'inc/metatags.php';

	// origen externo
	$origne_url = explode('?origen=', $_SERVER['REQUEST_URI']);
	if($origne_url && !empty($origne_url[1]) && intval($origne_url[1]))
		$_SESSION['origen'] = intval( $origne_url[1] );
	if( empty($_SESSION['origen']) || ( empty($_SESSION['origen']) && !$_SESSION['origen'] ) )
		$_SESSION['origen'] = 12;
	//echo $_SESSION['origen']; die(1);
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<link rel="icon" href="favicon.png">
		<link rel="apple-touch-icon" href="apple-touch-icon-180px.png" />
	    <link rel="apple-touch-icon" sizes="72x72" href="apple-touch-icon-72px.png" />
	    <link rel="apple-touch-icon" sizes="114x114" href="apple-touch-icon-114px.png" />
	    <link rel="apple-touch-icon" sizes="144x144" href="apple-touch-icon-144px.png" />
		<title><?php echo($titulo); ?></title>
		<meta name="description" content="<?php echo($descripcion); ?>">
		<meta name="keywords" content="<?php echo($keywords); ?>">
		<!-- <meta name="copyright" content=""> -->
		<meta name="author" content="Kubik Digital Agency" />
		<meta name="robots" content="index, follow">
		<meta name="googlebot" content="index, follow">
		<meta http-equiv="content-language" content="ES">
		<meta name="Rating" content="General">
		<!-- OPENGRAPH -->
		<meta property="og:title" content="<?php echo($titulo); ?>">
		<meta property="og:type" content="website">
		<meta property="og:url" content="<?php echo($url); ?>">
		<meta property="og:image" content="<?php echo($fb_img); ?>"/>
		<meta property="og:image:type" content="image/jpeg" />
		<meta property="og:image:width" content="1200">
		<meta property="og:image:height" content="630">
		<meta property="og:description" content="<?php echo($descripcion); ?>">
		<!-- TWITTER CARD -->
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="">
		<meta name="twitter:title" content="<?php echo($titulo); ?>">
		<meta name="twitter:description" content="<?php echo($descripcion); ?>">
		<meta name="twitter:image:src" content="<?php echo($fb_img); ?>">
		<!-- COLOR NAVEGADOR -->
    	<meta name="theme-color" content="#ff6600" />
		<!-- BOOTSTRAP CORE CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet">
		<!-- FONT AWESOME -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/brands.css">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/fontawesome.css">
		<!-- OWL CAROUSEL -->
		<link rel="stylesheet" href="css/owl.carousel.min.css">		
	    <!-- MODERNIZR -->
	    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
		<!-- ADITIONAL GOOGLE-FONT -->
		<link href="https://fonts.googleapis.com/css2?family=Exo:wght@300;400;700;900&display=swap" rel="stylesheet">
		<!-- CUSTOM CSS -->
		<link href="css/animate.css" rel="stylesheet">
		<?php
		if($section == 'preguntas-frecuentes')
			echo '<link href="css/awesomplete.css" rel="stylesheet">';
		?>
		<link rel="stylesheet" href="css/main.css">
		<?php
			if(file_exists("css/".$section.".css"))
		        echo '<link rel="stylesheet" href="css/'. $section .'.css">';
		?>


<?php /*
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-161417796-1"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'UA-161417796-1');
		</script>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s)
		{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};
		if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
		n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];
		s.parentNode.insertBefore(t,s)}(window,document,'script',
		'https://connect.facebook.net/en_US/fbevents.js');
		fbq('init', '2383893375025251'); 
		<?php
		if($section == 'gracias-contacto' || $section == 'gracias-suscripcion'){
			echo "fbq('track', 'CompleteRegistration');";
		}else{
			echo "fbq('track', 'PageView');";
		} ?>
		</script>
		<!-- End Facebook Pixel Code -->
*/ ?>

		<!-- Start of HubSpot Embed Code -->
		<script type="text/javascript" id="hs-script-loader" async defer src="//js.hs-scripts.com/7853198.js"></script>
		<!-- End of HubSpot Embed Code -->

	</head>

  <body>

	<?php require_once "sections/tpl/header.php"; ?>

    <?php 
		if(file_exists("sections/".$section.".php")){
			require_once "sections/".$section.".php";
		}else{
			require_once "sections/404.php";
		}
	?>

    <?php require_once "sections/tpl/footer.php"; ?>

	<!-- JQUERY -->
	<!-- <script src="//code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script>window.jQuery || document.write('<script src="js/jquery-3.2.1.slim.min.js"><\/script>')</script> -->
	<!-- <script src="js/jquery-3.4.1.min.js"></script> -->
	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>	
	<!-- BOOTSTRAP -->
	<script src="js/popper.min.js" ></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <!-- SCROLLMAGIC -->
    <script src="js/lib/greensock/TweenMax.min.js"></script>
    <script src="js/scrollmagic/minified/ScrollMagic.min.js"></script>
    <script src="js/scrollmagic/uncompressed/plugins/animation.gsap.js"></script>
    <!-- OWL CAROUSEL -->
    <script src="js/owl.carousel.min.js"></script>
    <!-- EASING -->
    <script src="js/jquery.easing.1.3.js"></script>
    <!-- CUSTOM -->
    <script src="js/jquery.mask.min.js"></script>
    <?php
	if($section == 'preguntas-frecuentes')
		echo '<script src="js/awesomplete.js"></script>';
	?>
    <script src="js/main.js"></script>

	<?php
	/*
	if($section == 'home')
		echo '<script src="js/scrollreveal.js"></script>';*/
	if($section == 'caracteristicas')
		echo '<script src="js/scrollreveal.js"></script>';
	if($section == 'requerimientos-tecnicos')
		echo '<script src="js/scrollreveal.js"></script>';
	

	if(file_exists("js/".$section.".js"))
		echo '
		<script src="js/'. $section .'.js"></script>';


	?>

	<script src="https://marketingapi.bisoft.com.mx/cdn/js/BisoftWidgets.js">
	</script>
	<script>
		bisoft.useFacebookPixel({ pixelId: '288868515277428', event: 'PageView' });
	</script>

  </body>
</html>