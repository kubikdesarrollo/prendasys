<?php
$vacios=$this->row("SELECT count(*) as cuantos FROM sucdistribuidores WHERE latitude='' OR longitude='' OR latitude IS NULL OR longitude IS NULL OR latitude='0' OR longitude='0'");
$esho.='<div class="btn-group pull-right col-lg-12" style="padding-bottom:20px;border:1px solid #ddd;margin-bottom:30px">
<div class="col-sm-12">IMPORTAR EXPORTAR DISTRIBUIDORES</div>
<a class="btn btn-info" style="margin-right:10px;display:inline-block" href="" onclick="return respaldadistribuidores()">Respaldar <i class="icon-paste"></i></a>
<a class="btn btn-danger" style="margin-right:10px;display:inline-block" href="" onclick="return restauradistribuidores()">Restaurar <i class="icon-refresh"></i></a>
<a class="btn btn-primary" style="margin-right:10px;display:inline-block" href="" onclick="return importadistribuidores()" id="importbtn">Importar <i class="icon-cloud-upload"></i></a>
<a class="btn btn-default" style="margin-right:10px;display:inline-block" href="scripts/downdistribuidores.php" target="_blank">Exportar <i class="icon-cloud-download"></i></a>
<a class="btn btn-default" style="margin-right:10px;" href="" onclick="return buscaubicaciones()" id="searchloc">Completar ubicaciones <i class="icon-search"></i></a>
</div>
<div class="col-lg-12 form-group" style="display:none;background-color:#ddd;padding:10px" id="importfilediv">
<form action="scripts/importdistribuidor.php" method="POST" enctype="multipart/form-data">
<div class="col-sm-6">Selecciona el CSV <input type="file" name="importfile"></div><div class="col-sm-6"> <button type="submit" class="btn btn-danger">Importar Distribuidores</button></div><div class="clearfix"></div>
</form>
</div>
<div class="col-lg-12 form-group" style="display:none;background-color:#ddd;padding:10px" id="searchlocdiv">
    <h3 class="green">Registros sin geoposición: '.$vacios["cuantos"].'</h3>	
    <progress id="progreso" value="0" max="100" style="width:100%;margin-bottom:10px">
        <div class="progress-bar"> <span style="width: 0%">0%</span></div>
    </progress>
    <div class="clear"></div>
    <input type="button" value="Procesar" style="cursor:pointer" id="btnsearchloc" class="btn btn-success" data-seccion="sucdistribuidores" data-registros="'.intval(@$vacios["cuantos"]).'">		
    <div class="clear"></div>
    <div id="resultlog"></div>
</div>
';
if (!empty($_REQUEST["saved"])){
    $esho.='<script>alert("Distribuidores importadas")</script>';
}
?>