<div id="d-descarga-reporte"> 
    <div class="rol">
    	<div class="col-md-3">
        	<strong>Rango de fechas: </strong>
        </div>
        <div class="col-md-3">
        	<strong>Inicio: </strong><input class="form-control" type="date" id="fecha_ini">
        </div>
        <div class="col-md-3">
        	<strong>Fin: </strong><input class="form-control" type="date" id="fecha_fin">
        </div>
        <div class="col-md-3">
	        <strong>Descargables: </strong>
	        <a class="btn btn-info brep" type="button" target="_blank" href="./reportes.php?token=fd89e7fd7&tipo=sugerencias">Dercargar Reporte</a>
	    </div>
    </div>
</div>

<script type="text/javascript">
$(document).ready(function(){
    $("#fecha_ini, #fecha_fin").change(function(){
        var dini = $("#fecha_ini").val();
        var dfin = $("#fecha_fin").val();
        $(".brep").each(function(){
            $(this).attr("href", "./reportes.php?token=fd89e7fd7&tipo=sugerencias&ini="+ dini +"&fin="+ dfin) ;
        });
    });
});
</script>

<style type="text/css">
#d-descarga-reporte{
	position: absolute;
	right: 3%;
	top: 10em;
}
@media screen and (max-width: 991px){
	#d-descarga-reporte{
		padding-top: 6em;
		position: relative;
		right: auto;
		top: auto;
	}
}
</style>