<?php
/*ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
if($_REQUEST['token']=='fd89e7fd7'){
	if(empty($_REQUEST['tipo']))
		$_REQUEST['tipo'] = 'contacto';

	$dini = '';
	if(!empty($_REQUEST['ini']))
		$dini = $_REQUEST['ini'];

	$dfin = '';
	if(!empty($_REQUEST['fin']))
		$dfin = $_REQUEST['fin'];

	$wheredate = '';
	if($dini || $dfin){
		if($dini && $dfin){
			$wheredate = " AND date BETWEEN '". $dini ." 00:00:00' AND '". $dfin ." 23:59:59' ";
		}elseif($dini && !$dfin){
			$wheredate = " AND date >= '". $dini ." 00:00:00' ";
		}elseif(!$dini && $dfin){
			$wheredate = " AND date <= '". $dfin ." 23:59:59' ";
		}
	}

	include 'config/configuracion_global.php';  // solo entra si esta logueado
	header("Content-type: text/csv");
	header("Content-Disposition: attachment; filename=Reporte-". $_REQUEST['tipo'] ."-". date("Y-m-d") .".csv");
	header("Pragma: no-cache");
	header("Expires: 0");

	function textcsv($text1){
		if( strpos($text1, ',') === false && strpos($text1, '"') === false ){}else{
			$text1 = '"'. str_replace('"', '""', $text1) .'"';
		}
		return $text1;
	}

	$sep = ',';
	if($_REQUEST['tipo']=='contacto'){
	
		$datimp1 = 'Fecha'. $sep .'Nombre'. $sep .'Apellido'. $sep .'Farmacia'. $sep .'Telefono'. $sep .'Email'. $sep .'Pais'. $sep .'Estado'. $sep .'Ciudad'. $sep .'Comentarios'. $sep .'Boletin'. $sep .'Origen'. PHP_EOL; 

		$query1	=	"SELECT * FROM `contact` WHERE 1 ". $wheredate ." ORDER BY date DESC";
		$res1   =   $CPanel->query($query1);
		foreach ($res1 as $record1) {
			//echo '<pre>'. print_r($record1, 1) .'</pre>'; die(1);

			$datimp1 .= textcsv($record1['date']) . $sep;
			$datimp1 .= textcsv($record1['name']) . $sep;
			$datimp1 .= textcsv($record1['last_name']) . $sep;
			$datimp1 .= textcsv($record1['farmacy']) . $sep;
			$datimp1 .= textcsv($record1['phone']) . $sep;
			$datimp1 .= textcsv($record1['email']) . $sep;
			$datimp1 .= textcsv($record1['country']) . $sep;
			$datimp1 .= textcsv($record1['state']) . $sep;
			$datimp1 .= textcsv($record1['city']) . $sep;
			$datimp1 .= textcsv($record1['comments']) . $sep;
			$datimp1 .= textcsv($record1['bulletin']) . $sep;
			$datimp1 .= textcsv($record1['origin']) . $sep;
			$datimp1 .= PHP_EOL;

		}
	}elseif($_REQUEST['tipo']=='suscripciones'){
	
		$datimp1 = 'Fecha'. $sep .'Nombre'. $sep .'Apellido'. $sep .'Email'. $sep .'Origen'. PHP_EOL; 

		$query1	=	"SELECT * FROM `register_suscripciones` WHERE 1 ". $wheredate ." ORDER BY date DESC";
		$res1   =   $CPanel->query($query1);
		foreach ($res1 as $record1) {
			//echo '<pre>'. print_r($record1, 1) .'</pre>'; die(1);

			$datimp1 .= textcsv($record1['date']) . $sep;
			$datimp1 .= textcsv($record1['name']) . $sep;
			$datimp1 .= textcsv($record1['last_name']) . $sep;
			$datimp1 .= textcsv($record1['email']) . $sep;
			$datimp1 .= textcsv($record1['origin']) . $sep;
			$datimp1 .= PHP_EOL;

		}
	}elseif($_REQUEST['tipo']=='farmacias'){
		$datimp1 = 'Fecha'. $sep .'Nombre'. $sep .'Apellido'. $sep .'Farmacia'. $sep .'Telefono'. $sep .'Email'. $sep .'Pais'. $sep .'Estado'. $sep .'Ciudad'. $sep .'Comentarios'. $sep .'Boletin'. $sep .'Origen'. PHP_EOL; 

		$query1	=	"SELECT * FROM `register_farmacias` WHERE 1 ". $wheredate ." ORDER BY date DESC";
		$res1   =   $CPanel->query($query1);
		foreach ($res1 as $record1) {
			//echo '<pre>'. print_r($record1, 1) .'</pre>'; die(1);

			$datimp1 .= textcsv($record1['date']) . $sep;
			$datimp1 .= textcsv($record1['name']) . $sep;
			$datimp1 .= textcsv($record1['last_name']) . $sep;
			$datimp1 .= textcsv($record1['farmacy']) . $sep;
			$datimp1 .= textcsv($record1['phone']) . $sep;
			$datimp1 .= textcsv($record1['email']) . $sep;
			$datimp1 .= textcsv($record1['country']) . $sep;
			$datimp1 .= textcsv($record1['state']) . $sep;
			$datimp1 .= textcsv($record1['city']) . $sep;
			$datimp1 .= textcsv($record1['comments']) . $sep;
			$datimp1 .= textcsv($record1['bulletin']) . $sep;
			$datimp1 .= textcsv($record1['origin']) . $sep;
			$datimp1 .= PHP_EOL;

		}

	}elseif($_REQUEST['tipo']=='distribuidores'){
		$datimp1 = 'Fecha'. $sep .'Nombre'. $sep .'Apellido'. $sep .'Compañia'. $sep .'Perfil'. $sep .'Telefono'. $sep .'Email'. $sep .'Pais'. $sep .'Estado'. $sep .'Ciudad'. $sep .'Comentarios'. $sep .'Boletin'. $sep .'Origen'. PHP_EOL; 

		$query1	=	"SELECT * FROM `register_distribuidores2` WHERE 1 ". $wheredate ." ORDER BY date DESC";
		$res1   =   $CPanel->query($query1);
		foreach ($res1 as $record1) {
			//echo '<pre>'. print_r($record1, 1) .'</pre>'; die(1);

			$datimp1 .= textcsv($record1['date']) . $sep;
			$datimp1 .= textcsv($record1['name']) . $sep;
			$datimp1 .= textcsv($record1['last_name']) . $sep;
			$datimp1 .= textcsv($record1['company']) . $sep;
			$datimp1 .= textcsv($record1['profile']) . $sep;
			$datimp1 .= textcsv($record1['phone']) . $sep;
			$datimp1 .= textcsv($record1['email']) . $sep;
			$datimp1 .= textcsv($record1['country']) . $sep;
			$datimp1 .= textcsv($record1['state']) . $sep;
			$datimp1 .= textcsv($record1['city']) . $sep;
			$datimp1 .= textcsv($record1['comments']) . $sep;
			$datimp1 .= textcsv($record1['bulletin']) . $sep;
			$datimp1 .= textcsv($record1['origin']) . $sep;
			$datimp1 .= PHP_EOL;

		}

	}elseif($_REQUEST['tipo']=='sugerencias'){
		$datimp1 = 'Fecha'. $sep .'Nombre'. $sep .'Telefono'. $sep .'Email'. $sep .'Pais'. $sep .'Estado'. $sep .'Comentarios'. $sep .'Origen'. PHP_EOL; 

		$query1	=	"SELECT * FROM `suggestions` WHERE 1 ". $wheredate ." ORDER BY date DESC";
		$res1   =   $CPanel->query($query1);
		foreach ($res1 as $record1) {
			//echo '<pre>'. print_r($record1, 1) .'</pre>'; die(1);

			$datimp1 .= textcsv($record1['date']) . $sep;
			$datimp1 .= textcsv($record1['name']) . $sep;
			$datimp1 .= textcsv($record1['phone']) . $sep;
			$datimp1 .= textcsv($record1['email']) . $sep;
			$datimp1 .= textcsv($record1['country']) . $sep;
			$datimp1 .= textcsv($record1['state']) . $sep;
			$datimp1 .= textcsv($record1['comments']) . $sep;
			$datimp1 .= textcsv($record1['origin']) . $sep;
			$datimp1 .= PHP_EOL;

		}

	}


    $datimp1 = utf8_decode($datimp1);
    //echo '<table border="1"><tr><td>' .str_replace(array($sep,PHP_EOL), array('</td><td>', '</td></tr><tr><td>'), $datimp1) . '</td></tr></table>';

	$file = fopen("php://output", "w");
	$a = fwrite($file, $datimp1);
	fclose($file);
}
?>
