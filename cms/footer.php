

  <div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalAlert" class="modal fade in" style="display: none">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">Mensaje</h4>
              </div>
              <div class="modal-body" id="modalAlertbody">
              </div>
              <div class="modal-footer">
                  <button class="btn btn-danger" data-dismiss="modal" aria-hidden="true">Cerrar</button>
              </div>
          </div>
      </div>
  </div>
  <div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalConfirm" class="modal fade in" style="display: none">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">ALERTA</h4>
              </div>
              <div class="modal-body" id="modalConfirmbody">
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-warning" onclick="actiondeletemultiple('<?php echo @$tabla?>','<?php echo @$indice?>')"> Confirmar</button>
              </div>
          </div>
      </div>
  </div>

  <div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalConfirmaut" class="modal fade in" style="display: none">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">ALERTA</h4>
              </div>
              <div class="modal-body" id="modalConfirmbodyaut">
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                  <button type="button" class="btn btn-warning" onclick="actionautoriza()"> Confirmar</button>
                  <input type="hidden" id="aut_seccion">
                  <input type="hidden" id="aut_id">
              </div>
          </div>
      </div>
  </div>

  <div aria-hidden="false" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modalConfirmsubtabla" class="modal fade in" style="display: none">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header">
                  <button aria-hidden="true" data-dismiss="modal" class="close" type="button">×</button>
                  <h4 class="modal-title">ALERT</h4>
              </div>
              <div class="modal-body" id="modalConfirmsubtablab">
              </div>
              <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-warning" onclick="actiondelete()"> Confirm</button>
                  <input type="hidden" id="hiddendelete" value="">
                  <input type="hidden" id="hiddentabledelete" value="">
                  <input type="hidden" id="hiddenidtabledelete" value="">
                  <input type="hidden" id="hiddencualform" value="">
              </div>
          </div>
      </div>
  </div>