<?php
    $descargas=$this->query("SELECT * FROM Cat_soporte WHERE itemcode=:item",array("item"=>@$datos["itemcode"]));
    $tipoSoporte=$this->query("SELECT * FROM Cat_tipoSoporte");
?>

<div class="form-group">
    <label class="col-lg-2 control-label">Soporte</label>
    <div class="alert alert-info fade in col-xs-10"><p>Archivos válidos: doc,docx,pdf,jpg,jpeg,zip,rar,xls,xlsx,ppt,pptx</p></div><div class="col-xs-2"></div>
    <div class="col-lg-10" id="contentDowns" style="max-height:200px">
        <?php for($k=0;$k<=count($descargas);$k++){
            $descarga=@$descargas[$k]; $lastdownload=($k==(count($descargas)))?true:false;
        ?>
        <div class="col-sm-12 <?php echo ($lastdownload)?'lastdownload':'bloqueurl'?>">
            <div class="col-sm-9">
                <input type="file" class="form-control filedoc" name="downloads[]"><input type="hidden" class="fileold" name="fileold[]" value="<?php echo @$descarga["documento"]?>">
                <?php if (!empty($descarga["documento"])){?>
                    <small>Archivo actual: <a href="../descargables/<?php echo $descarga["documento"]?>" target="blank" class="col-sm-12"> <?php echo $descarga["documento"]?></a></small>
                <?php } ?>
            </div>
            <div class="col-sm-2">
                <select name="downloadtype[]" class="form-control inputcolor">
                <?php foreach($tipoSoporte as $kc=>$type){?>
                <option value="<?php echo $type["id"]?>" <?php if($type["id_tipoSoporte"]==@$descarga["id_tipoSoporte"]){?>selected="selected"<?php }?>><?php echo $type["Descripcion"]?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-danger pull-right" onclick="delDocumend(this)"><i class="icon-remove"></i></button>
            </div>
            <div class="clear" style="padding-bottom:15px"></div>
        </div>    
        <?php } ?>
    </div>
    <div class="clear"></div>
    <div class="col-sm-12" style="padding-top:15px">
        <button type="button" id="agregadownload" class="btn btn-info pull-right">+Agregar descargable</button>
    </div> 
</div>
<style>.lastdownload{display:none;}</style>
<script type="text/javascript">
   $(document).ready(function () {
       $("#agregadownload").click(function(){
           var newcontentdown = $('<div>').addClass('col-sm-12').append($(".lastdownload").html());
           newcontentdown.find(".filedoc").val('');
           newcontentdown.find(".fileold").val('');
           var lastele=$("#contentDowns").append(newcontentdown);
           
       });
   });
   function delDocumend(ele){
       $(ele).parent().parent().remove();
   }
</script>