<?php
    $fields=$this->row("SELECT Cat_Articulos.*,categories.title as categoria,lineas.descripcion_linea as linea FROM Cat_Articulos LEFT JOIN categories ON Cat_Articulos.id_categoria=categories.id LEFT JOIN lineas ON Cat_Articulos.id_lineaProducto=lineas.id WHERE Cat_Articulos.itemcode='".@$datos["itemcode"]."'");
?>

<div class="form-group">
    <label class="col-lg-2 control-label">Producto</label>
    <div class="col-lg-10">
        <?php echo '<b>'.$fields["itemcode"]."</b> / <span class='badget-info'>".$fields["titulo_articulo"]."</span> <i>".$fields["categoria"]." / ".$fields["linea"].'</i>'?>
    </div>
    <div class="clear"></div>
    
</div>
