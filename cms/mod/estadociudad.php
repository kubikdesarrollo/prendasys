<?php
  $listStates=$this->select("SELECT * FROM kubik_estados ORDER BY name");
  if (!empty($datos["state_id"])){
    $listCities=$this->select("SELECT * FROM kubik_ciudades WHERE id_state='".$datos["state_id"]."' ORDER BY name");
  }
?>
        <div class="form-group">
              <label class="col-lg-2 control-label">Estado</label>
              <div class="col-lg-10">
                <select class="form-control" name="state_id" id="estadolist" onchange="cambiarXciudad(this.value)">
                <option value="">Selecciona un estado</option>
                <?php foreach($listStates as $stateitem){?>
                    <option value="<?php echo $stateitem->id_estado?>" <?php if (@$datos["state_id"]==$stateitem->id_estado){ echo 'selected="selected"';}?>><?php echo $stateitem->name?></option>
                <?php } ?>  
                </select>
              </div>
        </div> 


        <div class="form-group">
              <label class="col-lg-2 control-label">Ciudad</label>
              <div class="col-lg-10">
                <select class="form-control" name="city_id" id="ciudadlist">
                <?php if (!empty($listCities)){foreach($listCities as $cityitem){?>
                    <option value="<?php echo $cityitem->geonameid?>" <?php if (@$datos["city_id"]==$cityitem->geonameid){ echo 'selected="selected"';}?>><?php echo $cityitem->name?></option>
                <?php } }?>  
                </select>
              </div>
        </div>      

<script type="text/javascript">
  function cambiarXciudad(idEstate){
    $("#ciudadlist").html("");
    $.ajax({
    type: "POST",
    url: "scripts/ciudades.php?estado="+idEstate,
    data: {  }
    })
    .done(function( msg ) {    
            $("#ciudadlist").html(msg);
    });
  }
</script>
