<?php

	
	$rows_items_detail = $CPanel->select("
		SELECT * 
		FROM items 
		WHERE `type` = 'newsletter'");

	//echo '<pre>'. print_r($rows_items_detail, 1) .'</pre>';
	if($rows_items_detail){
		$rows_items_detail = $rows_items_detail[0];

?>
<section class="wrapper">
	<div class="row">

		<form name="form" class="form-horizontal tasi-form" id="form" enctype="multipart/form-data" action="scripts/guarda_suscripciones.php?seccion=suscripciones&tabla=items&indice=id" method="post" role="form" novalidate="novalidate">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
					BLOQUE DE SUSCRIPCIONES (NEWSLETTER)
					</header>
					<div class="panel-body">
						<button class="btn btn-success pull-right" type="submit"><i class="icon-save"></i> GUARDAR</button>
					</div>
					<div class="panel-body">
						<?php 

							bloqueinput('Titulo','title',$rows_items_detail->title,true,"",false);
							bloqueinput('Titulo (ingles)','title_en',$rows_items_detail->title_en,false,"",false);

							bloqueinput('Subtitulo','details',$rows_items_detail->details,true,"",false);
							bloqueinput('Subtitulo (ingles)','details_en',$rows_items_detail->details_en,true,"",false);
							
							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,2000, 'Logo'), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/suscribe" , "customsizes"=>array(),"customfields"=>array() );
							bloqueimagen($configuracion1, array('image' => $rows_items_detail->image, "id" => $rows_items_detail->id) ,'items','id','suscripciones');

							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("url",2000,2000, 'Foto 1 (polaroid)'), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/suscribe" , "customsizes"=>array(),"customfields"=>array() );
							bloqueimagen($configuracion1, array('image' => $rows_items_detail->url, "id" => $rows_items_detail->id) ,'items','id','suscripciones');
						
							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("url_en",2000,2000, 'Foto 2 (polaroid)'), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/suscribe" , "customsizes"=>array(),"customfields"=>array() );
							bloqueimagen($configuracion1, array('image' => $rows_items_detail->url_en, "id" => $rows_items_detail->id) ,'items','id','suscripciones');

							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("url_target",2000,2000, 'Imagen fija para mobiles'), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/suscribe" , "customsizes"=>array(),"customfields"=>array() );
							bloqueimagen($configuracion1, array('image' => $rows_items_detail->url_target, "id" => $rows_items_detail->id) ,'items','id','suscripciones');

						?>
					</div>
				</section>

			</div>
			<input type="hidden" name="published" value="on"> 
			<input type="hidden" name="type" value="<?php echo $rows_items_detail->type; ?>"> 
			<input type="hidden" class="indice" name="id" id="id" value="<?php echo $rows_items_detail->id; ?>"> 
			<input type="hidden" name="editar" id="editar" value="1">
			<input type="hidden" name="seccion" id="seccion" value="suscripciones">
			<input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>">
		</form>
	</div>

</section>

<?php 
	}
	
?>