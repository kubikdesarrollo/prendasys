<?php
	$idproperty=!empty($_REQUEST["id_edit"])?intval($_REQUEST["id_edit"]):0;

	if (! $idproperty){


		$rows_items = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE 1 
			ORDER BY ABS(priority) ASC");
		//echo '<pre>'. print_r($rows_items, 1) .'</pre>';

		$list_tiems = '';
		$list_types = array('campain',  'world-cuadra', 'celebrities-headers', 'press-headers', 'news-headers','editorial-headers','videos-headers','museum-headers','commitments-headers'); //'campain-mobil',
		foreach ($rows_items as $key2 => $row_item) {
			if( in_array($row_item->type, $list_types) ){
				switch ($row_item->type) {
					case 'campain':  $type_text = 'Cabecera de Campañas'; break;
					//case 'campain-mobil':  $type_text = 'Cabecera de Campañas Mobiles'; break;
					case 'world-cuadra':  $type_text = 'Cabecera Mundo Cuadra'; break;
					case 'celebrities-headers':  $type_text = 'Cabecera Personalidades'; break;
					case 'press-headers':  $type_text = 'Cabecera Prensa'; break;
					case 'news-headers':  $type_text = 'Cabecera Noticias'; break;
					case 'editorial-headers':  $type_text = 'Cabecera Editorial'; break;
					case 'videos-headers':  $type_text = 'Cabecera videos'; break;
					case 'museum-headers':  $type_text = 'Cabecera Museo'; break;
					case 'commitments-headers':  $type_text = 'Cabecera Compromisos'; break;
				}

				$list_tiems .= '
				<tr id="DTrow_11" class="odd"><td class="">'. $row_item->title .'</td><td class="">'. $type_text .'</td><td class=""><div style="width:120px"><a class="btn btn-primary btn-xs" style="margin:0 20px" href="admin.php?seccion=cabecera_mundo&amp;accion=editar&amp;id_edit='. $row_item->id .'"><i class="icon-pencil"></i></a></div></td></tr>'; //<td class=""><div aling="center">'. ($row_item->published?'<span class="badge bg-primary">Autorizado</span>':'') .'</div></td>
			}
		}
?>
<section class="wrapper">
	<section class="panel">
		<header class="panel-heading">
			CABECERAS MUNDO CUADRA
		</header>
		<div class="panel-body">
			<div class="adv-table editable-table ">

				<div class="space15"></div>
				<div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">

					<table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
						<thead>
							<tr role="row"><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Descripción</th><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Tipo</th><!--<th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Publicado</th>--><th style="width: 127px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th></tr>
						</thead>
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php echo $list_tiems; ?>
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</section>
</section>

<style type="text/css">
	#editable-sample_processing{display: none !important;}
</style>

<?php 
	} else{ 
	
		$rows_items_detail = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE id = ". $idproperty);

		//echo '<pre>'. print_r($rows_items_detail, 1) .'</pre>';
		if($rows_items_detail){
			$rows_items_detail = $rows_items_detail[0];

			switch ($rows_items_detail->type) {
				case 'campain':  $type_text = 'Cabecera de Campañas'; break;
				//case 'campain-mobil':  $type_text = 'Cabecera de Campañas Mobiles'; break;
				case 'world-cuadra':  $type_text = 'Cabecera Mundo Cuadra'; break;
				case 'celebrities-headers':  $type_text = 'Cabecera Personalidades'; break;
				case 'press-headers':  $type_text = 'Cabecera Prensa'; break;
				case 'news-headers':  $type_text = 'Cabecera Noticias'; break;
				case 'editorial-headers':  $type_text = 'Cabecera Editorial'; break;
				case 'videos-headers':  $type_text = 'Cabecera videos'; break;
				case 'museum-headers':  $type_text = 'Cabecera Museo'; break;
				case 'commitments-headers':  $type_text = 'Cabecera Compromisos'; break;
			}
?>
<section class="wrapper">
	<div class="row">

		<form name="form" class="form-horizontal tasi-form" id="form" enctype="multipart/form-data" action="scripts/guarda_items.php?seccion=cabecera_mundo&tabla=items&indice=id" method="post" role="form" novalidate="novalidate">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
					CABECERAS MUNDO CUADRA - <?php echo $type_text; ?>
					</header>
					<div class="panel-body"><button class="btn btn-default " type="button" onclick="javascript:document.location.href='admin.php?seccion=cabecera_mundo&last=1'"><i class="icon-long-arrow-left"></i> REGRESAR</button>
						<button class="btn btn-success pull-right" type="submit"><i class="icon-save"></i> GUARDAR</button>
					</div>
					<div class="panel-body">
						<?php 

						if($rows_items_detail->type == 'campain'){
							bloqueinput('Año','title_en',$rows_items_detail->title_en,true,"",false);
							bloquetextarea('Estaciones','details',$rows_items_detail->details);
							bloquetextarea('Estaciones (ingles)','details_en',$rows_items_detail->details_en);
						}
						//echo '<pre>'.print_r($rows_items_detail, 1) .'</pre>';
						if(in_array($rows_items_detail->type, array('world-cuadra', 'campain','celebrities-headers','press-headers' , 'news-headers','editorial-headers','videos-headers','museum-headers','commitments-headers') ) ){ //, 'campain-mobil'
							switch ($rows_items_detail->type) {
								case 'campain': $input_url = 'campana'; break;
								//case 'campain-mobil':  $input_url = 'campana'; break;
								case 'world-cuadra': $input_url = 'mundo-cuadra'; break;
								case 'celebrities-headers':  $input_url = 'personalidades'; break;
								case 'press-headers':  $input_url = 'prensa'; break;

								case 'news-headers':  $input_url = 'noticias'; break;
								case 'editorial-headers':  $input_url = 'editorial'; break;
								case 'videos-headers':  $input_url = 'videos'; break;
								case 'museum-headers':  $input_url = 'museo'; break;
								case 'commitments-headers':  $input_url = 'compromisos'; break;
							}

							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,2000,"Imagen"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/". $input_url , "customsizes"=>array(),"customfields"=>array() );

							bloqueimagen($configuracion1, array('image' => $rows_items_detail->image, "id" => $rows_items_detail->id) ,'items','id','cabecera_mundo');

							$configuracion2 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("url",2000,2000,"Imagen movil"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/". $input_url , "customsizes"=>array(),"customfields"=>array() );

							bloqueimagen($configuracion2, array('image' => $rows_items_detail->url, "id" => $rows_items_detail->id) ,'items','id','cabecera_mundo');
						}

						?>
					</div>
				</section>

			</div>
			<input type="hidden" name="published" value="on"> 
			<input type="hidden" name="type" value="<?php echo $rows_items_detail->type; ?>"> 
			<input type="hidden" class="indice" name="id" id="id" value="<?php echo $rows_items_detail->id; ?>"> 
			<input type="hidden" name="editar" id="editar" value="1">
			<input type="hidden" name="seccion" id="seccion" value="cabecera_mundo">
			<input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>">
		</form>
	</div>

</section>

<?php 
		}
	} 
?>