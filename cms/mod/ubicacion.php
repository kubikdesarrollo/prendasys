<div class="form-group ">
    <label class="col-lg-2 control-label">Pais</label>
    <div class="col-lg-10">
        <select name="id_country" id="id_country" class="form-control">
            <option value="">SELECCIONE UN PAIS</option>
            <?php
            $sql="SELECT * FROM countries";
            $res=$this->query($sql);
            foreach ($res as $res1){
                echo '<option value="'.$res1['numcode'].'" data-code="'.$res1['iso'].'" '. (@$datos['id_country'] ==$res1['numcode']?'selected':'') .'>'.$res1['printable_name'].'</option>';
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group ">
    <label class="col-lg-2 control-label">Estado</label>
    <div class="col-lg-10">
        <select name="id_state" id="id_state" class="form-control">
            <option value="">SELECCIONE UN ESTADO</option>
            <?php
            $sql="SELECT * FROM states";
            $res=$this->query($sql);
            foreach ($res as $res1){
                echo '<option value="'.$res1['id'].'" data-code="'.$res1['code'].'" '. (@$datos['id_state'] ==$res1['id']?'selected':'') .'>'.$res1['name'].'</option>';
            }
            ?>
        </select>
    </div>
</div>

<div class="form-group ">
    <label class="col-lg-2 control-label">Ciudad</label>
    <div class="col-lg-10">
        <input type="text" name="city" id="city" class="form-control" value="<?php echo @$datos['city']; ?>" />
    </div>
</div>

<div class="form-group ">
    <label class="col-lg-2 control-label">Latitud</label>
    <div class="col-lg-10">
        <input type="text" name="latitude" id="latitude" class="form-control" value="<?php echo !empty($datos['latitude'])?($datos['latitude']*1):''; ?>" />
    </div>
</div>

<div class="form-group ">
    <label class="col-lg-2 control-label">Longitud</label>
    <div class="col-lg-10">
        <input type="text" name="longitude" id="longitude" class="form-control" value="<?php echo !empty($datos['longitude'])?($datos['longitude']*1):''; ?>" />
    </div>
</div>

<div class="form-group">
    <label class="col-lg-2 control-label">Seleccione la ubicaci&oacute;n</label>
    <div class="col-lg-10">
        <div id="mapitems" style="width: 100%; height: 540px"></div>
    </div>
</div>

<script type="text/javascript" src="//maps.google.com/maps/api/js?key=AIzaSyBp_BsZgL3x7igZwCCToCcLDUJ6CLJPiIo"></script>
<script src="libs_js/if_gmap.js"></script>