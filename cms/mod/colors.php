<?php
    $colors=$this->query("SELECT * FROM colours order by title");
    $currentcolor=$this->query("SELECT * FROM colour_product WHERE itemcode=:item",array("item"=>@$datos["itemcode"]));

    function buscacolor($cual,$lista){
        foreach($lista as $li){
            if ($li["id_color"]==$cual) return true;
        }
        return false;
    }
?>

<div class="form-group">
    <label class="col-lg-2 control-label">Seleccione los colores</label>
    <div class="col-lg-10" style="max-height:200px;overflow:auto">
        <input type="text" class="form-control" placeholder="Buscar color" id="filtrocolor"><br>
        <?php foreach($colors as $k=>$color){?>
            <div class="form-check colorvalue" data-title="<?php echo str_replace('"','',$color["title"])?>" style="width:48%;display:inline-block">
        <input type="checkbox" class="form-check-input" id="checkcolor_<?php echo $k?>" name="colorsproduct[]" value="<?php echo $color["id"]?>" <?php if (buscacolor($color["id"],$currentcolor)){?>checked="checked"<?php } ?>>
                <label class="form-check-label" for="checkcolor_<?php echo $k?>"><span class="mycolor" style="background-color:<?php echo $color["hexa"]?>"></span><?php echo $color["title"]?></label>
            </div>
        <?php } ?>
    </div>
</div>

<style>
.mycolor{display:inline-block;margin-left:20px;margin-right:20px;height:20px;width:20px}
</style>

<script type="text/javascript">
   $(document).ready(function () {
       $('#filtrocolor').keyup(function () {  
           var buscado = $("#filtrocolor").val().toLowerCase();
           $(".colorvalue").each(function(){
                if (buscado!=''){
                    current = $(this).data("title").toLowerCase(); 
                    if (current.indexOf(buscado)!=-1)
                        $(this).show();
                    else
                        $(this).hide();
                } else {
                    $(this).show();
                }
           });
       });
   });
</script>