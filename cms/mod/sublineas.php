<?php
    $sublineas=$this->query("SELECT * FROM sublineas order by descripcion_linea");
    function buscasublinea($cual,$lista){
        foreach($lista as $li){
            if ($li["id_tech_spec"]==$cual) return true;
        }
        return false;
    }
?><div class="form-group"><label class="col-lg-2 control-label">Sublinea</label>
    <div class="col-lg-10">
        <select name="id_sublinea" id="id_sublinea" class="form-control">
            <option value="" data-linea="">Selecciona una</option>
            <?php foreach( $sublineas as $sublinea){?>
            <option value="<?php echo  $sublinea["id"]?>" data-linea="<?php echo  $sublinea["id_linea"]?>" <?php if(@$datos["id_sublinea"]==$sublinea["id"]){?>selected="selected"<?php } ?>><?php echo  $sublinea["descripcion_linea"]?></option>
            <?php } ?>
        </select>
    </div>
</div>

<script>
function cargasublinea(lin){
    $("#id_sublinea option").each(function(){ 
        if ($(this).data("linea").indexOf("|"+lin+"|")!==-1 || $(this).data("linea")==""){
            $(this).show();
        } else {
            $(this).hide();
        }
    });
}
$(function(){
    var lin=$("#id_lineaProducto").val();
    $("#id_lineaProducto").change(function(){
        $("#id_sublinea").val("");
        cargasublinea($("#id_lineaProducto").val());
    });

    cargasublinea(lin);
});
</script>