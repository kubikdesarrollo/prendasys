<div class="panel-body">
    <header class="panel-heading">
      SELECCIONE LA UBICACI&Oacute;N 
    </header>
    <?php
        bloqueinput("Latitud","latitud",@$datos['latitud']);
        bloqueinput("Longitud","longitud",@$datos['longitud']);
    ?>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&key=AIzaSyBp_BsZgL3x7igZwCCToCcLDUJ6CLJPiIo"></script>
    <script src="js/if_gmap.js"></script>


    
    <div class="row">
        <div class="form-group">
              <label class="col-lg-2 control-label"></label>
              <div class="col-lg-10">
                  <div>De click sobre el mapa para activarlo</div>
                  <dl id="maparea">
                        <div id="capaantieventos" onclick="liberarmapa()" style="width: 100%;height: 540px;position: absolute;cursor: pointer;z-index:2"></div>
                        <div id="mapitems" style="width: 100%; height: 540px"></div>
                  </dl>
              </div>
        </div>      
    </div>

</div>

<script type="text/javascript">
    if_gmap_init();
    if_gmap_loadpicker();
    function liberarmapa(){
      document.getElementById("capaantieventos").style.display='none';
    }
</script>