<?php
  $pattern='{"detalles":{"sexo":{"Mujeres":0,"Hombres":0},"edades":{"De 26 a 35":0,"Más de 45":0,"De 18 a 25":0,"De 35 a 45":0},"nivel":{"NSE C":0,"NSE D-E":0,"NSE A-B":0}}}';
  if (!empty($datos["statistics"])){
    $stats=json_decode($datos["statistics"],true);
  } else 
    $stats=json_decode($pattern,true);
  

  function despliega($stats,$nivel,$subnivel){
    $count=0;
    foreach ($stats as $key => $value) {
        if (is_array($value)) despliega($value,$count,($subnivel+1));
        else echo '<div class="col-sm-3">'.$key.' <input type="text" value="'.$value.'" name="key'.$nivel.'_'.$count.'_'.$subnivel.'" class="form-control"></div>';   
        $count++;   
    }
  }

?>
        <div class="form-group">
              <label class="col-lg-2 control-label">Estadisticas</label>
              <div class="col-lg-10">
              <select name="modostats" id="modostats" class="form-control">
                <option value="simple">Modo Simple</option>
                <option value="avanzado">Modo Avanzado</option>
              </select>
              <div id="modosimple" class="tabmodo on">
               <?php
               if (!empty($stats["detalles"]))
               despliega($stats["detalles"],0,0);
               ?> 
               <textarea name="patternstats" style="display: none"><?php echo (!empty($datos["statistics"]))?$datos["statistics"]:$pattern;?></textarea>
              </div>
              <div id="modoavanzado" class="tabmodo">
                <textarea name="statistics_avanzado" id="statistics_avanzado" class="form-control" placeholder="Estadisticas"><?php echo $datos["statistics"]?></textarea>
              </div>
              </div>
        </div> 


