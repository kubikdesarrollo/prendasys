<section class="wrapper">
	<div class="row">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
					GALERIA DE TRADICION ARTESANAL
					</header>
				</section>

			</div>
		<?php 

		$configuracion1 = array("nombre"=>"Galería de fotos", "idunico"=>"id", "seccion"=>"tradition", "tipo"=>"supergaleria", "id_padre"=>"id_parent", "tabla"=>"images_gallery_tradition", "id_tabla"=>"id", "anchoimagen"=>1000, "carpeta"=>"../../images/tradicion-artesanal/", "con_titulo"=>false, "campo"=>"file", "orden"=>"priority", "customsizes"=>array());

		bloquesupergaleria($configuracion1,1);

		?>
	</div>

</section>
<input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>">