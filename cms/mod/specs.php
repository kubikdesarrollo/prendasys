<?php
    $specs=$this->query("SELECT * FROM Cat_tech_specs_articulo order by tech_spec");
    $currentspecs=$this->query("SELECT * FROM Cat_tech_specs WHERE itemcode='".@$datos["itemcode"]."'");

    function buscaspec($cual,$lista){
        foreach($lista as $li){
            if ($li["id_tech_spec"]==$cual) return true;
        }
        return false;
    }
?>

<div class="form-group">
    <label class="col-lg-2 control-label">Seleccione las specs</label>
    <div class="col-lg-10" style="max-height:200px;overflow:auto">
        <input type="text" class="form-control" placeholder="Buscar spec" id="filtrospecs"><br>
        <?php foreach($specs as $k=>$spec){?>
            <div class="form-check specvalue" data-title="<?php echo str_replace('"','',$spec["tech_spec"])?>" style="width:48%;display:inline-block">
        <input type="checkbox" class="form-check-input" id="checkspec_<?php echo $k?>" name="specsproduct[]" value="<?php echo $spec["id"]?>" <?php if (buscaspec($spec["id"],$currentspecs)){?>checked="checked"<?php } ?>>
                <label class="form-check-label" for="checkspec_<?php echo $k?>"><?php echo $spec["tech_spec"]?></label>
            </div>
        <?php } ?>
    </div>
</div>

<script type="text/javascript">
   $(document).ready(function () {
       $('#filtrospecs').keyup(function () {  
           var buscado = $("#filtrospecs").val().toLowerCase();
           $(".specvalue").each(function(){
                if (buscado!=''){
                    current = $(this).data("title").toLowerCase(); 
                    if (current.indexOf(buscado)!=-1)
                        $(this).show();
                    else
                        $(this).hide();
                } else {
                    $(this).show();
                }
           });
       });
   });
</script>