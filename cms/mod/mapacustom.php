<?php
  if (!empty($datos["id"])){
    $imagenesprop=$this->query("SELECT * FROM photogallery WHERE type=5 and id_parent=".intval($datos["id_property"])); 
    foreach ($imagenesprop as $key => $value) {
      if($key==(intval(@$datos["level"])-1) && !empty($value["route"]))
        $imagenprop=$value;
    }
    if (!empty($imagenprop["route"])){
      $rutai="../images/propiedades/planos/".$imagenprop["route"];
      if (file_exists($rutai)){
        $tamano = getimagesize($rutai);
        if(intval(@$tamano[0])>0 && intval(@$tamano[1])>0){

?>
<div class="form-group">
      <label class="col-lg-2 control-label">Plano</label>
      <div class="col-lg-10">
        Usa doble clic para marcar el punto donde se encuentra el local
        <div class="text-right">Zoom 
          <select name="zoomimg" id="zoomimg">
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
          </select>
        </div>
        <div id="dragon">
          <img src="<?php echo $rutai?>" alt="" id="imglocal" data-width="<?php echo $tamano[0]?>" data-height="<?php echo $tamano[1]?>">
          <div id="puntoenmapa" data-topi="<?php echo ($tamano[1]*floatval(@$datos["posy"]))?>" data-lefti="<?php echo ($tamano[0]*floatval(@$datos["posx"]))?>"><img src="images/iconos/point.png" alt=""></div>
        </div>
        X: <input type="text" name="posx" id="xposition" value="<?php floatval(@$datos["posx"])?>"> | Y: <input type="text" name="posy" id="yposition" value="<?php floatval(@$datos["posy"])?>">
      </div>
</div> 
<?php } } } else{
  //sube imagen de mapa ?>
<div class="form-group">
  <label class="col-lg-2 control-label">Plano</label>
      <div class="col-lg-10">
        No existe plano para la propiedad en el nivel <?php echo intval(@$datos["level"]);?>.<br><a href="admin.php?seccion=propiedades&accion=editar&id_edit=" class="btn btn-warning" id="btnplano">Subir plano</a>
      </div>   
</div>
<?php  } }?>


