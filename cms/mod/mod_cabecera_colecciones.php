
<?php
	$idproperty=!empty($_REQUEST["id_edit"])?intval($_REQUEST["id_edit"]):0;

	if (! $idproperty){


		$rows_items = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE 1 
			ORDER BY type ASC, ABS(priority) ASC");
		//echo '<pre>'. print_r($rows_items, 1) .'</pre>';

		$list_tiems = '';
		$list_types = array('collection', 'menu-colection-woman', 'menu-colection-man');
		foreach ($rows_items as $key2 => $row_item) {
			if( in_array($row_item->type, $list_types) ){
				switch ($row_item->type) {
					case 'collection': $type_text = 'Imagen Colecciones'; break;
					case 'menu-colection-woman':  $type_text = 'Menu Damas'; break;
					case 'menu-colection-man':  $type_text = 'Menu Caballeros'; break;
				}

				$list_tiems .= '
				<tr id="DTrow_11" class="odd"><td class="">'. $row_item->title .'</td><td class="">'. $type_text .'</td><td class=""><div style="width:120px"><a class="btn btn-primary btn-xs" style="margin:0 20px" href="admin.php?seccion=cabecera_colecciones&amp;accion=editar&amp;id_edit='. $row_item->id .'"><i class="icon-pencil"></i></a></div></td></tr>'; //<td class=""><div aling="center">'. ($row_item->published?'<span class="badge bg-primary">Autorizado</span>':'') .'</div></td>
			}
		}
?>
<section class="wrapper">
	<section class="panel">
		<header class="panel-heading">
			CABECERAS Y MENUS COLECCIONES
		</header>
		<div class="panel-body">
			<div class="adv-table editable-table ">

				<div class="space15"></div>
				<div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">

					<table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
						<thead>
							<tr role="row"><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Descripción</th><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Tipo</th><!--<th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Publicado</th>--><th style="width: 127px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th></tr>
						</thead>
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php echo $list_tiems; ?>
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</section>
</section>

<style type="text/css">
	#editable-sample_processing{display: none !important;}
</style>

<?php 
	} else{ 
	
		$rows_items_detail = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE id = ". $idproperty);

		//echo '<pre>'. print_r($rows_items_detail, 1) .'</pre>';
		if($rows_items_detail){
			$rows_items_detail = $rows_items_detail[0];

			switch ($rows_items_detail->type) {
				case 'collection': $type_text = 'Imagen Colecciones'; break;
				case 'menu-colection-woman':  $type_text = 'Menu Damas'; break;
				case 'menu-colection-man':  $type_text = 'Menu Caballeros'; break;
			}
?>
<section class="wrapper">
	<div class="row">

		<form name="form" class="form-horizontal tasi-form" id="form" enctype="multipart/form-data" action="scripts/guarda_items.php?seccion=cabecera_colecciones&tabla=items&indice=id" method="post" role="form" novalidate="novalidate">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
					CABECERAS Y MENUS COLECCIONES - <?php echo $type_text; ?>
					</header>
					<div class="panel-body"><button class="btn btn-default " type="button" onclick="javascript:document.location.href='admin.php?seccion=cabecera_colecciones&last=1'"><i class="icon-long-arrow-left"></i> REGRESAR</button>
						<button class="btn btn-success pull-right" type="submit"><i class="icon-save"></i> GUARDAR</button>
					</div>
					<div class="panel-body">
						<?php 

						if(in_array($rows_items_detail->type, array('collection', 'menu-colection-woman', 'menu-colection-man') ) ){
							$input_url = '';
							switch ($rows_items_detail->type) {
								case 'collection': $input_url = 'coleccion'; break;
							}
							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,2000,"Imagen"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/". $input_url , "customsizes"=>array(),"customfields"=>array() );

							bloqueimagen($configuracion1, array('image' => $rows_items_detail->image, "id" => $rows_items_detail->id) ,'items','id','cabecera_colecciones');
						}

						?>
					</div>
				</section>

			</div>
			<input type="hidden" name="published" value="on"> 
			<input type="hidden" name="type" value="<?php echo $rows_items_detail->type; ?>"> 
			<input type="hidden" class="indice" name="id" id="id" value="<?php echo $rows_items_detail->id; ?>"> 
			<input type="hidden" name="editar" id="editar" value="1">
			<input type="hidden" name="seccion" id="seccion" value="cabecera_colecciones">
			<input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>">
		</form>
	</div>

</section>

<?php 
		}
	} 
?>