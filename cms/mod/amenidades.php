<?php
  $pattern='{"amenities":[],"visitors":0,"age":"2017","area_m2":0,"construction_m2":0,"rentable_area_m2":0,"business_premises":0,"levels":0,"parking_lot":0,"amenities_html":""}';

  if (!empty($datos["amenities"])){
    $amenities=json_decode($datos["amenities"],true);
  } else 
    $amenities=json_decode($pattern,true);
  
  //print_r($amenities);

  function despliega1($stats,$nivel,$subnivel){
  	$count=0;
	$amenities_list = [];
	if(isset($stats['amenities']) && $stats['amenities'])
		$amenities_list = $stats['amenities'];
	unset($stats['amenities']);
	//amenities
	$array_amenities = array(
		'rampa' => 'Acceso para personas con capacidades diferentes', 
		'taxi' => 'Sitio de Taxi',
		'escaleras' => 'Escaleras eléctricas',
		'rampacarro' => 'Rampa para carro de autoservicio',
		'cambiadoresbebes' => 'Sanitarios con cambiadores de bebés',
		'seguridad' => 'Sistema de seguridad 24 horas',
		'parabuses' => 'Parabuses',
		'lavacoches' => 'Lavacoches',
		'elevador' => 'Elevadores',
		'elevador_carga' => 'Elevador de Carga',
		'estacionamiento' => 'Estacionamiento Automatizado',
		'wiffi' => 'Zona Wiffi',
		'comida' => 'Zona de comida',
		'cajero' => 'Cajeros automaticos',
		'puertas' => 'Puertas electricas',
		'pasillo_servicio' => 'Pasillo de Servicio'
		);
	
	$resto=array();
	foreach ($array_amenities as $key => $value) {
		$checked = '';
		if(in_array($key, $amenities_list)){
			$checked = 'checked';
			$resto[]=$key;
		}
		echo '<div class="col-sm-3"> <input type="checkbox" value="'. $key .'" name="keya'.$nivel.'_'.$count.'_'.$subnivel.'" id="key'.$nivel.'_'.$count.'_'.$subnivel.'" class="form-control1" '. $checked .'> <label for="key'.$nivel.'_'.$count.'_'.$subnivel.'">'.$value.'</label></div>';   
		$count++;   
	}
	foreach ($amenities_list as $key => $value) {
		$checked = 'checked';
		if(!in_array($value, $resto)){
			echo '<div class="col-sm-3"> <input type="checkbox" value="'. $key .'" name="keya'.$nivel.'_'.$count.'_'.$subnivel.'" id="key'.$nivel.'_'.$count.'_'.$subnivel.'" class="form-control1" '. $checked .'> <label for="key'.$nivel.'_'.$count.'_'.$subnivel.'">'.$value.'</label></div>';   
			$count++; 
		}  
	}
	echo '<div style="clear:both;"></div>';

	$amenities_html = '';
	if(isset($stats['amenities_html']) && $stats['amenities_html'])
		$amenities_html = $stats['amenities_html'];
	unset($stats['amenities_html']);
    
    foreach ($stats as $key => $value) {
        if (is_array($value)) despliega1($value,$count,($subnivel+1));
        else echo '<div class="col-sm-3">'.$key.' <input type="text" value="'.$value.'" name="keya'.$nivel.'_'.$count.'_'.$subnivel.'" class="form-control"></div>';   
        $count++;   
    }

    echo '<div class="clearfix"></div><div class="col-sm-12">Amenidades en texto:<br/><textarea name="amenities_html" style="width:100%;" class="form-control">'. $amenities_html .'</textarea></div>';

    
  }

?>
        <div class="form-group">
              <label class="col-lg-2 control-label">Amenidades</label>
              <div class="col-lg-10">
              <select name="modostats1" id="modostats1" class="form-control">
                <option value="simple">Modo Simple</option>
                <option value="avanzado">Modo Avanzado</option>
              </select>
              <div id="modosimple1" class="tabmodo on">
               <?php
               if (!empty($amenities))
               	despliega1($amenities,0,0);
               ?> 
               <textarea name="patternamenities" style="display: none"><?php echo (!empty($datos["amenities"]))?$datos["amenities"]:$pattern;?></textarea>
              </div>
              <div id="modoavanzado1" class="tabmodo">
                <textarea name="amenities_avanzado" id="amenities_avanzado" class="form-control" placeholder="Amenidades"><?php echo $datos["amenities"]?></textarea>
              </div>
              </div>
        </div> 


