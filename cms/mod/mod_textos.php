<section class="wrapper">
<div class="row">
    <form role="form" method="post" action="scripts/guarda_configura_textos.php?seccion=configura&tabla=kubik_config_textos&indice=id" enctype="multipart/form-data" id="form" class="form-horizontal" name="form" novalidate="novalidate">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    CONFIGURACI&Oacute;N DEL SITIO
                </header>
                <div class="panel-body">
                    <button type="submit" class="btn btn-success pull-right"><i class="icon-save"></i> GUARDAR</button>
                </div>
                <div class="panel-body">
                <?php 
                	$sql="SELECT * FROM ".PREFIX."config_textos";
                	$res=$CPanel->query($sql);
                	foreach ($res as $datosconf){
                ?>		
				          <div class="form-group">
				              <label class="col-lg-2 control-label"><?php echo $datosconf["label"]?></label>
				              <div class="col-lg-10">
                          <?php
                          if($datosconf["type"] == 'textarea' || $datosconf["type"] == 'textarea-editor'){
                            $ceditor = '';
                            if($datosconf["type"] == 'textarea-editor') $ceditor = 'editor';
                            echo '<textarea title="'. $datosconf["name"] .'" placeholder="'. $datosconf["name"] .'" class="form-control '. $ceditor .'" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">'. $datosconf["value"] .'</textarea>';
                          }else{
                            echo '<input type="text" title="'. $datosconf["name"] .'" value="'. $datosconf["value"] .'" placeholder="'. $datosconf["name"] .'" class="form-control" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">';
                          }
				                  ?>
				              </div>
				          </div>
				<?php } ?>          
				</div>

                    
            </section>

        </div>
        <input type="hidden" value="30" id="id" name="id" class="indice"> 
        <input type="hidden" value="1" id="editar" name="editar">
        <input type="hidden" value="configura" id="seccion" name="seccion">
    </form>
</div>
        
</section>
