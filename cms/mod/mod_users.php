<?php
if (isset($server)){ include $server.'mod/initmodule.php';} else include 'initmodule.php';

$mymodule= new initmodule;
$mymodule->inicializa(@$permm_find);
$mymodule->initfiltro("nombre"); //campo principal de busqueda


                    

$numReg		=	!empty($_GET["iDisplayLength"])?intval($_GET["iDisplayLength"]):50;
$tabla		=	"kubik_admin"; 
$indice		=	"id_admin";
$seccion	=	"users";
$imagenes	=	"";
$where		=	$mymodule->filtro;
$campos		= 	"{$indice} as ID, CONCAT(nombre,' ',apellidos) AS nombres, user, {$indice}";
$campossort	= 	array("ID","nombres","user","ID");
$accion		=	array('delchecked','nuevo');
$pag=(isset($_GET['iDisplayStart']))?(intval($_GET['iDisplayStart'])/$numReg)+1:1;
$titulos	= 	array('ID[width=50]','Name[width=330]->deflinknombre(id_admin)','User');
$ancholista =	"910";
$orden		=	isset($_GET['iSortCol_0']) ? ($campossort[intval($_GET['iSortCol_0'])]." ".$_GET['sSortDir_0']) :" id_admin ASC"; 

if (isset($_REQUEST['where'])) {$where=base64_decode(str_replace(" ","+",urldecode($_REQUEST['where']))); }	

$query  = "SELECT {$imagenes}{$campos} FROM {$tabla} WHERE {$where} ";

$mymodule->listar(@$modo,$orden,$seccion,$query,$tabla,$indice,$accion,implode(",",$campossort),$titulos,$pag,$numReg,$ancholista);
?>
<?php  
      if ($listar){
				echo $CPanel->inicializa($seccion,$query,$tabla,$indice,$accion,implode(",",$campossort),$titulos,$pag,$numReg,$ancholista,$orden,$where);
			}
			else {	
				$permisos="";
                if ($modo=='editar'){
                    $id_edit=intval($_REQUEST['id_edit']);
					$User->editarRegistro($tabla,$indice,$id_edit);
					$datos=$User->data_exit;

                    $qM2    =   "SELECT * FROM ".PREFIX."permisos WHERE id_usuario =".intval($_REQUEST['id_edit']);
                    $res2   =   $CPanel->query($qM2);
                    foreach ($res2 as $datos2 ){
                        $permisos.='|'.$datos2["seccion"].'|';
                    }
				}
				$showform=true;
			}
?>
<?php if ($showform){ ?>

<section class="wrapper">
    <div class="row">
        <form name="form" class="form-horizontal" id="form_user" enctype="multipart/form-data" action="scripts/guarda_users.php?seccion=<?php echo $seccion?>&tabla=<?php echo $tabla?>&indice=<?php echo $indice?>" method="post" role="form">
            <div class="col-lg-12">
                <section class="panel">
                    <header class="panel-heading">
                        <?php echo strtoupper($seccion)?>
                    </header>
                    <div class="panel-body">
                        <button class="btn btn-default " type="button" onclick='javascript:document.location.href="admin.php?seccion=<?php echo $seccion?>&last=1<?php echo (isset($_REQUEST['page']))?'&page='.$_REQUEST['page']:''?>"'><i class="icon-long-arrow-left"></i> REGRESAR</button>
                        <button class="btn btn-primary pull-right elementstatic" type="button" onclick="editthis()"><i class="icon-edit"></i> EDITAR</button>
                        <button class="btn btn-success pull-right elementeditable" type="submit"><i class="icon-save"></i> GUARDAR</button>
                    </div>
                    <div class="panel-body">
                        <header class="panel-heading">
                          INFORMACIÓN DE CUENTA
                        </header>
                        <?php bloquetext("Nombre","nombre",@$datos['nombre'])?>
                        <?php bloquetext("Apellidos","apellidos",@$datos['apellidos'])?>
                        <?php bloquetext("Email","email",@$datos['email'])?> 
                        <?php bloquechecked("Bloqueada","bloqueada",@$datos['bloqueada'])?> 
                    </div>

                    <div class="panel-body">
                        <header class="panel-heading">
                          INFORMACIÓN DE INGRESO
                        </header>
                        <?php bloquetext("Usuario","user",@$datos['user'],false,(($modo=='editar')?false:true))?>
                        <?php bloquepassword("Contraseña","pass")?>
                        <?php bloquepassword("Repetir contraseña","pass2")?> 
                        <br><span id="passstrength"></span>
                    </div>

                    <div class="panel-body">
                        <header class="panel-heading">
                          PERMISOS
                        </header>
                        <div class="form-group">
                            <label class="col-lg-2 control-label">SECCIONES</label>
                            <div class="col-lg-10">
                                    <?php for ($mai=0;$mai<count($menu_portal);$mai++){
                                        for ($maj=0;$maj<count(@$menu_portal[$mai]['campos']);$maj++){
                                            if ($menu_portal[$mai]['campos'][$maj][1]!='exception'){
                                    ?>
                                    <dl class="col-lg-12 col-sm-12" style="height:22px; margin:0" >
                                        <dd class="elementstatic" style="float:left"><img src="images/<?php if (strpos($permisos,"|".$menu_portal[$mai]['campos'][$maj][3]."|")!==false ) echo 'yes'; else echo 'no';?>.png" /></dd>
                                        <dd class="elementeditable" style="float:left">
                                        <input type="checkbox" value="<?php echo $menu_portal[$mai]['campos'][$maj][3]?>" id="permiso<?php echo $menu_portal[$mai]['campos'][$maj][3]?>" name="permiso[]" class="checkpermisos" <?php if (strpos($permisos,"|".$menu_portal[$mai]['campos'][$maj][3]."|")!==false ) echo 'checked="checked"';?>  /> 
                                        </dd>
                                        <dt class="Mcontrol-label" style="float:left; margin-left:10px"><?php echo $menu_portal[$mai]['titulo']." - ".$menu_portal[$mai]['campos'][$maj][0]; ?> </dt>
                                    </dl>
                                    <?php } } }?>
                            </div>
                        </div>    
                    </div>
                </section>

            </div>
            <input type="hidden" class="indice" name="<?php echo $indice?>" id="<?php echo $indice?>" value="<?php echo @$datos[$indice]?>" /> 
            <?php if (@$_REQUEST['id_edit']>0){?><input type="hidden" name="editar" id="editar" value="1" /><?php }?>
            <input type="hidden" name="seccion" id="seccion" value="<?php echo $seccion?>" />
            <input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>" />
        </form>
    </div>
        
</section>
<script type="text/javascript">
    $().ready(function() {
                jQuery.validator.addMethod("rulepass", function(value, element) {
                  // allow any non-whitespace characters as the host part
                  return this.optional( element ) || /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$])(?!.*\s).{6,15}$/.test( value );
                }, 'strong pass.');


                $("#form_user").validate({
                    rules: {
                        nombre: {required:true},
                        user: {
                            required: true,
                            minlength: 4
                        },
                        pass: {
                            minlength: 8,
                            rulepass:true,
                            required:($("#id_admin").val()=="")
                        },
                        pass2: {
                            minlength: 8,
                            equalTo: "#pass"
                        }
                    },
                    messages: {
                        nombre:"Nombre requerido",
                        user: {
                            required: "Requerido",
                            minlength: "Tu nombre de usuario debe contener al menos 4 caracteres"
                        },
                        pass: {
                            required: "Requerido",
                            minlength: "Tu contraseña debe contener al menos 8 caracteres",
                            rulepass: "Debe contener al menos una mayuscula, un numero y un simbolo !@#$"
                        },
                        pass2: {
                            required: "Confirma tu contraseña",
                            minlength: "Tu contraseña debe contener al menos 8 caracteres",
                            equalTo: "La contraseña no coincide"
                        }
                    }
                });
    });

</script>

<?php } ?>

<input type="hidden" name="token" id="token" value="<?php echo md5('kbi0000JoK'.$_REQUEST['seccion'])?>">
<?php if (@$_REQUEST['accion']=='new'){?><script>$(function(){ editthis()});</script><?php } ?>

