<?php
	$idproperty=!empty($_REQUEST["id_edit"])?intval($_REQUEST["id_edit"]):0;

	if (! $idproperty){


		$rows_items = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE 1  
			ORDER BY type ASC, ABS(priority) ASC");
		//echo '<pre>'. print_r($rows_items, 1) .'</pre>';

		$list_tiems = '';
		$list_types = array('categories-home-title', 'categories-home', 'home-banners-duo', 'home-banners-duo-title', 'home-baner-tall-3', 'home-baner-tall-3-title', 'home-baner-cta', 'contact');
		foreach ($rows_items as $key2 => $row_item) {
			if( in_array($row_item->type, $list_types) ){
				switch ($row_item->type) {
					case 'categories-home-title':  $type_text = 'Titulo para Categorias para seccion home'; break;
					case 'categories-home': $type_text = 'Categorias para seccion home'; break;
					case 'home-banners-duo': $type_text = 'Banners duos'; break;
					case 'home-banners-duo-title': $type_text = 'Titulo para banners duos'; break;
					case 'home-baner-tall-3': $type_text = 'Banners trios'; break;
					case 'home-baner-tall-3-title': $type_text = 'Titulo para Banners trios'; break;
					case 'home-baner-cta': $type_text = 'Banner full (tamaño completo)'; break;
					case 'contact': $type_text = 'Contacto'; break;
					//default: $type_text = $row_item->type;
				}

				$list_tiems .= '
				<tr id="DTrow_11" class="odd"><td class="">'. $row_item->title .'</td><td class="">'. $type_text .'</td><td class=""><div style="width:120px"><a class="btn btn-primary btn-xs" style="margin:0 20px" href="admin.php?seccion=varios_home&amp;accion=editar&amp;id_edit='. $row_item->id .'"><i class="icon-pencil"></i></a></div></td></tr>'; //<td class=""><div aling="center">'. ($row_item->published?'<span class="badge bg-primary">Autorizado</span>':'') .'</div></td>
			}
		}
?>
<section class="wrapper">
	<section class="panel">
		<header class="panel-heading">
			TEXTOS Y OTROS
		</header>
		<div class="panel-body">
			<div class="adv-table editable-table ">

				<div class="space15"></div>
				<div id="editable-sample_wrapper" class="dataTables_wrapper form-inline" role="grid">

					<table class="table table-striped table-hover table-bordered dataTable" id="editable-sample" aria-describedby="editable-sample_info">
						<thead>
							<tr role="row"><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Titulo</th><th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Tipo</th><!--<th style="width: 488px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1">Publicado</th>--><th style="width: 127px;" class="sorting_disabled" role="columnheader" rowspan="1" colspan="1"></th></tr>
						</thead>
						<tbody role="alert" aria-live="polite" aria-relevant="all">
							<?php echo $list_tiems; ?>
						</tbody>
					</table>


				</div>
			</div>
		</div>
	</section>
</section>

<style type="text/css">
	#editable-sample_processing{display: none !important;}
</style>

<?php 
	} else{ 
	
		$rows_items_detail = $CPanel->select("
			SELECT * 
			FROM items 
			WHERE id = ". $idproperty);

		//echo '<pre>'. print_r($rows_items_detail, 1) .'</pre>';
		if($rows_items_detail){
			$rows_items_detail = $rows_items_detail[0];

			switch ($rows_items_detail->type) {
				case 'categories-home-title':  $type_text = 'Titulo para Categorias para seccion home'; break;
				case 'categories-home': $type_text = 'Categorias para seccion home'; break;
				case 'home-banners-duo': $type_text = 'Banners duos'; break;
				case 'home-banners-duo-title': $type_text = 'Titulo para banners duos'; break;
				case 'home-baner-tall-3': $type_text = 'Banners trios'; break;
				case 'home-baner-tall-3-title': $type_text = 'Titulo para Banners trios'; break;
				case 'home-baner-cta': $type_text = 'Banner full (tamaño completo)'; break;
				case 'contact': $type_text = 'Contacto'; break;
				default: $type_text = $row_item->type;
			}
?>
<section class="wrapper">
	<div class="row">

		<form name="form" class="form-horizontal tasi-form" id="form" enctype="multipart/form-data" action="scripts/guarda_items.php?seccion=varios_home&tabla=items&indice=id" method="post" role="form" novalidate="novalidate">
			<div class="col-lg-12">
				<section class="panel">
					<header class="panel-heading">
					TEXTOS Y OTROS - <?php echo $type_text; ?>
					</header>
					<div class="panel-body"><button class="btn btn-default " type="button" onclick="javascript:document.location.href='admin.php?seccion=varios_home&last=1'"><i class="icon-long-arrow-left"></i> REGRESAR</button>
						<button class="btn btn-success pull-right" type="submit"><i class="icon-save"></i> GUARDAR</button>
					</div>
					<div class="panel-body">
						<?php 

						if($rows_items_detail->type != 'contact'){
							bloqueinput('Titulo','title',$rows_items_detail->title,true,"",false);

							bloqueinput('Titulo (ingles)','title_en',$rows_items_detail->title_en,false,"",false);
							
							//bloqueenumvalues('Tipo','type',$rows_items_detail->type, 'Categorias para seccion home,Banners duos,Titulo para banners duos,Titulo para banners duos,Banners trios,Titulo para Banners trios,Banner full (tamaño completo)', 'categories-home,home-banners-duo,home-banners-duo-title,home-baner-tall-3,home-baner-tall-3-title,home-baner-cta');

							if(!in_array($rows_items_detail->type, array('home-banners-duo','home-baner-tall-3','home-baner-cta' ) ) ){
								bloqueeditor('Descripción','details',$rows_items_detail->details);
								//bloquetextarea('Descripción','details',$rows_items_detail->details,'');
								bloqueeditor('Descripción (ingles)','details_en',$rows_items_detail->details_en);
								//bloquetextarea('Descripción (ingles)','details_en',$rows_items_detail->details_en,'');
							}

							bloqueinput('Liga','url',$rows_items_detail->url,false,"",false);
							bloqueinput('Liga (ingles)','url_en',$rows_items_detail->url_en,false,"",false);
							bloqueinput('Target html','url_target',$rows_items_detail->url_target,false,"",false);

							if(in_array($rows_items_detail->type, array('categories-home','home-banners-duo','home-baner-tall-3','home-baner-cta' ) ) ){
								switch ($rows_items_detail->type) {
									case 'categories-home': $input_url = 'square-banners'; break;
									case 'home-banners-duo': $input_url = 'duo-banners'; break;
									case 'home-baner-tall-3': $input_url = 'banners-estilo'; break;
									case 'home-baner-cta': $input_url = 'banners-cta'; break;
								}
								$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,2000,"Imagen"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images2/home/". $input_url , "customsizes"=>array(),"customfields"=>array() );

								bloqueimagen($configuracion1, array('image' => $rows_items_detail->image, "id" => $rows_items_detail->id) ,'items','id','varios_home');
							}

							if($rows_items_detail->type == 'home-baner-cta'){

								$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("details_en",2000,2000,"Imagen (ingles)"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images2/home/banners-cta" , "customsizes"=>array(),"customfields"=>array() );

								bloqueimagen($configuracion1, array('image' => $rows_items_detail->details_en, "id" => $rows_items_detail->id) ,'items','id','varios_home');

								bloqueinput('Url Video','details',$rows_items_detail->details,false,"",false);
								//bloqueinput('Url Video (ingles)','details_en',$rows_items_detail->details_en,false,"",false);
							}

							//bloqueenumvalues('Prioridad','priority',$rows_items_detail->priority,'0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20','0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20');

							//bloquechecked('Publicada','published',$rows_items_detail->published*1);
						}else{
							$configuracion1 = array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,2000,"Imagen"), "recorta_normal"=>false, "resize_normal"=>false, "carpeta"=>"../images/contacto" , "customsizes"=>array(),"customfields"=>array() );

							bloqueimagen($configuracion1, array('image' => $rows_items_detail->image, "id" => $rows_items_detail->id) ,'items','id','varios_home');
						}

						?>
					</div>
				</section>

			</div>
			<input type="hidden" name="published" value="on"> 
			<input type="hidden" name="type" value="<?php echo $rows_items_detail->type; ?>"> 
			<input type="hidden" class="indice" name="id" id="id" value="<?php echo $rows_items_detail->id; ?>"> 
			<input type="hidden" name="editar" id="editar" value="1">
			<input type="hidden" name="seccion" id="seccion" value="varios_home">
			<input type="hidden" id="Token_CSRF" name="Token_CSRF" value="<?php echo get_token_csrf(); ?>">
		</form>
	</div>

</section>

<?php 
		}
	} 
?>