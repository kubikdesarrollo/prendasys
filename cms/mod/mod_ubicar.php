<?php
  $propiedades=$CPanel->query("SELECT * FROM properties");
  $locales=$CPanel->query("SELECT * FROM locals");
  $planos=$CPanel->query("SELECT * FROM photogallery WHERE type=5");
  $level=!empty($_REQUEST["level"])?intval($_REQUEST["level"]):0;
  $idproperty=!empty($_REQUEST["id_edit"])?intval($_REQUEST["id_edit"]):0;
  if (! $idproperty && !$level){
?>
<section class="wrapper">
    <section class="panel">
        <header class="panel-heading">
            SELECCIONA UNA PROPIEDAD Y UN NIVEL
        </header>
        <div class="panel-body">
            <div class="adv-table editable-table ">
                <div class="space15"></div>
                <table class="table table-striped table-hover table-bordered" id="">
                    <thead>
                      <tr><th style="width:412px">Propiedad</th><th style="width:412px">NIVELES</th></tr>
                    </thead>
                    <tbody>
                    <?php foreach ($propiedades as $kp => $propiedad) {?>
                      <tr id="DTrow_13" class="odd"><td class=""><a href="admin.php?seccion=propiedades&accion=editar&id_edit=<?php echo $propiedad["id"]?>#nestable_propiedades"><?php echo $propiedad["name"]?></a></td><td class="">
                      <?php $leven=0; foreach ($planos as $kl => $plano) { if($plano["id_parent"]==$propiedad["id"]){ $leven++;?>
                      <a class="btn btn-primary btn-xs" style="margin:0 20px" href="admin.php?seccion=ubicar&&accion=editar&id_edit=<?php echo $propiedad["id"]?>&level=<?php echo $leven?>"><?php echo $leven?></a><?php } }?>
                      </td></tr><?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</section>
<?php } else{ ?>
<section class="wrapper">
    <section class="panel">
        <header class="panel-heading">
            <?php 
            foreach ($propiedades as $kp => $propiedad) {
              if ($propiedad["id"]== $idproperty) $propertysel=$propiedad;
            }
            $arrplanes=array();
            foreach ($planos as $kl => $plano) {
              if ($plano["id_parent"]== $idproperty) $arrplanes[]=$plano;
            }
            if (!empty($arrplanes[($level-1)]["route"]) && !empty($propertysel)){ 
              $rutai="../images/propiedades/planos/".$arrplanes[($level-1)]["route"]; 
              if (file_exists($rutai)){
                $tamano = getimagesize($rutai);
                if(intval(@$tamano[0])>0 && intval(@$tamano[1])>0){
          ?>
            LOCALES EN <?php echo $propertysel["name"]?> NIVEL <?php echo $level?><br>
            <small>Selecciona un local del listado y luego da clic en el mapa para marcar su ubicación, es necesario guardar cada cambio que hagas para que se aplique permanentemente.</small>
            <?php }}}?>
        </header>
        <div class="panel-body">
        <?php if(!empty($tamano[0]) && !empty($tamano[1])){?>
        <form action="admin.php?seccion=ubicar&&accion=editar&id_edit=<?php echo $idproperty?>&level=<?php echo $level?>&saved=1" method="POST" id="fubicapuntos">
            <div id="listadotodospuntos" class="col-sm-4">
            <?php foreach ($locales as $kl => $local) { if ($local["id_property"]==$idproperty){?>
              <div class="col-sm-4"><label><input type="radio" name="punto" data-local="<?php echo $local["local"]?>" id="punton_<?php echo $local["id"]?>" value="[<?php echo $local["posx"]?>,<?php echo $local["posy"]?>]" data-idi="<?php echo $local["id"]?>" class="radiospuntos"><?php echo $local["local"]?></label></div><div class="col-sm-8" <?php if(!empty($local["posx"]) && $local["level"]==$level) echo 'style="color:#111111"';?>><?php echo str_replace(["S.A. DE C.V.","SAPI DE C.V.","S.A. C.V.","S. DE R.L."], "", $local["lessee"])?></div><div class="clearfix" style="border: 1px solid #eee"></div>
            <?php } } ?>  
            </div>
            <div class="col-sm-1"></div>
            <div id="puntostodos" class="col-sm-7">
              <img src="<?php echo $rutai?>" alt="" id="imglocal" data-width="<?php echo $tamano[0]?>" data-height="<?php echo $tamano[1]?>" width="<?php echo ($tamano[0]/4)?>">
              <div id="puntoenmapa" data-topi="" data-lefti=""><img src="images/iconos/point.png" alt="" ></div>
            </div>
            
            <input type="hidden" name="zoomimg" id="zoomimg" value="4">
        </form>    
        <?php } ?>    
        </div>
    </section>
</section>  
<input type="hidden" id="tokenCSRF" value="<?php echo get_token_csrf()?>">  
<input type="hidden" id="nivelactual" value="<?php echo $level?>">    
<?php } ?> 