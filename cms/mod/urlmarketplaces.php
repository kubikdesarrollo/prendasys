<?php
    $urls=$this->query("SELECT * FROM Cat_url_marketplace WHERE itemcode=:item",array("item"=>@$datos["itemcode"]));
    $tiendas=$this->query("SELECT * FROM marketplaces order by priority");
?>

<div class="form-group">
    <label class="col-lg-2 control-label">Marketplaces</label>
    <div class="col-lg-10" id="contentmarket" >
        <?php for($k=0;$k<=count($urls);$k++){
            $url=@$urls[$k]; $lasturl=($k==(count($urls)))?true:false;
        ?>
        <div class="col-sm-12 <?php echo ($lasturl)?'lasturl':'bloqueurl'?>">
            <div class="col-sm-5">
                <input type="text" class="form-control inputurl" name="marketplaces[]" value="<?php echo @$url["url"]?>" placeholder="url">
            </div>
            <div class="col-sm-2">
                <select name="marketplacescolor[]" class="form-control inputcolor">
                    <option value="0">Color</option>
                <?php foreach($colors as $kc=>$colour){?>
                <option value="<?php echo $colour["id"]?>" <?php if($colour["id"]==@$url["id_color"]){?>selected="selected"<?php }?>><?php echo $colour["title"]?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-sm-2">
                <select name="marketplacesnames[]" class="form-control inputmarkname">
                <option value="0">Marketplace</option>
                <?php foreach($tiendas as $kc=>$tienda){?>
                <option value="<?php echo $tienda["id"]?>" <?php if($tienda["id"]==@$url["id_marketplace"]){?>selected="selected"<?php }?>><?php echo $tienda["title"]?></option>
                <?php }?>
                </select>
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control inputquantity" name="quantity[]" value="<?php echo @$url["cantidad"]?>" placeholder="cantidad">
            </div>
            <div class="col-sm-1">
                <button type="button" class="btn btn-danger pull-right" onclick="delMarket(this)"><i class="icon-remove"></i></button>
            </div>
            <div class="clear" style="padding-bottom:15px"></div>
        </div>    
        <?php } ?>
    </div>
    <div class="clear"></div>
    <div class="col-sm-12" style="padding-top:15px">
        <button type="button" id="agregaurl" class="btn btn-info pull-right">+Agregar url</button>
    </div> 
</div>
<style>.lasturl{display:none;}</style>
<script type="text/javascript">
   $(document).ready(function () {
       $("#agregaurl").click(function(){
           var newcontent = $('<div>').addClass('col-sm-12').append($(".lasturl").html());
           newcontent.find(".inputurl").val('');
           newcontent.find(".inputquantity").val('');
           var lastele=$("#contentmarket").append(newcontent);
           
       });
   });
   function delMarket(ele){
       $(ele).parent().parent().remove();
   }
</script>