<section class="wrapper">
<div class="row">
    <form role="form" method="post" action="scripts/guarda_configura.php?seccion=configura&amp;tabla=kubik_config&amp;indice=id" enctype="multipart/form-data" id="form" class="form-horizontal" name="form" novalidate="novalidate">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    CONFIGURACI&Oacute;N
                </header>
                <div class="panel-body">
                    <button type="submit" class="btn btn-success pull-right"><i class="icon-save"></i> GUARDAR</button>
                </div>
                <div class="panel-body">
                <?php 
                	$sql="SELECT * FROM ".PREFIX."config";
                	$res=$CPanel->query($sql);
                	foreach ($res as $datosconf){
                ?>		
				          <div class="form-group">
				              <label class="col-lg-2 control-label"><?php echo $datosconf["name"]?></label>
				              <div class="col-lg-10">
                          <?php
                          if($datosconf["type"] == 'textarea' || $datosconf["type"] == 'textarea-editor'){
                            $ceditor = '';
                            if($datosconf["type"] == 'textarea-editor') $ceditor = 'editor';
                            echo '<textarea title="'. $datosconf["name"] .'" placeholder="'. $datosconf["name"] .'" class="form-control '. $ceditor .'" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">'. $datosconf["value"] .'</textarea>';
                          }else{
                            echo '<input type="text" title="'. $datosconf["name"] .'" value="'. $datosconf["value"] .'" placeholder="'. $datosconf["name"] .'" class="form-control" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">';
                          }
				                  ?>
				              </div>
				          </div>
				<?php } ?>          
				</div>

                <div class="panel-body">
                    <div class="form-group">
                      <label class="col-lg-2 control-label">post_max_size</label>
                      <div class="col-lg-10"><?php echo ini_get('post_max_size');?></div>
                    </div>  
                    <div class="form-group">
                      <label class="col-lg-2 control-label">upload_max_filesize</label>
                      <div class="col-lg-10"><?php echo ini_get('upload_max_filesize');?></div>
                    </div>  
                    <div class="form-group">
                      <label class="col-lg-2 control-label">session.gc_maxlifetime</label>
                      <div class="col-lg-10"><?php echo ini_get('session.gc_maxlifetime');?></div>
                    </div>  
                    <div class="form-group">
                      <label class="col-lg-2 control-label">session.cookie_lifetime</label>
                      <div class="col-lg-10"><?php echo ini_get('session.cookie_lifetime');?></div>
                    </div> 
                    <div class="form-group">
                      <label class="col-lg-2 control-label">gc_divisor</label>
                      <div class="col-lg-10"><?php echo ini_get('session.gc_divisor');?></div>
                    </div> 
                </div>

                    
            </section>

        </div>
        <input type="hidden" value="30" id="id" name="id" class="indice"> 
        <input type="hidden" value="1" id="editar" name="editar">
        <input type="hidden" value="configura" id="seccion" name="seccion">
    </form>
</div>
        
</section>
