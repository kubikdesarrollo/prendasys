<section class="wrapper">
<div class="row">
    <form role="form" method="post" action="scripts/guarda_configura_textoshome.php?seccion=configura&tabla=kubik_config_textoshome&indice=id" enctype="multipart/form-data" id="form" class="form-horizontal" name="form" novalidate="novalidate">
        <div class="col-lg-12">
            <section class="panel">
                <header class="panel-heading">
                    CONFIGURACI&Oacute;N EXTRA DEL HOME
                </header>
                <div class="panel-body">
                    <button type="submit" class="btn btn-success pull-right"><i class="icon-save"></i> GUARDAR</button>
                </div>
                <div class="panel-body">
                <?php 
                	$sql="SELECT * FROM ".PREFIX."config_textoshome";
                	$res=$CPanel->query($sql);
                	foreach ($res as $datosconf){
                ?>		
				          <div class="form-group">
				              <label class="col-lg-2 control-label"><?php echo $datosconf["label"]?></label>
				              <div class="col-lg-10">
                          <?php
                          if($datosconf["type"] == 'textarea' || $datosconf["type"] == 'textarea-editor'){
                            $ceditor = '';
                            if($datosconf["type"] == 'textarea-editor') $ceditor = 'editor';
                            echo '<textarea title="'. $datosconf["name"] .'" placeholder="'. $datosconf["name"] .'" class="form-control '. $ceditor .'" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">'. $datosconf["value"] .'</textarea>';
                          }else{
                            echo '<input type="text" title="'. $datosconf["name"] .'" value="'. $datosconf["value"] .'" placeholder="'. $datosconf["name"] .'" class="form-control" id="'. $datosconf["name"] .'" name="'. $datosconf["name"] .'">';
                          }
				                  ?>
				              </div>
				          </div>
				<?php } ?>          
				</div>

                    
            </section>

        </div>
        <input type="hidden" value="30" id="id" name="id" class="indice"> 
        <input type="hidden" value="1" id="editar" name="editar">
        <input type="hidden" value="configura" id="seccion" name="seccion">
    </form>
</div>
        
</section>


<script src="assets/codemirror/codemirror.js"></script>
<link rel="stylesheet" href="assets/codemirror/codemirror.css">
<script src="assets/codemirror/mode/javascript/javascript.js"></script>
<script src="assets/codemirror/mode/css/css.js"></script>
<script src="assets/codemirror/mode/htmlmixed/htmlmixed.js"></script>
<link rel="stylesheet" href="assets/codemirror/theme/abcdef.css">

<script>
  var editor = CodeMirror.fromTextArea(document.getElementById("CODIGOS_HOME"), {
    lineNumbers: true,
    mode: "htmlmixed",
    theme: "abcdef"
  });
  var editor2 = CodeMirror.fromTextArea(document.getElementById("CSS_HOME"), {
    lineNumbers: true,
    mode: "css",
    theme: "abcdef"
  });
  var editor3 = CodeMirror.fromTextArea(document.getElementById("JS_HOME"), {
    lineNumbers: true,
    mode: "javascript",
    theme: "abcdef"
  });
</script>