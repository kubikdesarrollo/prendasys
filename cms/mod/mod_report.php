<?php
	$locales = $CPanel->query("
		SELECT l1.*, p1.name as property_name, e1.name as state_name, c1.name as city_name 
		FROM  locals AS l1 
		INNER JOIN properties AS p1 ON l1.id_property = p1.id 
		INNER JOIN kubik_estados AS e1 ON p1.state_id = e1.id_estado 
		INNER JOIN kubik_ciudades AS c1 ON p1.city_id = c1.geonameid 
		WHERE 1");
?>
<section class="wrapper">
    <section class="panel">
        <header class="panel-heading">
            REPORTE DE LOCALES
        </header>
        <div class="panel-body">
            <div class="adv-table editable-table ">
                <div class="space15"></div>

                <div id="d-filters" class="row">
                    <h4 class="col-md-12">Filtros:</h4>
                    <div id="d-filter-2" class="col-md-4 col-xs-12"><label>Tipo de cliente:</label> </div>
                    <div id="d-filter-9" class="col-md-4 col-xs-12"><label>Propiedad:</label> </div>
                    <div id="d-filter-10" class="col-md-4 col-xs-12"><label>Estado:</label> </div>
                    <div id="d-filter-11" class="col-md-4 col-xs-12"><label>Ciudad:</label> </div>
                    <div id="d-filter-12" class="col-md-4 col-xs-12"><label>Disponible:</label> </div>
                </div>

                <h4>Exportar a:</h4>

                <table id="myTable" class="display nowrap" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th>SAP</th>
                            <th>Local</th>
                            <th>T. Cte.</th>
                            <th>Arrendatario</th>
                            <th>Superficie (m<sup>2</sup>)</th>
                            <th>Inicio</th>
                            <th>Fin</th>
                            <th>Renta ($)</th>
                            <th>Precio m<sup>2</sup> ($)</th>
                            <th>Propiedad</th>
                            <th>Estado</th>
                            <th>Ciudad</th>
                            <th>Disponible</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php 
                    	foreach ($locales as $kl => $local) {
							$date1t = '';
							$date2t = '';
							if($local["date_start"] && $local["date_end"] ){
								$date1 = new DateTime($local["date_start"]);
								$date2 = new DateTime($local["date_end"]);
								$date1t = $date1->format('d/m/Y');
								$date2t = $date2->format('d/m/Y');
							}

                    		$disponible = 'No';
                    		if( intval($local['status']) ) $disponible = 'Si';

                    		echo '
                                <tr>
                                    <td>'. $local["sap_code"] .'</td>
                                    <td>'. $local["local"] .'</td>
                                    <td>'. $local["type"] .'</td>
                                    <td>'. $local["lessee"] .'</td>
                                    <td>'. number_format($local["area_m2"], 2, '.', ',') .'</td>
                                    <td>'. $date1t .'</td>
                                    <td>'. $date2t .'</td>
                                    <td>'. number_format($local["price"], 2, '.', ',') .'</td>
                                    <td>'. number_format($local["price_m2"], 2, '.', ',') .'</td>
                                    <td>'. $local["property_name"] .'</td>
                                    <td>'. $local["state_name"] .'</td>
                                    <td>'. $local["city_name"] .'</td>
                                    <td>'. $disponible .'</td>
                                </tr>';
                    	} 
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
</section>

<script type="text/javascript" language="javascript" src="//cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.4.2/js/dataTables.buttons.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script type="text/javascript" language="javascript" src="//cdn.datatables.net/buttons/1.4.2/js/buttons.html5.min.js"></script>

<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.16/css/jquery.dataTables.min.css">
<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/buttons/1.4.2/css/buttons.dataTables.min.css">

<script type="text/javascript">
$(document).ready(function(){

    //https://datatables.net/extensions/buttons/examples/html5/filename.html
    $('#myTable').DataTable({
        "language": { // pone en español
            "url": "//cdn.datatables.net/plug-ins/9dcbecd42ad/i18n/Spanish.json"
        },
        "pageLength": 50, // paginador en 30
        initComplete: function () { // funcionalidad para generar filtros
            var cont = 0;
            var filterId = [2,9,10,11,12]; // 2 - tipo, 9 - propieadad, 10 - estado, 11 - ciudad, 12 - disponible 
            this.api().columns().every( function () {
                if(filterId.indexOf(cont) !== -1){
                    var column = this;
                    var select = $('<select><option value=""></option></select>')
                        .appendTo( $("#d-filter-"+cont) )
                        .on( 'change', function () {
                            var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                            );
     
                            column
                                .search( val ? '^'+val+'$' : '', true, false )
                                .draw();
                        } );
     
                    column.data().unique().sort().each( function ( d, j ) {
                        select.append( '<option value="'+d+'">'+d+'</option>' )
                    } );
                }
                cont++;
            } );
        },
        dom: 'Bfrtip', // opciones de exportacion
        buttons: [ { extend: 'excelHtml5', title: 'Reporte de Locales' } ]
    });
});
</script>

<style type="text/css">
table.dataTable tbody tr {
    background-color: #ffffff;
}
table.dataTable.stripe tbody tr.odd, table.dataTable.display tbody tr.odd {
    background-color: #f9f9f9;
}

</style>