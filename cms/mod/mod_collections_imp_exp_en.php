<?php

	$query1	=	"SELECT COUNT(id) AS count1 FROM `collections_en` WHERE 1";
	$res1   =   $CPanel->query($query1);

	//echo '<br/><br/><br/><pre>'. print_r($res1,1). '</pre>';

?>
<section class="wrapper">
	<div>
		<div style="text-align: center;">
			<h2>Exportar <span id="cont_soc"><?=$res1[0]['count1']?></span> Colecciones Ingles a un Documento Excel</h2>
			<form action="imp_exp_collections_en.php" method="post" target="_blank">
				<input type="hidden" name="token" value="<?php echo md5('kbi0000JoK'.$_REQUEST['seccion'])?>">
				<input type="hidden" name="seccion" value="<?php echo $_REQUEST['seccion']?>">
				<input type="hidden" name="tipo" value="1">
				<br/><br/>
				<p><button type="submit" >Descargar Excel <img src="img/file-search/xls.png" alt="icono excel" /></button></p>
				<br/>
			</form>

			<h2>Limpiar lista de Colecciones Ingles</h2>
			<form id="f-limpieza" action="imp_exp_collections_en.php" method="post" target="_blank">
				<input type="hidden" name="token" id="token" value="<?php echo md5('kbi0000JoK'.$_REQUEST['seccion'])?>">
				<input type="hidden" name="seccion" id="seccion" value="<?php echo $_REQUEST['seccion']?>">
				<input type="hidden" name="tipo" value="3">
				<br/><br/>
				<p><button type="submit">Limpiar Lista de Colecciones Ingles</button></p>
				<br/>
			</form>

			<h2>Importar Colecciones Ingles</h2>
			<form id="f-importacion" action="imp_exp_collections_en.php" method="post" enctype="multipart/form-data" target="_blank">
				<input type="hidden" name="token" value="<?php echo md5('kbi0000JoK'.$_REQUEST['seccion'])?>">
				<input type="hidden" name="seccion" value="<?php echo $_REQUEST['seccion']?>">
				<input type="hidden" name="tipo" value="2">
				<input type="hidden" name="MAX_FILE_SIZE" value="10000000" />
				<br/><br/>
				<p><strong>Archivo CSV:</strong><br/> <input type="file" name="documento" style="display: inline;" /></p>
				<p><button type="submit">Importar</button><span id="f-importacion-cargando"></span></p>
				<br/>
			</form>
			<div style="">
				<h2>Importar imágenes de Colecciones Ingles</h2>
				<form id="f-importacion2" action="imp_exp_collections_en.php" method="post" enctype="multipart/form-data" target="_blank">
					<input type="hidden" name="token" value="<?php echo md5('kbi0000JoK'.$_REQUEST['seccion'])?>">
					<input type="hidden" name="seccion" value="<?php echo $_REQUEST['seccion']?>">
					<input type="hidden" name="tipo" value="4">
					<br/>
					<p>Compara las imágenes previamente cargadas en la carpeta images/colecciones contra los SKU de las colecciones previamente cargadas,
					y si encuentra coincidencias las agrega como sus imágenes.</p>
					<p>IMPORTANTE: <br/>
					- Las imágenes ya deben de haber sido cargadas por FTP en la carpeta: images/colecciones. <br/> 
					- También sus miniaturas respectivas en la carpeta: images/colecciones/thumbs.<br/>
					- Ejemplo de nombre de archivo: 3F13PH-Brown-Botin-de-tacon-color-cobre-01.jpg
					</p>
					<p><button type="submit">Importar imágenes</button></p>
					<br/>
				</form>
			</div>
			<div style="text-align: left; display: inline-block; width: 30%; vertical-align: top;">
				<h3>Lista de categorias permitidas</h3>
				<?php

					$rows_categories = $CPanel->select("SELECT id, title, id_parent FROM categories WHERE 1 ORDER BY ABS(priority) ASC");
					$list_categories0 = array();
					foreach ($rows_categories as $key => $row_categories){
					    $list_categories0[ $row_categories->id_parent*1 ][] = $row_categories;
					}

					//echo '<pre>'. print_r($list_categories0, 1) . '</pre>';

					foreach ($list_categories0[0] as $key => $row_categories) {
					    echo $row_categories->id .'-'. strtolower(trim($row_categories->title)) .'<br/>';

					    foreach ($list_categories0[$row_categories->id*1] as $key1 => $row_categories1){
					        echo ' - '. $row_categories1->id .'-'. strtolower(trim($row_categories1->title)) .'<br/>';

					        foreach ($list_categories0[$row_categories1->id*1] as $key2 => $row_categories2){
					            echo ' - - '. $row_categories2->id .'-'. strtolower(trim($row_categories2->title)) .'<br/>';

					            foreach ($list_categories0[$row_categories2->id*1] as $key3 => $row_categories3){
					                echo ' - - - '. $row_categories3->id .'-'. strtolower(trim($row_categories3->title)) .'<br/>';
					            }
					        }
					    }
					}
				?>
			</div>
			<div style="text-align: left; display: inline-block; width: 30%; vertical-align: top;">
				<h3>Lista de pieles permitidas</h3>
				<?php

					$query2	=	"SELECT title FROM `skins` WHERE 1";
					$res2   =   $CPanel->query($query2);
					foreach ($res2 as $record2) {
						echo ' - '. strtolower(trim($record2['title'])) .'<br/>';
					}
				?>
			</div>
			<div style="text-align: left; display: inline-block; width: 30%; vertical-align: top;">
				<h3>Lista de colores permitidos</h3>
				<?php

					$query2	=	"SELECT title FROM `colours` WHERE 1";
					$res2   =   $CPanel->query($query2);
					foreach ($res2 as $record2) {
						echo ' - '. strtolower(trim($record2['title'])) .'<br/>';
					}
				?>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
$(document).ready(function(){
	$("#f-limpieza").submit(function(e){
		var r = confirm("¿De verdad desea limpiar la Lista de Colecciones Ingles?");
		if (r == true) {
			$.ajax({
				url : $("#f-limpieza").attr('action'),
				data : $("#f-limpieza").serialize(),
				type : 'POST',
				dataType : 'json',
				success : function(json) {
					if(json){
						console.log(json);
						$("#cont_soc").html('0');
						alert('Listo, se ha limpiado la lista de Colecciones Ingles');
					}
				},
				error : function(xhr, status) {
					console.log(xhr);
					console.log(status);
					alert('Disculpe, hubo un problema');
				}
			});
		}

		e.preventDefault();
		return false;
	});

	/*$("#f-importacion2").submit(function(e){
		var r = confirm("¿De verdad desea recargar la Lista de Imagenes de Colecciones?");
		if (r != true) {
			e.preventDefautl();
			return false;
		}
	});*/

	/*$("#f-importacion").submit(function(e){
		$("#f-importacion-cargando").html('Cargando...');
		$("#f-importacion input, #f-importacion button").attr('disabled', true);
		var formData = new FormData(document.getElementById("f-importacion"));
		$.ajax({
			url : $("#f-importacion").attr('action'),
			type : 'POST',
			dataType: "html",
			data: formData,
			cache: false,
			contentType: false,
			processData: false,
			success : function(json) {
				console.log(json);
				if(json){
					if(json.result*1 >= 1){
						alert('Listo, se importaron '+ json.cnew +' nuevas Colecciones y se actualizaron '+ json.cupd +' Colecciones.');
					}else{
						alert('No se ha podido importar');
					}
					
					//window.location.reload(true);
				}else{
					alert('Disculpe, hubo un problema');
				}
				$("#f-importacion-cargando").html('');
				$("#f-importacion input, #f-importacion button").attr('disabled', false);
			},
			error : function(xhr, status) {
				console.log(xhr);
				console.log(status);
				alert('Disculpe, hubo un problema');
				$("#f-importacion-cargando").html('');
				$("#f-importacion input, #f-importacion button").attr('disabled', false);
			}
		});
		
		e.preventDefault();
		return false;
	});*/
		
});
</script>
