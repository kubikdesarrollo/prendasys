<?php
require_once("configuracion_global.php");
$tag		=	"";
$json		=	"";
$tabla 		= 	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", @$_GET['tabla']);				//Nombre tabla a guardar o editar.
$indice_t	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", @$_GET['indice']);				//Nombre identificador referente a elemento a editar.

if ($tabla==""){ 
	$User->redirecciona($server_name.$carpeta_dev.$path_cms.'index.php');
}

if(isset($_POST[$indice_t]))
	$indice		=	$_POST[$indice_t]; 			//Valor del identificador.
	

// FECHAS
if(isset($_POST['fecha']))
	$_POST['fecha'] = fecha($_POST['fecha'],1);

// cambio para categorias primarias (dama y caballero)
if(isset($_POST['seccion']) && $_POST['seccion'] == 'categorias' && $_POST['editar'] == '1' && ($_POST['id'] == '1' || $_POST['id'] == '2') )
	$_POST['id_parent'] = ["0"];

//SI EXISTE ID DE NOTA

if(isset($_POST[$indice_t]) && $_POST[$indice_t] != 0 )
	$n_id = intval( $_POST[$indice_t] );		//Valor del identificador a Editar.
else { 
	$User->ultimoRegistro($tabla,$indice_t,"si",$debug);
	$n_id	=	$User->ultimoReg;				//Valor del identificador siguiente, cuando se es nuevo.
}
(isset($_POST['editar']))?$editar=@$indice:$editar=0;
$nombre_original	=	"img".$n_id;			//Valor del identificador.
?>