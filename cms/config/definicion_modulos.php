<?php

class definicion_modulos {
      /*****      DEFINICION DE MODULOS BASADOS EN FORMATO STANDARD  **********/
      var $modulos_portal= array();

      function __construct(){
            $contadores = '0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20';

            $cont=0;

            //sliders_home READY
            $this->modulos_portal[$cont]=array('tablamodule'=>'banners' ,'nameseccion'=>'sliders_home' ,'id_table'=>'id' ,'titlemodule'=>'Sliders home' ,'custom_save'=>false ,'save_file'=>'', 'sinimagen'=>true, 'fields_search'=>"title", 'titles_search'=>"title", 'fieldlist' =>"priority,title,url,published", 'tituloslist'=>array('Orden[width=50]->handlersort','Titulo','URL','Publicado[width=50]->active_check'),"orden"=>"priority ASC", "is_sortable" => true );
            $this->modulos_portal[$cont]["fields"]=array(
                                            array("nombre"=>"title",   "tipo"=>"input",    "titulo"=>"Título", "required" => true),
                                            array("nombre"=>"url",   "tipo"=>"input",    "titulo"=>"Liga"),
                                            array("nombre"=>"url_target",   "tipo"=>"enumvalues",    "titulo"=>"Target html", "values"=>'_self,_blank', "enum"=>'Misma ventana,Nueva Ventana'),
                                            array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",2000,800,"Imagen principal"), "recorta_normal"=>true, "resize_normal"=>false, "carpeta"=>"../images/home/slideshow", "customsizes"=>array(),"customfields"=>array(), "required" => true ),
                                            array("nombre"=>"image_sm",    "tipo"=>"imagen",   "extra1"=>array("image_sm",2000,800,"Imagen tablet"), "recorta_normal"=>true, "resize_normal"=>false, "carpeta"=>"../images/home/slideshow", "customsizes"=>array(),"customfields"=>array() ),
                                            array("nombre"=>"image_xs",    "tipo"=>"imagen",   "extra1"=>array("image_xs",2000,800,"Imagen Mobil"), "recorta_normal"=>true, "resize_normal"=>false, "carpeta"=>"../images/home/slideshow", "customsizes"=>array(),"customfields"=>array() ),
                                            array("nombre"=>"priority",   "tipo"=>"enumvalues",   "titulo"=>"Prioridad", "enum"=>$contadores, "values"=>$contadores),
                                            array("nombre"=>"published",   "tipo"=>"checkbox", "titulo"=>"Publicada")
                                          ); 
        
            
            //TIPO FAQS 
            $cont++;
            $this->modulos_portal[$cont]=array('tablamodule'=>'faqs_type' ,'nameseccion'=>'tipofaqs' ,'id_table'=>'id' ,'titlemodule'=>'CATEGORIAS DE FAQS' ,'custom_save'=>false ,'save_file'=>'', 'sinimagen'=>true,'fields_search'=>"title", 'titles_search'=>"title", 'fieldlist' =>"priority,title,published", 'tituloslist'=>array('Orden[width=50]->handlersort','Título','Publicado[width=50]->active_check'), "orden"=>"priority ASC", "is_sortable" => true   );
            $this->modulos_portal[$cont]["fields"]=array(
                                            array("nombre"=>"title",        "tipo"=>"input",    "titulo"=>"Titulo", "required"=>true),
                                            array("nombre"=>"image",    "tipo"=>"imagen",   "extra1"=>array("image",500,500,"Icono"), "recorta_normal"=>true, "resize_normal"=>false, "carpeta"=>"../images/preguntas-frecuentes", "customsizes"=>array(),"customfields"=>array() ),
                                            array("nombre"=>"priority",   "tipo"=>"enumvalues",   "titulo"=>"Prioridad", "enum"=>$contadores, "values"=>$contadores),
                                            array("nombre"=>"published",   "tipo"=>"checkbox", "titulo"=>"Publicada"),
                                           );
            //FAQS
            $cont++;
            $this->modulos_portal[$cont]=array('tablamodule'=>'faqs' ,'nameseccion'=>'faqs' ,'id_table'=>'id' ,'titlemodule'=>'Preguntas Frecuentes' ,'custom_save'=>false ,'save_file'=>'', 'extra_actions'=>'', 'sinimagen'=>true, 'fields_search'=>"question", 'titles_search'=>"question", 'fieldlist' =>"priority,type_id,question,published", 'tituloslist'=>array('Orden[width=50]->handlersort','Tipo->cat_tipoFAQS','Pregunta','Publicado[width=50]->active_check'),"orden"=>"priority ASC", "is_sortable" => true );
            $this->modulos_portal[$cont]["fields"]=array(
                                            array("nombre"=>"type_id",   "tipo"=>"select",   "titulo"=>"categoría", "extra1"=>array(
                                              "tabla"=>"faqs_type","nombre"=>"type_id","id_tabla"=>"id","orden"=>"title","seleccionado"=>"","campomostrar"=>"title","funcionjavascript"=>"","tipodeconsuta"=>"tabla","titulovacio"=>"Selecciona un tipo")),
                                            array("nombre"=>"question",   "tipo"=>"input",    "titulo"=>"Pregunta", "required" => true),
                                            array("nombre"=>"answer",   "tipo"=>"editor",    "titulo"=>"Respuesta"),
                                            array("nombre"=>"priority",   "tipo"=>"enumvalues",   "titulo"=>"Prioridad", "enum"=>$contadores, "values"=>$contadores),
                                            array("nombre"=>"published",   "tipo"=>"checkbox", "titulo"=>"Publicada")

                                          );
            /*
            //  reporte_contacto 
            $cont++;
            $this->modulos_portal[$cont]=array('tablamodule'=>'contact' ,'nameseccion'=>'reporte_contacto' ,'id_table'=>'id' ,'titlemodule'=>'Reporte de Contactos' ,'custom_save'=>false ,'save_file'=>'', 'sinimagen'=>true, 'fields_search'=>"name", 'titles_search'=>"name", 'fieldlist' =>"date,name,phone,email,country,state,city,home,comments,bulletin", 'tituloslist'=>array('Fecha', 'Nombre', 'Farmacia', 'Telefono', 'Email', 'Pais', 'Estado', 'Ciudad', 'Home', 'Comentarios', 'Boletin'),"orden"=>"date DESC", "is_sortable" => false, "readonly" => true  );
            $this->modulos_portal[$cont]["fields"]=array();
            */
          
      }


}

?>
