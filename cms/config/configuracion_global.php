<?php

$debug=0;
$show_errors=0;

///////////////////////////
if ($show_errors){
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
} else {
	error_reporting(0);
}

/***** CONFIGURION DE DIRECTORIOS *****/
$server_name	=	"http://".$_SERVER['SERVER_NAME'];
$path_cms		=	"cms/";
$autourladm=substr($_SERVER['PHP_SELF'],0,strpos($_SERVER['PHP_SELF'],$path_cms));
$carpeta_dev		=	$autourladm;    // Cambiar por "" cuando se suba a su dominio ( /carpeta) 


$server	=	$_SERVER['DOCUMENT_ROOT'].$carpeta_dev;
$server	.=	$path_cms;
//$server		=	"";

$seccion	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", @$_GET['seccion']);

/***** ARCHIVO DE CONFIGURACIONES *****/

ini_set('session.gc_probability', 1);
ini_set('session.gc_divisor', 100);

# Start the session

@session_start();
@header ("Expires: Fri, 14 Mar 1980 20:53:00 GMT"); 				//la pagina expira en fecha pasada
@header ("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");	//ultima actualizacion ahora cuando la cargamos
@header ("Cache-Control: no-cache, must-revalidate"); 				//no guardar en cache
@header ("Pragma: no-cache"); 										//Paranoia, no guarda cache 
@define("NAME_WEBSITE","ADMINISTRADOR DE CONTENIDOS");

/***** DIRECTIVAS DE RESTRICCION  PHP.INI *****/
ini_set("memory_limit", "512M");		//para manejo de archivos grandes
ini_set("upload_max_filesize", "100M");	//para manejo de archivos grandes
ini_set("max_execution_time", "-1");	//tiempo de script infinito
ini_set("post_max_size", "100M");		//tiempo de script infinito
date_default_timezone_set("America/Mexico_City");


/***** CONEXION A BASE DE DATOS *****/

@define("CONNECTION_FILE","../inc/config.php");
require_once $server.CONNECTION_FILE;

$_prefijo=PREFIX;

/***** ROOTS IMAGENES Y VIDEOS *****/


/***** (LIBRERIAS Y CLASES) *****/
require_once($server.'libs_php/Db.frontclass.php');
require_once($server.'libs_php/class.backOffice.php');
require_once($server.'libs_php/class.backPage.php');
require_once($server.'libs_php/class.upload.php');
require_once($server.'libs_php/class.stdModules.php');
require_once($server.'libs_php/class.controlPanel.php');

require_once($server.'functions/funciones.php');
require_once($server.'functions/adicionales1.php');

/***** INCIAMOS NUESTRO MOTOR MYSQL DEL SISTEMA (backOffice.Class) *****/
$User = new backPage;
$CPanel = new controlPanel;

/******   MENU Y PERMISOS PORTAL        ******/
require_once $server."config/definicion_menu.php";

/*****	DEFINICION DE MODULOS BASADOS EN FORMATO STANDARD  **********/
require_once $server."config/definicion_modulos.php";

$definitionsCMS = new definicion_modulos;

/***** PARA DEBUGGEAR CONSULTAS Y ENVIO DE REQUEST *****/
//$User->debug	=	"yes"; /*ELIMINAR COMENTARIOS DE LA LINEA DE $DEBUG*/ 

/***** Acciones de los MOD'S (modulos de secciones) *****/
$listar=true;
$modo=(empty($_REQUEST['accion']))?"listar":$_REQUEST['accion'];
$showform=false;
if(isset($_REQUEST['accion'])) {
	$listar		=	($_REQUEST['accion']=="listar")?true:false;
	$nuevo		=	($_REQUEST['accion']=="nuevo")?true:false;
	if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
		$mostrar		=($_REQUEST['accion']=="mostrar")?true:false;
		$modificar		=($_REQUEST['accion']=="editar")?true:false;
		$modificarPics	=($_REQUEST['accion']=="editarpics")?true:false;
	}
}
 


/**/

if (!isset($seccion_no_restringida) ){
	if(!isset($_SESSION['nombre']) && !isset($_SESSION['id_admin']))
		$User->redirecciona($server_name.$carpeta_dev.$path_cms.'index.php');
	
	$permm_find=0;
	if($seccion!='' && $seccion!='home'){   //Busca la seccion en el menu y sus permisos y los compara con  el nivel de usuario
		for ($mi=0;$mi<count($menu_portal);$mi++){
			for ($mj=0;$mj<count(@$menu_portal[$mi]['campos']);$mj++){
				if($menu_portal[$mi]['campos'][$mj][1]==$seccion){
					for ($pi=0;$pi<count($_SESSION['permisosadm']);$pi++){
						if ($_SESSION['permisosadm'][$pi]==$menu_portal[$mi]['campos'][$mj][3])
							$permm_find=1;
					}
				}
			}
		}
		if ($permm_find==0){
			$User->redirecciona($server_name.$carpeta_dev.$path_cms.'index.php');
		}
	}
}

if(!empty($_REQUEST['initial'])){
	unset($_SESSION['lastsearch']);
}  
if (!empty($_REQUEST['last']) && !empty($_SESSION['lastsearch'])){ 
	foreach($_SESSION['lastsearch'] as $k=>$v){
		$_POST[$k]=$_SESSION['lastsearch'][$k];
		$_REQUEST[$k]=$_SESSION['lastsearch'][$k];
	}
} else {
}

$load_global=1;

?>