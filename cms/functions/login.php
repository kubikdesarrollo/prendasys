<?php
	
	session_start();

	if(!function_exists('hash_equals'))
	{
	    function hash_equals($str1, $str2)
	    { 
	        if(strlen($str1) != strlen($str2))
	        {
	            return false;
	        }
	        else
	        {
	            $res = $str1 ^ $str2;
	            $ret = 0;
	            for($i = strlen($res) - 1; $i >= 0; $i--)
	            {
	                $ret |= ord($res[$i]);
	            }
	            return !$ret;
	        }
	    }
	}

	/***** CONEXION A BASE DE DATOS *****/
	require_once "../../inc/config.php";
	require_once('../libs_php/Db.frontclass.php');
	require_once('../libs_php/class.user.php');
	require_once('../functions/funciones.php');
	$User = new User;
	
	$query	=	"SELECT * FROM ".PREFIX."admin WHERE user = :userNickName";
	$User->login($_REQUEST['user'],$query);
	
	if ($User->userIntentos<6){
		if ($User->userExist) {
			$iteraciones = "100000";
			$hash_base = base64_decode($User->userPass); 
			$salt_base = base64_decode($User->token); 
			$salt2_sha256 = '$5$rounds='.$iteraciones.'$'.$salt_base.'$'; 
			//Obtener la contraseña introducida 
			$password_entrada = $_REQUEST['pass']; 
			if (hash_equals(crypt($password_entrada, $salt2_sha256),$hash_base) ){ 
				session_regenerate_id();
				$_SESSION['id_admin']		=	$User->userID;
				$_SESSION['tipo']			=	$User->userLevel;
				$_SESSION['nombre']			=	$User->userName;
				$_SESSION['admin'] 			= 	$User->userNickName;
				$_SESSION['departamento'] 	= 	$User->userDepartamento;
				$_SESSION['facebook']	=	$User->userUsuariofb;
				$_SESSION['emailUsuario']	=	$User->userEmail;
				
				echo '{ "nombre" : "'.$_SESSION['nombre'].'" , "aceptar" : "si" }';
				
				$sqlperms="SELECT * FROM ".PREFIX."permisos WHERE id_usuario='".$_SESSION['id_admin']."'";
				$resperms=$User->query($sqlperms);
				$permsadm= array();
				foreach ($resperms as $datoperms){
					$permsadm[]=$datoperms["seccion"];
				}
				$_SESSION['permisosadm']=$permsadm;
			
			} else {
					//revisamos los intentos y bloqueamos cuenta
					$sqlup="UPDATE ".PREFIX."admin SET intentos=".($User->userIntentos+1).(($User->userIntentos+1>=6)?', bloqueada=1':'')." WHERE id_admin=".$User->userID;
					$User->query($sqlup);
					outSession();
				}
			} else {
				outSession();
			}
		} else {
			@session_unset();
			@session_destroy();
			sleep(4);
			echo '{ "aceptar" : "no", "intentos":6}';
			@ob_end_flush();	
		}
	
	@ob_end_flush();
	$User->CloseConnection();
?>