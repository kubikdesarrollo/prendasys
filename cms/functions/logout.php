<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");

session_start();

	session_destroy();
	session_unset();
	
	header( "Location: ../index.php" );
?> 