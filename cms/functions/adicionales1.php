<?php
//********************************//
//    FUNCIONES ADICIONALES       //
//********************************//

function generararbol($tabla,$id_tabla,$nombrecampo,$padre,$id_padre,$acciones,$nivel,$orden="",$maximo_subniveles=100){
	$controles="";
	for ($i=0;$i<count($acciones);$i++){
		switch($acciones[$i]){
			case 'editar': $controles.= '<a onclick="editararbol(_id_)">Editar</a>'; break;
			case 'eliminar': $controles.= '<a onclick="eliminararbol(_id_)">eliminar</a>'; break;
		}
	}
	$sql="SELECT * FROM ".$tabla." WHERE ".$padre."=".$id_padre;
	if (strlen($orden)>1)
		$sql.=" ORDER BY ".$orden;
	$res=mysql_query($sql);
	if ($nivel!=0) echo '<ul class="arbol_categorias"  title="'.$id_padre.'" >';
	while ($datos=@mysql_fetch_assoc($res)){
		echo '<li id="itemmenulist_'.$datos[$id_tabla].'"><div class="titulosarbol" >';
		for ($j=0;$j<$nivel;$j++){
			echo "&nbsp;&nbsp;&nbsp;";
		}
		echo '<a class="handlerimgch"></a>'.$datos[$nombrecampo].'</div><div class="controlsarbol">'.str_replace("_id_",$datos[$id_tabla],$controles).'</div>';
		
		$sqlhijo="SELECT * FROM ".$tabla." WHERE ".$padre."=".$datos[$id_tabla]; 
		$reshijo=mysql_query($sqlhijo);
		if (mysql_num_rows($reshijo)){
			if ($maximo_subniveles>$nivel)
				generararbol($tabla,$id_tabla,$nombrecampo,$padre,$datos[$id_tabla],$acciones,($nivel+1),$orden,$maximo_subniveles);
		}
		echo '</li>';
	}
	if ($nivel!=0) echo '</ul>';
}

function arbolselect($nombre,$tabla,$id_tabla="id",$padre,$orden="",$campomostrar="nombre",$titulovacio="",$maximo_subniveles=100){  
	echo '<select name="'.$nombre.'"  id="'.$nombre.'" '.((!empty($funcionjavascript))?'onchange="'.$funcionjavascript.'"':'').'>';
	if ($titulovacio!="")
		echo '<option value="">'.$titulovacio.'</option>';
	
	recursiveselect($tabla,$id_tabla,$campomostrar,$padre,0,0,$orden,$maximo_subniveles);
	
	echo '</select>';
}

function recursiveselect($tabla,$id_tabla,$nombrecampo,$padre,$id_padre,$nivel,$orden="",$maximo_subniveles=100){ 
	$sql="SELECT * FROM ".$tabla." WHERE ".$padre."=".$id_padre;
	if (strlen($orden)>1)
		$sql.=" ORDER BY ".$orden;
	$res=mysql_query($sql);
	while ($datos=@mysql_fetch_assoc($res)){
		echo '<option value="'.$datos[$id_tabla].'">';
		for ($j=0;$j<$nivel;$j++){
			echo "&nbsp;&nbsp;&nbsp;";
		}
		echo $datos[$nombrecampo].'</option>';
		
		$sqlhijo="SELECT * FROM ".$tabla." WHERE ".$padre."=".$datos[$id_tabla]; 
		$reshijo=mysql_query($sqlhijo);
		if (mysql_num_rows($reshijo)){
			if ($maximo_subniveles>$nivel)
				recursiveselect($tabla,$id_tabla,$nombrecampo,$padre,$datos[$id_tabla],($nivel+1),$orden,$maximo_subniveles);
		}
	}
}

function __json_encode( $data ) {           
    if( is_array($data) || is_object($data) ) {
        $islist = is_array($data) && ( empty($data) || array_keys($data) === range(0,count($data)-1) );
       
        if( $islist ) {
            $json = '[' . implode(',', array_map('__json_encode', $data) ) . ']';
        } else {
            $items = Array();
            foreach( $data as $key => $value ) {
                $items[] = __json_encode("$key") . ':' . __json_encode($value);
            }
            $json = '{' . implode(',', $items) . '}';
        }
    } elseif( is_string($data) ) {
		$data=utf8_encode_all($data);
        # Escape non-printable or Non-ASCII characters.
        # I also put the \\ character first, as suggested in comments on the 'addclashes' page.
        $string = '"' . addcslashes($data, "\\\"\n\r\t/" . chr(8) . chr(12)) . '"';
        $json    = '';
        $len    = strlen($string);
        # Convert UTF-8 to Hexadecimal Codepoints.
        for( $i = 0; $i < $len; $i++ ) {
           
            $char = $string[$i];
            $c1 = ord($char);
           
            # Single byte;
            if( $c1 <128 ) {
                $json .= ($c1 > 31) ? $char : sprintf("\\u%04x", $c1);
                continue;
            }
           
            # Double byte
            $c2 = ord($string[++$i]);
            if ( ($c1 & 32) === 0 ) {
                $json .= sprintf("\\u%04x", ($c1 - 192) * 64 + $c2 - 128);
                continue;
            }
           
            # Triple
            $c3 = ord($string[++$i]);
            if( ($c1 & 16) === 0 ) {
                $json .= sprintf("\\u%04x", (($c1 - 224) <<12) + (($c2 - 128) << 6) + ($c3 - 128));
                continue;
            }
               
            # Quadruple
            $c4 = ord($string[++$i]);
            if( ($c1 & 8 ) === 0 ) {
                $u = (($c1 & 15) << 2) + (($c2>>4) & 3) - 1;
           
                $w1 = (54<<10) + ($u<<6) + (($c2 & 15) << 2) + (($c3>>4) & 3);
                $w2 = (55<<10) + (($c3 & 15)<<6) + ($c4-128);
                $json .= sprintf("\\u%04x\\u%04x", $w1, $w2);
            }
        }
    } else {
        # int, floats, bools, null
        $json = strtolower(var_export( $data, true ));
    }
    return $json;
} 
function utf8_decode_all($dat) // -- It returns $dat decoded from UTF8
{
  if (is_string($dat)) return utf8_decode($dat);
  if (!is_array($dat)) return $dat;
  $ret = array();
  foreach($dat as $i=>$d) $ret[$i] = utf8_decode_all($d);
  return $ret;
}
function utf8_encode_all($dat) // -- It returns $dat encoded to UTF8
{
  if (is_string($dat)) return utf8_encode($dat);
  if (!is_array($dat)) return $dat;
  $ret = array();
  foreach($dat as $i=>$d) $ret[$i] = utf8_encode_all($d);
  return $ret;
} 

///////////////////////////////////////////////////////////////////////////
///   funciones para el listado personalizado modulo_activar
function buscanombrecat($variable){
	$listcats=explode("]",NOMBRECATEGORIAS);
	$v=explode("|",$variable);
	$cadena="";
	for ($i=0;$i<count($v);$i++){
		for ($j=0;$j<count($listcats);$j++){
			if ("|".$v[$i]."|"=="|".substr($listcats[$j],0,strpos($listcats[$j],"|"))."|"){
				$cadena.=substr($listcats[$j],strpos($listcats[$j],"|")+1);
			}
		}
	}
	return $cadena;
}
///////////////////////////////////////////////////////////////////////////
///  funciones del nuevo panel
function bloqueradio($label,$field,$datos){
	echo '<dl class="Mcontrol-group">
                                <dt class="Mcontrol-label">'.$label.': </dt>
                                <dd class="ddnoeditable"><span id="'.$field.'_value" >'.(@$datos[$field]).'</span></dd>
                                <dd class="ddsieditable">
                                    Yes <input type="radio" name="'.$field.'" value="yes" '.((@$datos[$field]=='yes')?'checked="checked"':'').'/> 
                                    No <input type="radio" name="'.$field.'" value="no" '.((@$datos[$field]=='no')?'checked="checked"':'').'/> 
                                    N/A <input type="radio" name="'.$field.'" value="na" '.((@$datos[$field]=='na')?'checked="checked"':'').' /> 
                                </dd>
                            </dl>
	';
}
function bloquetext($label,$field,$value,$required=false,$editable=true,$remote=false){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
              	  <p class="form-control-static elementstatic">'.$value.'&nbsp;</p>
                  '.(($editable)?'<input type="text" name="'.$field.'" id="'.$field.'" class="form-control elementeditable" placeholder="'.$label.'" value="'.$value.'">':'<p class="form-control-static elementeditable">'.$value.'&nbsp;</p>').'
              </div>
          </div>';

}
function bloqueinput($label,$field,$value,$required=false, $clase="", $noeditable="false"){
	echo '<div class="form-group '.$clase.'">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">';
          if($noeditable!=true){
            echo '<input type="text" name="'.$field.'" id="'.$field.'" class="form-control '.((@$required==true)?'required':'').'" placeholder="'.$label.'" value="'.$value.'" title="'.$label.' Requerido">';
          } else {
            echo '<input type="hidden" name="'.$field.'" id="'.$field.'"  value="'.$value.'">'.$value;
          }
  echo  '     </div><div class="clear"></div>
          </div>';

}
function bloquetelefono($label,$field,$value,$required=false, $clase="", $noeditable="false"){
  echo '<div class="form-group '.$clase.'">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">';
          if($noeditable!=true){
            echo '<input type="text" name="'.$field.'" id="'.$field.'" class="form-control '.((@$required==true)?'required':'').'" placeholder="'.$label.'" value="'.$value.'" title="'.$label.' Requerido" data-mask="(99) 9999-9999"><span class="help-inline">(99) 9999-9999</span>';
          } else {
            echo '<input type="hidden" name="'.$field.'" id="'.$field.'"  value="'.$value.'">'.$value;
          }
  echo  '     </div><div class="clear"></div>
          </div>';

}
function bloquenumeric($label,$field,$value,$required=false,$decimals=false){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$field.'" id="'.$field.'" class="form-control '.((@$decimals==true)?'number':'digits').' '.((@$required==true)?'required':'').'" placeholder="'.$label.'" value="'.$value.'" title="* Solo numeros '.((@$decimals==true)?'con decimales':'').'">
              </div>
          </div>';

}
function bloquetextarea($label,$field,$value,$clase=''){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <textarea name="'.$field.'" id="'.$field.'" class="form-control '. $clase .'" placeholder="'.$label.'">'.$value.'</textarea>
              </div><div class="clear"></div>
          </div>';

}
function bloqueyoutube($label,$field,$value){
   echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$field.'" id="'.$field.'" class="form-control" placeholder="'.$label.'" value="'.$value.'"><br>
                  <div class="col-xs-6 col-sm-3">'.((!empty($value))?'<iframe width="560" height="315" src="https://www.youtube.com/embed/'.$value.'" frameborder="0" allowfullscreen></iframe>':'').'</div>';
    echo '    </div>
          </div>';
}
function bloquevimeo($label,$field,$value){
   echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$field.'" id="'.$field.'" class="form-control" placeholder="'.$label.'" value="'.$value.'"><br>
                  <div class="col-xs-6 col-sm-3">'.((!empty($value))?'<iframe width="560" height="315" src="https://player.vimeo.com/video/'.$value.'?title=0&byline=0&portrait=0" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe><script src="https://player.vimeo.com/api/player.js"></script>':'').'</div>';
    echo '    </div>
          </div>';
}
function bloqueeditor($label,$field,$value){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <textarea name="'.$field.'" id="'.$field.'" class="form-control ckeditor" placeholder="'.$label.'">'.$value.'</textarea>
              </div>
          </div>';

}
function bloqueenumvalues($label,$field,$value,$enum,$values,$clase=""){ 
	$options="";
	$valuestatic="";
	$opciones=array();
	$enumera=explode(",",$enum);
	$vals=explode(",",$values);
	foreach ($enumera as $key => $valuei) {
		$opciones[$vals[$key]]=$valuei;
	}
	if (count($opciones)>1){
		foreach( $opciones as $k=> $v){ 
    		$options.= '<option value="'.$k.'" '.(($value==$k)?'selected="selected"':'').'>'.$v.'</option>';
    		if ($value==$k) $valuestatic=$v;
		}
	}

	echo '<div class="form-group '.$clase.'">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <select name="'.$field.'" id="'.$field.'" class="form-control">'.$options.'</select>
              </div>
          </div>';
}
function bloquechecked($label,$field,$datos,$clase=""){
	echo '<div class="form-group '.$clase.'">
			<label class="col-lg-2 control-label">'.$label.'</label>
			<div class="col-lg-10">
					<div class="switch switch-square"
					   data-on-label="<i class=\' icon-ok\'></i>"
					   data-off-label="<i class=\'icon-remove\'></i>">
					  <input name="'.$field.'" id="'.$field.'" type="checkbox" '.(($datos==1)?'checked=""':'').' />
					</div>
			</div>
		</div>';			
}
function bloquedate($label,$field,$value){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$field.'" id="'.$field.'" class="form-control fecha_input" placeholder="'.$label.'" value="'.(!empty($value)?$value:date('Y-m-d')).'">
              </div>
          </div>';

}
function bloquedatetime($label,$field,$value){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$field.'" id="'.$field.'" class="form-control fechatiempo_input" placeholder="'.$label.'" value="'.(!empty($value)?$value:date('Y-m-d')).'">
              </div>
          </div>';

}
function bloquepassword($label,$field){
	echo '<div class="form-group elementeditable">
              <label class="col-lg-2 control-label">'.$label.'</label>
              <div class="col-lg-10">
                  <input type="password" name="'.$field.'" id="'.$field.'" class="form-control" placeholder="'.$label.'" value="">
              </div>
          </div>';

}
function bloquecheckbox($label,$field,$datos){
	echo '<div class="form-group">
			<label class="col-lg-2 control-label">'.$label.'</label>
			<div class="col-lg-10">
              	  	<p class="form-control-static elementstatic">'.(($datos==1)?"YES":"NO").'</p>
					<div class="switch switch-square elementeditable"
					   data-on-label="<i class=\' icon-ok\'></i>"
					   data-off-label="<i class=\'icon-remove\'></i>">
					  <input name="'.$field.'" id="'.$field.'" type="checkbox" '.(($datos==1)?'checked=""':'').' />
					</div>
			</div>
		</div>';			
}

function bloqueimagen($config,$datos,$tabla="",$id_tabla="",$seccion="",$required=false){
  $postmax=ini_get('upload_max_filesize');
  echo '<div class="col-xs-2"></div><div class="alert alert-info fade in col-xs-10"><p>La Imagen debe ser de Ancho: '.((intval(@$config["extra1"][1])>0)?$config["extra1"][1].'px':"libre").' por Alto: '.((intval(@$config["extra1"][2])>0)?$config["extra1"][2].'px':"libre").'. Tamaño Máximo del archivo: '.$postmax.'B</p></div>';
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$config["extra1"][3].'</label>
              <div class="col-lg-10"><div id="itemg_'.$config["nombre"].'">';
              $ruta=$config["carpeta"]."/".@$datos[$config["nombre"]];
              if (file_exists($ruta) && !empty($datos[$config["nombre"]])){
                $required = false;
              	echo '<a href="'.$ruta.'" rel="group" class="fancybox"><img src="'.$ruta.'" class="imagenfrompanel"></a>';
                if (!empty($tabla)){
                  echo '<button type="button" class="btn btn-danger pull-right" onclick="delImagen(\''.$config["nombre"].'\',\''.$tabla.'\',\''.$id_tabla.'\',\''.$seccion.'\',\''.$datos[$id_tabla].'\')"><i class="icon-remove"></i></button>';
                }
              }
              echo "</div><div style='clear:left'>".'<input type="file" name="imagen_'.$config["extra1"][0].'" '.((@$required==true)?'required':'').' id="imagen_'.$config["extra1"][0].'" /></div>';
              echo '
              </div>
          </div>';
}

function bloquecomavalues($config,$valor){ 
  $valor1 = explode(',', $valor);
  echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$config["titulo"].'</label>
              <div class="col-lg-10">
                <select class="multiselect" '. ((isset($config["multi"]) && $config["multi"] == 'off')?'':'multiple="multiple" style="height: 150px;"') .' name="'.$config["nombre"].'[]" id="'.$config["nombre"].'" >';
                foreach ($config["values"] as $key => $value) { 
                  echo '<option value="'.$key.'" '.((in_array($key, $valor1)!==false)?'selected="selected"':'').'>'.$value.'</option>';
                }                     
              echo '</select>
              </div>
          </div>
      <script type="text/javascript">
      $(document).ready(function() {
        //$("#'.$config["nombre"].'").multiselect({numberDisplayed: 6, includeSelectAllOption: true, selectAllText: "Todos"});
      });
      </script>
          ';
}

function bloquecolor($configuracion,$value){
	echo '<div class="form-group">
              <label class="col-lg-2 control-label">'.$configuracion["titulo"].'</label>
              <div class="col-lg-10">
                  <input type="text" name="'.$configuracion["nombre"].'" id="'.$configuracion["nombre"].'" class="form-control" placeholder="'.$configuracion["titulo"].'" value="'.$value.'" title="'.$configuracion["titulo"].'" style="height: 2em;">
              </div>
          </div>
          <script>
          $(function(){
			       $("#'.$configuracion["nombre"].'").minicolors();
          });
		  </script>
          ';

}

function autorizar($value){
		return '<button type="button" onclick="autorizar(\''.$_REQUEST['seccion'].'\',\''.$value[1].'\')" id="irowlist_'.$value[1].'" '.(($value[0]==0)?' class="btn btn-danger"><i class="icon-remove"></i></button>':' class="btn btn-success" ><i class="icon-check-sign"></i></button>');
}
function bloqueautorizar($label,$field,$datos,$indice){
	if ($indice>0){
		echo '<div class="form-group">
				<label class="col-lg-2 control-label">'.$label.'</label>
				<div class="col-lg-10">
						<button type="button" onclick="autorizar(\''.$_REQUEST['seccion'].'\',\''.$indice.'\')" id="irowlist_'.$indice.'" '.(($datos==0)?' class="btn btn-danger"><i class="icon-remove"></i></button>':' class="btn btn-success" ><i class="icon-check-sign"></i></button>').'
				</div>
			</div>';	
	}		
}
function bloquesupergaleria($configuracion,$idtable){
	include_once 'libs_php/class.gallery.php';
  $galleria = new ClassGallery();
  $galleria->builuploader($configuracion,$idtable);
  $galleria->listimages();
}
function bloquesubtabla($configuracion,$idtable){
  include 'libs_php/class.subtabla.php';
  $subtabla= new ClassSubtabla();
  $subtabla->builmodule($configuracion,$idtable);
}
function bloquemodal($cual){
  include_once 'libs_php/class.subtabla.php';
  $subtabla= new ClassSubtabla();
  $subtabla->contentform($cual["config"],$cual["id"],"");
}
function deflinknombre($value){
	if (!empty($value[0])){
		return '<a href="admin.php?seccion='.$_REQUEST['seccion'].'&accion=editar&id_edit='.$value[1].'&ret='.$_REQUEST['seccion'].'" >'.$value[0].'</a>';
	} else return "";
}

function bloquenota($text){
  echo '<div class="col-xs-2"></div><div class="alert alert-info fade in col-xs-10"><p>'.$text.'</p></div>';
}

function tdfordate($value){
	if (empty($value[0])) return "";
	else return '<div class="tdfordate">'.$value[0].'</div>';
}
function tablenumber($value){
	return '<div align="center">'.num($value[0]).'</div>';
}

function num($valor){
	return number_format(intval(@$valor),0,'.',',');
}
function iniciosemana($valor){
	$dateTime = new DateTime($valor);
    if ($dateTime->format("w")!=1)
      $dateTime->modify('last Monday');
    return $dateTime->format('Y-m-d 00:00:00');
}
function dateformat($valor,$formato='Y-m-d H:i:s'){
	$dateTime = new DateTime($valor);
	return $dateTime->format($formato);
}

/*especificos */
function showcategories($valor){
	if (empty($valor[0])) return "";
	else {
		switch($valor[0]){
			case 1: $cadena='NOVIA'; break;
			case 2: $cadena='NOCHE'; break;
			case 3: $cadena='XV´s'; break;
			case 4: $cadena='ACCESSORIOS'; break;
		}
		return '<div aling="center">'.$cadena.'</div>';
	}
}
function esdestacado($valor){
	if (empty($valor[0])) return "";
	else return '<div aling="center"><span class="badge bg-info"><i class="icon-asterisk"></i>Destacado</span></div>';
}
function esfavorito($valor){
	if (empty($valor[0])) return "";
	else return '<div aling="center"><span class="badge bg-primary"><i class="icon-gittip"></i>Favorito</span></div>';
}

function contanuncios($valor){
	$select="SELECT * from vestidos where id_usuario='".$valor[0]."' AND final=1";
	$res=mysql_query($select);
	$total=0;
	$vendidos=0;
	while($dato=@mysql_fetch_assoc($res)){
		if ($dato["vendido"]==1) $vendidos++;
		$total++;
	}
	$select="SELECT * from accesorios where id_usuario='".$valor[0]."' AND  final=1";
	$res=mysql_query($select);
	while($dato=@mysql_fetch_assoc($res)){
		if ($dato["vendido"]==1) $vendidos++;
		$total++;
	}
	return $total;
}
function estapagado($valor){
  if (empty($valor[0])) return "";
  else return '<div aling="center"><span class="badge bg-info">Pagado</span></div>';
}
function esenviado($valor){
  if (empty($valor[0])) return "";
  else return '<div aling="center"><span class="badge bg-success">Enviado</span></div>';
}

function fechashort($valor){
  $meses=array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');
  if (empty($valor[0])) return "";
  else if($valor[0]!="0000-00-00 00:00:00"){
    $date = new DateTime($valor[0]);
    return  $date->format('d - '). @$meses[$date->format('n')];
  } else
    return "";
}
function active_check($valor){
  if (empty($valor[0])) return "";
  else return '<div aling="center"><span class="badge bg-primary">Autorizado</span></div>';
}
function cat_tipoFAQS($valor){
  global $CPanel;
  $nombrecategoria = '';
  $cualcate=$CPanel->row("SELECT title FROM faqs_type WHERE id=".@$valor[0]);
  if (!empty($cualcate))
      $nombrecategoria = $cualcate["title"];
  return $nombrecategoria;
}
function handlersort($valor){
  return '<div class="btn btn-white handlersort" data-toggle="button"><i class="icon-move"></i></div>';
}

function licascada($valores,$idtabla,$campo,$seleccionado){
  for($i=0;$i<count($valores);$i++){
    if (empty($valores[$i]["id_padre"])){
      //echo '<option value="'.$valores[$i][$idtabla].'" '.(($seleccionado==$valores[$i][$idtabla])?'selected':'').'>'.$valores[$i][$campo]."</option>";
    }
    else {
      for($j=0;$j<count($valores);$j++){
        if ($valores[$j][$idtabla]==$valores[$i]["id_padre"]) $nombrepadre=$valores[$j][$campo];
      }
      echo '<option value="'.$valores[$i][$idtabla].'" '.(($seleccionado==$valores[$i][$idtabla])?'selected':'').'>'.@$nombrepadre." > ".$valores[$i][$campo]."</option>";
    }

  }
}



function ver_productos($valor){
  if (empty($valor[0]) ) return "";
  else {
    if (@$_SESSION["child"]>0){
      return '<div aling="center"><a href="admin.php?seccion=actividades&categoria='.$valor[1].'">Ver Actividades</a></div>';
    }
    else return "";
  }
}

function ver_contenidos($valor){
  return '<div aling="center"><a href="admin.php?seccion=editardesarrollo&id_desarrollo='.$valor[1].'">Editar contenidos</a></div>';
}

function nombreempresa($ide){
  global $CPanel; 
  $cualcate=$CPanel->row("SELECT * FROM ".PREFIX."empresas WHERE id_empresa=".@$ide[0]);
    if (!empty($cualcate)){
        return '<a href="admin.php?seccion=directorio&accion=editar&id_edit='.$cualcate["id_empresa"].'">'.$cualcate["nombre"]. "</a>";
    }
}

function nombrecategoria($ide){
  global $CPanel;
  $nombrecategoria = '';
  $cualcate=$CPanel->row("SELECT * FROM categories WHERE id=".@$ide[0]);
  if (!empty($cualcate))
      $nombrecategoria = $cualcate["title"];
  return $nombrecategoria;
}

function echoid($ide){
  return @$ide[0];
}

function nombrespec($ide){
  global $CPanel; 
  $cualcate=$CPanel->row("SELECT * FROM Cat_tech_specs_articulo WHERE id=".@$ide[0]);
    if (!empty($cualcate)){
        return $cualcate["tech_spec"];
    }
}

function iconOS($ide){
  global $CPanel; 
  $cualcate=$CPanel->row("SELECT * FROM os_equipos WHERE id=".@$ide[0]);
    if (!empty($cualcate)){
        return $cualcate["title"];
    }
}
function tiposoporte($ide){
  global $CPanel; 
  $cualcate=$CPanel->row("SELECT * FROM Cat_tipoSoporte WHERE id=".@$ide[0]);
    if (!empty($cualcate)){
        return $cualcate["Descripcion"];
    }
}
function nombreproducto($ide){
  global $CPanel; 
  $cualcate=$CPanel->row("SELECT * FROM Cat_Articulos WHERE itemcode=:code",array("code"=>''.@$ide[0]));
    if (!empty($cualcate)){
        return $cualcate["itemcode"]." / ".$cualcate["titulo_articulo"];
    }
}
?>