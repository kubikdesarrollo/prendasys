<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title>Control Panel</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script language="javascript" src="libs_js/jquery.tools.min.js"></script>    
    <script language="javascript" src="libs_js/login.js"></script>
</head>

  <body class="login-body" id="log">

    <div class="container">

      <form class="form-signin"  id="login_form" method="post" name="login">
        <h2 class="form-signin-heading">
            <img src="images/logo.png" width="300" alt=""><br>
            PANEL DE ADMINISTRACIÓN
        </h2>
        <div class="login-wrap">
            <div class="alert alert-block alert-danger fade in" id="login_alert" style="display:none">
                  <button type="button" class="close close-sm" data-dismiss="alert">
                      <i class="icon-remove"></i>
                  </button>
                  <strong>Error!</strong> Datos incorrectos.
            </div>        
            <input type="text" class="form-control" placeholder="Usuario" autofocus id="user" name="user">
            <input type="password" class="form-control" placeholder="Contraseña" id="pass" name="pass">
            <button class="btn btn-lg btn-login btn-block loginbutton" type="submit" id="Entrar"  name="Login">Entrar</button>
            <div class="loading"></div>
        </div>

      </form>

    </div>


  </body>
</html>
