<?php 
// Formato clasico, foto y miniatura
function procesaimagen($campopost, $campotabla, $new_filename,$ruta_imags,$ancho_normal,$alto_normal,$resize_normal=true,$recorta_normal=true,$genera_miniatura=false,$ancho_miniatura,$alto_miniatura){
	$nota="";
	if(!empty($_FILES[$campopost]['name'])) {
		$extencion 	= 	ext($_FILES[$campopost]['name']);
		$extencion	=	strtolower('.'.$extencion);
		
		if ($extencion==".gif" || $extencion==".svg"){
			if (is_uploaded_file($_FILES[$campopost]['tmp_name'])){
				$url=$ruta_imags.$new_filename.$extencion;
				$tmp_name = $_FILES[$campopost]["tmp_name"];
				@move_uploaded_file($tmp_name,$url); 
				$_POST[$campotabla]	=	$new_filename.$extencion;	
			}
		}else{
		
		$foo = new Upload($_FILES[$campopost]);
		$foo->file_new_name_body 	= 	$new_filename;
		$foo->jpeg_quality 			=	70;
		$foo->image_resize          = 	$resize_normal;
		$foo->image_ratio_crop  	= 	$recorta_normal;
		
		if ($alto_normal>0)		
			$foo->image_y 			= 	$alto_normal;
		if ($ancho_normal>0)
			$foo->image_x 			= 	$ancho_normal;
		if (@$ancho_normal==0)
			$foo->image_ratio_x		= 	true;
		if (@$alto_normal==0)
			$foo->image_ratio_y		= 	true;


		if ($foo->uploaded) {
			$foo->Process($ruta_imags);
			if ($foo->processed) { 
				$_POST[$campotabla]	=	$new_filename.$extencion;		
				@chmod($ruta_imags.$new_filename.$extencion, 0777);
			} else { 
				echo 'error : ' . $foo->error;
			}	
		} else { echo 'error : ' . $foo->error; }	
		
		/********* Thumbnail **********/
			if ($genera_miniatura){
				$foo->file_new_name_body 	= 	$new_filename;
				$foo->jpeg_quality 			=	70;
				$foo->image_resize 			= 	true;
				$foo->image_ratio_crop  	= 	false;
				$foo->image_y 				= 	$alto_miniatura;
				$foo->image_x 				= 	$ancho_miniatura;
				$foo->image_ratio_y			= 	true;

				$foo->Process($ruta_imags."thumbs/");
				if ($foo->processed) { 
					$nota	.=	" Thumb = Thumb subido exitosamente <br> ";
				} else { 
					$nota	.= ' errorThumb = ' . $foo->error . ' <br> ' ;
				}	
			}
			$foo->Clean(); 
		}
	}
}

// Sube la foto principal y genera N copias con diferentes tama�os
function procesaimagenmulti($campopost, $campotabla, $new_filename,$ruta_imags,$ancho_normal,$alto_normal,$resize_normal=true,$recorta_normal=true,$otrasmedidas=array()){
	$nota="";
	if(!empty($_FILES[$campopost]['name'])) {
		$extencion 	= 	ext($_FILES[$campopost]['name']);
		$extencion	=	strtolower('.'.$extencion);
		
		if ($extencion==".gif" || $extencion==".svg"){
			if (is_uploaded_file($_FILES[$campopost]['tmp_name'])){
				$url=$ruta_imags.$new_filename.$extencion;
				$tmp_name = $_FILES[$campopost]["tmp_name"];
				@move_uploaded_file($tmp_name,$url); 
				$_POST[$campotabla]	=	$new_filename.$extencion;	
			}
		} else {
		
		
			$foo = new Upload($_FILES[$campopost]);
			$foo->file_new_name_body 	= 	$new_filename;
			$foo->jpeg_quality 			=	70;
			$foo->image_resize          = 	$resize_normal;
			$foo->image_ratio_crop  	= 	$recorta_normal;
			//$foo->image_ratio = true;
			//$foo->image_ratio_fill = true;
			if ($alto_normal>0)		
				$foo->image_y 			= 	$alto_normal;
			if ($ancho_normal>0)
				$foo->image_x 			= 	$ancho_normal;
			if (@$ancho_normal==0)
				$foo->image_ratio_x		= 	true;
			if (@$alto_normal==0)
				$foo->image_ratio_y		= 	true;
	
			if ($foo->uploaded) {
				$foo->Process($ruta_imags);
				if ($foo->processed) { 
					$_POST[$campotabla]	=	$new_filename.$extencion;		
					@chmod($ruta_imags.$new_filename.$extencion, 0777);
				} else { 
					echo 'error : ' . $foo->error;
				}	
			} else { echo 'error : ' . $foo->error; }	
			
			/********* Otras medidas **********/
			if (count($otrasmedidas)>0){ 
				for($i=0;$i<count($otrasmedidas);$i++){
					$tempsize=$otrasmedidas[$i];
					$foo->file_new_name_body 	= 	$new_filename;
					$foo->jpeg_quality 			=	70;
					$foo->image_resize 			= 	true;//(@$tempsize["resize"]==false)?false:true;
					$foo->image_ratio_crop  	= 	true;//(@$tempsize["recorta"]==false)?false:true;
					if ($tempsize["ancho"]>0)
						$foo->image_x 				= 	intval($tempsize["ancho"]);
					if ($tempsize["alto"]>0)
						$foo->image_y 				= 	intval($tempsize["alto"]);
						
					if ($tempsize["ancho"]==0)
						$foo->image_ratio_x			= 	true;
					else
						$foo->image_ratio_x			= 	false;
					if ($tempsize["alto"]==0)
						$foo->image_ratio_y			= 	true;
					else
						$foo->image_ratio_y			= 	false;

					if ($campotabla=="imagen_gris"){
						$foo->image_greyscale = true;
					}

					$foo->Process($ruta_imags.$tempsize["carpeta"]."/");
					if ($foo->processed) { 
						$nota	.=	" Thumb = Thumb subido exitosamente <br> ";
					} else { 
						$nota	.= ' errorThumb = ' . $foo->error . ' <br> ' ;
					}	
				}
			}
			$foo->Clean();
			/////////////////////////////////////
		}
	}
}

//Foto y miniatura a partir de una existente en el servidor
function procesaimagentemp($campopost, $campotabla, $new_filename,$ext,$ruta_imags,$ancho_normal,$alto_normal,$resize_normal=true,$recorta_normal=true,$genera_miniatura=false,$ancho_miniatura,$alto_miniatura){
	$nota="";
	if(!empty($campopost)) {
		$extencion 	= 	$ext;
		
		if ($extencion==".php" || $extencion==".html" || $extencion==".txt"){
		}else{
		
		$foo = new Upload($campopost); 
		$foo->file_new_name_body 	= 	$new_filename;
		$foo->jpeg_quality 			=	70;
		$foo->image_resize          = 	$resize_normal;
		$foo->image_ratio_crop  	= 	$recorta_normal;
		
		if ($alto_normal>0)		
			$foo->image_y 			= 	$alto_normal;
		if ($ancho_normal>0)
			$foo->image_x 			= 	$ancho_normal;
		if (@$ancho_normal==0)
			$foo->image_ratio_x		= 	true;
		if (@$alto_normal==0)
			$foo->image_ratio_y		= 	true;

		if ($foo->uploaded) {
			$foo->Process($ruta_imags);
			if ($foo->processed) { 
				@chmod($ruta_imags.$new_filename.$extencion, 0777);
			} else { 
				echo 'error : ' . $foo->error;
			}	
		} else { echo 'error : ' . $foo->error; }	
		
		/********* Thumbnail **********/
			if ($genera_miniatura){
				$foo->file_new_name_body 	= 	$new_filename;
				$foo->jpeg_quality 			=	70;
				$foo->image_resize 			= 	true;
				$foo->image_ratio_crop  	= 	false;
				$foo->image_y 				= 	$alto_miniatura;
				$foo->image_x 				= 	$ancho_miniatura;
				$foo->image_ratio_y			= 	true;
				$foo->Process($ruta_imags."thumbs/");
				if ($foo->processed) { 
					$nota	.=	" Thumb = Thumb subido exitosamente <br> ";
				} else { 
					$nota	.= ' errorThumb = ' . $foo->error . ' <br> ' ;
				}	
			}
			$foo->Clean();
		}
	}
}
?>