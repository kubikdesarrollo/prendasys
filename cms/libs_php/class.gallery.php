<?php
	class ClassGallery extends DBfront
	{
		var $config = array();
		var $edit   = 0;

		public function builuploader($config,$idtable){
			$this->config 	=$config;
			$this->edit 	=$idtable;

			/*
			array("nombre"=>"galeria_home", "tipo"=>"supergaleria", "id_padre"=>"id_slide_home", "tabla"=>PREFIX."slide_home_imagenes", "id_tabla"=>"slide_image", "archivo_guardar"=>"guarda_fotos_home", "anchoimagen"=>970, "altoimagen"=>290, "carpeta"=>"../../images/images_slide_home/", "con_titulo"=>false, "max_images"=>6, "customsizes"=>array(
                array("carpeta"=>"thumbs","ancho"=>100,"alto"=>100, "recorta"=>false, "resize"=>true)
                )
            )
			*/
			echo '<div class="col-lg-12">
				<section class="panel">
	                  <header class="panel-heading">
	                      <h2>Subir imagenes /'.$this->config["nombre"].'</h2> (Tamaño de imagenes: '.(empty($this->config["anchoimagen"])?'Ancho libre':'Ancho '.$this->config["anchoimagen"]."px").' , '.(empty($this->config["altoimagen"])?'Alto libre':'Alto '.$this->config["altoimagen"]."px").')
	                      <span class="tools pull-right">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                          </span>
	                  </header>
	                  <div class="panel-body">
	                      <form action="scripts/dropzoneupload.php?seccion='.$this->config["seccion"].'&tabla='.$this->config["tabla"].'&indice='.$this->config["id_tabla"].'&idpadre='.$idtable.'&idunico='.$this->config["idunico"].'" class="dropzone" id="'.$this->config["nombre"].'-dropzone">
	                      </form>
	                  </div>
	            </section></div>
			';
		}

		public function listimages(){
			$token	=	sha1($_SESSION['id_admin'].time()."kbi");
			$_SESSION['tokenimagenes']= $token;
			$rutaprincipal = (str_replace("../../images", "../images", $this->config["carpeta"]));

			echo '
			<div class="col-lg-12">
				<section class="panel">
                      <header class="panel-heading">
                          Listado de imagenes /'.$this->config["nombre"].'
                          <span class="tools pull-right">
                            <a href="javascript:;" class="icon-chevron-down"></a>
                          </span>
                      </header>
                      <div class="panel-body">
                          <div class="dd" id="nestable_'.$this->config["seccion"].'">
                              <ol class="dd-list" id="elementlister'.$this->config["tabla"].'">';

                              $sql="SELECT * FROM ".$this->config["tabla"]." WHERE ".$this->config["id_padre"]."=".$this->edit .((!empty($this->config["where"]))?' AND ('.$this->config["where"].') ':'')." ORDER BY ".$this->config["orden"];
                              $res=$this->query($sql);
                              foreach ($res as $datos){

                              echo '<li class="dd-item dd3-item" data-id="'.$datos[$this->config["id_tabla"]].'" id="itemg_'.$datos[$this->config["id_tabla"]].'">
                                      <div class="dd-handle dd3-handle"></div>
                                      <div class="dd3-content"><div class="ddcontainimg"><img src="'.$rutaprincipal.$datos[$this->config["campo"]].'"></div>

										
										'."<div class=\"botonesgale\" ><a onclick='eImagen(".$datos[$this->config["id_tabla"]].",\"".$this->config["tabla"]."\",\"".$this->config["id_tabla"]."\",\"".$this->config["seccion"]."\")' title='Eliminar imagen' class='eliminar'>&nbsp;</a>
										<a class='ver_pic fancyboxgg'  href='".$rutaprincipal.$datos[$this->config["campo"]]."' id='picno".$datos[$this->config["id_tabla"]]."' >&nbsp;</a>
										<a class='modinfo_pic' onclick=\"guardarinfopic('".$datos[$this->config["id_tabla"]]."','".$this->config["tabla"]."','nombre','$token','".$this->config["id_tabla"]."')\" >&nbsp;</a>
										</div><div class=\"textoinfogalesgale slidecapas\" >";
										if (empty($this->config["customfields"])){
										/*echo '
										Inicio <input type="text" id="inicio_'.$datos[$this->config["id_tabla"]].'" value="'.@$datos['inicio'].'"><br>
										Duraci&oacute;n <input type="text" id="duracion_'.$datos[$this->config["id_tabla"]].'" value="'.@$datos['duracion'].'"><br>
										Efecto <select id="efecto_'.$datos[$this->config["id_tabla"]].'"><option value="Izquierda" '.(@$datos['efecto']=='Izquierda'?'selected="selected"':'').'>Izquierda</option><option value="Fade" '.(@$datos['efecto']=='Fade'?'selected="selected"':'').'>Fade</option><option value="Abajo" '.(@$datos['efecto']=='Abajo'?'selected':'').'>Abajo</option><option value="Arriba" '.(@$datos['efecto']=='Arriba'?'selected="selected"':'').'>Arriba</option><option value="Derecha" '.(@$datos['efecto']=='Derecha'?'selected="selected"':'').'>Derecha</option></select>
										';*/
										} else {
											for($i=0;$i<count($this->config["customfields"]);$i++){
												switch ($this->config["customfields"][$i]["tipo"]) {
													case 'string':
														echo $this->config["customfields"][$i]["titulo"].' <input type="text" id="'.($this->config["customfields"][$i]["nombre"]).'_'.$datos[$this->config["id_tabla"]].'" value="'.@$datos[$this->config["customfields"][$i]["nombre"]].'" class="gal_extracustfields" data-grupo="'.$datos[$this->config["id_tabla"]].'" data-campo="'.($this->config["customfields"][$i]["nombre"]).'"><br>';
														break;
													case 'enumvalues':
														echo $this->config["customfields"][$i]["titulo"].' <select id="'.($this->config["customfields"][$i]["nombre"]).'_'.$datos[$this->config["id_tabla"]].'" class="gal_extracustfields" data-grupo="'.$datos[$this->config["id_tabla"]].'" data-campo="'.($this->config["customfields"][$i]["nombre"]).'">';
														$enumera=explode(",", $this->config["customfields"][$i]["enum"]);
														$valora=explode(",", $this->config["customfields"][$i]["values"]);
														foreach ( $enumera as $ku=>$enum) {
															echo '<option value="'.$valora[$ku].'" '.(@$datos[$this->config["customfields"][$i]["nombre"]]==$valora[$ku]?' selected="selected" ':'').'>'.$enum.'</option>';
														}
														echo '</select><br>';
														break;
												}
											}
										}

										echo '<br><input type="text" id="texto_'.$datos[$this->config["id_tabla"]].'" value="'.@$datos['nombre'].'" style=" display:none">
										</div>
                                      </div>
                                  </li>';
                               }   

  						echo '</ol>
							    <div class="modal fade" id="Modalgallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	                                  <div class="modal-dialog">
	                                      <div class="modal-content">
	                                          <div class="modal-header">
	                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	                                              <h4 class="modal-title"></h4>
	                                          </div>
	                                          <div class="modal-body">
	                                          </div>
	                                          <div class="modal-footer">
	                                              <button data-dismiss="modal" class="btn btn-default" type="button">Close</button>
	                                              <button class="btn btn-warning" type="button" id="clickconfirm"> Confirm</button>
	                                          </div>
	                                      </div>
	                                  </div>
	                            </div>
                          </div>
                      </div>
                </section>
            </div>

            <script>
            $(function(){
            	$("#nestable_'.$this->config["seccion"].'").nestable({maxDepth:1}).on("change", function(event) { 
            		if (event.target.tagName == "DIV" ){
						var serial= $(this).nestable("serialize");
						var obj = {};
						var elementos="";
						for (var i = 0, l = serial.length; i < l; i++) {
						    elementos+=((elementos!="")?",":"")+serial[i].id;
						}
						$.ajax({
				          	url: "functions/sortable.php?tabla='.$this->config["tabla"].'&indice='.$this->config["id_tabla"].'&campoord='.$this->config["orden"].'",
				          	type: "POST",
				          	data: {listadoi: elementos}
				        })
				        .done(function(data) {
				          	
				        });
					}
				});
				jQuery(".fancyboxgg").fancybox();
            });
			

            </script>    
			';
		}

	}
?>