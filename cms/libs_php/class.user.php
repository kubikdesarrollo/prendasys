<?php
class User extends DBfront{
	
	var $debug			= 	"";
	var $debugExit		= 	"";
	
	var	$res_data		=	array();
	var	$res_num		=	"";
	
	var $tipo			=	"";
	var $tabla			=	"";
	var $excluye		=	"";
	var $siguienteId	=	"";
	var $salida			=	array();
	var $mostrarDato	=	array();
	var $mostrarJson	=	"";
	
	var $ultimoReg		=	"";
	  
	var $mode			=	"assoc"; 
	var $error			=	"";
	var	$data_exit		=	array();

	var $userName 		= "";
	var $userID	 		= "";
	var $userNickName 	= "";
	var $userEmail		= "";
	var $userUsuariofb	= "";
	var $userExist	 	= "";
	var $userDepartamento	= 0;
	var $userLevel	 	= "";
	var $userPass	 	= "";	
	var $permisosArray	= array();
	var $userIntentos	=	0;
	var $token			= "";

	
	
	function login($user,$query) {				
		$user 	=  	trim(htmlspecialchars($user));
		$user 	= 	substr(str_replace("'", "'", $user), 0, 25);
		$userdata	=	array("userNickName"=>$user);
		$datos	=	$this->row($query,$userdata);
		
		if (!empty($datos)) {
			$this->userName 		= $datos['nombre'].' '.$datos['apellidos'];
			$this->userID	 		= $datos['id_admin'];
			$this->userNickName 	= $datos['user'];
			$this->userPass		 	= $datos['pass'];
			$this->userEmail		= $datos['email'];
			$this->userExist	 	= "yes";
			$this->userLevel	 	= @$datos['admintype'];		
			$this->token			= @$datos['token']; 	
			$this->userIntentos		= empty($datos["intentos"])?0:intval($datos["intentos"]);	
		}
		else{
			$this->error	=	"Error : Nombre de usuario � contrase�as incorrecto";
		}
	}		
	
		
}
?>