<?php
/* Class Control Panel 1.0 By Jok*/ 
/*
	
*/
class controlPanel extends backOffice {
	
	var $anchogriddef=910;
	var $altogriddef=520;
	var $anchoiconos=32;

	function inicializa ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="",$where=1,$titlemodule="") {	
		//Verificar los titulos
		$temptitles=array();
		$anchostitles=array();
		$pindex=0;
		$actionnuew=false;
		
		for ($i=0;$i<count($titles);$i++){
			if (strpos($titles[$i],"->")){
				$temptitles[$pindex]=substr($titles[$i],0,strpos($titles[$i],"->"));
				$pindex++;
			}
			else if ($titles[$i]!='NONE'){
				$temptitles[$pindex]=$titles[$i];
				$pindex++;
			}
		}
		$tempacciones=array();
		for($h=0;$h<count($actions);$h++){
			if ($actions[$h]!="nuevo") $tempacciones[]= $actions[$h];
			else $actionnuew=true;
		}
		$actions=$tempacciones;

		$anchototal=($ancho>0)?$ancho:$this->anchogriddef;
		$anchototal=$anchototal-(count($actions)*$this->anchoiconos)-22;
		$anchocolumna=floor($anchototal/count($temptitles));
		
		$ajusteancho=0;
		for ($i=0;$i<count($temptitles);$i++){
			if (strpos($temptitles[$i],"[width=")>0){
				$anchostitles[$i]=substr($temptitles[$i],strpos($temptitles[$i],"=")+1,(strpos($temptitles[$i],"]")-strpos($temptitles[$i],"=")-1));
				$temptitles[$i]=substr($temptitles[$i],0,strpos($temptitles[$i],"["));
				$ajusteancho+=($anchocolumna-$anchostitles[$i]>0)?$anchocolumna-$anchostitles[$i]:0;
			}
			else {
				if ($i==count($temptitles)-1)
					$anchostitles[$i]=$anchocolumna+($ajusteancho);
				else
					$anchostitles[$i]=$anchocolumna;
			}
		}

		$esho= '<section class="wrapper">
              <section class="panel">
                  <header class="panel-heading">
                      '.strtoupper(($titlemodule=="")?$seccion:$titlemodule).'
                  </header>
                  <div class="panel-body">
                      <div class="adv-table editable-table ">
                          <div class="clearfix">
							  ';
							  $issearchadvanced=false;
							  if (file_exists("mod/filtro".$seccion.".php")){
							  	$issearchadvanced=true;
							  }
							  if (file_exists("toolbars/".$seccion.".php")){
							  	include 'toolbars/'.$seccion.'.php';
							  }
                              
                              if (!$issearchadvanced)
                              	$esho.='
									  <div class="pull-left col-lg-6">
			                              <form action="admin.php?seccion='.$seccion.'" method="post" enctype="multipart/form-data" role="form" class="form-inline">
			                                  <div class="form-group col-lg-9">
			                                      <label for="searchInput2" class="sr-only">Search</label>
			                                      <input type="text" placeholder="Search" id="searchInput2" name="querysearch" class="form-control" value="'.@$_POST['querysearch'].'">
			                                  </div>
			                                  <button class="btn btn-success" type="submit" name="busqueda" value="search"><i class="icon-search"></i></button>
			                              </form>
			                          </div>
			                          ';
                              $esho.=((($actionnuew)=="true")?'<div class="btn-group pull-right"><a class="btn btn-success" style="margin-right:10px" href="admin.php?seccion='.$seccion.'&accion=new">Agregar nuevo <i class="icon-plus"></i></a></div>':'');
                              if (in_array("delchecked", $actions)){
			                  	$esho.='
			                  		<div class="btn-group pull-right">
			                  		<button class="btn btn-danger" type="button" style="margin-right:10px" onclick="eliminarmultiple(\''.$tabla.'\',\''.$indice.'\')">
									<i class="icon-trash"></i>
									Borrar
									</button></div>
			                  ';
			                  }
			                  $esho.=' 
                          </div>
                          <div class="space15"></div>
                          <table class="table table-striped table-hover table-bordered" id="editable-sample">
                              <thead>
								  <tr>';
								  for ($i=0;$i<count($temptitles);$i++){
								   		$esho.= '<th style="width:'.(($i==count($temptitles))?(count($actions)*$this->anchoiconos):$anchostitles[$i]).'px">'.$temptitles[$i].'</th>';
								  }
								  if (count($actions)>0){
								  	$esho.= '<th style="width:'.(count($actions)*($this->anchoiconos+8)).'px"></th>';
								  }
								  $esho.='</tr>
							  </thead>
                          </table>
                      </div>
                  </div>
              </section>
</section>';
 		$campoord=str_replace(" ASC", "", $orden);
 		$campoord=str_replace(" ASC", "", $orden);
		$esho.= '
		<input type="hidden" name="seccion" id="seccion" value="'.$seccion.'" />
		<input type="hidden" name="indicesec" id="indicesec" value="'.$indice.'" />
		<input type="hidden" name="campoord" id="campoords3C" value="'.$campoord.'" />
		<input type="hidden" name="tablesec" id="tablesec" value="'.$tabla.'" />

		';
        
        echo $esho;
    }  
	
	function listarAjax ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="") {  
			echo $this->generadatosJSON($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg,$ancho,$orden);
	}
	
	function totalrows($seccion,$query){
		if ($this->verifica_permisos($seccion)){ 
			$res=$this->query($query); 
			return count($res);
		} else
			return 0;
	}
	function linkpaginas($seccion,$query,$max,$pag=1){
		if ($this->verifica_permisos($seccion)){ 
			$res=$this->query($query); 
			$total= count($res);
			$echo="Total of ".$seccion.": ".$total.". &nbsp;&nbsp;&nbsp;&nbsp;Page:";
			if (ceil($total/$max)<=20){
				for($i=0;$i<ceil($total/$max);$i++){
					$echo.='<a id="pagnum_'.($i+1).'" onclick="cargapagBD('.($i+1).')" class="pagesnumbers '.(($pag==($i+1))?'pagselected':'').'">'.($i+1).'</a>';
				}
			} else {
				$echo.='<select class="paginacioncombo" onchange="cargapagBD(this.value)">';
				for($i=0;$i<ceil($total/$max);$i++){
					$echo.='<option value="'.($i+1).'">'.($i+1).'</option>';
				}
				$echo.='</select>';
			}
			return $echo;
		} else
			return 0;
	}

	function generadatosJSON ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="") { 
		$echo ="";
		$indice=str_replace($tabla.".", "", $indice);
		$queryoriginal=$query;
		if ($this->verifica_permisos($seccion)){ 
			if ($orden!=""){
				$query .= " ORDER BY {$orden} ";
			}
			if($pag != ''){
				$inicio = ($pag - 1) * $numReg;
				$query	=	$query. " LIMIT {$inicio}, {$numReg}";
			}
				
			$res=$this->query($query); 
			$nullresponse=array();
			$arrayresponse= $nullresponse;
			$cebra=0; 
			$irecords=count($res);
			foreach($res as $datosli){ 
				$rowresponse=array();
				$posi=0; 
				foreach($datosli as $k => $v){ 
					if ($k!=$indice && @$titles[$posi]!="NONE" && !strpos(@$titles[$posi],"->"))
						$rowresponse[]=(empty($v))?"":((strpos(@$titles[$posi],"center")!==false)?'<div align="center">':'').$v.((strpos(@$titles[$posi],"width")!==false)?'</div>':'');
					else if (strpos(@$titles[$posi],"->")!==false){
						$nombreftemp=substr($titles[$posi],(strpos($titles[$posi],"->")+2));
						$parametros=array($v); 
						if (strpos($nombreftemp,"(")!==false){
							$params=substr($nombreftemp,strpos($nombreftemp,"(")+1);	
							$nombreftemp=substr($nombreftemp,0,strpos($nombreftemp,"("));
							$params=str_replace(")","",$params);
							$params=explode(",",$params);
							foreach($params as $param){
								if (strpos($param,"*")!==false)
									$parametros[]=str_replace("*","",$param);
								else
									$parametros[]=@$datosli[$param];
							}
						}
												 
						if (function_exists($nombreftemp)){
							$rowresponse[]=call_user_func($nombreftemp,$parametros);
							//Parametro Cero es el valor del campo, del 1 en adelante es el campo descrito o si empieza con * es un valor
						} else
							$rowresponse[]="";
					}
					$posi++;
				}
				if (count($actions)>0){
					$accionsrow= '<div style="width:'.(count($actions)*40).'px">'; 
					$titlesfilds=explode(",",$campos); 
					//Acciones
					for ($i=0;$i<count($actions);$i++){
						switch ($actions[$i]){
							case 'editar': 		$accionsrow.= '<a class="btn btn-primary btn-xs" style="margin:0 20px" href="admin.php?seccion='.$seccion.'&accion=editar&id_edit='.$datosli[$indice].'"><i class="icon-pencil"></i></a>'; break; 
							case 'eliminar':  	$accionsrow.= "<a class=\"btn btn-danger btn-xs\" onclick=\"eliminarRow({$datosli[$indice]},'{$tabla}','{$indice}','".addslashes(@$datosli[trim(@$titlesfilds[0])])."')\" ><i class=\"icon-remove\"></i></a>";  break;
							case 'delchecked':  $accionsrow.= '<input type="checkbox" class="itemsdel" value="'.$datosli[$indice].'" title="'.addslashes(@$datosli[trim(@$titlesfilds[0])].' '.@$datosli[trim(@$titlesfilds[1])]).'" />'; break; 
							default: {
								if (function_exists("actionList_".$actions[$i])){
									$accionsrow.=call_user_func("actionList_".$actions[$i],$datosli[$indice]);
								}
							}
						}
					} 
					$accionsrow.='</div>';
					$rowresponse[]=$accionsrow;
				}
				$cebra++;
				$rowresponse["DT_RowId"]="row_".$datosli[$indice];
				//$rowresponse["DT_RowData"]=array("position"=>"0");
				$arrayresponse[]=$rowresponse;
			}
			$itotalrows=$this->totalrows($seccion,$queryoriginal);
			$output = array(
				"sEcho" => intval(@$_GET['sEcho']),
				"iTotalRecords" => $itotalrows,
				"iTotalDisplayRecords" => $itotalrows,
				"aaData" => $arrayresponse
			);
		} 
		return json_encode($output); 
	}
	
	function verifica_permisos($seccion){
		if (($seccion==$_GET['seccion']) &&  isset($_SESSION['id_admin']))
			return true;
		else
			return false;
	}

}
?>