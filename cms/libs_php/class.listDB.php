<?php
/* Class listDB 1.0 By Jok*/ 
/*
	
*/
class listDB extends backOffice {
	
	var $anchogriddef=910;
	var $altogriddef=520;
	var $anchoiconos=32;

	function inicializa ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="",$where=1) {	
		//Verificar los titulos
		$temptitles=array();
		$anchostitles=array();
		$pindex=0;
		
		for ($i=0;$i<count($titles);$i++){
			if (strpos($titles[$i],"->")){
				$temptitles[$pindex]=substr($titles[$i],0,strpos($titles[$i],"->"));
				$pindex++;
			}
			else if ($titles[$i]!='NONE'){
				$temptitles[$pindex]=$titles[$i];
				$pindex++;
			}
		}
		
		$anchototal=($ancho>0)?$ancho:$this->anchogriddef;
		$anchototal=$anchototal-(count($actions)*$this->anchoiconos)-22;
		$anchocolumna=floor($anchototal/count($temptitles));
		
		$ajusteancho=0;
		for ($i=0;$i<count($temptitles);$i++){
			if (strpos($temptitles[$i],"[width=")>0){
				$anchostitles[$i]=substr($temptitles[$i],strpos($temptitles[$i],"=")+1,(strpos($temptitles[$i],"]")-strpos($temptitles[$i],"=")-1));
				$temptitles[$i]=substr($temptitles[$i],0,strpos($temptitles[$i],"["));
				$ajusteancho+=($anchocolumna-$anchostitles[$i]>0)?$anchocolumna-$anchostitles[$i]:0;
			}
			else {
				if ($i==count($temptitles)-1)
					$anchostitles[$i]=$anchocolumna+($ajusteancho);
				else
					$anchostitles[$i]=$anchocolumna;
			}
		}
		
		
		$esho= '
		<table id="listadogenBD">
		 <thead>
		  <tr>';
		  for ($i=0;$i<count($temptitles);$i++){
		   		$esho.= '<th>'.$temptitles[$i].'</th>';
		  }
		  $esho.= '<th></th></tr>
		 </thead>';
		 $esho.=$this->generadatos($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg,$ancho,$orden);
		 $esho.= ' 
		</table>
		<script type="text/javascript">
		$(document).ready(
			function() {
				$("#listadogenBD").ingrid({ 
					url: "mod/mod_'.$seccion.'.php?seccion='.$seccion.'&accion=listar&where='.base64_encode($where).'",
					height: '.$this->altogriddef.',
					colWidths: ['; for ($j=0;$j<=count($temptitles);$j++){ $esho.= ($j==count($temptitles))?(count($actions)*$this->anchoiconos):$anchostitles[$j].",";} $esho.='],
					paging: true,
					totalRecords: '.$this->totalrows($seccion,$query).',
					onRowSelect: function(tr, selected){ ingridselectrow(tr, selected)},
					onLoadStart: function(){$("#selItemsGridt").val("")}
				});
			}
		); 
		</script>
		<input type="hidden" id="selItemsGridt" value="" />
		';
        
        echo $esho;
    }  
	
	function listar ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="") { 
			$res=mysql_query($query); 
			echo '<table>';
			echo $this->generadatos($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg,$ancho,$orden);
			echo '</table>';
	}
	
	function totalrows($seccion,$query){
		if ($this->verifica_permisos($seccion)){ 
			$res=mysql_query($query) or die ("Error in query: "); 
			return mysql_num_rows($res);
		} else
			return 0;
	}
	
	function generadatos ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$orden="") {
		$echo ="";
		if ($this->verifica_permisos($seccion)){ 
			if ($orden!=""){
				$query .= " ORDER BY {$orden} ";
			}
			if($pag != ''){
				$inicio = ($pag - 1) * $numReg;
				$query	=	$query. " LIMIT {$inicio}, {$numReg}";
			}
				
			$res=mysql_query($query); 
			$echo.= '<tbody>';
			while($datosli=@mysql_fetch_assoc($res)){
				$echo.='<tr>';
				$posi=0;
				foreach($datosli as $k => $v){ 
					if ($k!=$indice && @$titles[$posi]!="NONE" && !strpos(@$titles[$posi],"->"))
						$echo.= '<td>'.$v.'</td>'; 
					else if (strpos(@$titles[$posi],"->")!==false){
						$nombreftemp=substr($titles[$posi],(strpos($titles[$posi],"->")+2));
						if (function_exists($nombreftemp)){
							$echo.='<td>'.call_user_func($nombreftemp,$v).'</td>'; 
						}
					}
					$posi++;
				}
				$echo.= '<td>'; 
				//Acciones
				for ($i=0;$i<count($actions);$i++){
					switch ($actions[$i]){
						case 'editar': 		$echo.= '<a class="editaringrid" onclick="actions('.$datosli[$indice].',\'editar\')">&nbsp;</a>'; break; 
						case 'eliminar':  	$echo.= "<a class=\"eliminaringrid\" onclick=\"eliminaringrid({$datosli[$indice]},'{$tabla}','{$indice}')\" >&nbsp;</a>";  break;
						default:
							if (is_array($actions[$i])){
								switch ($actions[$i][0]){
									case 'check':  	$echo.= "<a class=\"checkingrid".(($datosli[$actions[$i][1]]!=1)?"off":"")."\" onclick=\"checkingrid({$datosli[$indice]},'{$tabla}','{$indice}','".$actions[$i][1]."')\" >&nbsp;</a>";  break;
								}
							}
						break;
					}
				}
				
				$echo.= '</td></tr>';
			}
			$echo.= '</tbody>';
		} 
		return $echo; 
	}
	
	function verifica_permisos($seccion){
		if (($seccion==$_GET['seccion']) &&  isset($_SESSION['id_admin']))
			return true;
		else
			return false;
	}

}
?>