<?php
 class ClassSubtabla{
 	var $config = array();
 	var $pop = "";

 	function builmodule($config,$id){
 		$this->config=$config;
 		$this->pop=md5($this->config["nombre"]);
 		$columns_titles=explode(",",$this->config["list_titles"]);
 		$columns_fields=explode(",",$this->config["list_fields"]);
 		if (!empty($id)){
 			$sql="SELECT * FROM ".$this->config["nombre_subtabla"]." WHERE ".$this->config["id_padre"]."='".$id."'".((@$this->config["is_sortable"]==true)?' ORDER BY '.@$this->config["orden"]:'');

 			echo '<div class="col-lg-12">
 					<section class="panel">
                          <header class="panel-heading">
                              '.$this->config["nombre"].' 
                              <span class="pull-right">
	                            <button type="button" onclick="clearandloadpop(\''.$this->pop.'\')" class="btn btn-success">Nuevo</button>
	                          </span>
                          </header>
                          <table class="table table-hover" id="table_'.$this->pop.'">';
                          $this->reloadinfo($config,$id,$sql);    
                        echo '  </table>
                          <input type="hidden" id="page_'.$this->pop.'" value="1" data-contentinfo="mod/info_ajax.php?seccion='.$this->config["nameseccion"].'&tabla='.$this->config["nombre_subtabla"].'&indice='.$this->config["id_subtabla"].'&id_padre='.$id.'">
                    </section>
				</div>
				<input type="hidden" id="indicesec" value="'.$this->config["id_subtabla"].'">
				<input type="hidden" id="campoords3C" value="'.$this->config["orden"].'">
				<input type="hidden" id="tablesec" value="'.$this->config["nombre_subtabla"].'">
			';
 		}
 	}

 	function contentform($config,$id,$datos){
 		$this->config=$config;
 		$this->pop=md5($this->config["nombre"]);
 		$columns_titles=explode(",",$this->config["list_titles"]);
 		$columns_fields=explode(",",$this->config["list_fields"]);
 		echo '<div class="modal fade" id="'.$this->pop.'" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                  <div class="modal-dialog">
                                  <form class="formpop" id="form_'.$this->pop.'" action="scripts/save_ajax.php?seccion='.$this->config["nameseccion"].'&tabla='.$this->config["nombre_subtabla"].'&indice='.$this->config["id_subtabla"].'" method="post" enctype="multipart/form-data">
                                      <div class="modal-content">
                                          <div class="modal-header">
                                              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                              <h4 class="modal-title">'.$this->config["nombre"].'</h4>
                                          </div>
                                          <div class="modal-body">';
                                          $this->dataform($config,$datos);

            echo '                        
            							  </div>
                                          <div class="modal-footer">
                                              <button data-dismiss="modal" class="btn btn-default" type="button">Cerrar</button>
                                              <button class="btn btn-success" type="button" onclick="sendpop(\''.$this->pop.'\')">Guardar</button>
                                          </div>
                                      </div>
                                      <input type="hidden" value="'.((@$datos[$this->config["id_subtabla"]])>0?1:0).'" name="editar">
                                      <input type="hidden" value="'.sha1($this->config["nombre_subtabla"] ."_". $_SESSION['id_admin']).'" name="token">
                                      <input type="hidden" value="'.$id.'" name="'.$this->config["id_padre"].'">
                                      </form>
                                  </div>
                              </div>';

 	}

 	function dataform($config,$datos){
 		$this->config=$config;
 		$this->pop=md5($this->config["nombre"]);
 		$columns_titles=explode(",",$this->config["list_titles"]);
 		$columns_fields=explode(",",$this->config["list_fields"]);
 		foreach ($this->config["tablefields"] as $key => $field) {
              switch($field["tipo"]){
				case "input":		bloqueinput($field["titulo"],$field["nombre"],@$datos[$field["nombre"]],@$field["required"],@$field["clase"],@$field["noeditable"]); break;
				case "textarea":	bloquetextarea($field["titulo"],$field["nombre"],@$datos[$field["nombre"]]); break;
				case "editor":		bloqueeditor($field["titulo"],$field["nombre"],@$datos[$field["nombre"]]); break;
				case "checkbox":	bloquechecked($field["titulo"],$field["nombre"],@$datos[$field["nombre"]],@$field["clase"]); break;
				case "enumvalues":	bloqueenumvalues($field["titulo"],$field["nombre"],@$datos[$field["nombre"]],$field["enum"],$field["values"],@$field["clase"]); break; 
				case "datetime":	bloquedatetime($field["titulo"],$field["nombre"],@$datos[$field["nombre"]]); break;
				case "imagen": 		bloqueimagen($field,@$datos); break;

			  }
		}
		echo '<input type="hidden" value="'.intval(@$datos[$this->config["id_subtabla"]]).'" name="'.$this->config["id_subtabla"].'" class="id_editpop" ><div class="clear"></div>';

 	}

 	function reloadinfo($config,$id,$query){
 		$this->config=$config;
 		$this->pop=md5($this->config["nombre"]);
 		$columns_titles=explode(",",$this->config["list_titles"]);
 		$columns_fields=explode(",",$this->config["list_fields"]);
 		$temptitles=array();
		$pindex=0;

 		for ($i=0;$i<count($columns_titles);$i++){
			if (strpos($columns_titles[$i],"->")){
				$temptitles[$pindex]=substr($columns_titles[$i],0,strpos($columns_titles[$i],"->"));
				$pindex++;
			}
			else if ($columns_titles[$i]!='NONE'){
				$temptitles[$pindex]=$columns_titles[$i];
				$pindex++;
			}
		}

 		$res=mysql_query($query);
 		echo '<thead>
                              <tr><th></th>';
					          foreach ($temptitles as $key => $value) {
					            	echo '<th>'.$value.'</th>';
					          }
            				echo '<th></th>
                              </tr>
                              </thead>
                              <tbody class="ui-sortable">';
                              while ($rows=@mysql_fetch_array($res)) {
	                              	echo '<tr id="DTrow_'.@$rows[$this->config["id_subtabla"]].'"><td><div class="btn btn-white handlersort" data-toggle="button"><i class="icon-move"></i></div></td>';
	                              	$posi=0;
						            foreach ($columns_fields as $key => $value) {
						            	$respuesta=$rows[$value];
						            	if (strpos(@$columns_titles[$posi],"->")!==false){
											$nombreftemp=substr($columns_titles[$posi],(strpos($columns_titles[$posi],"->")+2));
											$parametros=array($rows[$value]); 
											if (strpos($nombreftemp,"(")!==false){
												$params=substr($nombreftemp,strpos($nombreftemp,"(")+1);	
												$nombreftemp=substr($nombreftemp,0,strpos($nombreftemp,"("));
												$params=str_replace(")","",$params);
												$params=explode(",",$params);
												foreach($params as $param){
													if (strpos($param,"*")!==false)
														$parametros[]=str_replace("*","",$param);
													else
														$parametros[]=@$rows[$param];
												}
											}
																	 
											if (function_exists($nombreftemp)){
												$respuesta=call_user_func($nombreftemp,$parametros);
												//Parametro Cero es el valor del campo, del 1 en adelante es el campo descrito o si empieza con * es un valor
											} else
												$respuesta="";
										}


						            	echo '<td>'.$respuesta.'</td>';
						            	$posi++;
						            }
						            echo '<td style="width:114px"><button type="button" onclick="loadeditinfo(\'mod/info_ajax.php?seccion='.$this->config["nameseccion"].'&tabla='.$this->config["nombre_subtabla"].'&indice='.$this->config["id_subtabla"].'&id_padre='.$id.'&id_edit='.$rows[$this->config["id_subtabla"]].'\',\''.$this->pop.'\')" style="margin:0 20px" class="btn btn-primary btn-xs"><i class="icon-pencil"></i></button>

						            <a onclick="eliminarRow('.$rows[$this->config["id_subtabla"]].',\''.$this->config["nombre_subtabla"].'\',\''.$this->config["id_subtabla"].'\',\''.$this->limpiacadena($rows[0]." - ".$rows[1]).'\',\''.$this->pop.'\')" class="btn btn-danger btn-xs"><i class="icon-remove"></i></a></td>';
						            echo '</tr>';
					         }
			echo '		            
                              </tbody>';
			if(@$this->config["is_sortable"]==true){					
				echo '<script>$(function(){maketsortable(\'table_'.$this->pop.'\',1);});</script>';
			}
 	}

 	function limpiacadena($cadena){
			$cadena = strip_tags(strtolower(utf8_decode($cadena)));
			$cadena = str_replace(array("á","é","í","ó","ú","ñ"),array("a","e","i","o","u","n"),$cadena);
			return preg_replace('(([^0-9a-zA-Z ])+)', " ",$cadena);
 	}
 }
?>