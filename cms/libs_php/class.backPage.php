<?php
/* Class backPage 3.0 */ 
/*
	Si ancho = 0: entonces toma el default de la  hoja de estilos. Si es diferente de 0: es el numero de pixeles.
*/
class backPage extends backOffice {

	function listar ($seccion,$query,$tabla,$indice,$actions,$campos,$titles,$pag,$numReg=10,$ancho=0,$subsecc="") {	
     	$select		=	"";
		$titulos	=	"";
    	$listado	=	"<ul class=\"items\" id=\"items\">";
		
		$estatusord[0]='Sin contestar';
		$estatusord[1]='En proceso';
		$estatusord[2]='Cerrado Resuelto';
		$estatusord[3]='Cancelado';
		
		if ($pag == 1) {
			$inicio = 	0;
			$pag	=	1;
		} else if($pag == ""){
		
		}  
		else{
			$inicio = ($pag - 1) * $numReg;
		}
		/***** Si existe Imagen *****/
		if (@$images[0] != "" && !empty($d[$images]) ) {
			$listado	.=	" <img src=\"".$d[$images]."\" border=\"0\" />";
			$clases	=	"contentPhoto";	
		} else 
			$clases	=	"content";	
			
		/***** Consulta para listar *****/
		if($pag == '')
			$qListado	=	$query;
		else
			$qListado	=	$query. " LIMIT {$inicio}, {$numReg}";
			
        $rListado	=	mysql_query($qListado) or die("Error : ".$qListado);
		$nListado	=	mysql_num_rows($rListado);
		
		if ($nListado > 0) {
			for ($a=0;$d=mysql_fetch_assoc($rListado);$a++) { 
				
				$b	=	0;
				$listado	.=	"	<li id=\"item_".$d[$indice]."\" class='ui-state-default' ".(($ancho>0)?'style="width:'.$ancho.'px"':'')." >";
				
				//***** Si existe Imagen *****/
				if ((isset($d['imagen']) || isset($d['thumb']) || isset($d['imagen_v']) || isset($d['imagen_logo'])) && (!empty($d['imagen']) || !empty($d['thumb']) || !empty($d['imagen_v']) || !empty($d['imagen_logo']) ) ) {
					(isset($d['imagen']))?$imagen=$d['imagen']:$imagen=$d['thumb'];
					if(isset($d['imagen_v'])) $imagen=$d['imagen_v'];
					$dir	=	$seccion.'/thumbs/';
					
					if(isset($d['imagen_logo'])) { $imagen=$d['imagen_logo']; $dir	=	$tabla.'/';}
					
					$imagen=$imagen."?r=".rand(1,99999);
					
						
						if (strpos($imagen,"ttp://")>0)  //Es direccion absoluta
							$listado	.=	"<div class=\"img_list\" style=\"background: url('".$imagen."') no-repeat center top ; \" ></div>";
						else
							$listado	.=	"<div class=\"img_list\" style=\"background: url('../images/".$dir.$imagen."') no-repeat center top ; \" ></div>";

						
					$clases	=	"contentPhoto";	
				} else 
						$clases	=	"content";	
						
				$listado	.=	"	<div class=\"".$clases."\">";
				
				/***** Seleccion de campos *****/
				foreach ($d as $campo => $valor ) {
					$valor	=	($valor);
					if($b < sizeof($titles) ) {
						if (in_array($campo,array("imagen","imagen_v","imagen_logo","thumb","home","id_noticia","destacado"))) {
							$b=$b;
							$true	=	false;
						} else
							$true	=	true;
						 if ($campo == 'acabado' && $true) {
							$valores	=	explode('-',$valor);
							foreach($valores as $acabado){
								$acabados	.=	$acabado.", ";
							}
							$listado	.=	"<b>".$titles[$b]." :</b> <strong>".$acabados."<br /></strong>";
							$acabados	=	"";
							$b=$b+1;
						} 							
						else if ($campo == 'url' && $true) {
							$listado	.=	"<a target=\"_blank\"  href=\"".$d['url']."\">Ir a Link</a><br />";
							$b=$b+1;
						} 
						else if ($campo == 'status' ) {
							$listado	.=	'<b>Status:&nbsp;</b>'.$estatusord[$d['status']]."<br />";
							$b=$b+1;
						} 
						else if ($campo == 'asunto' ) {
							$listado	.=	'<b>Asunto:&nbsp;</b>'.substr($d['asunto'],0,90)."<br />";
							$b=$b+1;
						} 
						else if (in_array($campo,array("fecha","hasta","desde","fecha_campana")) && $true) {
							$listado	.=	"	<b>".$titles[$b]." :</b> <strong>".fecha($valor,3)."<br /></strong>";
							$b=$b+1;
						} 
						else if ($campo == 'ID') {
							$listado	.=	"	<b>".$titles[$b]." :</b> <strong class='id_{$valor}'>".description2($valor)."<br /></strong>";
							$b=$b+1;
						} 
						else if ($true) { 
							$listado	.=	"	<b>".$titles[$b]." :</b> <strong>".description2($valor)."<br /></strong>";
							$b=$b+1;
						}
					}
				}

				
				$listado	.=	"</div>";
				
				/***** Panel de Acciones *****/			
				
				$listado	.=	"<div class=\"actions aviso".$d[$indice]."\">";
				for ($a=0;$a<sizeof($actions);$a++) {
					if($actions[$a]=="mostrar")
							$listado	.=	"           <a onclick=\"actions({$d[$indice]},'mostrar')\" class=\"ver mostrar\" title=\"Mostrar\">&nbsp;</a>";
					else if($actions[$a]=="editar")			
							$listado	.=	"           <a onclick=\"actions({$d[$indice]},'editar')\" class=\"ver editar\" title=\"Editar Info\">&nbsp;</a>";
					else if($actions[$a]=="editar_sub")			
							$listado	.=	"           <a onclick=\"editar_sub({$d[$indice]},{$subsecc})\" class=\"ver editar\" title=\"Editar Info\">&nbsp;</a>";
					else if($actions[$a]=="config_pagina")			
							$listado	.=	"           <a onclick=\"config_pagina({$d[$indice]})\" class=\"ver configurar\" title=\"Configurar Info\">&nbsp;</a>";
					else if($actions[$a]=="versiones")			
							$listado	.=	"           <a onclick=\"versiones({$d[$indice]},'{$subsecc}')\" class=\"ver versiones\" title=\"Configurar Versiones\">&nbsp;</a>";
					else if($actions[$a]=="eliminar")
							$listado	.=	"           <a onclick=\"eliminar({$d[$indice]},'{$tabla}','{$indice}')\" class=\"eliminar\" title=\"Eliminar Registro\">&nbsp;</a>";
					else 
							$listado	.=	"           <a onclick=\"{$actions[$a]}({$d[$indice]},'{$tabla}','{$indice}')\" class=\"{$actions[$a]}\" title=\"{$actions[$a]}\">&nbsp;</a>";
				}
				$listado	.=	"       </div>";
				$listado	.=	"   </li>  ";  			
			} 
		} else { 
			$listado	.=	"<li class=\"no_exist\">No existen registros para esta categoria.</li>";
		}
		$listado	.=	" </ul>   "; 
		return $listado;
    }  
	
	function paginador($tabla,$where,$nR) {
	
		$page	=	"";
		if($nR != "") {
			$qBusca	=	" SELECT * FROM ".$tabla." WHERE ".$where." ";
			$rBusca	=	mysql_query($qBusca);
			$nBusca	= 	mysql_num_rows($rBusca);
			$total 	= 	ceil($nBusca / $nR); 	
			
			$page	.=	"<p class=\"paginador\">";
			$page	.=	"<input type=\"hidden\" class=\"total_num\" value=\"{$total}\">";
			$page	.=	"	<a class=\"anterior disabled\" onclick=\"pag.prev();\"> </a> ";
			$page	.=	"Mostrando p&aacute;gina <select name=\"num_page\" id=\"num_page\" onChange=\"page(this.value)\" >";
			if ($total > 0) {
				
				for ($a=1;$a<=$total;$a++) {
					($a==1)?$sel="selected":$sel="";
					$page	.=	"<option value=\"{$a}\">{$a}</option>";
					
				}
				$page	.=	"</select> de <span class='numero'>{$total}</span> ";	
			} else {	
				$page	.=	"<option value=\"1\">1</option>";
				$page	.=	"</select> de <span class='numero'>1</span> ";	
			}
			if($total > 1)
				$page	.=	" <a class=\"siguiente\" onclick=\"pag.next();\"> </a>";

				$page	.=	" | <a style='width:120px' onclick=\"page('')\">Ver Todos los registros </a>";

			$page	.="</p>";
		/*} else 
		*/
		}
			$page	.= ($nBusca > 0)?$nBusca." registros.":"";
		return	$page;
		
	}

	//PARA ENVIO DE CORREO ///////////////////////////////
	function enviarMail($origen,$nombre_origen,$destino,$nombre_destino,$contenido,$asunto)
	{
	}
	
	function enviarFlyer($origen,$nombre_origen,$destino,$nombre_destino,$asunto,$archivo,$cadena_buscar, $cadena_sustituta)
	{
			// Abro el archivo
			$fh = fopen($archivo, 'r');
			$html = fread($fh, filesize($archivo));
			$contenido=str_replace($cadena_buscar,$cadena_sustituta,$html);
			$this->enviarMail($origen,$nombre_origen,$destino,$nombre_destino,$contenido,$asunto);						
	}
		 
	 function deleteCache() {
			@$dir		=	'../reportes';
			@$directorio	=	opendir($dir); 
			
			while (@$archivo = readdir($directorio)) {				
				@unlink($dir.'/'.$archivo);
			}
			@closedir($directorio);  
	 }
	 
	function redirecciona($url)
	{
		/*echo '<script languaje="javascript">location.href=\''.$url.'\';</script>';*/
        header("Location: ".$url);
		exit();
	}
}
?>