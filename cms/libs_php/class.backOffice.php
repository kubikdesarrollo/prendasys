<?php
/* Class backOffice 3.0 */
class backOffice extends DBfront {
	
	var $debug			= 	"";
	var $debugExit		= 	"";
	
	var	$res_data		=	array();
	var	$res_num		=	"";
	
	var $tipo			=	"";
	var $tabla			=	"";
	var $excluye		=	"";
	var $siguienteId	=	"";
	var $salida			=	array();
	var $mostrarDato	=	array();
	var $mostrarJson	=	"";
	var $mostrarJsonfix	=	array();
	
	var $ultimoReg		=	"";
	  
	var $mode			=	"assoc"; 
	var $error			=	"";
	var	$data_exit		=	array();

	var $userName 		= "";
	var $userID	 		= "";
	var $userNickName 	= "";
	var $userEmail		= "";
	var $userExist	 	= "";
	var $userLevel	 	= "";
	var $userPass	 	= "";	
	var $idBar		 	= "";	
	var $nombreBar	 	= "";	
	var $permisosArray	= array();
	var $lastid		= 0;


	
	/**** Resultados Consutla ****/
	function _makeQuery ($query,$num="") {
		$this->res_data			= 	$this->query($query);				
		($num)?$this->res_num	= 	count($this->res_data):"";
	}

	/**** Ultimo registro ****/
	function describeTabla($tabla, $excluye="") {
		$tabla	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $tabla);
		$this->excluye	=	$excluye;
		$salida			=	array();
		$dDescribe		=	array();
		$o=$this->describe($tabla); 
		foreach ($o as $dDescribe) { 
			if( (is_array($excluye) && !in_array($dDescribe,$excluye)) || (!is_array($excluye) && $excluye != $dDescribe) )
				$salida[]=$dDescribe;
		}
		$this->salida	= 	$salida;
	}
	function describeTablaAll($tabla, $excluye="") {
		$tabla	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $tabla);
		$this->excluye	=	$excluye;
		$salida			=	array();
		$dDescribe		=	array();
		$o=$this->describeAll($tabla); 
		foreach ($o as $dDescribe) { 
			if( (is_array($excluye) && !in_array($dDescribe,$excluye)) || (!is_array($excluye) && $excluye != $dDescribe) )
				$salida[]=$dDescribe;
		}
		$this->salida	= 	$salida;
	}
	
	/**** Manipula datos ****/

  	function manipulaDatos($tabla, $indice, $datos , $id,$excluye = "") {
  		$tabla	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $tabla);
		$indice	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $indice);
		  
		$this->describeTablaAll($tabla,$excluye); 
		$query	=	"";
		$names = array();
		foreach($this->salida as $claveall){
			$clave=$claveall["Field"];
			if(isset($datos[$clave]) && $indice!=$clave){
				$query		.=	(($query=="")?"":",")."$clave='".addslashes(strip_tags($datos[$clave]))."'";
				if (strpos($claveall["Type"],'int(')!==false){
					$names[$clave]=intval($datos[$clave]);
				} else 
					$names[$clave]=strip_tags($datos[$clave],'<b><i><p><div><ul><li><pre><strong><center><ol><br><table><td><th><tbody><thead><tr><font><span><hr><img><iframe><a>');
			} 
				
		}
	   if ($id!=0){
	   		$query="UPDATE $tabla SET ".$query." WHERE ($indice='".$id."')";
	   		$where=array();
	   		$where[$indice]=$id; 
	   		$this->update($tabla,$names,$where);
	   		$this->lastid=$id;
	   	} else {
	   		$query="INSERT INTO $tabla SET $query";  
	   		$this->lastid=$this->insert($tabla,$names);
		}
		
		
  	}
	
	/**** Eliminar 1 registro ****/
	function eliminarRegistro($tabla,$indice,$valor) {
		$tabla	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $tabla);
  		$indice	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $indice);

		$query		=	"DELETE FROM $tabla WHERE ($indice='".intval($valor)."') LIMIT 1";
		if (!empty($this->debug)) 
			$this->_makeQuery($query);
		else
			$this->debugExit .= "<p>function eliminarRegistro()</p>";
	}

	function editarRegistro($tabla,$indice,$edit,$tipo="assoc") {
		$tabla	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $tabla);
  		$indice	=	preg_replace("/[^a-zA-Z\_\-\.0-9]/", "", $indice);
  		$edit= intval($edit);
  		$query	=	"SELECT * FROM {$tabla} WHERE {$indice} = {$edit}";
		
		$this->_makeQuery($query,1); 
		$exist		=	$this->res_num;
		if ($exist > 0) {
			$qShowDat	=	"";
			@$jSon	.=	'{ "datos": [ ';
			$jSonfix= array("datos"=>array());
			$this->data_exit=(!empty($this->res_data[0]))?$this->res_data[0]:array(); 
			foreach ($this->data_exit as $dato => $valor) {
				if($this->is_date($valor)){  //in_array($dato, $fechas) 
					$valor	=	preg_replace( "/^\s*([0-9]{1,4})[\/\. -]+([0-9]{1,2})[\/\. -]+([0-9]{1,2})/" , "\\3/\\2/\\1" ,$valor);
					$jSonfix["datos"][]=array("data"=>$dato,"value"=> addslashes(utf8_decode($valor)));
				} else {
					$valuefix=str_replace('"','\\"',stripslashes((mb_detect_encoding($valor)=="UTF-8")?utf8_decode($valor):$valor));
					$jSonfix["datos"][]=array("data"=>$dato,"value"=> $valuefix);
				}
			}
			$jSon	.=	$qShowDat."}]}";	
			$this->mostrarJson	=	$jSon;
		} else 
			$this->mostrarDato	=	"No hay datos disponibles";	
			
		(!empty($this->debug))?$this->debugExit = "<p>function mostrarRegistro ()</p>"."<pre>".print_r($this->mostrarDato)."</pre>":"";
		
		$this->mostrarJsonfix=$jSonfix;
		
	}
	
	function is_date( $str )
	{	
	  if (strlen($str)!=10)
	  {
		 return FALSE;
	  } else {

	 	  $stamp[0] = substr( $str,0,4 );
		  $stamp[1] = substr( $str,5,2 );
		  $stamp[2] = substr( $str,8,2 ); 
		  if (@checkdate($stamp[1], $stamp[2], $stamp[0]))
			 return TRUE;
		  else
	  		return FALSE;
	  }
	} 
	
	function cuales_tipousuario(){
		$sqlmin="SELECT * FROM mega_admintype WHERE id_type='".$_SESSION['tipo']."' ";
		$resmin=mysql_query($sqlmin);
		if ($datomin=@mysql_fetch_assoc($resmin)){
			return $datomin["codigo"];
		} else
			return -1;
	}

	function ultimoRegistro ($tabla,$indice,$siguienteId="si") {
		$query		=	"SHOW TABLE STATUS LIKE '".$tabla."'";
		$next=$this->row($query);	
		$this->ultimoReg = intval(@$next["Auto_increment"]);
	}
	
}
?>