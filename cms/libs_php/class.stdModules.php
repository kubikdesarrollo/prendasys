<?php
class stdModules extends backPage {
	var $tablamodule	=	"";
	var $nameseccion	=	"";
	var $id_table		=	"";
	var	$custom_save	=	false;
	var	$save_file		=	"guarda_stdmodule.php";
	var $titlemodule	=	"";
	
	var $defaultfields	=	true;
	var $fieldsStd		=	array(array("nombre"=>"nombre","tipo"=>"input","titulo"=>"Nombre"));
	
	var $defaultlist	=	true;
	var $fieldlist		=	"nombre";
	var $tituloslist	=	array('N&uacute;mero','Nombre');
	
	var $imgsmodule		=	array(array("imagen",800,600,"Imagen"));  //nombrecampo,ancho,alto
	var $customimgfolders	= array();
	var $actionsmod		=	array('editar','delchecked','nuevo');
	var $fielsearch		=	"nombre";
	var $wheremodule	=	"1";
	var $prioridadmod	=	"prioridad ASC";
	var $filtro_tabla_buscador ="";
	var $id_filtro_tabla_buscador ="";
	var $sinimagen		= false;
	var $CPanel			= "";
	var $configmod 		=	"";
	var $customorder		= 0;
	var $is_sortable	= 0;
	var $inner			= "";
	
	
	function buildModule($configmodulecms){
		extract($configmodulecms, EXTR_PREFIX_SAME, "cms_");
		
		$this->tablamodule	=	$tablamodule;
		$this->nameseccion	=	$nameseccion;
		$this->id_table		=	$id_table;
		$this->titlemodule	=	$titlemodule;
		$this->custom_save	=	$custom_save;
		$this->filtro_tabla_buscador 	=	isset($filtro_tabla_buscador)?$filtro_tabla_buscador:"";
		$this->id_filtro_tabla_buscador =	isset($id_filtro_tabla_buscador)?$id_filtro_tabla_buscador:"";
		$this->configmod 	= 	$configmodulecms;
		$this->inner = (!empty($inner))?$inner:"";

		
		if ($custom_save==true)
			$this->save_file=$save_file;
		if ($sinimagen==true){ 
			$this->sinimagen		= true;
			$this->imgsmodule		=	array();  //Modulo sin imagenes
		} 
		if (isset($fieldlist)){ 
			$this->customlistModule($fieldlist,$tituloslist);
		}
		if (isset($fields_search)){
			$this->customsearchField($fields_search);
		}
		
		if (isset($fields))
			$this->fieldsModule($fields);
		if (isset($orden))
			$this->setOrden($orden);
		if (@$is_sortable==true)
			$this->is_sortable		= 	1;
		if ((@$readonly==true))
			$this->actionsmod		=	array();
		if (!empty($extra_actions))
			$this->customextra_actions($extra_actions);

	}
	function fieldsModule($campos){
		$this->defaultfields	=	false;
		$this->fieldsStd		=	$campos;	//Matriz de array("nombre"=>"campo","tipo"=>"tipocampo","titulo"=>"Titulo")
		$countimgs=0;
		for ($i=0;$i<count($campos);$i++){
			if ($campos[$i]["tipo"]=="imagen"){
				$this->imgsmodule[$countimgs]=$campos[$i]["extra1"];
				if(isset($campos[$i]["carpeta"])){
					$this->customimgfolders[$countimgs]=$campos[$i]["carpeta"];
				}else{
					$this->customimgfolders[$countimgs]="";
				}
				$countimgs++;
			}
		}
	}
	
	function customlistModule($campos,$arraytitulos){
		$this->defaultlist		=	false;
		$this->fieldlist		=	$campos; 	//(string separados por coma)
		$this->tituloslist		=	$arraytitulos;	//array de strings
		$this->fielsearch		=	$campos;
	}
	function customsearchField($fields){
		$this->fielsearch		=	$fields;
	}
	function customextra_actions($actions){ //actions deben ser array de string
		$newactions=array();
		foreach ($actions as $key => $value) { 
			$newactions[]=$value;
		}
		foreach ($this->actionsmod as $key => $value) {
			$newactions[]=$value;
		}
		$this->actionsmod=$newactions;	
	}
	
	function setOrden($query){
		$this->prioridadmod=$query;
		$this->customorder=1;
	}
	
	function makeselect($configuracion){
		$valores=explode(",",$configuracion["enum"]);
		echo '
		<select name="'.$configuracion["nombre"].'" id="'.$configuracion["nombre"].'">';
		  for ($i=0;$i<count($valores);$i++) {
		  echo '<option value="'.$valores[$i].'">'.$valores[$i].'</option>';
		  }
		echo '</select>
		';
	}
	
	function makeselectvalues($configuracion){
		$lavels=explode(",",$configuracion["enum"]);
		$valores=explode(",",$configuracion["values"]);
		echo '
		<select name="'.$configuracion["nombre"].'" id="'.$configuracion["nombre"].'">';
		  for ($i=0;$i<count($valores);$i++) {
		  echo '<option value="'.$valores[$i].'">'.$lavels[$i].'</option>';
		  }
		echo '</select>
		';
	}
	
	function makefileinput($configuracion,$datos="",$anotacion=""){
		$valores=explode(",",$configuracion["extensiones"]);
		$currentfile=(file_exists("../".$configuracion["carpeta"]."/".$datos) && !empty($datos))?$datos:"";
		echo '
		<div class="archivo_manual">
			<h4>Seleccionar un archivo (Extensiones validas: '.$configuracion["extensiones"].')</h4>
			<h6>Tama&ntilde;o maximo: '.ini_get('upload_max_filesize').'</h6>
			<div class="">
				<input type="file" class="" name="sitefiles_'.$configuracion["nombre"].'" id="sitefiles_'.$configuracion["nombre"].'" />
			</div>	
			<br><a id="'.$configuracion["nombre"].'" name="'.$configuracion["nombre"].'" href="../'.$configuracion["carpeta"]."".$currentfile.'" rel="../'.$configuracion["carpeta"].'" target="_blank">'.(!empty($currentfile)?'<strong>Ver archivo actual:</strong> '.$currentfile:"").'</a>'.$anotacion.'
		</div>
		';
	}
	
	function makemultinivelselect($configuracion,$datos=array(),$padre='0',$nivel=0,$rutapadre=""){
		$res=$this->CPanel->query("SELECT * FROM ".$configuracion["tabla"]." WHERE id_parent='".$padre."'");
		foreach ($res as $cfgdatas){
			for ($i=0;$i<$nivel;$i++) echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			echo '<div class="col-sm-6 col-lg-4"><input type="checkbox" name="'.$configuracion["tabla"].'__'.$cfgdatas[$configuracion["indicetabla"]].'" id="'.$configuracion["tabla"].'__'.$cfgdatas[$configuracion["indicetabla"]].'" value="1" class="'.$rutapadre.'_'.$cfgdatas[$configuracion["indicetabla"]].'" onclick="checapadre(\''.$rutapadre.'\')" '.(strpos($datos,'|'.$cfgdatas[$configuracion["indicetabla"]].'|')!==false?'checked="checked"':'').' />'.$cfgdatas[$configuracion["camponombre"]]."</div>";
			$this->makemultinivelselect($configuracion,$datos,$cfgdatas[$configuracion["indicetabla"]],($nivel+1),($rutapadre."_".$cfgdatas[$configuracion["indicetabla"]]));
		}
	}
	
	function creaselect($configuracion,$datos){
			if (empty($datos))
				$selected=$configuracion["extra1"]["seleccionado"];
			else
				$selected=@$datos[$configuracion["nombre"]];
			echo '<div class="form-group"><label class="col-lg-2 control-label">';
			echo $configuracion["titulo"].'</label><div class="col-lg-10">'; 
			creaselect($configuracion["extra1"]["nombre"],$configuracion["extra1"]["tabla"],$configuracion["extra1"]["id_tabla"],$configuracion["extra1"]["orden"],$selected,$configuracion["extra1"]["campomostrar"],$configuracion["extra1"]["funcionjavascript"],$configuracion["extra1"]["tipodeconsuta"],$configuracion["extra1"]["titulovacio"],@$configuracion["extra1"]["preprocesa"]); 
			echo '</div></div>';
	}
	
	/****    FORMATO ESTANDARD   ******/
	function execModuleStd(){

		if(!empty($_REQUEST['initial'])){
		    unset($_SESSION['lastsearchprom']);
		}  
		if (!empty($_REQUEST['last']) && !empty($_SESSION['lastsearchprom'])){ 
		    foreach($_SESSION['lastsearchprom'] as $k=>$v){
		        $_POST[$k]=$_SESSION['lastsearchprom'][$k];
		        $_REQUEST[$k]=$_SESSION['lastsearchprom'][$k];
		    }
		}


		$numReg		=	!empty($_GET["iDisplayLength"])?intval($_GET["iDisplayLength"]):50;
		$tabla		=	$this->tablamodule;
		$indice		=	$this->id_table;
		$seccion	=	$this->nameseccion;
		$filtersearchextra=$this->filtro_tabla_buscador;
		$idfiltersearchextra=$this->id_filtro_tabla_buscador;
		$imagenes	=	""; 
		if (count($this->imgsmodule)>0){
			$imagenes	.=	$this->imgsmodule[0][0].",";
		}
		$where		=	$this->wheremodule;

		if ($this->defaultlist==true){
			$campos		= 	" ".$indice." as ID, ".$this->fieldlist." , {$indice}";
			$campossort	= 	"ID";
			$campossort	= 	explode(",",$this->fieldlist);
			$campossort[]=	"ID";
		}
		else{// SET LIST OF FIELDS
			$campos		= 	" ".$this->fieldlist." , {$indice}";
			$campossort	= 	explode(",",$this->fieldlist);
			$campossort[]=	"ID";
		}
		foreach ($campossort as $keysort => $valuesort) {
			$pedazo=explode(" AS ", $valuesort);
		    $pedazo=explode(" as ", $pedazo[0]);
			$campossort[$keysort]=$pedazo[0];
		}
			
		$accion		=	$this->actionsmod; 
		$titulos	= 	$this->tituloslist;
		
		$pag=(isset($_GET['iDisplayStart']))?(intval($_GET['iDisplayStart'])/$numReg)+1:1;
		$ancholista =	"910";
		if (isset($_GET["sEcho"])){
			if (0){ //$_GET["sEcho"]>1 esto es para cuando se pueden ordenar columnas, aun no esta implementado
				$orden		=	isset($_GET['iSortCol_0']) ? ($campossort[intval($_GET['iSortCol_0'])]." ".$_GET['sSortDir_0']) :" ".$indice."  DESC"; 
			} else {
				if ($this->customorder) $orden = $this->prioridadmod;
				else $orden		=	isset($_GET['iSortCol_0']) ? ($campossort[intval($_GET['iSortCol_0'])]." ".$_GET['sSortDir_0']) :" ".$indice."  DESC"; 
			}
		} else {
			if ($this->customorder) $orden = $this->prioridadmod;
		}
		
		if (isset($_REQUEST['busqueda'])){ 
		    if (!empty($_POST['querysearch'])){
		        $referencias=explode(",", $_POST["querysearch"]);
		        if (strpos(strtoupper($this->fielsearch), "CONCAT")!==false){ 
		        	$this->fielsearch=str_replace(",' ',","+++' '+++", $this->fielsearch);
		        }
				$camposbusqueda=explode(",",$this->fielsearch); 
				$where2="";
				for ($cont=0;$cont<count($camposbusqueda);$cont++){
					$where2.= (($cont>0)?" OR ":"");
			        foreach ($referencias as $key => $value) {
			             $where2.= (($key>0)?" OR ":" ").$camposbusqueda[$cont]." LIKE '%".trim(str_replace("'","",$value))."%' ";
			        }
				}
				if (!empty($where2)) $where.= " AND (".$where2.")";
		    }
		    $_POST['busqueda']=$_REQUEST['busqueda'];
		    $_SESSION['lastsearchprom']=$_POST; 
		    $where=str_replace("+++", ",", $where);
		}
		if (file_exists("../filtros/".$seccion.".php")) include "../filtros/".$seccion.".php"; if (file_exists("filtros/".$seccion.".php")) include "filtros/".$seccion.".php"; 
		
		if (isset($this->configmod["prewhere"])) $where.= " AND ".$this->configmod["prewhere"]." ";
		
		if (empty($this->inner))
			$query	=	"SELECT ".(($this->sinimagen!=true)?"{$imagenes}":"")."{$campos} FROM {$tabla} WHERE {$where}";
		else
			$query	=	"SELECT ".(($this->sinimagen!=true)?"{$imagenes}":"")."{$campos} FROM ".($this->inner)." WHERE {$where}";
		
		$listar=true;
		$modo=(empty($_REQUEST['accion']))?"listar":$_REQUEST['accion'];
		$showform=false;
		if(isset($_REQUEST['accion'])) {
			$listar		=	($_REQUEST['accion']=="listar")?true:false;
			$nuevo		=	($_REQUEST['accion']=="nuevo")?true:false;
			if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
				$mostrar		=($_REQUEST['accion']=="mostrar")?true:false;
				$modificar		=($_REQUEST['accion']=="editar")?true:false;
				$modificarPics	=($_REQUEST['accion']=="editarpics")?true:false;
			}
		} 

		if ($modo=="pagina"){ 
		    $orderby=$orden;
		    $this->CPanel->listarAjax($seccion,$query,$tabla,$indice,$accion,$campos,$titulos,$pag,$numReg,$ancholista,$orderby);
		    exit();
		}
		if ($listar){ 
			echo $this->CPanel->inicializa($seccion,$query,$tabla,$indice,$accion,implode(",",$campossort),$titulos,$pag,$numReg,$ancholista,$orden,$where,$this->titlemodule);
			echo '<input type="hidden" name="is_stdmodule" id="is_stdmodule" value="1" />
			<input type="hidden" name="token" id="token" value="'. md5('kbi0000JoK'.$_REQUEST['seccion']).'">';
		} else {	
                if ($modo=='editar'){
                    $id_edit=intval($_REQUEST['id_edit']);
					$this->CPanel->editarRegistro($tabla,$indice,$id_edit);
					$datos=$this->CPanel->data_exit;
				}
				$showform=true;
		}
		if ($showform){
			$indice=str_replace($tabla.".", "", $indice);
			echo '
			<section class="wrapper">
			    <div class="row">
			        <form name="form" class="form-horizontal tasi-form" id="form" enctype="multipart/form-data" action="scripts/'.$this->save_file.'?seccion='.$seccion.'&tabla='.$tabla.'&indice='.$indice.'" method="post" role="form">
			            <div class="col-lg-12">
			                <section class="panel">
			                    <header class="panel-heading">
			                        '.strtoupper($this->titlemodule).'
			                    </header>
			                    <div class="panel-body">'.(
			                    	(@$_REQUEST["backrefer"]==1)?(
			                    	'<button class="btn btn-default " type="button" onclick=\'javascript:document.location.href="'.$_SERVER['HTTP_REFERER'].'"\'><i class="icon-long-arrow-left"></i> REGRESAR</button>'):(
			                        '<button class="btn btn-default " type="button" onclick=\'javascript:document.location.href="admin.php?seccion='.$seccion.'&last=1'. ((isset($_REQUEST['page']))?'&page='.$_REQUEST['page']:'').'"\'><i class="icon-long-arrow-left"></i> REGRESAR</button>')).'
			                        <button class="btn btn-success pull-right" type="submit"><i class="icon-save"></i> GUARDAR</button>
			                    </div>
			                    <div class="panel-body">';
			                    $supergalerias=array();
			                    $modales=array();
			                    for ($i=0;$i<count($this->fieldsStd);$i++){
										$nota=isset($this->fieldsStd[$i]["descripcion"])?'<div class="stddescrip">'.$this->fieldsStd[$i]["descripcion"]."</div>":"";
										switch($this->fieldsStd[$i]["tipo"]){
											case "input":		if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloqueinput($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@htmlspecialchars(@$datos[$this->fieldsStd[$i]["nombre"]]),@$this->fieldsStd[$i]["required"],@$this->fieldsStd[$i]["clase"],@$this->fieldsStd[$i]["noeditable"]);  break;
											case "numerico":	if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloquenumeric($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]],@$this->fieldsStd[$i]["required"],@$this->fieldsStd[$i]["decimals"]);  break;
											case "textarea":	if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloquetextarea($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]],(isset($this->fieldsStd[$i]["clase"])?$this->fieldsStd[$i]["clase"]:''));  break;
											case "editor":		if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloqueeditor($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "checkbox":	if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloquechecked($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]],@$this->fieldsStd[$i]["clase"]); break;
											case "select":		$this->creaselect($this->fieldsStd[$i],@$datos); break;
											case "youtube":		bloqueyoutube($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "vimeo":		bloquevimeo($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "dateinput":	bloquedate($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "supergaleria": $supergalerias[]=array($this->fieldsStd[$i],@$datos[$this->id_table]); break;
											case "enum": 		echo $this->makeselect($this->fieldsStd[$i]).$nota; break;
											case "enumvalues":	bloqueenumvalues($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]],$this->fieldsStd[$i]["enum"],$this->fieldsStd[$i]["values"],@$this->fieldsStd[$i]["clase"]); break; 
											case "archivo":		$this->makefileinput($this->fieldsStd[$i],@$datos[$this->fieldsStd[$i]["nombre"]],$nota);break;
											case "selectmultinivel":	echo '<div class="form-group "><label class="col-lg-2 control-label">'.$this->fieldsStd[$i]["titulo"].'</label><div class="col-lg-10">'; $this->makemultinivelselect($this->fieldsStd[$i],@$datos[$this->fieldsStd[$i]["nombre"]]); echo "</div><div class='clear'></div></div>";break;
											case "color":		bloquecolor($this->fieldsStd[$i],@$datos[$this->fieldsStd[$i]["nombre"]]); break; 
											case "datetime":	bloquedatetime($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "imagen": 		bloqueimagen($this->fieldsStd[$i],@$datos,$this->tablamodule,$this->id_table,$this->nameseccion,@$this->fieldsStd[$i]["required"]); break;
											case "autorizar":  	bloqueautorizar($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@$datos[$this->fieldsStd[$i]["nombre"]],@$datos[$indice]); break;
											case "modulo": 		include ($this->fieldsStd[$i]["ruta"]); break;
											case "comavalues":	bloquecomavalues($this->fieldsStd[$i],@$datos[$this->fieldsStd[$i]["nombre"]]); break;
											case "separador":	echo '<div class="clear"></div>'; break;
											case "subtabla": 	bloquesubtabla($this->fieldsStd[$i],@$datos[$indice]); $modales[]=array("config"=>$this->fieldsStd[$i],"id"=>@$datos[$indice]); break;
											case "telefono":	if(!empty($this->fieldsStd[$i]["nota"])) bloquenota($this->fieldsStd[$i]["nota"]); bloquetelefono($this->fieldsStd[$i]["titulo"],$this->fieldsStd[$i]["nombre"],@htmlspecialchars(@$datos[$this->fieldsStd[$i]["nombre"]]),@$this->fieldsStd[$i]["required"],@$this->fieldsStd[$i]["clase"],@$this->fieldsStd[$i]["noeditable"]);  break;
										}
								}
			                        
			                    echo '</div>

			                    
			                </section>

			            </div>
			            <input type="hidden" class="indice" name="'.$indice.'" id="'.$indice.'" value="'.@$datos[$indice].'" /> 
			            '.((@$_REQUEST['id_edit']>0)?'<input type="hidden" name="editar" id="editar" value="1" />':"").'
			            <input type="hidden" name="seccion" id="seccion" value="'.$seccion.'" />
			            <input type="hidden" id="Token_CSRF" name="Token_CSRF" value="'.get_token_csrf() .'" />
			        </form>';

			        if (!empty($supergalerias)) {
			        	if (!empty($datos[$this->id_table])){
				        	foreach ($supergalerias as $sgal => $valuegal) {
				        		bloquesupergaleria($valuegal[0],$valuegal[1]);
				        	}
			        	}
		        	}
		        	if (!empty($modales)){
		        		foreach ($modales as $kem => $modal) {
		        			bloquemodal($modal);
		        		}
		        	} 
		        	echo '
			    </div>
			        
			</section>
			';
		}
		
	}
	function setCPanel($Controlpanel){
		$this->CPanel=$Controlpanel;
	}
	/************** FIN FORMATO ESTANDARD ***********************/
}
?>