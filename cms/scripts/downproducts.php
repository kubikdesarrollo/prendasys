<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

$fp = fopen('../reportes/productos.csv', 'w');

$fields=$db->query("SELECT * FROM Cat_Articulos");
$categorias=$db->query("SELECT * FROM categories");
$lineas=$db->query("SELECT * FROM lineas");
$sublineas=$db->query("SELECT * FROM sublineas");
$specs=$db->query("SELECT * FROM Cat_tech_specs INNER JOIN Cat_tech_specs_articulo ON Cat_tech_specs.id_tech_spec=Cat_tech_specs_articulo.id");
$colors=$db->query("SELECT * FROM colour_product INNER JOIN colours ON colour_product.id_color=colours.id");
$columnas=array();
foreach($fields as $k=>$field){
    unset($field["id"]);
    if (empty($columnas)){
        foreach($field as $c=>$v){
            $columnas[]=$c;
        }
        $columnas[]='specs';
        $columnas[]='colores';
        fputcsv($fp, $columnas);
    }

    //Reemplazamos las categorias y lineas
    foreach($categorias as $c){
        if ($field["id_categoria"]==$c["id"]){
            $field["id_categoria"]=$c["title"];
        } 
    }
    foreach($lineas as $l){
        if ($field["id_lineaProducto"]==$l["id"]){
            $field["id_lineaProducto"]=$l["descripcion_linea"];
        } 
    }
    
    foreach($sublineas as $l){
        if ($field["id_sublinea"]==$l["id"]){
            $field["id_sublinea"]=$l["descripcion_linea"];
        } 
    }
    // cargamos specs
    $field["specs"]='';
    foreach($specs as $spec){
        if ($spec["itemcode"]==$field["itemcode"]){
            $field["specs"].=(!empty($field["specs"])?",":"").$spec["tech_spec"];
        }
    }
    // cargamos colores
    $field["colores"]='';
    foreach($colors as $color){
        if ($color["itemcode"]==$field["itemcode"]){
            $field["colores"].=(!empty($field["colores"])?",":"").$color["title"];
        }
    }
    

    fputcsv($fp, $field);
}

fclose($fp);

$path = '../reportes/productos.csv';
$type = '';
 
if (is_file($path)) {
 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 $type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
 $info = finfo_open(FILEINFO_MIME);
 $type = finfo_file($info, $path);
 finfo_close($info);
 }
 if ($type == '') {
 $type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 header("Content-Disposition: attachment; filename=Productos.csv");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);
} else {
 die("El archivo no existe.");
}