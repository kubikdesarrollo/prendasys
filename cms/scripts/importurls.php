<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

if (is_uploaded_file(@$_FILES["importfile"]['tmp_name'])){ 
    $extencion  =   ext($_FILES["importfile"]['name']);
    $blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
    foreach ($blacklist as $file)
    {if(preg_match("/$file\$/i", $_FILES["importfile"]['name'])){
      echo 'Archivo no permitido';
      exit();}
    }
    $urlf='../reportes/importurls.csv'; 
    $tmp_namef = $_FILES["importfile"]['tmp_name'];
    @move_uploaded_file($tmp_namef,$urlf); 

    if (($gestor = fopen("../reportes/importurls.csv", "r")) !== FALSE) { 
        $linea=0;
        $fields=array();
        $db->query("TRUNCATE TABLE Cat_url_marketplace");
        $colours=$db->query("SELECT * FROM colours");
        $marketplaces=$db->query("SELECT * FROM marketplaces");
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            if($datos[0]=="itemcode"){
                $fields=$datos;
            } else {
                if (!empty($fields)){
                    $names=array();
                    foreach($fields as $i=>$f){
                        $names[$f]=$datos[$i];
                    }
                    //Reemplazamos las colores y marketplaces
                    if (empty($names["id_color"])) $names["id_color"]="negro";
                    
                    foreach($colours as $c){
                        if (trim(strtolower($names["id_color"]))==trim(strtolower($c["title"])) ){
                            $names["id_color"]=$c["id"];
                        } 
                    }
                    foreach($marketplaces as $l){
                        if (trim(strtolower($names["id_marketplace"]))==trim(strtolower($l["title"])) ){
                            $names["id_marketplace"]=$l["id"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_color"]=intval($names["id_color"]);
                    $names["id_marketplace"]=intval($names["id_marketplace"]);

                    $names["unico"]=$names["itemcode"]."/".$names["id_marketplace"]."/".$names["id_color"];
                    $names["priority"]=intval(@$names["priority"]);
                    //insertamos
                    if (!empty($names["unico"])){
                        $existe=$db->row("SELECT * FROM Cat_url_marketplace where unico=:unic",array("unic"=>$names["unico"]));
                        if(empty($existe)){
                            $db->insert("Cat_url_marketplace",$names);
                        }
                    }
                }
            }
        }
    }
}
header("Location: ../admin.php?seccion=urls&saved=1");
