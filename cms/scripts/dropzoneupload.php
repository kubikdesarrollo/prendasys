<?php	
/****************** CONFIGURACION DE VARIABLES *********************/

require_once('../config/configuracion_guarda.php'); 
require_once('../libs_php/recortar_imagen.php'); 


$esmod_std=0;
for  ($din=0;$din<count($definitionsCMS->modulos_portal) && $esmod_std==0;$din++){
	if ($_GET['seccion']==$definitionsCMS->modulos_portal[$din]['nameseccion']){
		$_definicion=$definitionsCMS->modulos_portal[$din];
		foreach ($_definicion["fields"] as $keyf => $valuef) {
			if (@$valuef["tabla"]==$_REQUEST["tabla"] && (@$valuef["idunico"]==$_REQUEST["idunico"])){
				$esmod_std=1;
				$_config=$valuef;
			}
		}
	}
}


if (!empty($_FILES["file"]) && ($esmod_std!=0)){
		$extencion 	= 	ext($_FILES['file']['name']);
		$extencion	=	strtolower('.'.$extencion);
		
		if ($_config["tabla"]="images_gallery_products"){
			$new_filename 	=	str_replace(
				array($extencion," ","(",")"),
				array("","_","",""),
				$_FILES['file']["name"]);
		} else 
			$new_filename 	=	$n_id.'_'.niceURL($_FILES['file']["name"]).time().rand(111,999);
		$ruta_imags=$_config["carpeta"];

		$ancho_normal=$_config["anchoimagen"];
		$alto_normal=$_config["altoimagen"];
		$resize_normal=(isset($_config["resize_normal"]))?$_config["resize_normal"]:true;
		$recorta_normal=(isset($_config["recorta_normal"]))?$_config["recorta_normal"]:true;
		$campoimagen=$_config["campo"];

		
			
			
			$foo = new Upload($_FILES['file']);
			$foo->file_new_name_body 	= 	$new_filename; 
			$foo->jpeg_quality 			=	100;
			$foo->image_ratio_crop  	= 	true;
			if ($_config["tabla"]="images_gallery_products"){
				$foo->file_overwrite = false;
			}

			$foo->image_resize          = 	$resize_normal;
			$foo->image_ratio_crop  	= 	$recorta_normal;
			//$foo->image_ratio = true;
			//$foo->image_ratio_fill = true;
			if ($alto_normal>0)		
				$foo->image_y 			= 	$alto_normal;
			if ($ancho_normal>0)
				$foo->image_x 			= 	$ancho_normal;
			if ($ancho_normal==0)
				$foo->image_ratio_x		= 	true;
			if ($alto_normal==0)
				$foo->image_ratio_y		= 	true;

	
			if ($foo->uploaded) {
				$foo->Process($ruta_imags); 
				if ($foo->processed) { 
					$_POST[$campoimagen]	=	$foo->file_dst_name;//$new_filename.$extencion;	
					$_POST['efecto']=	"Izquierda";
					@chmod($ruta_imags.$new_filename.$extencion, 0777);
				} else { 
					echo 'error Process: ' . $foo->error;
				}	
			} else { echo 'error uploaded: ' . $foo->error; }	
			
			if (count($_config["customsizes"])>0){
			/********* Thumbnail **********/
				for($k=0;$k<count($_config["customsizes"]);$k++){
					$new_filename 	=	str_replace($extencion,'',$_POST[$campoimagen]);
	
					$foo->file_new_name_body 	= 	$new_filename;
					$foo->jpeg_quality 			=	100;
					
					$foo->image_resize 			= 	isset($_config["customsizes"][$k]["resize"])?$_config["customsizes"][$k]["resize"]:true;
					$foo->image_ratio_crop  	= 	isset($_config["customsizes"][$k]["recorta"])?$_config["customsizes"][$k]["recorta"]:true;
					if (isset($_config["customsizes"][$k]["ancho"])){
						if ($_config["customsizes"][$k]["ancho"]>0)
							$foo->image_x=$_config["customsizes"][$k]["ancho"];
						else{
							if ($_config["customsizes"][$k]["ancho"]==0)
								$foo->image_ratio_x		= 	true;
							else
								$foo->image_x	= 	100;
						}
					} else
								$foo->image_x	= 	100;
					if (isset($_config["customsizes"][$k]["alto"])){
						if ($_config["customsizes"][$k]["alto"]>0)
							$foo->image_y=$_config["customsizes"][$k]["alto"];
						else{
							if ($_config["customsizes"][$k]["alto"]==0)
								$foo->image_ratio_y		= 	true;
							else
								$foo->image_y	= 	100;
						}
					} else
								$foo->image_y	= 	100;
								
					$foo->Process($ruta_imags.$_config["customsizes"][$k]["carpeta"]."/");
					if ($foo->processed) { 
						$nota	.=	" Thumb = Thumb subido exitosamente <br> ";
					} else { 
						echo $nota	.= ' errorThumb = ' . $foo->error . ' <br> ' ;
					}
				}
			}
			
			$foo->Clean();
		
	
		$editar=0;
		$_POST['nombre']="";
		if (!empty($_config["preset"])){
			foreach ($_config["preset"] as $kid => $prevalue) {
				$_POST[$kid]=$prevalue;
			}
		}
		$_POST[$_config["id_padre"]]=intval(@$_REQUEST['idpadre']); 
		$User->manipulaDatos($tabla,$indice_t,$_POST,$editar,"",$debug);
}

echo "Processed";
?>