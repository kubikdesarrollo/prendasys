<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

if (is_uploaded_file(@$_FILES["importfile"]['tmp_name'])){ 
    $extencion  =   ext($_FILES["importfile"]['name']);
    $blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
    foreach ($blacklist as $file)
    {if(preg_match("/$file\$/i", $_FILES["importfile"]['name'])){
      echo 'Archivo no permitido';
      exit();}
    }
    $urlf='../reportes/mayoristas.csv'; 
    $tmp_namef = $_FILES["importfile"]['tmp_name'];
    @move_uploaded_file($tmp_namef,$urlf); 

    if (($gestor = fopen("../reportes/mayoristas.csv", "r")) !== FALSE) { 
        $linea=0;
        $fields=array();

        $fields=$db->query("SELECT * FROM sucmayoristas");
        $countries=$db->query("SELECT * FROM countries");
        $states=$db->query("SELECT * FROM states");
        $mayoristas=$db->query("SELECT * FROM mayoristas");

        $db->query("TRUNCATE TABLE sucmayoristas");
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            if($datos[0]=="id_country"){
                $fields=$datos;
            } else {
                if (!empty($fields)){
                    $names=array();
                    foreach($fields as $i=>$f){
                        $names[$f]=$datos[$i];
                    }
                    //Reemplazamos las countries
                    foreach($countries as $c){
                        if ($names["id_country"]==$c["name"]){
                            $names["id_country"]=$c["numcode"];
                        } 
                    }
                    foreach($states as $c){
                        if ($names["id_state"]==$c["name"]){
                            $names["id_state"]=$c["id"];
                        } 
                    }
                    foreach($mayoristas as $c){
                        if ($names["id_mayorista"]==$c["title"]){
                            $names["id_mayorista"]=$c["id"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_country"]=intval($names["id_country"]);
                    $names["id_state"]=intval($names["id_state"]);
                    $names["id_mayorista"]=intval($names["id_mayorista"]);

                    
                    //insertamos
                    $db->insert("sucmayoristas",$names);
                }
            }
        }
    }
}
header("Location: ../admin.php?seccion=mayoristas&saved=1");
