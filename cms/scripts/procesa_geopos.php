<?php

session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();
$limit=" LIMIT ".intval($_REQUEST["counter"]);
if ($_REQUEST["seccion"]=="sucdistribuidores")
    $tabla="sucdistribuidores";
else if ($_REQUEST["seccion"]=="sucmayoristas")
    $tabla="sucmayoristas";
else exit("false");
$registros=$db->query("SELECT ".$tabla.".*,states.name FROM ".$tabla." LEFT JOIN states ON ".$tabla.".id_state=states.id WHERE ".$tabla.".latitude='' OR ".$tabla.".longitude='' OR ".$tabla.".latitude IS NULL OR ".$tabla.".longitude IS NULL OR ".$tabla.".latitude='0' OR ".$tabla.".longitude='0' ".$limit);

function getRemoteFileg($url, $timeout = 10) {
    $ch = curl_init();
    curl_setopt ($ch, CURLOPT_URL, $url);
    curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
    curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
    $file_contents = curl_exec($ch);
    $infoexerunit = curl_getinfo($ch); 
    $errorexerunit = curl_error($ch); 
    curl_close($ch);
    return ($file_contents) ? $file_contents : FALSE;
}

$noencontradas=array();
foreach($registros as $registro){
    $address=preg_replace("/[\r\n|\n|\r]+/", " ",trim($registro["address"]));
    $ruta = 'https://maps.googleapis.com/maps/api/geocode/json?address='.
    urlencode($address).',+'.urlencode($registro["city"]).',+'.urlencode($registro["name"]).'&key=AIzaSyA5LgqBMFNf0lf94eSWxZmbS_jxO0Y7Xs4';
    
    $res=getRemoteFileg($ruta);
    

    if ($res){ 
        $lata1 = json_decode($res);
        if (!empty($lata1->results)){
            
            foreach ($lata1->results as $k=> $item) {
                if ($k==0){
                    $lat = $item->geometry->location->lat;
                    $lng = $item->geometry->location->lng;
                    if(!empty($lat) && !empty($lng)){
                        $names=array(
                            "latitude"=>$lat,
                            "longitude"=>$lng
                        ); 
                        $db->update($tabla,$names,array("id"=>$registro["id"]));
                    }
                }
            }
        } else {
            $noencontradas[]="This API project is not authorized to use this API";    
        }
    } else  {
        $noencontradas[]=$registro["title"]." ".$address;
    }
}
if (empty($noencontradas))
    echo 'true';
else {
    foreach($noencontradas as $no){
        echo $no."<br>";
    }
}