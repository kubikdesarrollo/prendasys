<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

$fp = fopen('../reportes/urls.csv', 'w');

$fields=$db->query("SELECT * FROM Cat_url_marketplace");
$colours=$db->query("SELECT * FROM colours");
$marketplaces=$db->query("SELECT * FROM marketplaces");
$columnas=array();
foreach($fields as $k=>$field){
    unset($field["id"]);
    if (empty($columnas)){
        foreach($field as $c=>$v){
            $columnas[]=$c;
        }
        fputcsv($fp, $columnas);
    }

    //Reemplazamos las colores y marketplaces
    foreach($colours as $c){
        if ($field["id_color"]==$c["id"]){
            $field["id_color"]=$c["title"];
        } 
    }
    foreach($marketplaces as $l){
        if ($field["id_marketplace"]==$l["id"]){
            $field["id_marketplace"]=$l["title"];
        } 
    }

    fputcsv($fp, $field);
}

fclose($fp);

$path = '../reportes/urls.csv';
$type = '';
 
if (is_file($path)) {
 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 $type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
 $info = finfo_open(FILEINFO_MIME);
 $type = finfo_file($info, $path);
 finfo_close($info);
 }
 if ($type == '') {
 $type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 header("Content-Disposition: attachment; filename=urls.csv");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);
} else {
 die("El archivo no existe.");
}