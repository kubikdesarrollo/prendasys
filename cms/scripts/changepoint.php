<?php
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();
$id=!empty($_POST["item"])?intval($_POST["item"]):0;
$nivel=!empty($_POST["nivel"])?intval($_POST["nivel"]):0;

if (check_valid_csrf('post') && !empty($_POST["Token_CSRF"]) && !empty($id) && !empty($nivel)){
	$valor=explode(",",preg_replace("/[^\.\,0-9]/", "", @$_POST['valor']));
	if(count($valor>2)){
		$posx=floatval($valor[0]);
		$posy=floatval($valor[1]);
		if(!empty($posx) && !empty($posy)){
			//Actualizamos valores y regeneramos csrf
			$db->update("locals",["posx"=>$posx,"posy"=>$posy,"level"=>$nivel],["id"=>$id]);
			$nuevo=get_token_csrf(); 
			echo $nuevo;
		} else echo "0";
	} else echo "0";
} else {
	echo  "0";
}
?>