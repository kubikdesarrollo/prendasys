<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

$fp = fopen('../reportes/sucursales_distribuidores.csv', 'w');

$fields=$db->query("SELECT * FROM sucdistribuidores");
$distribuidores=$db->query("SELECT * FROM distribuidores");
$states=$db->query("SELECT * FROM states");
$countries=$db->query("SELECT * FROM countries");

$columnas=array();
foreach($fields as $k=>$field){
    unset($field["id"]);
    unset($field["featured"]);
    unset($field["priority"]);
    if (empty($columnas)){
        foreach($field as $c=>$v){
            $columnas[]=$c;
        }
        fputcsv($fp, $columnas);
    }

    //Reemplazamos las distribuidores
    foreach($distribuidores as $c){
        if ($field["id_distribuidor"]==$c["id"]){
            $field["id_distribuidor"]=$c["title"];
        } 
    }

    //Reemplazamos estados
    foreach($states as $c){
        if ($field["id_state"]==$c["id"]){
            $field["id_state"]=$c["name"];
        } 
    }

    //Reemplazamos paises
    foreach($countries as $c){
        if ($field["id_country"]==$c["numcode"]){
            $field["id_country"]=$c["name"];
        } 
    }

    fputcsv($fp, $field);
}

fclose($fp);

$path = '../reportes/sucursales_distribuidores.csv';
$type = '';
 
if (is_file($path)) {
 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 $type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
 $info = finfo_open(FILEINFO_MIME);
 $type = finfo_file($info, $path);
 finfo_close($info);
 }
 if ($type == '') {
 $type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 header("Content-Disposition: attachment; filename=sucursales_distribuidores.csv");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);
} else {
 die("El archivo no existe.");
}