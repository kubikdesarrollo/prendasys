<?php	
/****************** CONFIGURACION DE VARIABLES *********************/
require_once('../config/configuracion_guarda.php');
require_once('../libs_php/recortar_imagen.php');

if (1){//check_valid_csrf("post")
	$esmod_std=0;
	for  ($din=0;$din<count($definitionsCMS->modulos_portal) && $esmod_std==0;$din++){
		if ($_GET['seccion']==$definitionsCMS->modulos_portal[$din]['nameseccion']){
			$_definicion=$definitionsCMS->modulos_portal[$din];
			
			if($tabla == $_definicion["tablamodule"] ) { 
				$_camposMod=$_definicion["fields"];
				
				$ci=0;
				for ($i=0;$i<count($_camposMod);$i++){
					//subir imagenes si aplica
					if ($_camposMod[$i]["tipo"]=="imagen"){
						$ancho_normal=$_camposMod[$i]["extra1"][1];
						$alto_normal=$_camposMod[$i]["extra1"][2];
						$resize_normal=(isset($_camposMod[$i]["resize_normal"]))?$_camposMod[$i]["resize_normal"]:true;
						$recorta_normal=(isset($_camposMod[$i]["recorta_normal"]))?$_camposMod[$i]["recorta_normal"]:true;
						$genera_miniatura=(isset($_camposMod[$i]["extra1"][4]))?$_camposMod[$i]["extra1"][4]:false;
						$ancho_miniatura=(isset($_camposMod[$i]["extra1"][5]))?$_camposMod[$i]["extra1"][5]:80;
						$alto_miniatura=(isset($_camposMod[$i]["extra1"][6]))?$_camposMod[$i]["extra1"][6]:80;
						$carperadeimagen=(!empty($_camposMod[$i]["carpeta"]))?$_camposMod[$i]["carpeta"]:('../images/'.$_definicion["nameseccion"]);
						
						$ruta_imags="../".$carperadeimagen."/";
						$new_filename 	=	$n_id.'_'.niceURL(@$_POST['nombre'])."_".@$_camposMod[$i]["nombre"] ."_". time();
						if (count(@$_camposMod[$i]["customsizes"])>0 ){
							procesaimagenmulti('imagen_'.$_camposMod[$i]["nombre"], $_camposMod[$i]["nombre"], $new_filename,$ruta_imags,$ancho_normal,$alto_normal,$resize_normal,$recorta_normal,$_camposMod[$i]["customsizes"]);
						} else { 
							procesaimagen('imagen_'.$_camposMod[$i]["nombre"], $_camposMod[$i]["nombre"], $new_filename,$ruta_imags,$ancho_normal,$alto_normal,$resize_normal,$recorta_normal,$genera_miniatura,$ancho_miniatura,$alto_miniatura);
						}
						$ci++;
					}
					//Validamos checkbox
					if ($_camposMod[$i]["tipo"]=="checkbox"){
						$_POST[$_camposMod[$i]["nombre"]]=(isset($_POST[$_camposMod[$i]["nombre"]]))?1:0;
					}
					//Validamos multiselect: Valores separados por coma
					if ($_camposMod[$i]["tipo"]=="comavalues"){
						$_POST[$_camposMod[$i]["nombre"]]=(isset($_POST[$_camposMod[$i]["nombre"]]))?implode(",",@$_POST[$_camposMod[$i]["nombre"]]):"";
					}
					//Validamos Videos  Youtube
					if ($_camposMod[$i]["tipo"]=="youtube"){
						if (!empty($_POST[$_camposMod[$i]["nombre"]]))
							$_POST[$_camposMod[$i]["nombre"]]=linkifyYouTubeURLs($_POST[$_camposMod[$i]["nombre"]]);
					}
					//Validamos Videos  Vimeo
					if ($_camposMod[$i]["tipo"]=="vimeo"){
						if (!empty($_POST[$_camposMod[$i]["nombre"]]))
							$_POST[$_camposMod[$i]["nombre"]]=linkifyVimeoURLs($_POST[$_camposMod[$i]["nombre"]]);
					}
					//Validamos fecha
					if ($_camposMod[$i]["tipo"]=="dateinput"){
						$_POST[$_camposMod[$i]["nombre"]] = fecha($_POST[$_camposMod[$i]["nombre"]],1);
					}
					if ($_camposMod[$i]["tipo"]=="vimeoyoutube"){
						if ( @$_POST["tipo"]=="youtube"){
							if (!empty($_POST[$_camposMod[$i]["nombre"]]))
								$_POST[$_camposMod[$i]["nombre"]]=linkifyYouTubeURLs($_POST["videourl"]);
						} 
					}
					//Validamos carga de archivos individuales
					if ($_camposMod[$i]["tipo"]=="archivo"){
						$extvalid=explode(",",$_camposMod[$i]["extensiones"]);
						if (is_uploaded_file($_FILES["sitefiles_".$_camposMod[$i]["nombre"]]['tmp_name'])){
							$extencion 	= 	ext($_FILES["sitefiles_".$_camposMod[$i]["nombre"]]['name']);
							
							$blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
							foreach ($blacklist as $file)
							{if(preg_match("/$file\$/i", $_FILES["sitefiles_".$_camposMod[$i]["nombre"]]['name']))
							exit();}
							if (in_array($extencion,$extvalid)){
								$newfn=str_replace($extencion,"",niceURL($_FILES["sitefiles_".$_camposMod[$i]["nombre"]]['name'])).".".$extencion;
								$urlf='../../'.$_camposMod[$i]["carpeta"].$newfn;
								$tmp_namef = $_FILES["sitefiles_".$_camposMod[$i]["nombre"]]['tmp_name'];
								@move_uploaded_file($tmp_namef,$urlf); 
								$_POST[$_camposMod[$i]["nombre"]]=$newfn;
							} else{
								echo "Archivo invalido"; exit();
							}
						}
					}
					// Validamos Minigaleria
					if ($_camposMod[$i]["tipo"]=="minigaleria"){
						$procesaminigal=1; $_cfgminigaleria=$_camposMod[$i];
					}
					//Validamos selectmultinivel
					if ($_camposMod[$i]["tipo"]=="selectmultinivel"){
						$sqlaux="SELECT * FROM ".$_camposMod[$i]["tabla"];
						$resaux=$User->query($sqlaux);
						$compuesto="";
						foreach($resaux as $datosaux){
							if (@$_POST[$_camposMod[$i]["tabla"].'__'.$datosaux[$_camposMod[$i]["indicetabla"]]]=='1'){
								$compuesto.=(($compuesto=="")?"|":"").$datosaux[$_camposMod[$i]["indicetabla"]]."|";
							}
						}
						$_POST[$_camposMod[$i]["nombre"]]=$compuesto;
					}
				}
				
				
				//Guardamos
				/*foreach ($_POST as  $k => $v) {
					if (is_array($v)){
						foreach ($v as  $key => $val) {
							$_POST[$k][$key]=utf8_decode($val);
						}
					} else{
						$_POST[$k]=utf8_decode($v);
					}
				}*/
				$User->manipulaDatos($tabla,$indice_t,$_POST,$editar,"",@$debug);
				
				//Obtenemos id registro
				if(!empty($_REQUEST[$indice_t])){
					$lastindex=intval($_REQUEST[$indice_t]);
				}else{
					$lastindex=$User->lastid;		
				}
				
				//Procesamos Minigaleria
				if (intval(@$procesaminigal)>0){
						$ancho_normal=$_cfgminigaleria["anchoimagen"];
						$alto_normal=$_cfgminigaleria["altoimagen"];
						$resize_normal=(isset($_cfgminigaleria["resize_normal"]))?$_cfgminigaleria["resize_normal"]:true;
						$recorta_normal=(isset($_cfgminigaleria["recorta_normal"]))?$_cfgminigaleria["recorta_normal"]:true;
						$genera_miniatura=(isset($_cfgminigaleria["generaminiatura"]))?$_cfgminigaleria["generaminiatura"]:false;
						$ancho_miniatura=(isset($_cfgminigaleria["ancho_miniatura"]))?$_cfgminigaleria["ancho_miniatura"]:80;
						$alto_miniatura=(isset($_cfgminigaleria["alto_miniatura"]))?$_cfgminigaleria["alto_miniatura"]:80;

						for ($i=1;$i<=$_REQUEST['numimgs'];$i++){
							if(is_uploaded_file($_FILES['imagenes']['tmp_name'][$i-1])){
								$time=time()."_".rand(1,999);
								$ruta_imags=$_cfgminigaleria["carpeta"];
								$new_filename 	=	$lastindex.'_'.$time."_".$i;
								$extencion 	= 	ext($_FILES['imagenes']['name'][$i-1]);
								$extencion	=	strtolower('.'.$extencion);
								move_uploaded_file($_FILES['imagenes']['tmp_name'][$i-1],$ruta_imags.$new_filename."_aux");
								procesaimagentemp($ruta_imags.$new_filename."_aux", $_cfgminigaleria["nombre"], $new_filename, $extencion, $ruta_imags,$ancho_normal,$alto_normal,$resize_normal,$recorta_normal,$genera_miniatura,$ancho_miniatura,$alto_miniatura);
								@unlink($ruta_imags.$new_filename."_aux");
								$sqlmini=mysql_query("INSERT INTO imagenes VALUES('','$lastindex','".$new_filename.$extencion."','0')");
							}
						}
				}

				//procesamos custom modules
				if (file_exists(@$_GET["seccion"].".php")){ 
					include $_GET["seccion"].".php";
				}
			}
			$esmod_std=1; 
		}//fin if seccion
	}

} else {
	header("Location: ../admin.php?seccion=".$_GET['seccion']);
	exit(0);
}	

$target = "../admin.php?seccion=".$_GET['seccion']."&accion=editar&id_edit=".$lastindex."&saved=1";
if (headers_sent()) {
    echo "<meta http-equiv=\"refresh\" content=\"0; URL={$target}\" />";
} else {
    header("Location: {$target}");
}

?>