<?php
class mailtopress {

	private $dBase = "";
	
	function niceURL($string)
	{
		/***** Special Characters *****/
		$no_sc			=	array('?','!','¿','¡','´',"'",'.','@',':',',',';','º','&','%','"','(',')','{','}','[',']','/','\\');	
		$vali_sc		=	array('' ,'' ,'' ,'' ,'' ,"" ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'' ,'','','');

		$url		=	strtolower( str_replace( $no_sc,$vali_sc,utf8_decode($string)));

		/***** ABC *****/
		$no_abc		=	array('á','é','í','ó','ú','ñ','Á','É','Í','Ó','Ú','Ñ');
		$vali_abc	=	array('a','e','i','o','u','n','a','e','i','o','u','n');

		$url		=	strtolower( str_replace( $no_abc,$vali_abc,$url));

		/***** blank space *****/
		$no_abc		=	array(' ');
		$vali_abc	=	array('-');

		$url		=	strtolower( str_replace( $no_abc,$vali_abc,trim($url)));
		
		$url = substr($url,0,50);
		$url = preg_replace("/[^0-9a-zA-Z-_]/", "", $url);
		return $url;
	}
	public function initMDB($db){
		$this->dBase=$db;
	} 
	public function enviar_empresa($idaeditar){
		$enviado = 0;
		if(!empty($idaeditar)){
				
				$dominio=str_replace("cms/scripts/autorizar.php","", "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
				$fecha= date("d/m/Y");
				$sqluser = "SELECT * FROM kubik_empresas WHERE id_empresa=$idaeditar";
				$datouser=$this->dBase->row($sqluser);
				$email=$datouser["email"]; 
				$nombre=$datouser["nombre"];
				$nombre_contacto=$datouser["nombre_contacto"];
				$direccion=$datouser["direccion"];
				$colonia=$datouser["colonia"];
				$ciudad=$datouser["ciudad"];
				$estado=$datouser["estado"];
				$telefono=$datouser["telefono"];
				$giro=$datouser["giro"];

				$body		= file_get_contents('../forms/activacion-empresa.php');
				$body 		= str_replace( array('{dominio}', '{fecha}', '{email}','{nombre}','{nombre_contacto}','{direccion}','{colonia}','{ciudad}','{estado}','{telefono}','{giro}' ), 
					array($dominio, $fecha, $email,$nombre,$nombre_contacto,$direccion,$colonia,$ciudad,$estado,$telefono,$giro), $body );	
						
					
				include '../../inc/class.phpmailer.php';
				include '../../inc/class.smtp.php';
				include '../../inc/class.email.php';

				//Creamos Email de recuperación de contraseña
				$titulomail    = utf8_decode("REGISTRO EMPRESA");
				$from_string="GCC SITE";
				$from_domain="gcc.com.mx";
				$to_email=$email;
				$to_name =$nombre; 
				$from_email =""; //si vacio(""), entonces se utilizara el email del cliente que escribe el comentario 

				$dominio     = str_replace("socios/sections/registroempresa.php","", "http://".$_SERVER['SERVER_NAME'].$_SERVER['PHP_SELF']);
				$fecha       = date('d/m/Y');


				$from       = $from_string." <".$from_email.">";
				$subject    = $titulomail;
				//Enviamos email
				if (!empty($to_email)){
				        $params=array(
				          "cuerpo"=>$body,
				          "from"=>$from_email,
				          "fromname"=>$from_string,
				          "subject"=> $subject,
				          "to"=>$to_email,
				          "toname"=>$to_name);  
				        $mail = new Emailer($params);
				        $success=$mail->sendEmail();
				}					
					
		}
	}

	
}
?>