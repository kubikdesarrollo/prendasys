<?php
    $User->delete("Cat_tech_specs",array("itemcode"=>$_POST["itemcode"]));
    foreach($_POST["specsproduct"] as $k=>$spec){
        if (intval($spec)>0){
            $User->insert("Cat_tech_specs",array("itemcode"=>$_POST["itemcode"],"id_tech_spec"=>$spec));
        }
    }


    $User->delete("colour_product",array("itemcode"=>$_POST["itemcode"]));
    foreach($_POST["colorsproduct"] as $k=>$color){
        if (intval($color)>0){
            $User->insert("colour_product",array("itemcode"=>$_POST["itemcode"],"id_color"=>$color));
        }
    }

    $User->delete("Cat_url_marketplace",array("itemcode"=>$_POST["itemcode"]));
    foreach($_POST["marketplaces"] as $k=>$market){
        if (!empty($_POST["marketplaces"][$k])){
            $User->insert("Cat_url_marketplace",array(
                "itemcode"=>$_POST["itemcode"],
                "id_marketplace"=>$_POST["marketplacesnames"][$k],
                "id_color"=>$_POST["marketplacescolor"][$k],
                "cantidad"=>$_POST["quantity"][$k],
                "url"=>$_POST["marketplaces"][$k]
            ));
        }
    }

    
    $anteriores=$User->query("SELECT * FROM Cat_soporte WHERE itemcode=:code",array("code"=>$_POST["itemcode"]));
    foreach($_POST["fileold"] as $k=>$market){ 
        $pathfinal=@$_POST["fileold"][$k]; 
        if (is_uploaded_file($_FILES['downloads']['tmp_name'][$k])){
            $extencion 	= 	ext($_FILES['downloads']['name'][$k]);
            $extencion	=	strtolower('.'.$extencion);
            $new_filename = niceURL($_POST["itemcode"]).rand(1111,9999).time().$extencion;
            $url='../../descargables/'.$new_filename;
            $tmp_name = $_FILES['downloads']["tmp_name"][$k];
            $moved=@move_uploaded_file($tmp_name,$url); 
            if( $moved ) {} else {
                echo "Not uploaded because of error #".$_FILES['downloads']['error'][$k]; exit(0);
            }
            $pathfinal=$new_filename;
        }
        if (!empty($pathfinal)){
            $User->insert("Cat_soporte",array(
                "itemcode"=>$_POST["itemcode"],
                "id_tipoSoporte"=>@$_POST["downloadtype"][$k],
                "documento"=>$pathfinal
            ));
        }
    }
    if (count($anteriores)>0){
        foreach($anteriores as $anterior){
            $User->delete("Cat_soporte",array("id"=>$anterior["id"]));
        }
    }

    $tabsp=$User->query("SELECT * FROM tabs_productos");
    $tipotabb=array('Caracteristicas','Tech-Spec','Soporte','Comentarios','Configuracion');
    foreach($tabsp as $tab){
        if (!in_array($tab["tipo"],$tipotabb) && isset($_POST["tabextra_".$tab["id"]])){
            $existe=$User->row("SELECT * FROM tabsextra where itemcode=:code AND id_tab=:tab",array("code"=>@$_POST["itemcode"],"tab"=>$tab["id"]));
            if ($existe){
                $names=array("content"=>$_POST["tabextra_".$tab["id"]]);
                $User->update("tabsextra",$names,array("itemcode"=>$_POST["itemcode"],"id_tab"=>$tab["id"]));
            } else {
                $names=array("itemcode"=>$_POST["itemcode"],"id_tab"=>$tab["id"],"content"=>$_POST["tabextra_".$tab["id"]]);
                $User->insert("tabsextra",$names);
            }
        }
    }


?>