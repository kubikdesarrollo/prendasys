<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

$fp = fopen('../reportes/mayoristas_sucursales.csv', 'w');

$fields=$db->query("SELECT * FROM sucmayoristas");
$countries=$db->query("SELECT * FROM countries");
$states=$db->query("SELECT * FROM states");
$mayoristas=$db->query("SELECT * FROM mayoristas");

$columnas=array();
foreach($fields as $k=>$field){
    unset($field["id"]);
    if (empty($columnas)){
        foreach($field as $c=>$v){
            $columnas[]=$c;
        }
        fputcsv($fp, $columnas);
    }

    //Reemplazamos las countries
    foreach($countries as $c){
        if ($field["id_country"]==$c["numcode"]){
            $field["id_country"]=$c["name"];
        } 
    }
    //Reemplazamos las states
    foreach($states as $c){
        if ($field["id_state"]==$c["id"]){
            $field["id_state"]=$c["name"];
        } 
    }
    //Reemplazamos mayoristas
    foreach($mayoristas as $c){
        if ($field["id_mayorista"]==$c["id"]){
            $field["id_mayorista"]=$c["title"];
        } 
    }

    fputcsv($fp, $field);
}

fclose($fp);

$path = '../reportes/mayoristas_sucursales.csv';
$type = '';
 
if (is_file($path)) {
 $size = filesize($path);
 if (function_exists('mime_content_type')) {
 $type = mime_content_type($path);
 } else if (function_exists('finfo_file')) {
 $info = finfo_open(FILEINFO_MIME);
 $type = finfo_file($info, $path);
 finfo_close($info);
 }
 if ($type == '') {
 $type = "application/force-download";
 }
 // Definir headers
 header("Content-Type: $type");
 header("Content-Disposition: attachment; filename=mayoristas_sucursales.csv");
 header("Content-Transfer-Encoding: binary");
 header("Content-Length: " . $size);
 // Descargar archivo
 readfile($path);
} else {
 die("El archivo no existe.");
}