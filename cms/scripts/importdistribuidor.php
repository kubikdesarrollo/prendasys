<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

if (is_uploaded_file(@$_FILES["importfile"]['tmp_name'])){ 
    $extencion  =   ext($_FILES["importfile"]['name']);
    $blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
    foreach ($blacklist as $file)
    {if(preg_match("/$file\$/i", $_FILES["importfile"]['name'])){
      echo 'Archivo no permitido';
      exit();}
    }
    $urlf='../reportes/distribuidores.csv'; 
    $tmp_namef = $_FILES["importfile"]['tmp_name'];
    @move_uploaded_file($tmp_namef,$urlf); 

    if (($gestor = fopen("../reportes/distribuidores.csv", "r")) !== FALSE) { 
        $linea=0;
        $fields=array();
        $db->query("TRUNCATE TABLE sucdistribuidores");
        $distribuidores=$db->query("SELECT * FROM distribuidores");
        $states=$db->query("SELECT * FROM states");
        $countries=$db->query("SELECT * FROM countries");
        
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            if($datos[0]=="id_country"){
                $fields=$datos;
            } else {
                if (!empty($fields)){
                    $names=array();
                    foreach($fields as $i=>$f){
                        $names[$f]=$datos[$i];
                    }
                    $nombre_estado=trim($names["id_state"]);

                    //Reemplazamos distribuidores
                    foreach($distribuidores as $c){
                        if (strtoupper(trim($names["id_distribuidor"]))==strtoupper(trim($c["title"]))){
                            $names["id_distribuidor"]=$c["id"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_distribuidor"]=intval($names["id_distribuidor"]);

                    //Reemplazamos estados
                    foreach($states as $c){
                        if (strtoupper(trim($names["id_state"]))==strtoupper(trim($c["name"]))){
                            $names["id_state"]=$c["id"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_state"]=intval($names["id_state"]);

                    //Reemplazamos estados
                    foreach($countries as $c){
                        if (strtoupper(trim($names["id_country"]))==strtoupper(trim($c["name"]))){
                            $names["id_country"]=$c["numcode"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_country"]=intval($names["id_country"]);

                    //Limpiamos ciudad
                    $names["city"]=str_replace(", ".$nombre_estado,"",$names["city"]);

                    
                    //insertamos
                    $db->insert("sucdistribuidores",$names); 
                }
            }
        }
    }
}
header("Location: ../admin.php?seccion=sucdistribuidores&saved=1");
