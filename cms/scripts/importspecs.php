<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();

if (is_uploaded_file(@$_FILES["importfile"]['tmp_name'])){ 
    $extencion  =   ext($_FILES["importfile"]['name']);
    $blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
    foreach ($blacklist as $file)
    {if(preg_match("/$file\$/i", $_FILES["importfile"]['name'])){
      echo 'Archivo no permitido';
      exit();}
    }
    $urlf='../reportes/importspecs.csv'; 
    $tmp_namef = $_FILES["importfile"]['tmp_name'];
    @move_uploaded_file($tmp_namef,$urlf); 

    if (($gestor = fopen("../reportes/importspecs.csv", "r")) !== FALSE) { 
        $linea=0;
        $fields=array();
        $db->query("TRUNCATE TABLE Cat_tech_specs");
        $specs=$db->query("SELECT * FROM Cat_tech_specs_articulo");
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            if($datos[0]=="itemcode"){
                $fields=$datos;
            } else {
                if (!empty($fields)){
                    $names=array();
                    foreach($fields as $i=>$f){
                        $names[$f]=$datos[$i];
                    }
                    //Reemplazamos las colores y marketplaces
                    foreach($specs as $c){
                        if (trim(strtolower($names["id_tech_spec"]))==trim(strtolower($c["tech_spec"]))){
                            $names["id_tech_spec"]=$c["id"];
                        } 
                    }
                    //limpiamos posibles valores incorrectos
                    $names["id_tech_spec"]=intval($names["id_tech_spec"]);

                    
                    //insertamos
                    $db->insert("Cat_tech_specs",$names);
                }
            }
        }
    }
}
header("Location: ../admin.php?seccion=specsimport&saved=1");
