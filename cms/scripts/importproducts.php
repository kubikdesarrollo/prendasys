<?php
ini_set("error_reporting", E_ALL);
ini_set("display_errors", "On");
ini_set("display_startup_errors", "On");
session_start();
include('../../inc/config.php');
include('../libs_php/Db.frontclass.php');
include('../functions/funciones.php');
$db=new DBfront();
$simulate=!empty($_REQUEST["simulate"])?true:false;

if (is_uploaded_file(@$_FILES["importfile"]['tmp_name'])){ 
    $extencion  =   ext($_FILES["importfile"]['name']);
    $blacklist = array(".php", ".phtml", ".php3", ".php4", ".js", ".shtml", ".pl" ,".py");
    foreach ($blacklist as $file)
    {if(preg_match("/$file\$/i", $_FILES["importfile"]['name'])){
      echo 'Archivo no permitido';
      exit();}
    }
    $urlf='../reportes/importproducts.csv'; 
    $tmp_namef = $_FILES["importfile"]['tmp_name'];
    @move_uploaded_file($tmp_namef,$urlf); 

    if (($gestor = fopen("../reportes/importproducts.csv", "r")) !== FALSE) { 
        $linea=0;
        $fields=array();
        $errors=array();
        $countinserts=0;
        
        $fotosanteriores=$db->query("SELECT images_gallery_products.*,Cat_Articulos.itemcode FROM images_gallery_products LEFT JOIN Cat_Articulos ON images_gallery_products.id_parent=Cat_Articulos.id");
        if (!$simulate) $db->query("TRUNCATE TABLE Cat_Articulos");
        
        
        $specslist=$db->query("SELECT * FROM Cat_tech_specs_articulo");
        $colorlist=$db->query("SELECT * FROM colours");
        $categorias=$db->query("SELECT * FROM categories");
        $lineas=$db->query("SELECT * FROM lineas");
        $sublineas=$db->query("SELECT * FROM sublineas");
        while (($datos = fgetcsv($gestor, 1000, ",")) !== FALSE) {
            $specsinserts=array();
            $colorinserts=array();
            if($datos[0]=="itemcode"){
                $fields=$datos;
            } else {
                if (!empty($fields)){
                    $names=array();
                    foreach($fields as $i=>$f){
                        $names[$f]=$datos[$i];
                    }
                    //Reemplazamos las categorias y lineas
                    foreach($categorias as $c){
                        if (strtolower(trim($names["id_categoria"]))==strtolower(trim($c["title"])) ){
                            $names["id_categoria"]=$c["id"];
                        } 
                    }
                    if(empty(intval($names["id_categoria"])) && $names["id_categoria"]!="") $errors[]="Categoria no encontrada: ".$names["id_categoria"];

                    

                    foreach($lineas as $l){
                        if (strtolower(trim($names["id_lineaProducto"]))==strtolower(trim($l["descripcion_linea"])) ){
                            $names["id_lineaProducto"]=$l["id"];
                        } 
                    }
                    if(empty(intval($names["id_lineaProducto"])) && $names["id_lineaProducto"]!="") $errors[]="Linea no encontrada: ".$names["id_lineaProducto"];

                    foreach($sublineas as $l){
                        if (strtolower(trim($names["id_sublinea"]))==strtolower(trim($l["descripcion_linea"])) ){
                            $names["id_sublinea"]=$l["id"];
                        } 
                    }
                    if(empty(intval($names["id_sublinea"])) && intval($names["id_sublinea"])!=0 ) $errors[]="Sublinea no encontrada: ".$names["id_sublinea"];

                    

                    //limpiamos posibles valores incorrectos
                    $names["id_categoria"]=intval($names["id_categoria"]);
                    $names["id_lineaProducto"]=intval($names["id_lineaProducto"]);
                    $names["id_sublinea"]=intval($names["id_sublinea"]);
                    $names['priority']=intval($names["priority"]);
                   

                    //Reemplazamos specs
                    $specs=explode(",",$names["specs"]);
                    if (count($specs)>0){
                        foreach($specs as $specraw){
                            $spec=trim($specraw);
                            if (!empty($spec)){ 
                                $idtech=0;
                                foreach($specslist as $specitem){
                                    if (strtolower(trim($spec))==strtolower(trim($specitem["tech_spec"])) ) $idtech=$specitem["id"];
                                }
                                if ($idtech){
                                    $specsinserts[]="INSERT INTO Cat_tech_specs(itemcode,id_tech_spec) VALUES('".$names["itemcode"]."',".$idtech.")";
                                } else {
                                    $errors[]="SPECT no encontrado: ".$spec;
                                }
                            }
                        }
                    }
                    unset($names["specs"]);

                    //Reemplazamos colores
                    $colores=explode(",",$names["colores"]);
                    if (count($colores)>0){
                        foreach($colores as $colorraw){
                            $color=trim($colorraw);
                            if (!empty($color)){
                                $idcolor=0;
                                foreach($colorlist as $coloritem){
                                    if ($color==$coloritem["title"]) $idcolor=$coloritem["id"];
                                }
                                if ($idcolor){
                                    $colorinserts[]="INSERT INTO colour_product(itemcode,id_color) VALUES('".$names["itemcode"]."',".$idcolor.")";
                                } else {
                                    $errors[]="Color no encontrado: ".$color;
                                }
                            }
                        }
                    }
                    unset($names["colores"]);

                    
                    //insertamos
                    if(!$simulate) {
                        
                        $last=$db->insert("Cat_Articulos",$names);
                        //Rescribimos specs de este producto
                        $db->delete("Cat_tech_specs",array("itemcode"=>$names["itemcode"]));
                        foreach($specsinserts as $ins){
                            $db->query($ins);
                        }
                        //Rescribimos colores de este producto
                        $db->delete("colour_product",array("itemcode"=>$names["itemcode"]));
                        foreach($colorinserts as $ins){
                            $db->query($ins);
                        }
                        //Rescribimos imagenes
                        foreach($fotosanteriores as $fotoanterior){
                            if ($fotoanterior["itemcode"]==$names["itemcode"]){
                                $db->update("images_gallery_products",array("id_parent"=>$last),array("id"=>$fotoanterior["id"]));
                            }
                        }
                    }
                    $countinserts++;
                }
            }
        }
    }
}
if (!$simulate && empty($errors)) header("Location: ../admin.php?seccion=productos&saved=1");
else {
    echo 'Proceso finalizado, Insertados:'.$countinserts.", Errores: ".count($errors)."<br>";
    foreach($errors as $error){
        echo "<br>".$error;
    }
    echo '<br><a href="javascript:history.back();">Regresar</a>';
}
