<?php
function despliega($stats,$nivel,$subnivel){
    $count=0;
    foreach ($stats as $key => $value) {
        if (is_array($value)) $stats[$key]=despliega($value,$count,($subnivel+1));
        else {
        	if (isset($_POST["key".$nivel.'_'.$count.'_'.$subnivel])){
        		$stats[$key]=strip_tags($_POST["key".$nivel.'_'.$count.'_'.$subnivel]);
        	}
        } 
        $count++;   
    }
    return $stats;
}

function despliega1($stats,$nivel,$subnivel){
    $count=0; 
    $amenities_list = [];
    if(is_array(@$stats['amenities']))
        $amenities_list = $stats['amenities'];
    $stats['amenities']=array();

    $array_amenities = array(
        'rampa' => 'Acceso para personas con capacidades diferentes', 
        'taxi' => 'Sitio de Taxi',
        'escaleras' => 'Escaleras eléctricas',
        'rampacarro' => 'Rampa para carro de autoservicio',
        'cambiadoresbebes' => 'Sanitarios con cambiadores de bebés',
        'seguridad' => 'Sistema de seguridad 24 horas',
        'parabuses' => 'Parabuses',
        'lavacoches' => 'Lavacoches',
        'elevador' => 'Elevadores',
        'elevador_carga' => 'Elevador de Carga',
        'estacionamiento' => 'Estacionamiento Automatizado',
        'wiffi' => 'Zona Wiffi',
        'comida' => 'Zona de comida',
        'cajero' => 'Cajeros automaticos',
        'puertas' => 'Puertas electricas',
        'pasillo_servicio' => 'Pasillo de Servicio'
        );
    $resto=array();
    //Validamos solo las que son por default
    foreach ($array_amenities as $key => $value) { 
        if (isset($_POST["keya".$nivel.'_'.$count.'_'.$subnivel])){
            $stats['amenities'][]=$key;  
            $resto[]=$key;
        }
        $count++;   
    }

    //En seguida validamos el resto de amenidades que no son por default
    foreach ($amenities_list as $key => $value) { 
        if(!in_array($value, $resto)){
            if (isset($_POST["keya".$nivel.'_'.$count.'_'.$subnivel])){
                $stats['amenities'][]=$value;                 
            }
            $count++;  
        }
          
    }

    //Validamos el resto de parametros en amenidades
    foreach ($stats as $key => $value) { 
        if ($key!="amenities" && $key!="amenities_html"){
            if (isset($_POST["keya".$nivel.'_'.$count.'_'.$subnivel])){
                if (is_array($value)) despliega1($value,$count,($subnivel+1));
                else $stats[$key]=strip_tags($_POST["keya".$nivel.'_'.$count.'_'.$subnivel]);     
            }
            $count++; 
        }
    }

    $stats['amenities_html']=strip_tags($_POST['amenities_html']); 

    return $stats;
}

//Estadisticas
if ($_POST["modostats"]=="avanzado"){
	$_POST["statistics"]=strip_tags($_POST["statistics_avanzado"]);
} else {
	$pattern=strip_tags($_POST["patternstats"]);
	$stats=json_decode($pattern,true);
	if (!empty($stats["detalles"]))
        $stats["detalles"]=despliega($stats["detalles"],0,0);
    $_POST["statistics"]=json_encode($stats);
}
$resinsper=$User->update("properties",["statistics"=>$_POST["statistics"]],["id"=>$lastindex]);



if ($_POST["modostats1"]=="avanzado"){
    $_POST["amenities"]=strip_tags($_POST["amenities_avanzado"]);
} else { 
    $pattern=strip_tags($_POST["patternamenities"]); 
    $amenidades=json_decode($pattern,true); 
    if (!empty($amenidades))
        $amenidades=despliega1($amenidades,0,0);
    $_POST["amenities"]=json_encode($amenidades);
} 
$resinsper=$User->update("properties",["amenities"=>$_POST["amenities"]],["id"=>$lastindex]);


?>
