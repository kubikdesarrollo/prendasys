<?php
/****************** CONFIGURACION DE VARIABLES *********************/
require_once('../config/configuracion_guarda.php');
require_once('../libs_php/recortar_imagen.php');

/****************** GUARDA ADMINITRADORES********************/

//echo '<pre>'. print_r($_POST, 1) .'</pre>';
//echo '<pre>'. print_r($_FILES, 1) .'</pre>';

if($tabla == "items" ) { 
	if ( (isset($_POST['title']) && $_POST['title']!='') || (isset($_FILES['imagen_image']) && $_FILES['imagen_image'])  || (isset($_FILES['imagen_url']) && $_FILES['imagen_url']) || (isset($_FILES['imagen_details_en']) && $_FILES['imagen_details_en']) ){
		$input_url = '';
		$input_url2 = '';
		switch ($_POST["type"]) {
			case 'categories-home': $input_url = 'home/square-banners'; $input_url2 = '2'; break;
			case 'home-banners-duo': $input_url = 'home/duo-banners'; $input_url2 = '2'; break;
			case 'home-baner-tall-3': $input_url = 'home/banners-estilo'; $input_url2 = '2'; break;
			case 'campain': $input_url = 'campana'; break;
			case 'campain-mobil':  $input_url = 'campana'; break;
			case 'world-cuadra': $input_url = 'mundo-cuadra'; break;
			case 'collection': $input_url = 'coleccion'; break;
			case 'contact': $input_url = 'contacto'; break;
			case 'home-baner-cta': $input_url = 'home/banners-cta'; $input_url2 = '2'; break;
			case 'celebrities-headers':  $input_url = 'personalidades'; break;
			case 'press-headers':  $input_url = 'prensa'; break;
			case 'news-headers':  $input_url = 'noticias'; break;
			case 'editorial-headers':  $input_url = 'editorial'; break;
			case 'videos-headers':  $input_url = 'videos'; break;
			case 'museum-headers':  $input_url = 'museo'; break;
			case 'commitments-headers':  $input_url = 'compromisos'; break;
		}

		if (isset($_FILES['imagen_image'])){
			
			$ruta_imags="../../images". $input_url2 ."/".$input_url."/";
			$new_filename  = niceURL(@$_POST['type'])."_". time();

			procesaimagen('imagen_image', 'image', $new_filename,$ruta_imags,2000,2000,false,false,false,80,80);
			
		}

		if (isset($_FILES['imagen_url'])){

			$ruta_imags="../../images". $input_url2 ."/".$input_url."/";
			$new_filename  = niceURL(@$_POST['type'])."_". time();

			procesaimagen('imagen_url', 'url', $new_filename,$ruta_imags,2000,2000,false,false,false,80,80);
			
		}

		if (isset($_FILES['imagen_details_en'])){

			$ruta_imags="../../images". $input_url2 ."/".$input_url."/";
			$new_filename  = niceURL(@$_POST['type'])."_". time();

			procesaimagen('imagen_details_en', 'details_en', $new_filename,$ruta_imags,2000,2000,false,false,false,80,80);
			
		}

		$_POST['published'] = ($_POST['published']=='on'?1:0);

		$User->manipulaDatos($tabla,$indice_t,$_POST,(isset($_POST['editar']))?$editar=$indice:$editar=0,'',@$debug);

	}

	if ($editar<1)
		$lastidinsert=$User->lastid;
	else
		$lastidinsert=$editar;
	
}

header("Location: ../admin.php?seccion=". $_POST['seccion'] ."&accion=editar&id_edit=".$lastidinsert."&saved=1");
?>
