<?php 
    include 'config/configuracion_global.php';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">

    <title><?php echo NAME_WEBSITE ?></title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="css/owl.carousel.css" type="text/css">
    <link rel="stylesheet" href="assets/data-tables/DT_bootstrap.css" />
    <link href="assets/fancybox/source/jquery.fancybox.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
    <link rel="stylesheet" href="css/jquery.minicolors.css">
    <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
    <link rel="stylesheet" type="text/css" href="css/gallery.css" />
    <link rel="stylesheet" type="text/css" href="assets/gritter/css/jquery.gritter.css" />
    <link rel="stylesheet" type="text/css" href="css/bootstrap-datetimepicker.min.css">
    <link href="assets/dropzone/css/dropzone.css" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="assets/dropzone/css/basic.css">
    <link rel="stylesheet" type="text/css" href="assets/nestable/jquery.nestable.css" />
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <link href="css/style-responsive.css" rel="stylesheet" />
    <link href="css/tagmanager.css" rel="stylesheet" />
    <link href="css/panelmain.css" rel="stylesheet" />

    <!-- Editor jquery -->
    <link type="text/css" rel="stylesheet" href="css/jquery-te.min.css" charset="utf-8" >
    

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
  <script src="js/jquery-1.12.4.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/jquery.scrollTo.min.js"></script>
  <script src="js/jquery.nicescroll.js" type="text/javascript"></script>
  <script src="js/jquery-ui-1.9.2.custom.min.js"></script>
  <script src="js/jquery.sparkline.js" type="text/javascript"></script>
  <script src="assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>

  <script src="js/owl.carousel.js" ></script>
  <script src="js/jquery.customSelect.min.js" ></script>
  <script type="text/javascript" src="assets/data-tables/jquery.dataTables.js"></script>
  <script type="text/javascript" src="assets/data-tables/DT_bootstrap.js"></script>
  <script type="text/javascript" src="assets/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/date.js"></script>
  <script type="text/javascript" src="assets/bootstrap-daterangepicker/daterangepicker.js"></script>
  <script src="js/jquery.minicolors.min.js"></script>
  
  <script type="text/javascript" src="js/ga.js"></script>
  <script type="text/javascript" src="assets/gritter/js/jquery.gritter.js"></script>
  <script src="js/respond.min.js" ></script>
  <script src="assets/fancybox/source/jquery.fancybox.js"></script>
  <script type="text/javascript" src="ckeditor/ckeditor.js"></script>
  <script type="text/javascript" src="js/jquery.validate.min.js"></script>
  <script type="text/javascript" src="js/jquery.dataTables.rowReordering.js"></script>

  <script language="javascript" src="js/bootstrap-datetimepicker.js"></script>
  <script src="assets/dropzone/dropzone.js"></script>
  <script src="assets/nestable/jquery.nestable.js"></script>
   <script type="text/javascript" src="assets/bootstrap-inputmask/bootstrap-inputmask.min.js"></script>
   <script type="text/javascript" src="js/jquery-te.min.js" charset="utf-8"></script>


  <section id="container" class="">
      <!--header start-->
      <?php include("header.php") ?>
      <!--header end-->
      <!--sidebar start-->
      <?php include("menu.php") ?>
      <!--sidebar end-->
      <!--main content start-->
      <section id="main-content">
          
        <?php  require_once "functions/navegacion.php";?>
          
      </section>
      <!--main content end-->
  </section>
    
  <?php include("footer.php") ?> 
    


    <!--common script for all pages-->
    <script src="js/common-scripts.js"></script>
    <script src="js/typeahead.min.js"></script>
    <script src="js/hogan-2.0.0.js"></script>
    <script src="js/bootstrap-switch.js"></script>
    <script src="js/panelmain.js"></script>
    <script src="js/tagmanager.js"></script>

    <?php include("js/function.js.php") ?>



 

  </body>
</html>
