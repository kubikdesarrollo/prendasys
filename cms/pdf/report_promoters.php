<?php
require('init_pdf.php');

	include '../../inc/config.php'; 
	$res=mysql_query(base64_decode($_REQUEST['queryfull']));

	function fecha($dato){
		return substr($dato,5,2)."-".substr($dato,8,2)."-".substr($dato,2,2);
	}
	function td($campo,$pdf,$zebra,$lenght=3){
		
		if ($campo=='na'){ 
			$pdf->SetFillColor(255,153,0);
			$pdf->SetTextColor(252,248,182);
			$pdf->Cell($lenght,3,$campo,1,0,'C',true);
		}
		if ($campo=='yes') {
			$pdf->SetFillColor(92,184,92);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell($lenght,3,$campo,1,0,'C',true);
		}
		if ($campo=='no') {
			$pdf->SetFillColor(217,83,79);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell($lenght,3,$campo,1,0,'C',true);
		}
		if (empty($campo)) {
			if ($zebra%2==0) $pdf->SetFillColor(241,241,241); else $pdf->SetFillColor(228,228,228);
			$pdf->Cell($lenght,3,$campo,1,0,'C',true);
		}
	}


$pdf = new PDF('P');
$pdf->AddPage();


$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(250,250,250);
$pdf->SetTextColor(128,134,147);
$pdf->SetFont('helvetica','B',10);
$pdf->Cell(40,10,'',0,0,'L',false);
$pdf->Image('images/logo.jpeg',5,$pdf->GetY(),50);
$pdf->Cell(145,10,'PROMOTERS REPORT',0,0,'R',false);
$pdf->Ln();
$pdf->Ln();

$pdf->SetDrawColor(255,255,255);
$pdf->SetFillColor(0,0,0);
$pdf->SetTextColor(255,255,255);
$pdf->SetFont('helvetica','B',6);

$pdf->Cell(20,10,'FIRST NAME',1,0,'C',true);
$pdf->Cell(25,10,'LAST NAME',1,0,'C',true);
$pdf->Cell(55,10,'COMPANY',1,0,'C',true);
$pdf->Cell(40,10,'EMAIL',1,0,'C',true);
$pdf->Cell(25,10,'PHONE',1,0,'C',true);
$pdf->Cell(15,10,'T. SOLD',1,0,'C',true);
$pdf->Cell(10,10,'PRINTED',1,0,'C',true);

$zebra=0;
while($data=mysql_fetch_assoc($res)){
	$pdf->SetFont('helvetica','',6);
	foreach ($data as $key => $value) {
		$data[$key]=utf8_decode($data[$key]);
	}
	$pdf->Ln();
	if ($zebra%2==0) $pdf->SetFillColor(241,241,241); else $pdf->SetFillColor(228,228,228);
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(20,6,$data["promoter_first_name"],1,0,'L',true);
	$pdf->Cell(25,6,$data["promoter_last_name"],1,0,'L',true);
	$pdf->Cell(55,6,substr($data["promoter_company"],0,60),1,0,'C',true);
	$pdf->Cell(40,6,$data["promoter_email"],1,0,'C',true);
	$pdf->Cell(25,6,$data["promoter_phone"],1,0,'C',true);
	$pdf->Cell(15,6,number_format(intval($data["sold"]),0,'.',','),1,0,'C',true);
	$pdf->Cell(10,6,number_format(intval($data["printed"]),0,'.',','),1,0,'C',true);
   $zebra++;


	if ($pdf->GetY() > 260){
		$pdf->Ln();
		$pdf->Ln();
    	$pdf->SetDrawColor(255,255,255);
		$pdf->SetFillColor(0,0,0);
		$pdf->SetTextColor(255,255,255);
		$pdf->SetFont('helvetica','B',6);
    	$pdf->Cell(20,10,'FIRST NAME',1,0,'C',true);
		$pdf->Cell(25,10,'LAST NAME',1,0,'C',true);
		$pdf->Cell(55,10,'COMPANY',1,0,'C',true);
		$pdf->Cell(40,10,'EMAIL',1,0,'C',true);
		$pdf->Cell(25,10,'PHONE',1,0,'C',true);
		$pdf->Cell(15,10,'T. SOLD',1,0,'C',true);
		$pdf->Cell(10,10,'PRINTED',1,0,'C',true);
	}
}

 	 	 	 	 	

$pdf->Output();

?>