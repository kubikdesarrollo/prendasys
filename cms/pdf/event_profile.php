<?php
	require('class.pdfoffice.php');

	include '../../inc/config.php'; 
	
	$pdf = new pdfoffice('P');
	$pdf->AddPage();
	$pdf->inicializa();
	

	//$pdf->panel();
	
	$id_edit=(isset($_REQUEST['id']))?intval($_REQUEST["id"]):0;
	$sql="SELECT * FROM events_live LEFT JOIN event_profile ON event_profile.event_number=events_live.event_id  LEFT JOIN promoters ON event_profile.event_promoter_id=promoters.id_promotor WHERE events_live.id= ".$id_edit;

	$res=mysql_query($sql);
	$datos=mysql_fetch_assoc($res);

	$dateTime = new DateTime($datos["event_date"]);	 
	$dateevent =$dateTime->format('m/d/Y');
	$showtimequery=mysql_query("SELECT * FROM sku WHERE id_event='".@$datos["event_id"]."'");
	if ($datashowtime=@mysql_fetch_assoc($showtimequery)){
		$dateevent.= " - ".$datashowtime["showtime"];
	}


	
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFillColor(255,255,255);

	$pdf->Cell(50,10,'',0,0,'L',false);
	$pdf->Image('images/logo.jpeg',5,$pdf->GetY(),50);
	$pdf->SetFont('helvetica','',15);
	$pdf->SetTextColor(1,70,127);
	$pdf->Cell(140,5,'EVENT REPORT',0,0,'R',true);
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Ln();
	$pdf->SetTextColor(248,153,28);
	$pdf->Cell(100,5,'EVENT INFO',0,0,'L',true);
	$pdf->Cell(90,5,'VENUE INFO',0,0,'L',true);
	$pdf->SetFont('helvetica','',10);
	$pdf->Ln();	
	$pdf->Ln();
	$pdf->SetTextColor(1,70,127);	
	$pdf->SetFont('helvetica','B',10);
 	$pdf->Cell(100,5,$datos["title"],0,0,'L',true);	 
 	$pdf->Cell(90,5,$datos["venue_name"],0,0,'L',true);	 
 	$pdf->Ln();	
 	$pdf->SetFont('helvetica','',10); 	 
 	$pdf->SetTextColor(128,134,147);	
 	$pdf->Cell(100,5,$dateevent,0,0,'L',true);	
 	$pdf->Cell(90,5,"Address : ".$datos["venue_add"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"Event Type : ".$datos["event_type"],0,0,'L',true);	
 	$pdf->Cell(90,5,"City : ".$datos["venue_city"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"Presented by : ".$datos["presentedby"],0,0,'L',true);	
 	$pdf->Cell(90,5,"State : ".$datos["venue_state"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"Event Number : ".$datos["event_id"],0,0,'L',true);	
 	$pdf->Cell(90,5,"Zip Code : ".$datos["venue_zip"],0,0,'L',true);	
 	$pdf->Ln();	
 	$pdf->Cell(100,5,"",0,0,'L',true);	
 	$pdf->Cell(90,5,"Contact: ".$datos["venue_contact"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(90,5,"Website : ".$datos["venue_website"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(90,5,"Email : ".$datos["venue_email"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(90,5,"Phone : ".$datos["venue_phone"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Ln();

 	$pdf->SetFont('helvetica','',15);
	
	$pdf->SetTextColor(248,153,28);
 	$pdf->Cell(100,5,'PROMOTER',0,0,'L',true);
 	$pdf->Cell(90,5,'OPERATION',0,0,'L',true);
 	$pdf->Ln();
 	$pdf->SetTextColor(128,134,147);
	$pdf->SetFont('helvetica','B',10);	 
 	$pdf->Cell(100,5,$datos["promoter_first_name"]." ".$datos["promoter_last_name"],0,0,'L',true);
 	$pdf->SetFont('helvetica','',10);
 	$pdf->SetTextColor(128,134,147);
 	$pdf->Cell(90,5,"Will call list sent : ".$datos["event_will_call"],0,0,'L',true);	 
 	$pdf->Ln();	 	 	
 	$pdf->Cell(100,5,$datos["promoter_company"],0,0,'L',true);	
 	$pdf->Cell(90,5,"Scanners : ".$datos["event_scanners"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,$datos["promoter_email"],0,0,'L',true);
 	$pdf->Cell(90,5,"Activation : ".$datos["event_activation"],0,0,'L',true);		
 	$pdf->Ln();
 	$pdf->Cell(100,5,$datos["promoter_phone"],0,0,'L',true);	
 	$pdf->Cell(90,5,"Outlet Ticket Distribution :  ".$datos["event_outlet"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(90,5,"Outlet Tickets Sold : ".$datos["event_oulet_sold"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Ln();

 	$pdf->SetFont('helvetica','',15);
	$pdf->SetTextColor(248,153,28);
 	$pdf->Cell(100,5,'ACCOUNTING',0,0,'L',true);
 	$pdf->Cell(90,5,'EVENT STATS',0,0,'L',true);
 	$pdf->Ln();
 	$pdf->SetTextColor(128,134,147);
	$pdf->SetFont('helvetica','',10);		 
 	$pdf->Cell(100,5,'Payment Method : '.strtoupper($datos["event_payment_method"]),0,0,'L',true);	 
 	$pdf->Cell(60,5,'Total Tickets Sold : ',0,0,'L',true);
 	$pdf->Cell(30,5,strtoupper($datos["event_tickets_sold"]),0,0,'L',true);
 	$pdf->Ln();	 	 	
 	$pdf->Cell(100,5,'Payment Status : '.strtoupper($datos["event_payment_status"]),0,0,'L',true);
 	$pdf->Cell(60,5,'Total Printed Tickets : ',0,0,'L',true);	
 	$pdf->Cell(30,5,strtoupper($datos["event_printed_tickets"]),0,0,'L',true);
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(60,5,"Total Printed at Home : ",0,0,'L',true);	
 	$pdf->Cell(30,5,$datos["event_printed_home"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(60,5,"Total Will Call : ",0,0,'L',true);	
 	$pdf->Cell(30,5,$datos["event_wildcall_stats"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(60,5,"Total Shipped Tickets : ",0,0,'L',true);	
 	$pdf->Cell(30,5,$datos["event_shipped"],0,0,'L',true);
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(60,5,"Event Attendance : ",0,0,'L',true);	
 	$pdf->Cell(30,5,$datos["event_attendance"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(100,5,"",0,0,'L',true);
 	$pdf->Cell(60,5,"ONLINE SALES %: ",0,0,'L',true);	
 	$pdf->Cell(30,5,$datos["event_percentage"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Ln();
	$pdf->SetFont('helvetica','',15);
	$pdf->SetTextColor(248,153,28);
 	$pdf->Cell(185,5,'MARKETING',0,0,'L',true);
 	$pdf->Ln();
 	$pdf->SetTextColor(1,70,127);
	$pdf->SetFont('helvetica','B',10);	
	$pdf->Cell(100,5,"PHASE 1",0,0,'L',true);
	$pdf->Ln();
 	bloqueradio("PRINT TICKETS","phase1_print",@$datos,$pdf);
    bloqueradio("SHORT LINK ","phase1_short",@$datos,$pdf);
    bloqueradio("SEND JINGLE ","phase1_send_jingle",@$datos,$pdf);
    bloqueradio("SEND LOGO ","phase1_send_logo",@$datos,$pdf);
    bloqueradio("FLYERS W/ LOGO ","phase1_flyers",@$datos,$pdf);
    bloqueradio("POSTERS W/ LOGO ","phase1_posters",@$datos,$pdf);
    bloqueradio("DATA GATHERING ","phase1_data_gathering",@$datos,$pdf);
    $pdf->SetTextColor(1,70,127);
    $pdf->SetFont('helvetica','B',10);	
	$pdf->Ln();
	$pdf->Cell(100,5,"PHASE 2",0,0,'L',true);
	$pdf->Ln();
    bloqueradio("COMP TIX","phase2_comp_tix",@$datos,$pdf);
    bloqueradio("PROMO PAGE","phase2_promo_page",@$datos,$pdf);
    bloqueradio("EVENT IN HOME","phase2_in_home",@$datos,$pdf);
    bloqueradio("CRAIGLIST","phase2_craiglist",@$datos,$pdf);
    bloqueradio("POST IN S.M.","phase2_post_insm",@$datos,$pdf);
    bloqueradio("RADIO SPOT MENTION","phase2_radiospot",@$datos,$pdf);
    bloqueradio("TV SPOT MENTION	","phase2_tvspot",@$datos,$pdf);
    bloqueradio("GIVE-AWAY MENTION","phase2_giveaway",@$datos,$pdf);
    bloqueradio("RADIO EBLAST","phase2_radioeblast",@$datos,$pdf);
    bloqueradio("CONTEST IN S.M.","phase2_contest_insm",@$datos,$pdf);
    bloqueradio("EVENT IN EBLAST	","phase2_event_ineblast",@$datos,$pdf);
    bloqueradio("SPECIFIC EBLAST","phase2_specific_eblast",@$datos,$pdf);
    bloqueradio("TEXT BLAST","phase2_textblast",@$datos,$pdf);
    bloqueradio("ONLINE MEDIA ","phase2_onlinemedia",@$datos,$pdf);
    bloqueradio("ARTIST PAGE","phase2_artistpage",@$datos,$pdf);                       
    bloqueradio("ARTIST FACEBOOK","phase2_artistfacebook",@$datos,$pdf);
    bloqueradio("ARTIST TWITTER","phase2_artisttwitter",@$datos,$pdf);
    bloqueradio("PROMOTER WEBSITE","phase2_promoterwebsite",@$datos,$pdf);
    bloqueradio("PROMOTER FACEBOOK","phase2_promoterfacebook",@$datos,$pdf);
    bloqueradio("VENUE WEBSITE","phase2_venuewebsite",@$datos,$pdf);
    bloqueradio("VENUE FACEBOOK","phase2_venuefacebook",@$datos,$pdf);
    $pdf->SetTextColor(1,70,127);
    $pdf->SetFont('helvetica','B',10);	
	$pdf->Ln();
	$pdf->Cell(100,5,"PHASE 3",0,0,'L',true);
	$pdf->Ln();
    bloqueradio("PROMO LOCAL MEDIA ","phase3_promolocal",@$datos,$pdf);
    bloqueradio("ADS IN S.M.","phase3_ads_insm",@$datos,$pdf);
    bloqueradio("ADWORDS","phase3_adwords",@$datos,$pdf);

    function bloqueradio($titulo,$campo,$datos,$pdf){
    	$pdf->SetDrawColor(255,255,255);
    	if (empty($datos[$campo])) {
			$pdf->SetFillColor(241,241,241);
			$pdf->Cell(10,5,"",1,0,'C',true);
		}
    	else if ($datos[$campo]=='na'){ 
			$pdf->SetFillColor(255,153,0);
			$pdf->SetTextColor(252,248,182);
			$pdf->Cell(10,5,strtoupper($datos[$campo]),1,0,'C',true);
		}
		else if ($datos[$campo]=='yes') {
			$pdf->SetFillColor(92,184,92);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell(10,5,strtoupper($datos[$campo]),1,0,'C',true);
		}
		else if ($datos[$campo]=='no') {
			$pdf->SetFillColor(217,83,79);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell(10,5,strtoupper($datos[$campo]),0,0,'C',true);
		}
		$pdf->SetTextColor(128,134,147);
		$pdf->SetFont('helvetica','',10);
		$pdf->SetDrawColor(0,0,0);
		$pdf->SetFillColor(255,255,255);	
    	$pdf->Cell(90,5,"  ".$titulo,0,0,'L',true);	
 		if ($pdf->GetX() > 150){
 			$pdf->Ln();
 		}
    }




	$pdf->Output();

?>