<?php
	require('class.pdfoffice.php');

	include '../../inc/config.php'; 
	
	$pdf = new pdfoffice('P');
	$pdf->AddPage();
	$pdf->inicializa();
	

	//$pdf->panel();
	
	$id_edit=(isset($_REQUEST['id']))?intval($_REQUEST["id"]):0;
	$sql="SELECT * FROM events_live LEFT JOIN venues  ON events_live.venue_name=venues.venues_venue_name WHERE events_live.id = ".$id_edit;

	$res=mysql_query($sql);
	$datos=mysql_fetch_assoc($res);


	
	$pdf->SetDrawColor(0,0,0);
	$pdf->SetFillColor(250,250,250);
	$pdf->SetTextColor(128,134,147);
	$pdf->SetFont('helvetica','B',10);

	$pdf->Cell(40,10,'',0,0,'L',false);
	$pdf->Image('images/logo.jpeg',5,$pdf->GetY(),50);
	$pdf->Cell(145,10,'VENUE REPORT',0,0,'R',false);
	$pdf->Ln();
	$pdf->Ln();
 	$pdf->Cell(185,5,$datos["venue_name"],0,0,'L',true);	 
 	$pdf->Ln();	 	 	
 	$pdf->Cell(185,5,$datos["venue_add"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(185,5,$datos["venue_city"]." ".$datos["venue_state"]." ".$datos["venue_zip"],0,0,'L',true);	
 	$pdf->Ln();
 	if (!empty($datos["venue_capacity"])){
	 	$pdf->Cell(185,5,"Capacity: ".$datos["venue_capacity"],0,0,'L',true);	
	 	$pdf->Ln();
	}
 	$pdf->Cell(185,5,$datos["venue_contact"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(185,5,$datos["venue_website"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(185,5,$datos["venue_email"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Cell(185,5,$datos["venue_phone"],0,0,'L',true);	
 	$pdf->Ln();
 	$pdf->Ln();
 	$pdf->SetFont('helvetica','B',6);
 	$pdf->Cell(10,5,'DATE',0,0,'C',true);
 	$pdf->Cell(80,5,'EVENT NAME',0,0,'C',true);
 	$pdf->Cell(40,5,'PROMOTER',0,0,'C',true);
 	$pdf->Cell(18,5,'SOLD',0,0,'C',true);
 	$pdf->Cell(18,5,'PRINTED',0,0,'C',true);
 	$pdf->Cell(18,5,'ONLINE SALES',0,0,'C',true);

 	$sql="SELECT * from events_live LEFT join event_profile on events_live.event_id=event_profile.event_number LEFT JOIN promoters ON event_profile.event_promoter_id=promoters.id_promotor where events_live.venue_name='".mysql_real_escape_string($datos["venue_name"])."' ORDER by events_live.event_date DESC";
    $sql=(isset($_REQUEST["querypdf"]))?base64_decode($_REQUEST["querypdf"]):$sql;
    $res=mysql_query($sql);
    
    $tickets_sold=0;
    $sumevent_attendance=0;
    $totalprinded=0;
    $count=0;
    $sumaverage=0;
    $eventspromoter=array();
    while ($eventos=@mysql_fetch_assoc($res)){
    	foreach ($eventos as $key => $value) {
    		$eventos[$key]=utf8_decode($value);
    	}
 		$pdf->Ln();
 		$pdf->Cell(10,5,$pdf->fecha($eventos["event_date"]),0,0,'C',false);
	 	$pdf->Cell(80,5,substr($eventos["title"],0,60) ,0,0,'C',false);
	 	$pdf->Cell(40,5, $eventos["promoter_first_name"] ." ".$eventos["promoter_last_name"],0,0,'C',false);
	 	$pdf->Cell(18,5,number_format(intval($eventos["event_tickets_sold"]),0,'.',','),0,0,'C',false);
	 	$pdf->Cell(18,5,number_format(intval($eventos["event_printed_tickets"]),0,'.',','),0,0,'C',false);
	 	$pdf->Cell(18,5,(($eventos["event_attendance"]>0)?(number_format(((@$eventos["event_tickets_sold"]*100)/$eventos["event_attendance"]),2,'.',',')." %"):""),0,0,'C',false);
	 	
	 	$tickets_sold+=$eventos["event_tickets_sold"];
        $sumevent_attendance+=$eventos["event_attendance"];
        $totalprinded+=$eventos["event_printed_tickets"];
        $eventspromoter[]=array('id' => $eventos["id"], "sold"=>$eventos["event_tickets_sold"], "label"=> addslashes($eventos["title"]));
        $sumaverage+=(!empty($eventos["event_attendance"]))?$eventos["event_tickets_sold"]:0;
        $count++;
	 }
	$pdf->Ln();
	$pdf->SetFillColor(230,230,230);
 	$pdf->Cell(185,5,"TOTAL EVENTS: $count",0,0,'L',true);
 	$pdf->SetFillColor(250,250,250);
	$pdf->Ln();
	$pdf->Ln();
	$pdf->Image('images/promotersstats.jpg',5,$pdf->GetY(),190);
	$pdf->Ln();
	$pdf->Cell(54,4,number_format($tickets_sold,0,'.',','),0,0,'C',false);
 	$pdf->Cell(42,4,number_format($totalprinded,0,'.',','),0,0,'C',false);
 	$pdf->Cell(52,4,number_format($sumevent_attendance,0,'.',','),0,0,'C',false);
 	$pdf->Cell(44,4,((!empty($sumevent_attendance))?(number_format(((@$sumaverage*100)/@$sumevent_attendance),2,'.',',')." %"):""),0,0,'C',false);
 	$pdf->Ln();
 	$pdf->Cell(54,4,'TOTAL TICKETS SOLD',0,0,'C',false);
 	$pdf->Cell(42,4,'TOTAL PRINTED TICKETS',0,0,'C',false);
 	$pdf->Cell(52,4,'EVENT ATTENDANCE',0,0,'C',false);
 	$pdf->Cell(44,4,'ONLINE SALES %',0,0,'C',false);


	 


	$pdf->Output();

?>