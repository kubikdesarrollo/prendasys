<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Email</title>
</head>

<body style="background:font-family:'Trebuchet MS', Arial, Helvetica, sans-serif; color:#666">
    <div style="width:700px; margin:0 auto; ">
    	<img src="{dominio}/images/logo.png" width="322" alt="" style="float:left" />
        <h2 style="color:#9CDE46; padding-top:60px;">Su empresa fu&eacute; autorizada</h2>
        <div style="clear:both"></div>
        <center><hr /></center><br />
       	<center>
            <table width="80%" cellspacing="10">
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">FECHA:</td><td style="width:480px;">{fecha}</td></tr>
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">NOMBRE:</td><td style="width:480px;">{nombre} </td></tr>
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">TEL&Eacute;FONO:</td><td style="width:480px;">{telefono}</td></tr>
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">Direcci&oacute;n:</td><td style="width:480px;">{direccion}, {colonia}, {ciudad}, {estado}</td></tr>
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">CORREO ELECTR&Oacute;NICO:</td><td style="width:480px;">{email}</td></tr>
                <tr><td style="width:200px; color:#9CDE46; font-weight:bold; text-align:right;">Giro:</td><td style="width:480px;">{giro}</td></tr>
            </table>
        </center>
   	  <center><hr /></center><br />
        <p style="font-size:11px; text-align:center;"><a href="{dominio}" target="_blank" style="color:#666; text-decoration:none">{dominio}</a> &copy; <?=date('Y')?></p>
    </div>
</body>
</html>