$(function() {
	$.tools.validator.localize("es", {
		'*'					: 	'Este campo no puede estar vacio.',
		'[required]'		:	'Este campo no puede estar vacio.',
		'select[required]' 	: 	'Seleccione una opci�n'
	});		
	/*
	$.tools.validator.fn("[url]", function(el, value) {
		return /^(https?|ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)*(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(\#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(value) ? true : "Direccion web no valida";
	});
*/
	$.tools.dateinput.localize("es", {
		months		: 	'Enero,Febrero,Marzo,Abril,Mayo,Junio,Julio,Agosto,Septiembre,Octubre,Noviembre,Diciembre',
		shortMonths	:	'Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Sep,Oct,Nov,Dic',
		days		:	'Domingo,Lunes,Martes,Miercoles,Jueves,Viernes,Sabado',
		shortDays	:	'Dom,Lun,Mar,Mie,Jue,Vie,Sab'
	});
	$(".contentWrap").html($('<div id="mensajes_sistema"><p class=\"advertencia\"><img src="images/loading2.gif"></p></div>'))

	var over 	= 	$("#overlay").overlay({
				
						// some mask tweaks suitable for modal dialogs
						mask: {
							color: '#000',
							loadSpeed: 200,
							opacity: 0.6
						},
						top	:	'center',
						closeOnClick: false
					});
	
	/* Validacion*/
	$("#login_form").validator({
		
		lang		: 	'es',
		position	: 	'top left', 
		offset		: 	[-10, 100], 
		message		: 	'<div><em/></div>',
		messageAttr	:  	'data-invalid'
		
		}).submit(function(e) {
			
			$(".loading").slideDown();

			var form = $(this);
			if (!e.isDefaultPrevented()) {
				$(".contentWrap").html(
					$('<div id="mensajes_sistema"><p class=\"advertencia\"><img src="images/loading2.gif"></p></div>')
				)
				$("#overlay").overlay().load(); 
				$("#login_form").queue(function() {
					$.ajax({
						type: 'POST',
						dataType: 'json',
						url: 'functions/login.php',
						data: form.serialize(),
						success: function(response){
							
							$(".contentWrap").empty();
							
							if (response.aceptar == 'si') {
								location.href='admin.php';
							} else if (response.aceptar == 'no') {
								$("#login_alert").slideDown();
								$(".loading").slideUp();
							}
						},
						error:function (xhr, ajaxOptions, thrownError){
							alert(xhr.status);
							alert(thrownError);
							alert('error');
						}   
					});
					
					// prevent default form submission logic
					e.preventDefault();
					$(this).dequeue();
				});
			}
	});
	
});