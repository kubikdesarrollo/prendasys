/* Code based on Google Map APIv3 Tutorials */


var gmapdata;
var gmapmarker;
var infoWindow;

var def_zoomval = 9;
var def_longval = -102.1777017;
var def_latval = 24.3770397; 	

function if_gmap_init()
{ 
	if (document.getElementById("longitude").value.length>0)
		def_longval = document.getElementById("longitude").value;
		
	if (document.getElementById("latitude").value.length>0)
		def_latval = document.getElementById("latitude").value;

	if(def_latval || def_longval)
		def_zoomval = 9;
	
	var curpoint = new google.maps.LatLng(def_latval,def_longval); 

	gmapdata = new google.maps.Map(document.getElementById("mapitems"), {
		center: curpoint,
		zoom: def_zoomval,
		mapTypeId: 'roadmap'
		});

	gmapmarker = new google.maps.Marker({
					map: gmapdata,
					position: curpoint
				});

	infoWindow = new google.maps.InfoWindow;
	google.maps.event.addListener(gmapdata, 'click', function(event) {
		document.getElementById("longitude").value = event.latLng.lng().toFixed(6);
		document.getElementById("latitude").value = event.latLng.lat().toFixed(6);
		gmapmarker.setPosition(event.latLng);
		if_gmap_updateInfoWindow();
	});

	google.maps.event.addListener(gmapmarker, 'click', function() {
		if_gmap_updateInfoWindow();
		infoWindow.open(gmapdata, gmapmarker);
	});

	//document.getElementById("longitude").value = def_longval;
	//document.getElementById("latitude").value = def_latval;

	return false;
} // end of if_gmap_init


function if_gmap_loadpicker()
{
	var longval = document.getElementById("longitude").value;
	var latval = document.getElementById("latitude").value;

	if (longval.length > 0) {
		if (isNaN(parseFloat(longval)) == true) {
			longval = def_longval;
		} // end of if
	} else {
		longval = def_longval;
	} // end of if

	if (latval.length > 0) {
		if (isNaN(parseFloat(latval)) == true) {
			latval = def_latval;
		} // end of if
	} else {
		latval = def_latval;
	} // end of if

	var curpoint = new google.maps.LatLng(latval,longval); 

	gmapmarker.setPosition(curpoint);
	gmapdata.setCenter(curpoint);
	//gmapdata.setZoom(zoomval);

	if_gmap_updateInfoWindow();
	return false;
} // end of if_gmap_loadpicker



function if_gmap_updateInfoWindow()
{
	infoWindow.setContent("Longitud: "+ gmapmarker.getPosition().lng().toFixed(6)+"<br>"+"Latitud: "+ gmapmarker.getPosition().lat().toFixed(6));
} // end of if_gmap_bindInfoWindow

if_gmap_init();