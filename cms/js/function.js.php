<?php if (!empty($listar)){
?>          
      
      <script src="js/editable-table.js"></script>

      <!-- END JAVASCRIPTS -->
      <script>
          jQuery(document).ready(function() {
            
              <?php 
                  if (!empty($esmod_std)){
                    if (!empty($ModuloObj->customorder) && !empty($ModuloObj->is_sortable) ){
                      echo "EditableTable.init(1);";
                    } else
                      echo "EditableTable.init(0);";
                  } else
                      echo "EditableTable.init(0);";
              ?>
          });
      </script>
<?php   
      }
      if ($modo=="editar" || $modo=="nuevo"){
 ?>
      <script>
          $(function() {
              <?php if (!empty($_REQUEST["saved"])){
                echo '$("#modalAlertbody").html("Information saved successfully");';
                echo "$('#modalAlert').modal('show');";
              } ?>
          });
      </script>
 <?php
      }

      if ($seccion=="propiedades"){
?>      
      <script>
          $(function() {
              $("#modostats").change(function(){
                if($(this).val()=="simple"){
                  $("#modosimple").addClass('on');
                  $("#modoavanzado").removeClass('on');
                }
                if($(this).val()=="avanzado"){
                  $("#modosimple").removeClass('on');
                  $("#modoavanzado").addClass('on');
                }
              });

              $("#modostats1").change(function(){
                if($(this).val()=="simple"){
                  $("#modosimple1").addClass('on');
                  $("#modoavanzado1").removeClass('on');
                }
                if($(this).val()=="avanzado"){
                  $("#modosimple1").removeClass('on');
                  $("#modoavanzado1").addClass('on');
                }
              });

          });
      </script>
<?php
      }

      if ($seccion=="locales"){

?> 
<script src="js/drag-on.js"></script>
<script>
  function actualizapunto(){
      var mizoom=parseInt($('#zoomimg').val());
      var posenx = $("#puntoenmapa").data("lefti")/mizoom;
      var poseny = $("#puntoenmapa").data("topi")/mizoom;
      $("#puntoenmapa").css({"top":poseny,"left":posenx});
      $("#puntoenmapa").show();
      var porcentajex=posenx/(($('#imglocal').data("width")/mizoom));
      var porcentajey=poseny/(($('#imglocal').data("height")/mizoom));
      $("#xposition").val(porcentajex.toFixed(4));
      $("#yposition").val(porcentajey.toFixed(4));
  }

  $(document).ready(function() {
    $("#btnplano").click(function(event){
      event.preventDefault();
      if (parseInt($("#id_property").val())>0){
        document.location.href=$(this).attr("href")+$("#id_property").val()+"#nestable_propiedades";
      } else {
        alert("Elige primero una propiedad");
      }

    });
    $('#imglocal').dblclick(function(e) {
      var mizoom=parseInt($('#zoomimg').val());
      var offset = $(this).offset();
      var posenx=e.pageX - offset.left;
      var poseny=e.pageY - offset.top;
      $("#puntoenmapa").data("topi",(poseny*mizoom ));
      $("#puntoenmapa").data("lefti",(posenx*mizoom ));
      actualizapunto();

    });
    $('#zoomimg').change(function(){
      var mizoom=parseInt($(this).val());
      $('#imglocal').width($('#imglocal').data("width")/mizoom);
      $('#imglocal').height($('#imglocal').data("height")/mizoom); 
      actualizapunto();
    });

    actualizapunto();
    $("#dragon").dragOn();



  });
</script>
<?php
      }
      if ($seccion=="ubicar"){?>
<script>
var notificaciones=[];
function cerrarnot(puntoselec){
    var encontrado=-1; 
    notificaciones.forEach(function(item,index){ 
      if (item.punto==puntoselec) encontrado=index;
    }); 
    if(encontrado>-1){
      $.gritter.remove(notificaciones[encontrado].unico, { 
        fade: true,
        speed: 'fast'
      });
      notificaciones.splice(encontrado, 1);
    }

}
function guardarnot(puntoselec){
    cerrarnot(puntoselec);
    $.ajax({
      type: "POST",
      url: "scripts/changepoint.php",
      data: { "item": $('#'+puntoselec).data("idi"),"valor":$('#'+puntoselec).val(),"Token_CSRF":$("#tokenCSRF").val(),"nivel":$("#nivelactual").val() }
      })
      .done(function( msg ) {
        if(msg!="0"){
          $("#tokenCSRF").val(msg);
          $.gritter.add({
              title: 'POSICIÓN GUARDADA',
              text: 'El cambio se realizó exitosamente',
              sticky: false,
              time: 2000
          });
        }    
              
    });
}

$(document).ready(function() {
  
  function actualizapunto(puntoselec){
      var mizoom=parseInt($('#zoomimg').val());
      var posenx = $("#puntoenmapa").data("lefti")/mizoom;
      var poseny = $("#puntoenmapa").data("topi")/mizoom;
      $("#puntoenmapa").css({"top":poseny,"left":posenx});
      $("#puntoenmapa").show();
      var porcentajex=posenx/(($('#imglocal').data("width")/mizoom));
      var porcentajey=poseny/(($('#imglocal').data("height")/mizoom));
      $("#"+puntoselec).val("["+porcentajex.toFixed(4)+","+porcentajey.toFixed(4)+"]");
      var encontrado=-1;
      notificaciones.forEach(function(item,index){
        if (item.punto==puntoselec) encontrado=index;
      }); 
      if(encontrado>-1){
        cerrarnot(puntoselec);
      }
      var unico=$.gritter.add({
            title: 'La posición ha cambiado',
            text: '<div class="col-sm-12 text-center"><h5>¿Deseas guardar la nueva posición de '+$("#"+puntoselec).data("local")+'?</h5></div><div class="col-sm-6 text-center"><button class="btn btn-danger" type="button" onClick="cerrarnot(\''+puntoselec+'\')">Cancelar</button></div><div class="col-sm-6 text-center"> <button class="btn btn-success" type="button" onClick="guardarnot(\''+puntoselec+'\')" >Guardar</button></div>',
            sticky: true
      });
      notificaciones.push({"punto":puntoselec,"unico":unico});
  }
  
  $('#imglocal').click(function(e) {
      var puntoselec=$("#fubicapuntos input[type='radio']:checked").attr("id");
      if (puntoselec!=undefined){
        var mizoom=parseInt($('#zoomimg').val());
        var offset = $(this).offset();
        var posenx=e.pageX - offset.left;
        var poseny=e.pageY - offset.top;
        $("#puntoenmapa").data("topi",(poseny*mizoom ));
        $("#puntoenmapa").data("lefti",(posenx*mizoom ));

        actualizapunto(puntoselec);
      } else {
        alerta("Primero selecciona un local de la lista");
      }

    });

  $(".radiospuntos").click(function(){
      var str = $(this).val();
      var res = str.replace("[", ""); res = res.replace("]", "");
      var res = res.split(",");
      if (parseFloat(res[0])>0 && parseFloat(res[1])>0 ){
          var mizoom=parseInt($('#zoomimg').val());
          $("#puntoenmapa").data("lefti",($("#imglocal").data("width")*parseFloat(res[0])) );
          $("#puntoenmapa").data("topi",($("#imglocal").data("height")*parseFloat(res[1])) )
          var posenx = $("#puntoenmapa").data("lefti")/mizoom;
          var poseny = $("#puntoenmapa").data("topi")/mizoom;
          $("#puntoenmapa").css({"top":poseny,"left":posenx});
          $("#puntoenmapa").show();
      } else {
          $("#puntoenmapa").hide();
      }
  });
});  
</script>      

<?php
      }
      if ($seccion=="report"){
?>

<?php } ?>