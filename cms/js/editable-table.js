
var EditableTable = function () {

    return {

        //main function to initiate the module
        init: function (sortfield) {
            

            var oTable = $('#editable-sample').dataTable({
                "aLengthMenu": [
                    [50, 100, 200, -1],
                    [50, 100, 200, "All"] // change per page values here
                ],
                "oLanguage": {
                    "sProcessing": "<img src='images/loading.gif'>"
                },
                "iDisplayLength": 100,
                
                "sPaginationType": "bootstrap",
                "bProcessing": true,
                "bServerSide": true,
                "bSort": false,
                "bLengthChange": false,
                "bFilter": false,
                "sAjaxSource": "mod/mod_"+(($("#is_stdmodule").size()>0)?"stdmodule":$("#seccion").val())+".php?seccion="+$("#seccion").val()+"&token="+$("#token").val()+"&accion=pagina&last=1"
            });

            jQuery('#editable-sample_wrapper .dataTables_filter input').addClass("form-control medium"); // modify table search input
            jQuery('#editable-sample_wrapper .dataTables_length select').addClass("form-control xsmall"); // modify table per page dropdown

            var nEditing = null;

            if (sortfield==1){ 
                oTable.rowReordering();
            }

        }

    };

}();