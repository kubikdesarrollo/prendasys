$(function(){
	$( ".fecha_input" ).each(function() {
		$(this).datepicker({
            format: 'yyyy-mm-dd'
        });
	});

	$( ".fechatiempo_input" ).each(function() {
		$(this).datetimepicker({
            format: 'yyyy-MM-dd hh:mm:ss'
        });
	});

	if($("#seccion").length ) {
		if($("#seccion").val() == "categorias" && ($("#id").val() == "1" || $("#id").val() == "2" ) ){
			$("#id_parent").hide();
		}
	}
	
	$("#form").validate({});
	jQuery(".fancybox").fancybox();

	$(".editor").jqte();

	
	CKEDITOR.config.allowedContent = true; // permite poner cualquier atributo en los tags
	CKEDITOR.config.extraPlugins = 'imageuploader';
	CKEDITOR.dtd.$removeEmpty['i'] = false; // permite tags i
});

function editthis(){
	$( ".elementstatic" ).each(function( index ) {
		$(this).hide();
	});
	$( ".elementeditable" ).each(function( index ) {
		$(this).show();
	});
}

function showadvancesearch(){
	$("#swadvancesearch").hide();
	$("#advancesearchpanel").css("display","block");
}
function eliminarmultiple(tabla,id_tabla){
	
	var cualestextos="";
	$(".itemsdel:checked").each(function(){
		cualestextos+=((cualestextos!="")?",":"")+$(this).attr("title");
	});	
	if (cualestextos!=""){
	
		$("#modalConfirmbody").html("¿Desea borrar el elemento <strong>"+cualestextos+"</strong> de "+$("#seccion").val()+"?");
		$('#modalConfirm').modal('show');
	} else {
		 $("#modalAlertbody").html("Seleccione al menos un elemento");
         $('#modalAlert').modal('show');
	}
	
}
function actiondeletemultiple(tabla,id_tabla){
	var cuales="";
	$(".itemsdel:checked").each(function(){
		cuales+=((cuales!="")?",":"")+$(this).val();	
	});	
	$.gritter.add({
            title: 'Alert',
            text: 'The item was removed',
            sticky: false,
            before_open: function(){
            }
        });
	$.ajax({
		type: "POST",
		url: "scripts/delete.php?tabla="+tabla+"&indice="+id_tabla,
		data: { items: cuales }
		})
		.done(function( msg ) {	   
            document.location.href="admin.php?seccion="+$("#seccion").val()+"&last=1";
	});
}
function autorizar(seccion,idi){
	$("#modalConfirmbodyaut").html("¿Desea modificar la autorizaci&oacute;n para este elemento?");
	$('#modalConfirmaut').modal('show');
	$("#aut_seccion").val(seccion);
	$("#aut_id").val(idi);
}
function actionautoriza(){
	$(".modal-footer").hide();
	$("#modalConfirmbodyaut").html("Espere un momento");
	$.ajax({
		type: "POST",
		url: "scripts/autorizar.php?seccion="+$("#aut_seccion").val()+"&indice="+$("#aut_id").val()
		})
		.done(function( msg ) {	   
          document.location.href="admin.php?seccion="+$("#seccion").val()+"&last=1";
	});
}

function guardarinfopic(idpic,tabla,campo,token,idtable){
	var infoloc='info='+$("#texto_"+idpic).val()
	//if ($(".slidecapas").size()>0)
		//infoloc+="&inicio="+$("#inicio_"+idpic).val()+"&duracion="+$("#duracion_"+idpic).val()+"&efecto="+$("#efecto_"+idpic).val();
	if ($(".gal_extracustfields").size()>0){
		$(".gal_extracustfields").each(function(){
			if (idpic==$(this).data("grupo"))
				infoloc+="&"+$(this).data("campo")+"="+$("#"+$(this).data("campo")+"_"+idpic).val();
		});	
	}
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'functions/agregainfo.php?indice='+idpic+'&tabla='+tabla+'&idtable='+idtable+'&campo='+campo+'&token='+token,
		data: infoloc,
		success: function(data){
			$("<div id='mensajes_error'>INFORMACION GUARDADA</div>")
				.insertBefore( $("#texto_"+idpic) )
				.fadeIn('slow')
				.animate({opacity: 1.0}, 2000)
				.fadeOut('slow');
		}
	});	
}

function eImagen(id,tabla,indice,seccion) { 

	$(".modal-title").html("¿Desea eliminar la fotograf&iacute;a no: "+id+" ?")
	$(".modal-body").html('Este elemento se eliminar&aacute; junto con su contenido asociado, desea continuar?');
	$('#Modalgallery').modal('show');
	$('#clickconfirm').unbind().click(function(){
		$('#Modalgallery').modal('hide');
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions/deleteimg.php?tabla='+tabla+'&indice='+indice+'&seccion='+seccion,
			data: 'items='+id,
			success: function(data){
				if (data == true) {
					$("#itemg_"+id).fadeOut('slow').animate({opacity: 1.0}, 2000);	
				} else {
					$(".modal-body").html("No se pudo eliminar");
				}
			}
		});	
	});
}

function releoadgallery(){
	
}

function sendpop(cualform){
	$('#'+cualform).modal('hide');

	$.ajax({
			type: 'POST',
			dataType: 'json',
			url: $("#form_"+cualform).attr("action"),
			data :   $('#form_'+cualform).serialize(),
			success: function(data){
				if (data==1){
					$("#modalAlert .modal-title").html("Registro Guardado")
					$("#modalAlert .modal-body").html("El registro fue guardado exitosamente");
					$('#modalAlert').modal('show');
					reloadtabledinamic(cualform);

				} else {
					$("#modalAlert .modal-title").html("Error")
					$("#modalAlert .modal-body").html("Ocurrio un error, no se guardo el registro, verifique la informaci&oacute;n e intente de nuevo");
					$('#modalAlert').modal('show');
				}

			}
		});	
}

function reloadtabledinamic(cual){
	$.ajax({
			type: 'POST',
			data: "pag="+$("#page_"+cual).val(),
			url: $("#page_"+cual).data("contentinfo") ,
			success: function(data){
				$("#table_"+cual).html(data);
			}
		});	
}

function loadeditinfo(urload,pop){
	$.ajax({
			type: 'POST',
			url: urload ,
			success: function(data){
				$("#"+pop+" .modal-body").html(data);
				$("#"+pop).modal('show');
			}
		});	
}

function clearandloadpop(pop){
	$("#form_"+pop+" div.form-group input").each (function(){  $(this).val("");});
	$("#form_"+pop+" div.form-group textarea").each (function(){    $(this).val(""); });
	$("#form_"+pop+" div.form-group select").each (function(){    $(this).val(""); });
	$(".id_editpop").val("0");
	$("#"+pop).modal('show');
}


function maketsortable(tabla,sorting){ 
	var EditableTable = function () {

	    return {

	        //main function to initiate the module
	        init: function (sortfield) {
	            

	            var oTable = $('#'+tabla).dataTable({
	                "aLengthMenu": [
	                    [50, 100, 200, -1],
	                    [50, 100, 200, "All"] // change per page values here
	                ],
	                "oLanguage": {
	                    "sProcessing": "<img src='images/loading.gif'>"
	                },
	                "iDisplayLength": 100,
	                
	                "sPaginationType": "bootstrap",
	                "bProcessing": true,
	                "bSort": false,
	                "bLengthChange": false,
	                "bFilter": false,
	                "bDestroy": true
	            });
	            var nEditing = null;

	            if (sortfield==1){ 
	                oTable.rowReordering();
	            }

	        }

	    };

	}();

	EditableTable.init(sorting);
}

function eliminarRow(id_del,tabla,id_tabla,texto,cualform){
		$("#modalConfirmsubtablab").html("Quieres eliminar <strong>"+texto+"</strong> de "+$("#seccion").val()+"?");
		$('#modalConfirmsubtabla').modal('show');
		$('#hiddendelete').val(id_del);
		$('#hiddentabledelete').val(tabla);
		$('#hiddenidtabledelete').val(id_tabla);
		$('#hiddencualform').val(cualform);
}
function actiondelete(){
	if ($('#hiddencualform').val()!=""){
		$.gritter.add({
	            title: 'Alerta',
	            text: 'Registro eliminado',
	            sticky: false,
	            before_open: function(){
	            }
	        });
		$.ajax({
			type: "POST",
			url: "scripts/delete.php?tabla="+$('#hiddentabledelete').val()+"&indice="+$('#hiddenidtabledelete').val(),
			data: { items: $('#hiddendelete').val() }
			})
			.done(function( msg ) {	   
	            reloadtabledinamic($('#hiddencualform').val());
	            $('#modalConfirmsubtabla').modal('toggle');
		});
	}
}

function delImagen(campo,tabla,indice,seccion,id) { 
	$('#modalConfirmaut button.btn-warning').attr('onclick','').unbind('click');
	$(".modal-title").html("¿Desea eliminar la imagen ?");
	$(".modal-body").html('Se eliminara solo la imagen <br><br><img src="'+$("#itemg_"+campo+" img").attr("src")+'" style="height:120px"><br> desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'functions/deleteimgsingle.php?tabla='+tabla+'&indice='+indice+'&seccion='+seccion+'&campo='+campo,
			data: 'items='+id,
			success: function(data){
				if (data == true) {
					$("#itemg_"+campo).fadeOut('slow').animate({opacity: 1.0}, 2000);
					$('#modalConfirmaut').modal('toggle');	
				} else {
					$(".modal-body").html("No se pudo eliminar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});
}

function alerta(msg){
    $("#modalAlertbody").html(msg);
    $('#modalAlert').modal('show');
  }

//VORAGO
function respaldaproductos(){
	$(".modal-title").html("¿Desea respaldar los productos ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backupproducts.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}
function importaproductos(){
	$(".modal-title").html("¿Desea importar productos ?");
	$(".modal-body").html('Se eliminará cualquier producto anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function restauraproductos(){
	$(".modal-title").html("¿Desea restaurar los productos ?");
	$(".modal-body").html('Se eliminará cualquier producto anterior y se importaran los productos guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restoreproducts.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}


function respaldamarketplaces(){
	$(".modal-title").html("¿Desea respaldar los URLs ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backupurlsmarket.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}

function restauramarketplaces(){
	$(".modal-title").html("¿Desea restaurar los URLs ?");
	$(".modal-body").html('Se eliminará cualquier URL anterior y se importaran los URLs guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restoreurlsmarket.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}

function importamarketplaces(){
	$(".modal-title").html("¿Desea importar las URLS ?");
	$(".modal-body").html('Se eliminará cualquier URL anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function importall(){
	$("#simulate").val("0");
	$("#importform").submit();
}

function respaldaspecs(){
	$(".modal-title").html("¿Desea respaldar los SPECS ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backupspecs.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}

function restauraspecs(){
	$(".modal-title").html("¿Desea restaurar los SPECS ?");
	$(".modal-body").html('Se eliminará cualquier SPEC anterior y se importaran los SPECS guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restorespecs.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}

function importaspecs(){
	$(".modal-title").html("¿Desea importar las SPECS ?");
	$(".modal-body").html('Se eliminará cualquier SPEC anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function respaldadistribuidores(){
	$(".modal-title").html("¿Desea respaldar las sucursales de Distribuidores ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backupdistribuidores.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}

function restauradistribuidores(){
	$(".modal-title").html("¿Desea restaurar las sucursales de Distribuidores ?");
	$(".modal-body").html('Se eliminará cualquier Distribuidor anterior y se importaran los Distribuidores guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restoredistribuidores.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}

function importadistribuidores(){
	$(".modal-title").html("¿Desea importar las sucursales de Distribuidores ?");
	$(".modal-body").html('Se eliminará cualquier Distribuidor anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function respaldamayoristas(){
	$(".modal-title").html("¿Desea respaldar los Mayoristas ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backupmayoristas.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}

function restauramayoristas(){
	$(".modal-title").html("¿Desea restaurar los Mayoristas ?");
	$(".modal-body").html('Se eliminará cualquier Mayorista anterior y se importaran los Mayoristas guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restoremayoristas.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}

function importamayoristas(){
	$(".modal-title").html("¿Desea importar las Mayoristas ?");
	$(".modal-body").html('Se eliminará cualquier Mayorista anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function respaldarelaciones(){
	$(".modal-title").html("¿Desea respaldar los Productos relacionados ?");
	$(".modal-body").html('Se eliminará cualquier respaldo anterior<br> ¿Desea continuar?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/backuprelaciones.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos respaldada</h1>"); }, 1000);
				} else {
					$(".modal-body").html("No se pudo respaldar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});	
	});		
	return false;
}

function restaurarelaciones(){
	$(".modal-title").html("¿Desea restaurar los Productos relacionados ?");
	$(".modal-body").html('Se eliminará cualquier Producto relacionado anterior y se importaran los Productos relacionados guardados en el respaldo<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$.ajax({
			type: 'POST',
			dataType: 'json',
			url: 'scripts/restorerelaciones.php?',
			success: function(data){
				if (data == true) {
					$('#modalConfirmaut').modal('toggle');	
					setTimeout(function(){ alerta("<h1>Base de datos restaurada</h1><br> recarge la página");location.reload();  }, 1000);
				} else {
					$(".modal-body").html("No se pudo restaurar");
				}
			},
			error: function (request, status, error) {
		        console.log(request.responseText);
		    }
		});
	});	
	return false;
}

function importarelaciones(){
	$(".modal-title").html("¿Desea importar los Productos relacionados ?");
	$(".modal-body").html('Se eliminará cualquier Producto relacionado anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}

function buscaubicaciones(){
	$("#searchlocdiv").show();
	$("#searchloc").hide();
	return false;
}

function importalanzamientos(){
	$(".modal-title").html("¿Desea importar los lanzamientos ?");
	$(".modal-body").html('Se eliminará cualquier relación de lanzamiento anterior<br> ¿Desea continuar la importación?');
	$('#modalConfirmaut').modal('show');
	$('#modalConfirmaut button.btn-warning').unbind().click(function(){
		$('#modalConfirmaut').modal('toggle');	
		$("#importfilediv").show();
		$("#importbtn").hide();
	});	
	return false;
}


function fnsearchloc(offset_var,total){ 
	var per_send = 10;
	var pages=(Math.ceil(total / per_send)-1); 
	if(offset_var <= pages){
		$.ajax({
				type: "GET",
				url: "scripts/procesa_geopos.php?seccion="+$("#btnsearchloc").data("seccion"),
				data: {counter: per_send, offset: offset_var}, // serializes the form's elements.
				success: function(data)
				{
					if(data != 'true'){
						$("#resultlog").append(data);
					}
					var per_cent = (offset_var*100)/pages;
					$("#progreso").val(per_cent);
					$("#progreso .progress-bar span").css({width:per_cent+"px"}).html(per_cent);                                
					
					setTimeout(fnsearchloc(offset_var+1,total), 3000);
					


				}
		});        
	}else{
		var per_cent = 100;
		$("#progreso").val(per_cent);
		$("#progreso .progress-bar span").css({width:per_cent+"px"}).html(per_cent);              
		alert("Proceso terminado");

	}
}


$(function(){
	$("#btnsearchloc").click(function(){
		fnsearchloc(0,$(this).data("registros"));
	});

});